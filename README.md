# Textfiles Mirror

This is a mirror of Jason Scott's [textfiles website](http://textfiles.com/). Like other mirrors it includes the content of his website and all of the textfiles in it. Unlike other mirrors this mirror has the ability to go into and out of dark mode site-wide. It was something Jason started working on but the technology didn't exist at the time to make it feasible so he apparently abandoned the effort before the feature was finished.

## Features

In addition to mirroring his website content I made improvements:

- Ported all of the static content over to Hugo static site generator
- Created a file viewer to preview all text files right on the site
- For the first time those weird animation files can be seen animated

## Screenshot

![textfiles](https://vhs.codeberg.page/site/textfiles-mirror/images/screen_shot_2020-10-23_at_5.33.15_PM.png)

## Construction

Created by hand with some bash-fu and [After Dark](https://codeberg.org/vhs/after-dark). If you want to pull the site down, you'll either need to be on Linux or will need to have a drive or virtual drive capable of case-sensitivity as there's some wild stuff in the character encodings. If you pull this down on a Mac and think it won't work with macOS you'd be wrong because using a virtual disk you can make the file system case-sensitive. Building the site for development requires about 15 GB of RAM last I checked because that's how [Hugo](https://gohugo.io/) (used by After Dark) works or at least used to.

## Motivation

Someone told me it would be too difficult to do it. So I did it.

## Rights

I am not your lawyer. Jason states on his website the copyright of the contents are up in the air. Most of these textfiles were pulled off BBS systems where they were being shared a long, long time ago before most lawyers even knew what the Internet was. That send most of the content should be covered under Sui Generis. As for the source code wrapping the mirror and with the exception of anything Jason-specific it's all available under 0BSD. See the file LICENSE in the source code for more information.
