#!/bin/bash

# GENERATE WEBPAGES FOR ALL TEXTFILES
# Traverse into each subdir and run the generate script
# @see https://devhints.io/bash
# @see https://guide.bash.academy/
# @see https://www.shellcheck.net/

projectpath="/home/bali/Developer/textfiles.bit/"
script="bin/generate.sh"

# Get sorted list of directories at depth
directories=( $(find -type d -print) )
IFS=$'\n' directories=( $(sort <<< "${directories[*]}") )
unset IFS

# Echo working directory, prompt and confirm
echo "Working directory is $PWD"
echo "The following directories will be processed..."
echo "${directories[@]}"

# Prompt to continue
while true; do
  read -p "Are you sure you wish to continue? " yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

for directory in ${directories[@]} ; do
  if [ ! -d $directory ] ; then
    echo "$directory is not a directory. Aborting..." >&2 && exit 1
  fi
  if [ $directory = "." ] ; then
    /bin/bash $projectpath$script --noprompt
  else
    pushd $directory > /dev/null
    /bin/bash $projectpath$script --noprompt
    popd > /dev/null
  fi
done
