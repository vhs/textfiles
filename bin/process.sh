#!/bin/bash

# PREPARES A CONTENT SUBDIR (DEPTH: 1) AND DATA FILES
# @see https://devhints.io/bash
# @see https://guide.bash.academy/
# @see https://www.shellcheck.net/

# execute from content subdirectory (e.g. magazines)
# takes care of most tedious work activities
# expects an _index.md file to be copied into subdirs

# TIP save changes before running
# NOT tested for subdirs more than one level deep

# Known issues:
# - Skips tablefooter when first missing start tag

backupfile="_index.html.bak"

# backup index.html file
if [ -f "$PWD/index.html" ] ; then
  # :
  mv "$PWD/index.html" "$backupfile"
fi

# loop over each subdir
parentdirslug="$(echo "${PWD##*/}" | tr '[:upper:]' '[:lower:]')"
for file in * ; do
  if [ -d "$file" ] ; then
    dir="$file"
    dirslug="$(echo "${dir##*/}" | tr '[:upper:]' '[:lower:]')"
    parentindex="$(dirname "$dir")/_index.md"
    newindex="$dir/_index.md"
    
    # backup existing index.html file
    if [ -f "$dir/index.html" ] ; then
      # :
      mv "$dir/index.html" "$dir/$backupfile"
    fi

    # created newindex file if needed
    if [ ! -f "$newindex" ] ; then
      if [ -f "$parentindex" ] ; then
        # :
        touch "$newindex"
      fi
    fi

    #
    # UPDATE NEWINDEX FRONTMATTER
    #

    # set tabledata
    # BUG: not suitable for recursion
    if [ -f "$newindex" ] ; then
      # :
      data="$(<"$parentindex")"
      setting="$(grep "^tabledata" <<< "$data")"
      value="${parentdirslug}_${dirslug}"
      echo "${data//$setting/tabledata = \"$value\"}" > "$newindex"
    fi

    # set title
    # REQUIRE: xidel
    if [ -f "$newindex" ] ; then
      # :
      data="$(<"$newindex")"
      setting="$(grep "^title" <<< "$data")"
      pushd "$dir" > /dev/null
      value="$(xidel -s _index.html.bak -e '//h1/node()')"
      popd > /dev/null
      echo "${data//$setting/title = \"$value\"}" > "$newindex"
    fi

    # set description
    # REQUIRE: xidel
    # BUG: Fails on nested double-quote values
    if [ -f "$newindex" ] ; then
      # :
      data="$(<"$newindex")"
      setting="$(grep "^description" <<< "$data")"
      locator="$(echo "${dir##*/}")"
      value="$(xidel -s "$(dirname "$dir")/$backupfile" -e "//a[@href=\"$locator\"]/ancestor-or-self::td/following-sibling::td/b")"
      echo "$value"
      echo "${data//$setting/description = \"$value\"}" > "$newindex"
    fi

    # set tablefooter
    # REQUIRE: xidel
    if [ -f "$newindex" ] ; then
      # :
      data="$(<"$newindex")"
      setting="$(grep "^tablefooter" <<< "$data")"
      pushd "$dir" > /dev/null
      value="$(xidel -s _index.html.bak -e //table[2])"
      popd > /dev/null
      echo "${data//$setting/tablefooter = \"$value\"}" > "$newindex"
    fi

    # write content
    # REQUIRE: xidel
    if [ -f "$newindex" ] ; then
      # :
      pushd "$dir" > /dev/null
      content="$(
        xidel -s _index.html.bak -e '//table[1]/preceding-sibling::*[preceding-sibling::h1]' --printed-node-format=html
      )"
      # XXX: some cleaning that didn't work out (kept for reference)
      # content="$(
      #   xidel -s "$content" -e '/'
      # )"
      # content="$(echo $(paste -sd "" <<< $content))"
      content="$(echo -e "\n${content}")"
      popd > /dev/null
      echo "$content" >> "$newindex"
    fi

    #
    # CREATE DATA FILE STUBS
    #

    # create tabledata JSON file
    # BUG: hard-coded tabledatafile location
    tabledatafile="../../data/${parentdirslug}_${dirslug}.json"
    if [ ! -f "$tabledatafile" ] ; then
      # :
      echo "[{}]" | dd of="$tabledatafile" status=none
    fi
  fi
done
