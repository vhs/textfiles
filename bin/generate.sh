#!/bin/bash

# GENERATE WEBPAGES FOR TEXTFILES
# @see https://devhints.io/bash
# @see https://guide.bash.academy/
# @see https://www.shellcheck.net/

# execute from content subdir (e.g. magazines)
# expects hugo binary and a "textfile" archetype
# assumes no spaces in filenames though untested
# do not process dotfiles for images
# ignores _index.html, _index.md, index.html and index.md
# also ignores *.html.bak and other .md files
# ignore .zip files too
# exclude files larger than 1000000 bytes (hugo readFile limit)

# KNOWN ISSUES
# - [ ] Attempts to process images with space in filename

# IMPROVEMENTS
# - [ ] Traverse into subdirectories
# - [ ] ...excluding "history/EVILEXIDY"

#
# OPTIONAL ARGS
#

NOPROMPT=false
test "$1" = "--noprompt" && NOPROMPT=true

#
# VARIABLES
#

projectpath="/home/bali/Developer/textfiles.bit/"

#
# HELPER FUNCTIONS
#

elementIn () {
  local element match="$1"
  shift
  for element; do [[ "$element" == "$match" ]] && return 0; done
  return 1
} # @see https://stackoverflow.com/a/8574392/712334

#
# PRINT CHANGES
#

# define ignored files
markdownfiles=(
  $(find -maxdepth 1 -name '*.md' -exec basename '{}' \;)
)
dotfiles=(
  $(find -maxdepth 1 -type f \( -iname '.*' \) -exec basename '{}' \;)
)
htmlbackups=(
   $(find -maxdepth 1 -name '*.html.bak' -exec basename '{}' \;)
)
htmlfiles=(
  $(find -maxdepth 1 -name '*.html' -exec basename '{}' \;)
)
htmlfiles+=(
  $(find -maxdepth 1 -name '*.htm' -exec basename '{}' \;)
)
imagefiles=(
  $(find -maxdepth 1 -name '*.gif' -exec basename '{}' \;)
)
imagefiles+=(
  $(find -maxdepth 1 -name '*.jpg' -exec basename '{}' \;)
)
imagefiles+=(
  $(find -maxdepth 1 -name '*.jpeg' -exec basename '{}' \;)
)
imagefiles+=(
  $(find -maxdepth 1 -name '*.png' -exec basename '{}' \;)
)
archivefiles=(
  $(find -maxdepth 1 -name '*.zip' -exec basename '{}' \;)
)
archivefiles+=(
  $(find -maxdepth 1 -name '*.lzh' -exec basename '{}' \;)
)
largefiles=(
  $(find -maxdepth 1 -name "*" -size +1000000c -exec basename '{}' \;)
)
ignored=(
  "${htmlfiles[@]}"
  "${markdownfiles[@]}"
  "${htmlbackups[@]}"
  "${dotfiles[@]}"
  "${imagefiles[@]}"
  "${archivefiles[@]}"
  "${largefiles[@]}"
)

echo "Working directory is $PWD"
echo "${#ignored[@]} files found to ignore..."
echo "${ignored[@]}"

# report ignored directories, if any, and
# abort if running from root directory
declare -a dirlist
isroot=false
for file in * ; do
  if [ -d "$file" ] ; then
    test "$file" = "content" && isroot=true
    dirlist+=("$file")
  fi
done
test "$isroot" = true && echo "Root directory. Aborting..." >&2 && exit 1
if [ "${#dirlist[@]}" = 0 ] ; then
  echo "Not ignoring any directories..."
else
  echo "${#dirlist[@]} directories ignored..."
  echo "${dirlist[@]}"
fi

# report ignored files, if any, and
# build target list of files to generate
declare -a ignoredfiles
declare -a targetfiles
for file in * .[^.]* ; do
  if [ -f "$file" ] ; then
    if elementIn "$file" "${ignored[@]}" ; then
      ignoredfiles+=("$file")
    else
      targetfiles+=("$file")
    fi
  fi
done
numfiles="$(
  find -maxdepth 1 -type f -printf "\n" | wc -l
)" # include hidden, omit directories

# print list of ignored files
if [ "${#ignoredfiles[@]}" = 0 ] ; then
  echo 'Not ignoring any files...'
else
  echo "${#ignoredfiles[@]} files ignored..."
  echo "${ignoredfiles[@]}"
fi

# print list of target files
if [ "${#targetfiles[@]}" = 0 ] ; then
  echo "No files found to process..."
else
  echo "${#targetfiles[@]} files to process of ${numfiles} total..."
  echo "${targetfiles[@]}"
fi

#
# PROMPT TO CONTINUE
#

if [ NOPROMPT = false ] ; then
  while true; do
    read -p "Do you wish to continue? " yn
    case $yn in
      [Yy]* ) break;;
      [Nn]* ) exit;;
      * ) echo "Please answer yes or no.";;
    esac
  done
fi

#
# GENERATE FILES
#

workpath="$PWD"
workpath="${workpath#"$projectpath"}"
workpath="${workpath#content/}/"

pushd "$projectpath" > /dev/null

# process target files
for file in "${targetfiles[@]}" ; do
  hugo new --kind textfile $workpath$file.md
done

popd > /dev/null
