+++
linktitle = "calcium_fluoride.txt"
title = "calcium_fluoride.txt"
url = "science/CHEMICALS/calcium_fluoride.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CALCIUM_FLUORIDE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
