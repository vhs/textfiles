+++
linktitle = "lycopodium.txt"
title = "lycopodium.txt"
url = "science/CHEMICALS/lycopodium.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LYCOPODIUM.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
