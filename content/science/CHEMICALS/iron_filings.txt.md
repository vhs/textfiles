+++
linktitle = "iron_filings.txt"
title = "iron_filings.txt"
url = "science/CHEMICALS/iron_filings.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRON_FILINGS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
