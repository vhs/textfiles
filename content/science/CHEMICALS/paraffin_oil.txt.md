+++
linktitle = "paraffin_oil.txt"
title = "paraffin_oil.txt"
url = "science/CHEMICALS/paraffin_oil.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PARAFFIN_OIL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
