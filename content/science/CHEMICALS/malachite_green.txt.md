+++
linktitle = "malachite_green.txt"
title = "malachite_green.txt"
url = "science/CHEMICALS/malachite_green.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MALACHITE_GREEN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
