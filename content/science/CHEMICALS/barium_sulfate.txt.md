+++
linktitle = "barium_sulfate.txt"
title = "barium_sulfate.txt"
url = "science/CHEMICALS/barium_sulfate.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BARIUM_SULFATE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
