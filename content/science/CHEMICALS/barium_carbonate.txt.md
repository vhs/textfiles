+++
linktitle = "barium_carbonate.txt"
title = "barium_carbonate.txt"
url = "science/CHEMICALS/barium_carbonate.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BARIUM_CARBONATE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
