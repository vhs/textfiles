+++
linktitle = "methylene_chloride.txt"
title = "methylene_chloride.txt"
url = "science/CHEMICALS/methylene_chloride.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download METHYLENE_CHLORIDE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
