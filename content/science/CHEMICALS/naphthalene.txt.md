+++
linktitle = "naphthalene.txt"
title = "naphthalene.txt"
url = "science/CHEMICALS/naphthalene.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NAPHTHALENE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
