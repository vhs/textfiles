+++
linktitle = "phthalocyanine_blue.txt"
title = "phthalocyanine_blue.txt"
url = "science/CHEMICALS/phthalocyanine_blue.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PHTHALOCYANINE_BLUE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
