+++
linktitle = "copper_carbonate.txt"
title = "copper_carbonate.txt"
url = "science/CHEMICALS/copper_carbonate.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COPPER_CARBONATE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
