+++
linktitle = "mercury_oxide_yel.txt"
title = "mercury_oxide_yel.txt"
url = "science/CHEMICALS/mercury_oxide_yel.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MERCURY_OXIDE_YEL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
