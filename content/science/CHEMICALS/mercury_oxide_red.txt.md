+++
linktitle = "mercury_oxide_red.txt"
title = "mercury_oxide_red.txt"
url = "science/CHEMICALS/mercury_oxide_red.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MERCURY_OXIDE_RED.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
