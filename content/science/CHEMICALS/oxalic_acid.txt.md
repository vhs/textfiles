+++
linktitle = "oxalic_acid.txt"
title = "oxalic_acid.txt"
url = "science/CHEMICALS/oxalic_acid.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OXALIC_ACID.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
