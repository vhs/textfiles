+++
linktitle = "gallic_acid.txt"
title = "gallic_acid.txt"
url = "science/CHEMICALS/gallic_acid.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GALLIC_ACID.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
