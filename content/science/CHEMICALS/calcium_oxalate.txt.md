+++
linktitle = "calcium_oxalate.txt"
title = "calcium_oxalate.txt"
url = "science/CHEMICALS/calcium_oxalate.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CALCIUM_OXALATE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
