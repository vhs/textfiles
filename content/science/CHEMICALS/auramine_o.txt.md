+++
linktitle = "auramine_o.txt"
title = "auramine_o.txt"
url = "science/CHEMICALS/auramine_o.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AURAMINE_O.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
