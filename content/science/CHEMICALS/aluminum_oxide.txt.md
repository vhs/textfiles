+++
linktitle = "aluminum_oxide.txt"
title = "aluminum_oxide.txt"
url = "science/CHEMICALS/aluminum_oxide.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ALUMINUM_OXIDE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
