+++
linktitle = "ammonium_picrate.txt"
title = "ammonium_picrate.txt"
url = "science/CHEMICALS/ammonium_picrate.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AMMONIUM_PICRATE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
