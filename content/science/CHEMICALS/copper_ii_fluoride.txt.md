+++
linktitle = "copper_ii_fluoride.txt"
title = "copper_ii_fluoride.txt"
url = "science/CHEMICALS/copper_ii_fluoride.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COPPER_II_FLUORIDE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
