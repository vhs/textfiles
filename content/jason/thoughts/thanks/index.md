+++
title = "Thanks for the Kind Words"
layout = "thoughts-single"
url = "/thoughts/thanks.html"
+++

![textfiles](/images/tfile.gif)

Many people wrote in after hearing about my hard drive crash. They offered some cash, some advice, and a lot of support. I really appreciate that; I hear from folks telling me about how much they like the site, but not often do I hear from people saying how much they'd miss it if it were gone or missing things.

After careful thought, I'm not going to recover the disk anytime soon. It's a lot of money, and even though I could run a fund drive and make a lot of it, based on the letters, it would be silly money to spend. Maybe I would have a fund drive to add a new server to textfiles.com (so I could add searches, among other things) but making people pay for the same procedure with the same place that firms use to recover accounting data is over the top.

With VERY little exception, everything in that inbox was stored somewhere else. I even have a copy of the inbox, one year previous to when I had it. I'll use that, and I might miss a few hundred files, but I'm not that concerned; everything will show up again, I promise. It took me a while to determine that, but I do believe it's the case.

New sections and files are still showing up on textfiles.com, and I've been making some major inroads on the [documentary](http://www.bbsdocumentary.com) as well. Life is great. Thanks for making it so.

Jason Scott<br>
**TEXTFILES.COM**
