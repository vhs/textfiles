+++
title = "Jason's Thoughts on Textfiles"
description = "" # TODO
layout = "about-thoughts"
url = "/thoughts/"
tabledata = "jason_thoughts"
tablefooter = "There are 21 files for a total of 356,630 bytes."
[mediaobject]
  src = "/images/j1987.gif"
  alt = "Jason Scott (1987)"
+++

Every time I feel like writing about my experiences with textfiles or the process of running this site, I put them here. I was a part of this whole thing too, and I want to share my stories with you. Also, running TEXTFILES.COM has brought out a lot of interesting events in itself, and you're going to hear about those as well.

**Currently, I write a lot of my essays on my weblog, [ASCII.TEXTFILES.COM](http://ascii.textfiles.com").**
