+++
title = "AN APPRECIATION OF COUNT LAZLO HOLLIFELD NIBBLE /\\/oo\\/\\"
layout = "thoughts-single"
url = "/thoughts/nibble.html"
+++

It's too easy in life to forget the people who helped make you what you are, who inspired you and gave you the early lessons you took to heart. Sometimes, what they have to teach you is so basic to your personality that you don't even recognize that they come from somewhere else besides you.

I've got a number of people like that in my past, and every once in a while, if I'm lucky, a memory comes back to the surface about that person that takes me back to those formative years.  Today, that memory is of Count Nibble.

By the early 1980's, I'd already found my calling: collecting textfiles on BBSes. I was throwing them to floppies as fast as I could, logging onto a board and not stopping until I had entirely leeched their G-File or Text sections clean of everything I'd not seen before. If the site required validation, that was fine; I marked them down, entered what I needed to, and got back on the next day, validated, and sucked them clean. I was a kid with a mission, barely into my teens, thinking he could make a name for himself with a collection.

But after a while, you can get discouraged by [file](http://www.textfiles.com/apple/zorroplaying.txt) after [bland file](http://www.textfiles.com/apple/aecomman.app), all written like they were notes to themselves and left on the BBS by mistake. I grabbed all these files out of a sense of duty to my collecting, but I would barely read them as they went by. My own writing style wasn't so hot either, and I was producing the typical kind of [sub-par writing](http://www.textfiles.com/messages/letter.txt) one might expect from a clueless suburban kid. But I knew quality when it went by: those 80-column, upper/lowercase writings, long, well-thought out, almost as if they were "professional" in a pile of amateurish jotted notes.

I forget where it first happened, probably on an Ascii Express (AE) Line that had a bunch of files on it, but I came into contact with the work of Count Nibble, and everything changed.

He was certainly memorable with his unique signature, an ASCII bat that stared at you after his name:

**/\\/oo\\/\\**

But beyond this piece of cleverness stood a white-hot light of talent that blinded me to the other works appearing on the boards.

Here was a guy who could not only write, but who organized his text and thoughts with a practiced, steady hand. While in the adult world one might have access to high-quality text all the time, this was a huge rarity in the teenage-weighted places I found myself online.  It was like a breath of fresh air when you didn't even realize how choking the room had become. The 80-column screen wasn't empty to Nibble; it was a [canvas he painted his words onto](http://www.textfiles.com/anarchy/MISCHIEF/mrshim.txt), taking advantage of the unique properties of ASCII and ensuring that all things were in their rightful place. It wasn't just about throwing in lines and ASCII artwork to jazz up the piece; he [used space and breaks](http://www.textfiles.com/food/newcoke.txt) to organize his thoughts so that it all seemed that much more important. Capitals became headers, dashes became breaks, and when you got to the bottom of the text you felt you'd learned something, not just subjected to someone's lame opinion for 3 paragraphs.

He produced the Countlegger Series, a collection of textfiles written by others, meaning he was also a collector. But where I was happy enough to just have all these files, he would create these [compilations](http://www.textfiles.com/phreak/countleg.phk) that he would distribute on BBSes and AEs for others to download. I might have had eighty or ninety percent of these files in my collection, but the way he listed them in the contents made me excited anew for them; he showed me the treasure I didn't know I had.

He even took [files](http://www.textfiles.com/anarchy/SCAMS/counterfeiting) by others that perhaps lacked spelling or clarity and fixed them up, not wiping out their names but simply adding his own, showing his own mark of quality. This was a moral lesson, of sorts, that he passed on to me; I never took someone else's work and threw my name on it just because I thought it would bring me glory. I might fix it up and credit my editing of the file, but this was to put my own name behind my work to make someone else's work clearer. Plagarism and theft seemed unfathomable after seeing Nibble's efforts.

Lazlo had his own BBS, the Terrapin Station in 505, which was itself mysterious and interesting to me; I had no knowledge of New Mexico whatsoever, and it was a rare BBS indeed coming out of the area code. This only added to his mystery. I only got on there a few times, but I made it a point of reading what I could. His Grateful Dead reference didn't rub off on me (and in fact went over my head, setting me up for a shock of recognition years later) but his attitude and approach to running his BBS sure did, and I know I ran my own BBS, The Works, in a similar fashion when I started it a year or two later.

If we ever talked at that time, it was probably furtive, a few minutes in a chat session on his board. But what would I have to say or offer? I was 15, just happy to be there in his work, enjoying the talent and care he put into his online persona. There were lots of people I hung out with and transferred files and went to parties with, but Nibble was there, in the background, in another distant place, a goal to live up to.

Ultimately, Nibble drifted away into the world of the Internet and Usenet long before I had a chance to, where he gained a name for himself making absolutely
fantastic [Discographies and mailing lists](http://www.swcp.com/lazlo-bin/discogs) for bands that I, independently, had come to like very much. Gone from BBSes and moved on, he found a new home and expressed himself just as wonderfully there.

Like most stories, this could have ended with these memories, but a number of years ago I was searching for his name and happily stumbled upon [Count Nibble's Homepage](http://www.studio-nibble.com/). With all the same talent he'd shown a decade and a half earlier, Lazlo Nibble now provided [the history about himself I had never known](http://www.studio-nibble.com/lazlo/ego.html), a [retrospective of his Apple II days](http://www.studio-nibble.com/lazlo/swpg/index.html)
and best of all, [the files he'd written back then](http://www.studio-nibble.com/lazlo/writing/index.html#swpg). Some he'd even gotten from textfiles.com! As an added bonus, he even explained
[the story of his name and why it kept changing](http://www.studio-nibble.com/lazlo/images/lazname.gif).

Time had not dulled his talent, the world wide web had not diminished his efforts, and my own growing up had not fogged the perspective he'd bestowed upon me. Rare do mentors live up to such standards.

So thank you, Count Nibble. You helped make me what I am.

\- Jason Scott<br>
December 9, 2002
