+++
title = "GOODBYE, JOHN (AND GOOD RIDDANCE)"
layout = "thoughts-single"
url = "/thoughts/johnrichard.html"
+++

While it would be great if it happened a lot, in fact it is very rarely the case that you find out whatever happened to the subjects of these textfiles. They're spectacular nuggets of the past to me, of course. But it isn't often that the past comes out of these files and bursting into your face. When it happens, it always has the capability to delight or shock.

This very thing happened recently when a user contacted me and mogel about a couple of my files in the BBS section:

[A Warning to Sysops Regarding John Aleshe/Richard](http://www.textfiles.com/bbs/net282.txt)<br>
[Police Report regarding John Aleshe/Richard](http://www.textfiles.com/bbs/pcinfo.txt)

These files date back to 1988 and discuss the activities of a gentleman who called himself John Richard, John Aleshi, and a bunch of other names. He ingratiated himself in the FIDONET community in Minneapolis, MN within a few months, talked about the interesting things he'd done, arranged for some consulting jobs, and then spontaneously disappeared one day, taking many thousands of dollars of equipment and leaving behind many other thousands of unpaid bills. The uproar and the use of the FIDONET network revealed that he had performed this scam around the country for years before that.

The [first file](http://www.textfiles.com/bbs/net282.txt) is a well-written explanation of how the events came down in Minnesota. It is left open, like I said, because they never found him again.

Until now.

On May 23rd of this year, the recently-ex-owner of IQUEST communications in Indianapolis, Robert Hoquim, died of an heart attack in his home. When the police tried to track his next of kin, what they instead found was a house of cards built on lies, and his name, his history, his story all collapsed and revealed that he was the John Richard that these FIDONET users in Minnesota were looking for.

Here's the story in the Indianapolis Star:

[Local Net Firm Founder had lived a Double Life (June 8, 2000)](http://www.starnews.com/news/articles/IQUEST08.ART.html)

This story has intrigued me enough that I've tracked down and spoken to the writer of the original file, who had not heard of the news coming from the Star. He was delighted that this fellow who had caused him and his friends such pain and sadness was not only dead, but perhaps even holding an estate that could see reparation for the money they were defrauded.

The historical aspect fascinates me: a con-artist using his easily-gained and grown social power on FIDONET to scam fellow SysOps and users out of many thousands; how he had spent the 80's pulling this scam in town after town, leaving an ever-widening wake of broken promises and lies behind him. In decades past, he would be selling fake insurance, cooking the books in small boilerrooms, or just doing phone call after phone call to gullibles. All he had to do was open his BBS and wait for the openness of the community to swallow him in.

I find the fact that the writer of the textfile about John found himself called up 12 years later with the information he was seeking to be an interesting story in itself, even ignoring my part in it.

And I'm still amazed it was a simple ASCII document from the past that reached into the future and will bring closure to a long-forgotten injustice.

Goodbye, John Paul Aleshe. We all know your secret now.

*Jason Scott<br>June 9, 2000*

Some related links

[A spooked Iquest begs "It's not our fault"](http://www.iquest.net/060800-rel.html)<br>
[Indianapolis Channel WTHR Story](http://www.msnbc.com/local/WTHR/75363.asp)<br>
[WISH-8 TV In Indiana just slaps some info together.](http://www.wishtv.com/Global/story.asp?s=84881)
[Someone at Network World Fusion is amused at this story.](http://www.nwfusion.com/columnists/2000/0605compendium.html)
