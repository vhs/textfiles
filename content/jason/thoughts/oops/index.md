+++
title = "A MINOR (OR MAJOR) CATASTOPHE"
layout = "thoughts-single"
url = "/thoughts/oops.html"
+++

![textfiles](/images/tfile.gif)

Happens to the best of us; one of my hard drives died and I lost the  textfiles.com inbox.

Huge thing, it was; over 65,000 files of all sorts, many of them binaries wrapped next to other file sets. I'd been going through about a thousand every week or so, and was progressing. Probably a third of the sorted files were making it onto the final site.

I'm pretty good at my backups, and this hard drive crash killed little of my various projects; for example, textfiles.com is still fine, and the BBS Documentary Site is fine, and a host of other files were protected.

But I did lose a lot of stuff that I would like to see again. In the case of the inbox, I thought it was being backed up nightly, and it wasn't.

I'm thinking of sending the drive to a place called [drivesavers](http://www.drivesavers.com), who basically rip apart the drives and suck the data out. They quoted me prices of $1300-$3900 to do this. This is a lot of money, money I don't currently have.

So, I don't know. I could try a fund-raising drive for this site, like I've seen others, to do the data restoration (or to try one). Or I could give it up and just assume it's lost.

I'm looking for your opinion. [Let
me know.](mailto:jason@textfiles.com)

Jason Scott<br>
**TEXTFILES.COM**
