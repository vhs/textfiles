+++
title = "Textfile Sources"
layout = "about-sources"
url = "/jason/sources.html"
introtext = "<p>A natural question that occurs to people who browse a site like this for the first time is <i><b>\"Where have you gotten all these files!?\"</b></i> It seems beyond the ability of a single person to gather this much information alone.<p>This is certainly the truth in my case; while a lot of files have been personally gathered, many more have been donated or mailed to me by others who want to see this site grow, and I have purchased a number of CD-ROM Shovelware Discs that contained files I wanted."
+++

## Personal Collection

> During 1982-1988, I spent a lot of time collecting textfiles from all sorts of BBSs. Some of the more prominent places at the time were Sherwood Forest II and III, OSUNY, Dark Side of the Moon, Ripco, The South Pole, Broadway Show, and hundreds of others whose names I've forgotten.
>
> Like a lot of kids, I started my own BBS (The Works) so that people would come to ME and send me a lot of files, and I could just sit back and take them in. Of course, this didn't translate to an insane number of contributions, but I did get dozens of files as a result of my BBS, which touted itself as the "Largest Text-file BBS in the World!" There's actually no way to verify if this ever was the case. It probably wasn't. But I was plucky, and a lot of people thought that The Works was pretty darned huge for its time.
>
> All told, probably 3,000-4,000 of the textfiles on this site are from that initial collection. This is already less than half that are up on the site, and will constitute even less as time goes on.

## User Contributions

> Since TEXTFILES.COM has recieved media attention, more and more people are discovering the site and offering me files. I have asked them to mail whatever they have around, and many have obliged. In some cases, I get duplicates, but in many others, I get entirely new message bases or files I'd have never noticed. For that, I would like to thank the following users who have all gone above and beyond and sent me something back:
>
> **Erik Pedersen** sent me roughly 2 gigabytes of text (!!!) from all sorts of places. Our collections are the same in many places, but it feels good to know that a lot of the stuff I have would have been covered in this donation as well. Erik's BBS homepage is at [http://powerlink.servebbs.com](http://powerlink.servebbs.com).
>
> **Scott Brawner** was kind enough to send me the ISO image for *Forbidden Secrets*, a CD-ROM I'd heard about but couldn't find, and which had a couple hundred good files on it for me to use. 
>
> **Stein Gjoen** gave me a tip on where to find the magazine "Cheap Truth".
>
> **[Chris Orcutt](http://www.orcutt.net)** and I used to trade textfiles when we were both 14.  As it turns out, Chris kept a lot of files I lost, so he scrounged through his old 5 1/4" disks and sent me 600 files, of which 400 were entirely unique.
>
> **[Paul Schneider](http://www.outreach.uiuc.edu/~p-schne/)** donated a collection of about 200 textfiles, of which 113 harkened back to the 1984-1985 years and I didn't have.
>
> **Slade Winstone** was "El Presidente'" of the Banana Republic BBS in New Zealand, and sent me 300-400 textfiles that I've scattered throughout the site, including a set of files his friends came up with in the [GROUPS](/groups/bananarepublic) section. He's helping me become world-wide!
>
> Profetas from Brazil gave me 46mb of textfiles, some of them mine (after going through South America!) but quite a few not. Hundreds of textfiles. Thank you!
>
> **Neema Moraveji** took the time to send me 50 files, including some neat Virus disassemblies and better versions of a few anarchy files I had.
>
> **Nick Levay** sent me a CD of stuff he's gotten from all over the place, nearly 20,000 textfiles in all! At least half of these are unique. Thanks for the 600mb hit!
>
> **Paul Rubel** gave me 100 files of anarchy, mayhem, and general disarray.
>
> **Yves Barbero** let me have a copy of his novel and some clever files he had lying around.
>
> **Danny DiRocco** sent me a 10 megabyte mail attachment that yielded 300 one-of-a-kind files that peppered the magazine, phreak and hack sections.
>
> **Amit Margalit** used to run a Textfile-Only BBS in the 80's, and graciously sent me the entire archive from that BBS, giving me another couple hundred files. Many of these files were erotic stories or hysterical computer humor. Superb!
>
> The great folks at [CHAVEN](http://www.chaven.com) had squirred away 30 megabytes of Apple II Documentation and textfiles. Thanks to them, the Apple section tripled in size. Thank you!
>
> The Legendary **Dr. Who** rose from the sands of time and gave me a stack of files he'd done for his part of the LOD BBS Message project, including his transcribed/buffered files he had lying around from the early 80's. He also included an insightful message from the LOD group working on the project, which gives a really cool angle on the project.
>
> **Phox** gave me a collection of about 50 files, most of them somewhat modern in vintage, but many having that original textfiles spirit. He also included the [Diary of a Hacker](/bbs/diaryhack.txt) by The Cuisinart Blade, which was a great look back. Excellent!
>
> Douglas Cox gave me a great idea for how to handle the meta-descriptions, those little descriptions of the file you can click on. Thanks!
>
> Alfie John gave me a CD with a collection of Melbourne BBS information. Thanks, man!
>
> Finally, the following people sent me just one or two files or files I already had, and I wanted to thank them all for thinking of me: **John Brajkovic**, **Happyhammer**, **Michael Milbert**, **Tyler Bindon**, **ARA** and **Joe Pranevich**.

## FTP/WEB Sites

> It would be inevitable that as I browsed the Web myself, I would happen along some really enjoyable/helpful sites brimming with textfiles from the era I'm researching. Most of these sites also have other useful files and information that I don't have any use for at my site, but which are of use to others. For this reason, and as a thank you, I'll include a list of these sites below:

## CD-ROMS

> During the Shovelware Heyday (About 1990-1994) a lot of companies went out and threw together massive collections of files, often with only tenuous connection to each other, and then burned them on CD-ROM with some sort of cheap clip-art-esque cover and sold it as a package. These CD-ROMs usually retailed from $10 to $50 and were complete hack jobs. Usually, they had really sleazy, "underground" names, referring to some Forbidden or Dangerous subjects covered inside, and of course when you got them home and plugged them in, were YOU in for a surprise!
>
> However, I've made it a point to acquire as many of these CD-ROMs as I can and see if anything is of use from them, and then put the good stuff up on this site. I'd like to attribute those CDs here.
>
> As an unintended side-effect, you are about to get a series of CD-ROM reviews for these CDs I used. In many cases, I couldn't tell you where to purchase one of these CDs, as they've mostly been stumbled along on forgotten web sites, and purchased used from various folks.

Send mail to [jason](mailto:jason@textfiles.com) if you can suggest further sites for him to browse or have textfiles/messages you'd like to contribute.
