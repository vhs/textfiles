+++
title = "Adventure Textfiles"
description = "Walkthroughs and Hints for Text Adventures"
tabledata = "adventure"
tablefooter = "There are 491 files for a total of 5,968,568 bytes.<br>There is 1 directory."
+++

Among the more popular games of the 80's were text adventures, which described fantastic places and people in the form of paragraphs of text, and the player was asked to interact with these ASCII worlds and solve puzzles and dangers. Since these games required a lot of intelligence and perseverance to solve, it became a point of pride to release the solution or 'Solve" first. This was the set of instructions that provided a walkthrough for the game.

Other files in this directory include interviews with adventure game authors and the occasional completely inappropriate file.

Collections of walkthroughs for Apple II adventures are located in the <a href="/apple/WALKTHROUGHS">Apple</a> Directory.
