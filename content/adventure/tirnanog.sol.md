+++
linktitle = "tirnanog.sol"
title = "tirnanog.sol"
url = "adventure/tirnanog.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TIRNANOG.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
