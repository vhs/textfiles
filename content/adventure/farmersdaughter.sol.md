+++
linktitle = "farmersdaughter.sol"
title = "farmersdaughter.sol"
url = "adventure/farmersdaughter.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FARMERSDAUGHTER.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
