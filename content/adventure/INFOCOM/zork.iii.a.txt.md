+++
linktitle = "zork.iii.a.txt"
title = "zork.iii.a.txt"
url = "adventure/INFOCOM/zork.iii.a.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.III.A.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
