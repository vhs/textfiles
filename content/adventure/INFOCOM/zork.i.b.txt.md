+++
linktitle = "zork.i.b.txt"
title = "zork.i.b.txt"
url = "adventure/INFOCOM/zork.i.b.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.I.B.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
