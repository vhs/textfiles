+++
linktitle = "hitchgui.txt"
title = "hitchgui.txt"
url = "adventure/INFOCOM/hitchgui.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HITCHGUI.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
