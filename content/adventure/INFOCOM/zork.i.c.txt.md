+++
linktitle = "zork.i.c.txt"
title = "zork.i.c.txt"
url = "adventure/INFOCOM/zork.i.c.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.I.C.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
