+++
linktitle = "zork.ii.a.txt"
title = "zork.ii.a.txt"
url = "adventure/INFOCOM/zork.ii.a.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.II.A.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
