+++
linktitle = "bey_zlor.txt"
title = "bey_zlor.txt"
url = "adventure/INFOCOM/bey_zlor.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BEY_ZLOR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
