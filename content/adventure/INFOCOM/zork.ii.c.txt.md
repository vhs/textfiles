+++
linktitle = "zork.ii.c.txt"
title = "zork.ii.c.txt"
url = "adventure/INFOCOM/zork.ii.c.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.II.C.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
