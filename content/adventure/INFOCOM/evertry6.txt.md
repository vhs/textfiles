+++
linktitle = "evertry6.txt"
title = "evertry6.txt"
url = "adventure/INFOCOM/evertry6.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EVERTRY6.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
