+++
linktitle = "zork.iii.b.txt"
title = "zork.iii.b.txt"
url = "adventure/INFOCOM/zork.iii.b.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.III.B.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
