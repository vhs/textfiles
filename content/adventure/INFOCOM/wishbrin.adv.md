+++
linktitle = "wishbrin.adv"
title = "wishbrin.adv"
url = "adventure/INFOCOM/wishbrin.adv.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WISHBRIN.ADV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
