+++
title = "Adventure Textfiles: INFOCOM"
description = "Files About Infocom, the Greatest Adventure Makers Ever"
tabledata = "adventure_infocom"
tablefooter = "There are 55 files for a total of 680,963 bytes."
+++

Every once in a while, a company or group of people enter an industry (or create it) who are so far ahead of everyone else, so absolutely the best at their craft, that others stop even referring to them and just concentrate on doing the best they can compared the rest of the competitors. In this case, the industry was Text Adventures, and the company was Infocom.

Based in Cambridge, MA, and surviving for nearly a decade, Infocom was started by a group of MIT students who had worked on parsing out full sentences and having machines react properly. Whereas most games before infocom had simple commands like TAKE ROCK or LOOK, Infocom games let you work in full sentences, enabling you to type things like ASK THE TROLL ABOUT THE COINS and CLIMB ON TOP OF THE HOUSE. This great interface, combined with the year (or years) of work put into writing the descriptions and creating the puzzles, meant that you were riding on the best text adventure vehicle out there.

Infocom ultimately died as many companies do, with a few fatal business decisions and with a quiet disappearance, only to have its corpse propped up for a series of graphic adventures made by Activision. Regardless of its fate, this company was unique, and in the world of the 80x24 screen, they ruled supreme. 
