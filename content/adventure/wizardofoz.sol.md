+++
linktitle = "wizardofoz.sol"
title = "wizardofoz.sol"
url = "adventure/wizardofoz.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WIZARDOFOZ.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
