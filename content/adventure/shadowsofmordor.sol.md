+++
linktitle = "shadowsofmordor.sol"
title = "shadowsofmordor.sol"
url = "adventure/shadowsofmordor.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHADOWSOFMORDOR.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
