+++
linktitle = "mindfighter.sol"
title = "mindfighter.sol"
url = "adventure/mindfighter.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MINDFIGHTER.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
