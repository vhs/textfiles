+++
linktitle = "sherlockholmes.sol"
title = "sherlockholmes.sol"
url = "adventure/sherlockholmes.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHERLOCKHOLMES.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
