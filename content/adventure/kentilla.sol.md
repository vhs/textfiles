+++
linktitle = "kentilla.sol"
title = "kentilla.sol"
url = "adventure/kentilla.sol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KENTILLA.SOL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
