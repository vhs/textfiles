+++
title = "Survivalism"
description = "Be Suspicious, Be Worried, Be Prepared"
tabledata = "survival"
tablefooter = "There are 105 files for a total of 1,737,898 bytes."
+++

Survivalists tend to be the strongest mix of Politics, Self-Reliance, and Radicalism. As a result, the textfiles they write have one of the more unique blends of points of view of any other part of textfiles.com. While it's unlikely you'll agree with a lot of what's being said, you have to give them the fact that they say it loud and clear.

A lot of survivalist files are dedicated to preparing for a coming collapse of society, assuming the worst and preparing for it. They're not waiting for the calvary; they're looking to eat the horses if they come this way.
