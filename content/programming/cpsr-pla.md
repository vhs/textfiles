+++
linktitle = "cpsr-pla"
title = "cpsr-pla"
url = "programming/cpsr-pla.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CPSR-PLA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
