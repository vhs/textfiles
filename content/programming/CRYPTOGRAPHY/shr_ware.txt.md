+++
linktitle = "shr_ware.txt"
title = "shr_ware.txt"
url = "programming/CRYPTOGRAPHY/shr_ware.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHR_WARE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
