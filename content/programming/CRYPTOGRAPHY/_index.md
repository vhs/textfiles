+++
title = "Programming Textfiles: Cryptography"
description = "Information on Cryptography or Politics Surrounding Cryptography"
tabledata = "programming_cryptography"
tablefooter = "There are 71 files for a total of 3,194,574 bytes."
+++

A goal of all communicators, as far back as time records, is to ensure that those communications reach the right people and don't fall into the hands of others. To this end, codes and passwords and ciphers have been created to make it so anyone intercepting a communication is unable to know what the message is.

This goal has come into the computer era in the form of encrypted e-mail and secure networking, and the politics that have arisen as a result are what have brought a strong awareness of government policy and social change to computing. This directory contains both technical and political discussions.
