+++
linktitle = "suff_txt.dis"
title = "suff_txt.dis"
url = "programming/AMIGA/suff_txt.dis.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUFF_TXT.DIS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
