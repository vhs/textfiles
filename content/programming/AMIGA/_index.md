+++
title = "Programming Textfiles: The Amiga"
description = "Programming and Software information on the Amiga"
tabledata = "programming_amiga"
tablefooter = "There are 45 files for a total of 937,307 bytes."
+++

It was hard for me not to go completely over the top for the Commodore Amiga when I became aware of it in 1985. Here was a machine incredible graphics, stunning sound, amazing games, and a general sense that the thing could do most anything. I wasn't the only one; Amigas are still in use to this day for professional graphics work and video editing. But at the time, it just changed everything. Here's some files discussing the minutae of being a programmer on the Amiga.
