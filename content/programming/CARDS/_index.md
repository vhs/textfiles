+++
title = "Programming Textfiles: Bowen's Instruction Summary Cards"
description = "Bowen's Incredible Instruction Set Summary Cards"
tabledata = "programming_cards"
tablefooter = "There are 56 files for a total of 818,099 bytes."
+++

Sometimes, you'll stumble upon the work of someone that takes your breath away, for many different reasons. Such is the work of Jonathan Bowen of the Oxford University Computing Laboratory, who has maintained a truly <i>excellent</i> set of instruction summary cards for many major chips in use by computers from the 60's to the present day. While the first of these files is dated from 1982, Mr. Bowen has continued to maintain them for accuracy and new information for all the years hence; decades of vigilance to keep his work relevant and useful.

The intention of these summaries is to print them out and have at the ready as you work with these different processors, and they've been designed for easy printing and clear reference. 

Besides instruction sets, there are also command summaries, and, in an interesting tangent, the solution to several puzzles, including the original Rubik's Cube and its variants. This is truly a treasure.
