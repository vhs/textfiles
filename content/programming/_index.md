+++
title = "Programming Textfiles"
description = "All of the Deep Geek Stuff"
tabledata = "programming"
tablefooter = "There are 265 files for a total of 14,078,127 bytes.<br>There are 6 directories."
+++

Anything having to do with the programming of computers, or how to approach programming computers, and not covered by other sections, is stuck here. That means you might find a lot of similar information in the [Apple](apple) section, or even in the [computer](computer) section. A good portion of these files might seen out of date or irrelevant, but in fact show the initial approaches to programming concepts (like cryptography, artifical intelligence, and security) that apply very strongly to this day.
