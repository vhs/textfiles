+++
linktitle = "auto_trn.txt"
title = "auto_trn.txt"
url = "programming/FUZZYLOGIC/auto_trn.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AUTO_TRN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
