+++
linktitle = "fuzzy3.txt"
title = "fuzzy3.txt"
url = "programming/FUZZYLOGIC/fuzzy3.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUZZY3.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
