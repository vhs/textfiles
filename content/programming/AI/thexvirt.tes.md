+++
linktitle = "thexvirt.tes"
title = "thexvirt.tes"
url = "programming/AI/thexvirt.tes.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THEXVIRT.TES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
