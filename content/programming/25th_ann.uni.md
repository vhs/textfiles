+++
linktitle = "25th_ann.uni"
title = "25th_ann.uni"
url = "programming/25th_ann.uni.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 25TH_ANN.UNI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
