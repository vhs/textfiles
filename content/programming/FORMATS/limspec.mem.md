+++
linktitle = "limspec.mem"
title = "limspec.mem"
url = "programming/FORMATS/limspec.mem.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LIMSPEC.MEM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
