+++
linktitle = "rtfspec.txt"
title = "rtfspec.txt"
url = "programming/FORMATS/rtfspec.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RTFSPEC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
