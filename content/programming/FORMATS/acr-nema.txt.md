+++
linktitle = "acr-nema.txt"
title = "acr-nema.txt"
url = "programming/FORMATS/acr-nema.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ACR-NEMA.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
