+++
linktitle = "admusfmt.pro"
title = "admusfmt.pro"
url = "programming/FORMATS/admusfmt.pro.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ADMUSFMT.PRO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
