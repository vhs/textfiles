+++
linktitle = "xmformat.mus"
title = "xmformat.mus"
url = "programming/FORMATS/xmformat.mus.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download XMFORMAT.MUS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
