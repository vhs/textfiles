+++
linktitle = "8svx.pro"
title = "8svx.pro"
url = "programming/FORMATS/8svx.pro.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 8SVX.PRO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
