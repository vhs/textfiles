+++
linktitle = "dbasefil.inf"
title = "dbasefil.inf"
url = "programming/FORMATS/dbasefil.inf.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DBASEFIL.INF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
