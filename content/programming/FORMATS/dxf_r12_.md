+++
linktitle = "dxf_r12_"
title = "dxf_r12_"
url = "programming/FORMATS/dxf_r12_.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DXF_R12_ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
