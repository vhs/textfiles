+++
linktitle = "t3d_doc2.txt"
title = "t3d_doc2.txt"
url = "programming/FORMATS/t3d_doc2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download T3D_DOC2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
