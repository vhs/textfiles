+++
linktitle = "arc_fmts.txt"
title = "arc_fmts.txt"
url = "programming/FORMATS/arc_fmts.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARC_FMTS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
