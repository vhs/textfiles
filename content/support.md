+++
title = "How can I help?"
layout = "thoughts-single" # TODO generalize layout?
# description = "This text is displayed in search result listings." TODO
+++

<center>
<img src="../images/howc.gif" alt="HOW YOU CAN HELP">
</center>

Thanks for asking about how to help.

TEXTFILES.COM represents a multi-hundred-dollar cost to me each month, taking into consideration hosting and other concerns. To be honest, I can take it. I'm at an excellent job at a wonderful company and they pay me well enough to allow me this and other nice hobbies. This isn't to say I'll turn your money away, of course, but it shouldn't be your primary concern.

What *is* of primary concern to me is saving the files and text from the 1980's, and that text is located on now-aging disks, tape, and even, I dare say, 10 and 20 meg hard drives. When those media go, that's the end of that data, and I *know* I haven't gotten every important file ever created. If you've got some old disks that have files you never did anything with, please consider sending them or a copy of them to me. That stuff is precious; and it is rare; and it is finite. That's what matters.

If you're really a glutton for punishment, consider becoming a textfiles.com mirror. I could use more in other countries, than the US, which is pretty well sewed up. I can give details of the size and other matters.

Finally, if you're from that time, please consider mailing me about your experiences and what you learned from your time on the BBSes. Consider writing essays; consider sharing what you went through with the world. I'll happily provide a forum.

Thanks for caring.

<span style="text-align:right">

Jason Scott<br>
*Proprietor, TEXTFILES.COM*
  
</span>

<center>
<img src="/images/asciifor.gif"><br>
<img src="/images/gfiles.gif">
</center>
