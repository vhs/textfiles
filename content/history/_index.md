+++
title = "History"
description = "" # TODO
tabledata = "history"
tablefooter = "There are 67 files for a total of 1,055,415 bytes.<br>There is 1 directory."
pagefooter = "<p>I hope to have this section grow to be one of the most complete and popular parts of this site. If you want to contribute, please don't hesitate to <a href=\"mailto:jason@textfiles.com\">write to me</a>"
+++

While the rest of TEXTFILES.COM is about archiving and saving the textfiles of the 1980's, this section is about up-to-date essays and commentary about that time. We're looking for perspective here; you were young when these files made their debut, and now that the digital age is here, what do you look back and feel?<img src="/images/history.gif" style="float:left">

**WHAT THIS PAGE IS ABOUT**

The purpose of the history section is for people who lived through the
"Golden Age" of BBSes to write about their experience. They can write
about it from the point of view of someone looking back, or they can
try to recreate the feelings they had when they went through it in
childhood (or early adulthood).

A real nice bonus would be if people who wrote some of the textfiles
on this site could cross-reference them and discuss what they were
trying to achieve with the file, or give some context and perspective
to the time the file was written.
