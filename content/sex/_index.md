+++
title = "Sex, Sex Humor and Sexuality"
description = "Files about trying to Make More of You"
tabledata = "sex"
tablefooter = "There are 278 files for a total of 3,688,238 bytes.<br>There is 1 directory."
+++

Here are all files dealing with that most universal of concepts: Sex. This was obviously at the foremost of many a young mind using BBSes. Perhaps surprising (or not at all), sexual activity was kind of ancilliary to the whole online experience, since what impressed people was what you could accomplish online, not in bed. But the longings of adolescents everywhere is evident in all sorts of files written during this period.

Probably the most humorous files were the "Sex Guides", purporting to be tracts that would guarantee sexual activity for all those who read them, and obviously written by people who had never or seldom experienced any activity themselves.

Besides the erotic stories, dirty jokes, and the Purity Tests (which are an event in themselves), you can find articles and information about sexually transmitted diseases, and other health issues, which are informative about living a good life after you get over this "have sex with anything that says yes" phase.

A small site note: Obviously, with any web site mentioning sex and sexual activites, you're guaranteed hit upon hit as people looking for some sort of "dirty pictures" or erotic enlightment stumble upon your poor text-only page. To those people, I take their time to say ["Buy a T-shirt"](http://www.textfiles.com/support). Thank you.
