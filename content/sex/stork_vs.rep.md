+++
linktitle = "stork_vs.rep"
title = "stork_vs.rep"
url = "sex/stork_vs.rep.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download STORK_VS.REP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
