+++
title = "Sex, Sex Humor and Sexuality: Erotica"
description = "Erotic Writings, Stories, Narratives, and Just Plain Smut"
tabledata = "sex_erotica"
tablefooter = "There are 27 directories."
+++

A logical flow from the encouragement of writing on BBSes, people have been writing some form of erotica or sexual narrative for others for quite some time. With the advent of Fidonet and later Usenet, these stories achieved wider and wider distribution. Unfortunately, the nature of erotica is that it is often uncredited, undated, and hard to fix in time. As a result, you might be looking at stories much older or much newer than you might think.

A VERY Large amount of these stories came to textfiles.com in the memory of Universal Joint BBS, which had collected many thousands of them before it went down.

With literately thousands of files coming into this section, I have been forced to separate the section into sub-directories, classified by letter. I apologize for this, but there's no way I'm going to read thousands of stories to "classify" them according to subject matter.
