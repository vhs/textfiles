The Phone Number of the Beast
_____________________________

	The pale, white-skinned girl sat down on a chair on the far side
of the large wooden table.  She was in her early twenties, wearing a tank
top and apair of shorts with a wide leather belt, and big black boots.
Across from her was Chris, who was determined to get this one to break, as
soon as possible.  The girl in the chair was sweating bullets, she was
scared shitless.  But Chris knew which questions to ask, which buttons to
press, to get this one.  Piece o' cake.

	"So where did you meet her?" Chris asked, blowing cigarette smoke
overtly in the girl's face.

	"At a club where we all hang out, right near the campus," the man
stammered, "It's called Chez Mu."

	"I know it," Chris chimed in, "a lot of you kinky little pervs
like to hang out there, listen to your oddball music, and toot up in the
back, right?"

	"I don't do that kind of stuff," said the girl, arrogantly.

	Chris laughed.  "So tell me what happened?  I'm dying to find out
more about you and this 'chick' you met."

	The girl reached for a glass of water in front of her on the
table.  She managed to get one sip out of it before Chris snatched it out
of her hands, spilling it all over her little tube top.  Chris was quick
to notice how the soaked shirt accentuated her breasts.  Not only were her
nipples pert and perky, it was obvious, thanks to the water, that she had
rings through both of them.  How typical, thought Chris.  This week's
badge of honor among sweet little tough punk girls, getting their nipples
pierced.

	The girl recomposed herself and glared angrily at Chris for a
second, then eased back in her chair and started to tell her story.

	"Well, I thought she was really really cute from the moment I met
her.  She had this young untouched look about her that I liked.  But she
was obviously real curious, and eager.  She approached _me_, not the other
way around, you know."

	Chris nodded, mockingly, blowing more smoke in the girl's face.
"Sure, go on."

	"So we each had a coupla beers, and went out back... to fuck
around, y'know?"  There was a pause as the girl looked at Chris, making
sure it was clear that she wasn't doing something else out in the back.
"She said that she'd never been with a girl before, and she'd always
dreamed about a girl like me, y'know, pretty but tough looking, not femmy
but not dykey, someone right in the middle - that's how SHE put it, OK?"

	"And this was you to a tee, yes?"

	"Hey, I was dressed in my usuals, the chaps, the jacket, the
boots...  _I_ don't consider that especially ambiguous, do you?"

	"But obviously it was just right from this chick's perspective."

	"Obviously."  The girl mopped her brow with her arm.  "So, it was
no secret what I was after, what I was into.  I had just helped a friend
of mine, this gay guy I know, hold his lover down while he got whacked on
the butt.  That's how I play there, openly."

	"And this is... standard operating procedure at this club, I take it?"

	The girl sighed.  "I think you already know that it is."  She
paused for a second and resumed.  "So when she approached me it was pretty
obvious what she wanted."

	"And that was...?" Chris lingered over the last word in that
	question.

	"She wanted to get _taken_, goddammit!"

	"Oh, I see, and you 'knew' this from... from what?"

	"From the fact that she said 'Take me, goddammit' in the bathroom,
you stupid fuck."

	Chris slapped the girl across the face, and she was just on the
verge of leaping out of her chair to strike back, but she restrained
herself when she saw the 'Joy Stick' in Chris's other hand, poised at the
ready.  This wasn't the time for it.  Wouldn't do it for her, right now.
She sat back down.

	"Save your filthy tongue for the toilet bowl, young lady," barked
Chris, "you'll be making use of it there if you keep this up."

	The girl continued.  "She wanted to get done, OK?  She said, quite
explicitly, that what she wanted was for girls to take her, stuff things
up her pussy, make fun of her, call her a slut, bring a few straight guys
in to dump loads in her as we watched and laughed, make her fucking bleed
and cry till she came.  And she explicitly said, she made it _real_ clear,
that we should NOT listen to her if she tells us to stop doing any of
this."

	"And did you ask why she wanted this treatment?"

	"Fuck no," the girl blurted out.  "I mean, no, we didn't ask.  We
just did it."

	"Did what?"

	"Everything she asked us to do.  We got six of my girlfriends and
tied her over the back end of somebody's pickup truck, ripped off her
little skirt, started fiddling with her parts, got her screaming, and then
three of us fisted her."

	"Was that it?"

	"No, that wasn't it!  We threw her in the back of the truck,
practically naked by this point, and drove over to this redneck bar on the
other side of town.  It was me and two other girls in the truck, plus one
in the back to hold her down and keep her from trying to get out.  My
friend put a collar around her neck, locked it, and tied the lead to a
tree in the parking lot of the bar.  Then we just made a lot of noise so
that the guys in the bar would hear us and ran to hide in the bushes.  Two
guys came out first and reported what they saw to their friends inside.
Within twenty-five seconds - we counted - over fifteen guys were out in
the parking lot, hootin' and hollerin' like assholes.  Finally, one of
them got up the guts to actually do something, he grabbed her as she kept
running around trying to get loose.  Another guy came along and pretty
soon the two of them had her on the ground.  One of them unzipped his
pants and stuck his dick in her face, and the other guy pushed her head
real hard so that the dick went deep into her throat.  She called out my
name, I swear I heard it over her gurgling and choking, but she had said
not to interfere, and I was enjoying it far too much, so I kept watching."

	"And you... _like_ doing this sort of thing?"

	"Yes," she said indignantly, "I _like_ doing this sort of thing.
I _love_ doing this sort of thing.  I _enjoy_ doing this sort of thing."

	"Hmmm," said Chris, taking in a long drag and puffing the smoke
back in the girl's face.  "You don't see anything wrong in it?"

	"No, I don't."

	"OK," said Chris.  "Then what?"

	"Then fifteen guys gave her the gang bang of her fucking life."

	"And what was her reaction when she was done with these guys?"

	"She didn't have much 'reaction,' she was out cold.  I took out
her wallet and found out where she lived, so we drove her home and left
her there."

	"Left her where?"

	"Outside her house.  Nice house, burbs - where else?" the girl smirked.

	Chris nodded.  "I see.  And that was the end of it?"

	"No, not really," said the girl.  "I don't know how, but she
must've gotten a hold of my number, 'cuz when I got home that night, about
4:30 in the morning, my phone was ringing, and it was her."

	"Was she upset?  Mad?"

	"Fu..."  The girl caught herself.  "No, not at all.  She was
thanking me profusely."

	"What condition did she say she was in?"

	"Oh, pretty bad, her parts were bleeding and she was bruised a
bit, but no damage."

	'No damage,' Chris noted on his pad of paper.  "But she didn't
mind this?"

	"She _wanted_ to be in that condition.  Get it?  She LIKED it
rough, she was LOOKING for rough stuff, and she got it, and she was happy
for it."

	"So what did you two talk about?"

	"Well, I found out what inspired her to come out and meet me in
the first place.  She had heard about me.  Apparently a lot of the girls
at the high school have heard my name, I'm sort of a legend, I guess, but
most of them don't believe I really exist."

	Chris chortled in amusement.  "Um, what are you best 'known' for?"

	"For being a scary dangerous downright evil witch who'll tear you
up and eat you for breakfast!"

	Now Chris was on the verge of hysterical laughter.  "OK, OK, you
don't have to impress _me_ with your 'reputation', you know."  Chris
calmed down, but continued to snicker inaudibly.

	"You don't believe me, do you?  That's what I am.  I'm a sadistic
bitch who likes this sort of thing.  You get it?"

	"Believe me, I 'got it' some time ago, thank you."  There were a
few seconds of silence.  "Go on.  What else did you talk about?"

	"Well, she said she was getting over this guy, she'd been seeing
for a while.  I'd kinda guessed she was a straight chick.  So she told me
what a great wuss he was, how she wanted to do stuff with him and _he_
would pussy out - that's her words, OK?  He was like Mr. Clean, Mr.
Whitebread.  She hated him.  She was an honor student at school, but she
was sick of her wimpy friends, she wanted something exciting in her life."

	"And 'Mr. Whitebread' wouldn't provide this for her?"

	"I guess not."  The girl took a second to gulp for air, then
continued.  "So we made fun of this guy for about an hour.  Seems he
really was a wuss.  He wouldn't even touch her without asking her for
explicit permission.  So we played this little mocking game over the
phone, where I would ask her whether or not she _really_ wanted to do
something, and she'd say 'yes, yes, I want to.'"

	"What things did you ask her whether or not she wanted to do?"

	"It just kept building and building.  I asked her if she 'really'
wanted to touch herself between her legs for me, and she said 'yes, yes, I
do,' and I kept asking 'are you sure?' and she kept insisting 'yes, yes!'
I asked if she 'really' wanted to shove some object - I don't remember
what - in her ass for me, and she said 'yes, yes!'  I asked if she
'really' wanted to stick a knife into her pussy for me, and she said 'yes,
yes!'"  The girl stopped talking.

	"So?  Then what?"

	"I didn't KNOW she was really doing any of this, we were just
talking on the phone, it was just PRETEND, for Christ's sake!"

	Chris glared at the girl, staring right into her eyes.  "THEN what???"

	The girl began sobbing.  "I asked if she really wanted to stick
her finger across the exposed..."

	"And you said?"

	"I said, 'YES, YES!'"

	"And then?"

	"And then I heard the short and the scream.  Honestly, honestly, I
didn't know, I didn't know, I didn't know..."

	"What didn't you know?" asked Chris.

	"I didn't know she was really going to do any of these things!
This is all play, pretend, fantasy, you have some fun and you go back to
your life!  It's not supposed to be... like this."

	Chris blew some more smoke into the girl's face.  "Why not?"

	The girl just continued to sob uncontrollably.

	"Of _course_ this is the way it's supposed to be," Chris replied,
reaching out both arms and placing them on the girl's throat.

	The girl's face flushed with fear.  She turned pale white.  This
state of affairs was confusing to her.  Under any other circumstances, she
would have hauled off and fought back, but this time she didn't.  Why?  It
wasn't out of a sense of futility, she could have taken Chris if she'd
wanted to.  Did she think that... no, not that... that she DESERVED this?
That she had done something wrong and was going to be justly punished for
it?  No, it couldn't be; that would make her like THEM, the ones who felt
guilt and remorse for their actions like pathetic sheep.  She didn't do
guilt.  It wasn't her thing.  Then WHY?

	"Of _course_ this is the way it's supposed to be," repeated Chris.
The arms on the girl's throat reached around behind her neck and then
across her back to embrace her.

	"Angela is fine, Jill.  She's lost some motor coordination on the
right side of her body, but she'll get that back eventually.  There was no
damage done."

	No damage done.

	"I've been hoping that Angela would hook up with you for a long
time now.  I'd been hoping she could learn from you.  She's had these
desires for a long time.  I've watched her, I've tried to help her live
them out, I've done my bit to help her, but I could only do so much."

	"She's your daughter, isn't she?"

	"Yes, she is."

	"You're... her mother."

	"Yes, I am."  Chris hugged Jill even more closely.

	"You did the right thing.  My daughter is on the right track now.
I see her coming along just fine.  She's going to be happy, thanks to you."

	Jill smiled.  Arm in arm, the two women began to exit the room.

	"Just one thing," asked Jill.  "If she had these desires for so
long, how come she was involved with that wussy guy?"

	"Oh, you mean Steven?" Chris asked in response.  "Angela's been
'involved' with him ever since she was little.  He just never could get
the hang of getting into the kinds of things she really wanted.  And
there's only so much a mother can do... when a girl's father is so much of
a pussy that he can't even take care of the proper sexual education of his
own daughter, as much as she's tried to encourage him to.  God knows, he's
never been able to satisfy _me_!  Why do you think I have to get off
vicariously by thinking about my own daughter's lascivious sex acts?
Still, a girl's heart belongs to daddy... but even daddy's little girl can
only take so much inattentiveness to her needs."

	Jill's smile broadened.  "I think you and I are gonna get along
fine, Mrs. L!  What about your husband?"

	"What about him?  You're welcome to abuse him if you like.  He
doesn't mind.  It's probably the only way _he_ can get off.  Now HIM I
don't mind at ALL if you electrocute once and for all!"  They both
laughed.  "Come, let's go check up on Angela."  She winked at Jill as they
walked out the door.

	Jill thought a lot about what had just happened.  Most
significantly, about how for a brief moment, she doubted her desires, she
doubted that she was doing the right thing.  About how the thought entered
her head that what she wanted was wrong, that there were consequences to
her actions that she needed to consider, and that perhaps she ought to
reevaluate what her desires, and her lifestyle, were all about.

	She hoped Angela's mother didn't really notice.  And she vowed NEVER
to let _that_ happen again...
