+++
title = "Anarchy: Freeloading"
description = "Getting Something for Nothing"
tabledata = "anarchy_freeloading"
tablefooter = "There are 46 files for a total of 456,099 bytes."
+++

What interests a lot of young readers is finding out that something can be gotten for free; that if you put in a 5 instead of a 1, or clip the corner of a card, that you will personally mess up a system, and will personally gain. You can't fault them for being curious.
