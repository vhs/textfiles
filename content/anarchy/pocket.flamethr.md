+++
linktitle = "pocket.flamethr"
title = "pocket.flamethr"
url = "anarchy/pocket.flamethr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POCKET.FLAMETHR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
