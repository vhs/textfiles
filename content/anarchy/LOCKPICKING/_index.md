+++
title = "Anarchy: Lockpicking"
description = "Files About Getting Through Locks"
tabledata = "anarchy_lockpicking"
tablefooter = "There are 79 files for a total of 1,090,833 bytes."
pagefooter = "A lot of these files are doubled; I found it very hard, looking at two similar but differently mangled files, which one should be the \"canonical\" one. So, I often include both."
+++

One of the more interesting examples presented about the "Hacker Ethic" is that of not allowing locked doors to get in the way of using new technology or discovering new things.  The legend holds that at MIT, if a professor added a lock to his office to stop hackers from using a computer, they would crawl into the ceiling tiles and leave a note on the chair saying "Please don't lock your door." This eventually progressed to the general tunnel exploring that MIT provided, and how generations of students would pass along the knowledge they had recieved, to those who, in their view, were ready to recieve it. Of course, this whole idea got blown out when the MIT Guide to Lockpicking became generally available.

So Lockpicking has this rather odd Symbiosis with computers in some circles, and you can find examples of that below.
