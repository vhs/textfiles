+++
title = "Anarchy and Explosives: Pain and Hurt"
description = "Files about Hurting and Being Hurt"
tabledata = "anarchy_pain"
tablefooter = "There are 31 files for a total of 485,834 bytes."
+++

> Before you waste <u>one entire second</u> even <u>considering</u> mixing, creating, or otherwise implementing the instructions in these files, be aware that people have died from some of these very texts, attempting wholescale explosives and dangerous chemical reactions based on some flimsy text file written by god knows who for god knows what. It's just <u>way too easy</u> for someone to have typed a 5 instead of a 4 years and years ago when they were copying out of a badly photocopied pamphlet written by someone entirely different. Your life is too precious to take such an idiotic risk. If you're going to blow things (and yourself) up, at least go the route of learning with experts, where you'll become aware of risks and fun far beyond soaking tennis balls in gasoline and lighting them. OK?!
