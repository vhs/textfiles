+++
linktitle = "mcdonld.ana"
title = "mcdonld.ana"
url = "anarchy/MISCHIEF/mcdonld.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MCDONLD.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
