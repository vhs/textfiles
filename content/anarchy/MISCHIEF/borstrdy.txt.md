+++
linktitle = "borstrdy.txt"
title = "borstrdy.txt"
url = "anarchy/MISCHIEF/borstrdy.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BORSTRDY.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
