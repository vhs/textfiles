+++
linktitle = "hr-anarc.txt"
title = "hr-anarc.txt"
url = "anarchy/MISCHIEF/hr-anarc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HR-ANARC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
