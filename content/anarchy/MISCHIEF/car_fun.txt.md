+++
linktitle = "car_fun.txt"
title = "car_fun.txt"
url = "anarchy/MISCHIEF/car_fun.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CAR_FUN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
