+++
linktitle = "smth_cr2.txt"
title = "smth_cr2.txt"
url = "anarchy/MISCHIEF/smth_cr2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SMTH_CR2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
