+++
linktitle = "hydroxy1.txt"
title = "hydroxy1.txt"
url = "anarchy/MISCHIEF/hydroxy1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HYDROXY1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
