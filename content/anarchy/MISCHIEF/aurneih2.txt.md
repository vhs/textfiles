+++
linktitle = "aurneih2.txt"
title = "aurneih2.txt"
url = "anarchy/MISCHIEF/aurneih2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AURNEIH2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
