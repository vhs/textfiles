+++
linktitle = "urbter03.txt"
title = "urbter03.txt"
url = "anarchy/MISCHIEF/urbter03.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download URBTER03.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
