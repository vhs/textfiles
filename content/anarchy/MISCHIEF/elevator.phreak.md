+++
linktitle = "elevator.phreak"
title = "elevator.phreak"
url = "anarchy/MISCHIEF/elevator.phreak.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ELEVATOR.PHREAK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
