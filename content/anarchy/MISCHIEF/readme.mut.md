+++
linktitle = "readme.mut"
title = "readme.mut"
url = "anarchy/MISCHIEF/readme.mut.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download README.MUT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
