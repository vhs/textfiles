+++
linktitle = "prank_rev_16.txt"
title = "prank_rev_16.txt"
url = "anarchy/MISCHIEF/prank_rev_16.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRANK_REV_16.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
