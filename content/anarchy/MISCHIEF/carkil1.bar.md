+++
linktitle = "carkil1.bar"
title = "carkil1.bar"
url = "anarchy/MISCHIEF/carkil1.bar.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CARKIL1.BAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
