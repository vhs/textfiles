+++
linktitle = "geteven.phk"
title = "geteven.phk"
url = "anarchy/MISCHIEF/geteven.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GETEVEN.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
