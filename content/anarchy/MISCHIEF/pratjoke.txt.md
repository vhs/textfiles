+++
linktitle = "pratjoke.txt"
title = "pratjoke.txt"
url = "anarchy/MISCHIEF/pratjoke.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRATJOKE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
