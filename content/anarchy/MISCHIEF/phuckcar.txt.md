+++
linktitle = "phuckcar.txt"
title = "phuckcar.txt"
url = "anarchy/MISCHIEF/phuckcar.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PHUCKCAR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
