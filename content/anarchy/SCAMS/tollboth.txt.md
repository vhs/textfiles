+++
linktitle = "tollboth.txt"
title = "tollboth.txt"
url = "anarchy/SCAMS/tollboth.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TOLLBOTH.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
