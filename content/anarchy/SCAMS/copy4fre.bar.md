+++
linktitle = "copy4fre.bar"
title = "copy4fre.bar"
url = "anarchy/SCAMS/copy4fre.bar.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COPY4FRE.BAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
