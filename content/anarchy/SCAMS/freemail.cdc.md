+++
linktitle = "freemail.cdc"
title = "freemail.cdc"
url = "anarchy/SCAMS/freemail.cdc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FREEMAIL.CDC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
