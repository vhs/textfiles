+++
linktitle = "shoplift.ana"
title = "shoplift.ana"
url = "anarchy/SCAMS/shoplift.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHOPLIFT.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
