+++
linktitle = "getacar.bv5"
title = "getacar.bv5"
url = "anarchy/SCAMS/getacar.bv5.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GETACAR.BV5 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
