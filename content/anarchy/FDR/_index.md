+++
title = "Anarchy and Explosives and General Mayhem: The FDR Collection"
description = "The FDR Anarchy File Collection"
tabledata = "anarchy_fdr"
tablefooter = "There are 258 files for a total of 4,012,509 bytes."
+++

This collection was sent to me as one large archive. While many of the files in here are duplicates or very similar to other files in other parts of the site, it seemed appropriate to leave them together, since they do show one person's effort to put together a collective group. The vintage of the files range from the end of the 1990's to the earlier days of the BBS.

> Before you waste <u>one entire second</u> even <u>considering</u> mixing, creating, or otherwise implementing the instructions in these files, be aware that people have died from some of these very texts, attempting wholescale explosives and dangerous chemical reactions based on some flimsy text file written by god knows who for god knows what. It's just <u>way too easy</u> for someone to have typed a 5 instead of a 4 years and years ago when they were copying out of a badly photocopied pamphlet written by someone entirely different. Your life is too precious to take such an idiotic risk.
