+++
linktitle = "fdr-0034.txt"
title = "fdr-0034.txt"
url = "anarchy/FDR/fdr-0034.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FDR-0034.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
