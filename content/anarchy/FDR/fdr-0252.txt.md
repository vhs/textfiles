+++
linktitle = "fdr-0252.txt"
title = "fdr-0252.txt"
url = "anarchy/FDR/fdr-0252.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FDR-0252.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
