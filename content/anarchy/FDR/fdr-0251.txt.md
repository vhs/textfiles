+++
linktitle = "fdr-0251.txt"
title = "fdr-0251.txt"
url = "anarchy/FDR/fdr-0251.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FDR-0251.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
