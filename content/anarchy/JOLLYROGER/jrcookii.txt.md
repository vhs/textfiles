+++
linktitle = "jrcookii.txt"
title = "jrcookii.txt"
url = "anarchy/JOLLYROGER/jrcookii.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JRCOOKII.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
