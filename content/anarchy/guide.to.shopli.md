+++
linktitle = "guide.to.shopli"
title = "guide.to.shopli"
url = "anarchy/guide.to.shopli.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUIDE.TO.SHOPLI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
