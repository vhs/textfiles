+++
linktitle = "des1doc.ana"
title = "des1doc.ana"
url = "anarchy/INCENDIARIES/des1doc.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DES1DOC.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
