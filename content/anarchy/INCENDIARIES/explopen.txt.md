+++
linktitle = "explopen.txt"
title = "explopen.txt"
url = "anarchy/INCENDIARIES/explopen.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EXPLOPEN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
