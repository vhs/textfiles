+++
linktitle = "selfignt.txt"
title = "selfignt.txt"
url = "anarchy/INCENDIARIES/selfignt.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SELFIGNT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
