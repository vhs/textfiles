+++
linktitle = "mrse1.txt"
title = "mrse1.txt"
url = "anarchy/INCENDIARIES/mrse1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MRSE1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
