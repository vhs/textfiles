+++
linktitle = "shavingcream.txt"
title = "shavingcream.txt"
url = "anarchy/INCENDIARIES/shavingcream.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAVINGCREAM.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
