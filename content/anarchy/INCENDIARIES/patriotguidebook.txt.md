+++
linktitle = "patriotguidebook.txt"
title = "patriotguidebook.txt"
url = "anarchy/INCENDIARIES/patriotguidebook.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PATRIOTGUIDEBOOK.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
