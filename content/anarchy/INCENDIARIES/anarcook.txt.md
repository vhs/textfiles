+++
linktitle = "anarcook.txt"
title = "anarcook.txt"
url = "anarchy/INCENDIARIES/anarcook.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANARCOOK.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
