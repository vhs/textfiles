+++
linktitle = "anrky01.txt"
title = "anrky01.txt"
url = "anarchy/INCENDIARIES/anrky01.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANRKY01.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
