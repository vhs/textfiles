+++
linktitle = "pyroshitana.ana"
title = "pyroshitana.ana"
url = "anarchy/INCENDIARIES/pyroshitana.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PYROSHITANA.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
