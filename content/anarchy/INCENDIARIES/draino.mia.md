+++
linktitle = "draino.mia"
title = "draino.mia"
url = "anarchy/INCENDIARIES/draino.mia.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DRAINO.MIA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
