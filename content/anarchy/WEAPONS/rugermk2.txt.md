+++
linktitle = "rugermk2.txt"
title = "rugermk2.txt"
url = "anarchy/WEAPONS/rugermk2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RUGERMK2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
