+++
linktitle = "22pen.ana"
title = "22pen.ana"
url = "anarchy/WEAPONS/22pen.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 22PEN.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
