+++
linktitle = "anstrapr.rul"
title = "anstrapr.rul"
url = "anarchy/WEAPONS/anstrapr.rul.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANSTRAPR.RUL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
