+++
linktitle = "uzi_trig.txt"
title = "uzi_trig.txt"
url = "anarchy/WEAPONS/uzi_trig.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UZI_TRIG.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
