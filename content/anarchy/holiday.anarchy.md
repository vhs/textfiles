+++
linktitle = "holiday.anarchy"
title = "holiday.anarchy"
url = "anarchy/holiday.anarchy.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOLIDAY.ANARCHY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
