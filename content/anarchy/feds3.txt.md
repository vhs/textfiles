+++
linktitle = "feds3.txt"
title = "feds3.txt"
url = "anarchy/feds3.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FEDS3.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
