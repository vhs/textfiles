+++
linktitle = "advcrd13.c_f"
title = "advcrd13.c_f"
url = "anarchy/CARDING/advcrd13.c_f.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ADVCRD13.C_F textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
