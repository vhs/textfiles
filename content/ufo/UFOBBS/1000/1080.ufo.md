+++
linktitle = "1080.ufo"
title = "1080.ufo"
url = "ufo/UFOBBS/1000/1080.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1080.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
