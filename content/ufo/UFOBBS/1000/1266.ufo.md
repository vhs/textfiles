+++
linktitle = "1266.ufo"
title = "1266.ufo"
url = "ufo/UFOBBS/1000/1266.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1266.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
