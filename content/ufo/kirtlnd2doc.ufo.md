+++
linktitle = "kirtlnd2doc.ufo"
title = "kirtlnd2doc.ufo"
url = "ufo/kirtlnd2doc.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KIRTLND2DOC.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
