+++
linktitle = "ufopresi.ufo"
title = "ufopresi.ufo"
url = "ufo/ufopresi.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UFOPRESI.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
