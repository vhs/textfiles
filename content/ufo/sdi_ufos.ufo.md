+++
linktitle = "sdi_ufos.ufo"
title = "sdi_ufos.ufo"
url = "ufo/sdi_ufos.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SDI_UFOS.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
