+++
linktitle = "abduct2.ufo"
title = "abduct2.ufo"
url = "ufo/SIGHTINGS/abduct2.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ABDUCT2.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
