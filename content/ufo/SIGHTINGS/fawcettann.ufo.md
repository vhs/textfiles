+++
linktitle = "fawcettann.ufo"
title = "fawcettann.ufo"
url = "ufo/SIGHTINGS/fawcettann.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FAWCETTANN.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
