+++
linktitle = "jaqvals.txt"
title = "jaqvals.txt"
url = "ufo/SIGHTINGS/jaqvals.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JAQVALS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
