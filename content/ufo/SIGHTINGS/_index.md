+++
title = "UFOs: Sightings and Abductions"
description = "Articles and Essays about UFO Sightings and Abductions"
tabledata = "ufo_sightings"
tablefooter = "There are 91 files for a total of 761,910 bytes."
+++

Here's a large collection of stories about people seeing UFOs, of coming into contact with UFOs, and being abducted by UFOs. They come from news stories, from personal written accounts, from transcriptions. They make up the meat of the whole UFO phenomenon; people talking about being involved with UFOs on a deep personal level.
