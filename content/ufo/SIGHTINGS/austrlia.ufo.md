+++
linktitle = "austrlia.ufo"
title = "austrlia.ufo"
url = "ufo/SIGHTINGS/austrlia.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AUSTRLIA.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
