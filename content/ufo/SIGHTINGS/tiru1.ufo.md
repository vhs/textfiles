+++
linktitle = "tiru1.ufo"
title = "tiru1.ufo"
url = "ufo/SIGHTINGS/tiru1.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TIRU1.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
