+++
linktitle = "gulf1.ufo"
title = "gulf1.ufo"
url = "ufo/SIGHTINGS/gulf1.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GULF1.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
