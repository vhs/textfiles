+++
linktitle = "fyffe3.ufo"
title = "fyffe3.ufo"
url = "ufo/SIGHTINGS/fyffe3.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FYFFE3.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
