+++
title = "UFOs"
description = "Files indicating We Are Not Alone, or that We Are"
tabledata = "ufo"
tablefooter = "There are 441 files for a total of 7,256,438 bytes.<br>There are 2 directories."
+++

UFOs, or "Unidentified Flying Objects", are unexplained phenomena in the skies, sighted by regular everyday people, pilots, army folk, that the government has no explanation for. Aliens? Secret Military Weapons? Weather Balloons? Who knows? Well, a lot of the folks below think they do. They chronicle, they argue, they infight, and you can't dispute one thing: It's an involved, wide-spread culture. There have been BBSes dedicated to nothing but UFOs, and there have been electronic newsletters spread among non-UFO-centric BBSes that purport to tell you the truth about what's out there.

A smaller amount of files, by no means less interesting, chronicle the idea of conspiracies around UFOs, either indicating that they were created by Man, Nazis, or victorian scientists, or discussing how the aliens landed long ago and the government has made many deals and arrangements with them. Primary among these are the "Lear Text", which is below.

Some similar files can be found in the [conspiracy](conspiracy) section, as well.
