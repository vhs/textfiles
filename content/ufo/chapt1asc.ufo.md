+++
linktitle = "chapt1asc.ufo"
title = "chapt1asc.ufo"
url = "ufo/chapt1asc.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHAPT1ASC.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
