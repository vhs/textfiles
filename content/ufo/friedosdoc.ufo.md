+++
linktitle = "friedosdoc.ufo"
title = "friedosdoc.ufo"
url = "ufo/friedosdoc.ufo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FRIEDOSDOC.UFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
