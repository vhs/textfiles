+++
linktitle = "cia_ufo_story.txt"
title = "cia_ufo_story.txt"
url = "ufo/cia_ufo_story.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIA_UFO_STORY.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
