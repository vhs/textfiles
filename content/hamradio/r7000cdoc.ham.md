+++
linktitle = "r7000cdoc.ham"
title = "r7000cdoc.ham"
url = "hamradio/r7000cdoc.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download R7000CDOC.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
