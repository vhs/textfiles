+++
linktitle = "avoiding.9x"
title = "avoiding.9x"
url = "hamradio/avoiding.9x.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AVOIDING.9X textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
