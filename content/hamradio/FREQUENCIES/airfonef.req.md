+++
linktitle = "airfonef.req"
title = "airfonef.req"
url = "hamradio/FREQUENCIES/airfonef.req.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AIRFONEF.REQ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
