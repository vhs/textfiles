+++
linktitle = "satfreqs.txt"
title = "satfreqs.txt"
url = "hamradio/FREQUENCIES/satfreqs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SATFREQS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
