+++
linktitle = "oregonsp.ham"
title = "oregonsp.ham"
url = "hamradio/FREQUENCIES/oregonsp.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OREGONSP.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
