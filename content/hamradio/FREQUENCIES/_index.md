+++
title = "Ham Radio: Frequency Lists"
description = "Files Giving Lists of Ham Radio Frequencies"
tabledata = "hamradio_frequencies"
tablefooter = "There are 293 files for a total of 4,435,424 bytes."
+++

A major part of Ham Radio is scanning different frequencies, and one of the most popular kinds of Ham Radio textfiles are lists of interesting frequencies to try. They range from the mundane (allocations of different bands) to very specific in use by police, fire departments, amusement parks, etc. If you have a ham radio, it must be very interesting indeed.
