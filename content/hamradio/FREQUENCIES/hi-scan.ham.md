+++
linktitle = "hi-scan.ham"
title = "hi-scan.ham"
url = "hamradio/FREQUENCIES/hi-scan.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HI-SCAN.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
