+++
linktitle = "customs3.txt"
title = "customs3.txt"
url = "hamradio/FREQUENCIES/customs3.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CUSTOMS3.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
