+++
linktitle = "waltdis.ham"
title = "waltdis.ham"
url = "hamradio/FREQUENCIES/waltdis.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WALTDIS.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
