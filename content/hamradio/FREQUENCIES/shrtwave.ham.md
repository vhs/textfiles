+++
linktitle = "shrtwave.ham"
title = "shrtwave.ham"
url = "hamradio/FREQUENCIES/shrtwave.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHRTWAVE.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
