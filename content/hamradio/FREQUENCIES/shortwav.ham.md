+++
linktitle = "shortwav.ham"
title = "shortwav.ham"
url = "hamradio/FREQUENCIES/shortwav.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHORTWAV.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
