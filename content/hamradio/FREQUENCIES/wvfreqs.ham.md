+++
linktitle = "wvfreqs.ham"
title = "wvfreqs.ham"
url = "hamradio/FREQUENCIES/wvfreqs.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WVFREQS.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
