+++
linktitle = "scngde.ham"
title = "scngde.ham"
url = "hamradio/FREQUENCIES/scngde.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCNGDE.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
