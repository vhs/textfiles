+++
linktitle = "mil-uhf.ham"
title = "mil-uhf.ham"
url = "hamradio/FREQUENCIES/mil-uhf.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIL-UHF.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
