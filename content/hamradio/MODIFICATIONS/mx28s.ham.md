+++
linktitle = "mx28s.ham"
title = "mx28s.ham"
url = "hamradio/MODIFICATIONS/mx28s.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MX28S.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
