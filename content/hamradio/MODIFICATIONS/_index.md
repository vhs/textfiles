+++
title = "Ham Radio Textfiles: Modification Files"
description = "Files Recounting How to Modify Scanners and Radios for Increased Reception"
tabledata = "hamradio_modifications"
tablefooter = "There are 51 files for a total of 453,386 bytes."
+++

Ham Radio Modification files make up the most common collection of files that show up in textfile collections, because they're the bits of information that sounded devious enough to sound like they belonged in the file sections of a lot of BBSes people frequented.

The files were necessary because the Government decided that there were certain ranges of frequencies that should not be listened to by anyone. These included Cell Phone frequencies and the like. Instead of asking Cell Phone manufacturers to improve the technology behind their transmissions to protect the users of Cell Phones, they too a Typical Solution. They said that no manufacturer of radio scanners could listen to those bands anymore.

Naturally, this approach stunk on many levels, but one of the results of this law was that scanner manufacturers would simply throw in an extra chip or wire that would disallow the scanner to be used in the excluded  frequencies. Suddenly, turning your scanner back into a full-featured piece of equipment merely became a matter of removing that chip or clipping that wire. So, files explaining how to do this for all the brands of scanners became legion. Here are a good amount of them.
