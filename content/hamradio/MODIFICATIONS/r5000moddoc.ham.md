+++
linktitle = "r5000moddoc.ham"
title = "r5000moddoc.ham"
url = "hamradio/MODIFICATIONS/r5000moddoc.ham.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download R5000MODDOC.HAM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
