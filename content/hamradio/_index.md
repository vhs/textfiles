+++
title = "Ham Radio Textfiles"
description = "Ham Radio Operation Information, sort of"
tabledata = "hamradio"
tablefooter = "There are 283 files for a total of 4,186,712 bytes.<br>There are 2 directories."
+++

Somehow, always found myself here and there on different Ham Radio BBSs, where interesting Ham Radio stuff abounded. The whole Ham Radio thing passed me completely, so I don't really have any insightful information on this community, other than that Ham Radio BBSes were everywhere, and that most of the files in this directory are either Frequency Lists or instructions on how to modify every radio under the sun to pick up cellular phone frequencies.

A quick note on that, which I do know. The government decided to outlaw the ability of ham radios to listen to the 800-900Mhz radio band, because this was allocated to Cell Phones. This angered the Ham Radio community so much (after all, the broadcasting was going on all over ANYWAY) that mod files, clipping this wire or taking out this or that chip to fix this "limitation" were ubiquitous. Freedom uber alles.
