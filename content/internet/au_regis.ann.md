+++
linktitle = "au_regis.ann"
title = "au_regis.ann"
url = "internet/au_regis.ann.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AU_REGIS.ANN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
