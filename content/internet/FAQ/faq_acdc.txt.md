+++
linktitle = "faq_acdc.txt"
title = "faq_acdc.txt"
url = "internet/FAQ/faq_acdc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FAQ_ACDC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
