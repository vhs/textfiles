+++
title = "The Internet: Frequently Asked Questions"
description = "The Frequently Asked Questions Collection"
tabledata = "internet_faq"
tablefooter = "There are 413 files for a total of 19,937,514 bytes."
+++

One aspect of the Internet which did make it a better place for intelligent discourse was the relative speed at which 'Ground Rules' were set for what behavior was acceptable. More specifically, some of the standard pitfalls of BBSes and other online forums that could turn a somewhat intelligent trade of ideas into a useless war of words were addressed quite well in the 1980's Internet.

Among the most fascinating of these ground rules were the "Frequently Asked Questions" lists, or FAQs, which laid down all those dumb questions that anyone coming into a subject for the first time would be inclined to ask, even though everyone already involved already knew the answers. These FAQs were occasionally made by a single person, sometimes made by a committee, but what really made them shine were how over a relatively short period of time, they were these white-hot nuggets of information on a subject. Between these FAQs and the Discographies and other lists maintained on the Internet, a stunning array of human knowledge could be passed among participants quickly and efficiently.

The FAQs stand as what the potential of the Internet truly was, in how much they streamlined and improved the trade of ideas.
