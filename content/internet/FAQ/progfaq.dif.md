+++
linktitle = "progfaq.dif"
title = "progfaq.dif"
url = "internet/FAQ/progfaq.dif.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PROGFAQ.DIF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
