+++
linktitle = "faq-pktr.txt"
title = "faq-pktr.txt"
url = "internet/FAQ/faq-pktr.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FAQ-PKTR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
