+++
linktitle = "afu_flam.for"
title = "afu_flam.for"
url = "internet/FAQ/afu_flam.for.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AFU_FLAM.FOR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
