+++
linktitle = "guns_ptr.faq"
title = "guns_ptr.faq"
url = "internet/FAQ/guns_ptr.faq.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUNS_PTR.FAQ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
