+++
linktitle = "sendmail.faq"
title = "sendmail.faq"
url = "internet/sendmail.faq.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SENDMAIL.FAQ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
