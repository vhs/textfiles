+++
linktitle = "bbsad_tx.dis"
title = "bbsad_tx.dis"
url = "bbs/ADS/bbsad_tx.dis.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BBSAD_TX.DIS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
