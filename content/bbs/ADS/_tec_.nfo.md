+++
linktitle = "_tec_.nfo"
title = "_tec_.nfo"
url = "bbs/ADS/_tec_.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download _TEC_.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
