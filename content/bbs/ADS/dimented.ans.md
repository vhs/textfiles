+++
linktitle = "dimented.ans"
title = "dimented.ans"
url = "bbs/ADS/dimented.ans.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIMENTED.ANS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
