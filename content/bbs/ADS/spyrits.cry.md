+++
linktitle = "spyrits.cry"
title = "spyrits.cry"
url = "bbs/ADS/spyrits.cry.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SPYRITS.CRY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
