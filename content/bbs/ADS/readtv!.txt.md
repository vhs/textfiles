+++
linktitle = "readtv!.txt"
title = "readtv!.txt"
url = "bbs/ADS/readtv_.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download READTV!.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove.options]
  filetype = "ans"
+++
