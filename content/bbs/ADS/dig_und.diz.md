+++
linktitle = "dig_und.diz"
title = "dig_und.diz"
url = "bbs/ADS/dig_und.diz.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIG_UND.DIZ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
