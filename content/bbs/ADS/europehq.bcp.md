+++
linktitle = "europehq.bcp"
title = "europehq.bcp"
url = "bbs/ADS/europehq.bcp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EUROPEHQ.BCP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
