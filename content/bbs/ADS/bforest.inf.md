+++
linktitle = "bforest.inf"
title = "bforest.inf"
url = "bbs/ADS/bforest.inf.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BFOREST.INF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
