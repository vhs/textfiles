+++
linktitle = "x¯nocid¯.inc"
title = "x¯nocid¯.inc"
url = "bbs/ADS/x_nocid_.inc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download X¯NOCID¯.INC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
