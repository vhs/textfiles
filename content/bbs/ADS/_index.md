+++
title = "BBS Ads"
description = "Small Ads for Various BBSes"
tabledata = "bbs_ads"
tablefooter = "There are 567 files for a total of 4,951,245 bytes.<br>There is 1 directory."
pagefooter = "Important Credit: A majority portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
[cascade.ansilove]
  render = true
+++

With many programs that you downloaded from BBSes, you would often find a little hint of text in the unpacking of the archive, or when you read a file called READ.ME or CALLTHIS.BBS. These were ads for BBSes, and would
promise you the world if you called. Some of them go on for a long time, telling you every last detail about the BBS, and some just hint at the greatness you will find when you dial in.

It should be noted that this practice was most popular with pirate BBSes, because they would have many individual programs that they were trying to spread around, and get some fame as being the First and Best at getting
the latest stuff. On the other hand, a lot of the major, major BBSes would also "tag" every file in their collection because they wanted people to
subscribe to them, so they could make some cash. Fame, money, all in little
1k files.
