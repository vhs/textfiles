+++
linktitle = "elitendo.bbs"
title = "elitendo.bbs"
url = "bbs/ADS/elitendo.bbs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ELITENDO.BBS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
