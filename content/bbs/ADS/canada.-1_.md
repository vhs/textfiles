+++
linktitle = "canada.-1#"
title = "canada.-1#"
url = "bbs/ADS/canada.-1_.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CANADA.-1# textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
