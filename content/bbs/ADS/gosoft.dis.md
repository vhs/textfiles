+++
linktitle = "gosoft.dis"
title = "gosoft.dis"
url = "bbs/ADS/gosoft.dis.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOSOFT.DIS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
