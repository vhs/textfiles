+++
linktitle = "evvin.hom"
title = "evvin.hom"
url = "bbs/ADS/ANSI/evvin.hom.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EVVIN.HOM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
