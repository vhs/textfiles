+++
linktitle = "minist1.ans"
title = "minist1.ans"
url = "bbs/ADS/ANSI/minist1.ans.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MINIST1.ANS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
