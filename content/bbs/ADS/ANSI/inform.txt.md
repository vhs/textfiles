+++
linktitle = "inform.txt"
title = "inform.txt"
url = "bbs/ADS/ANSI/inform.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download INFORM.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove.options]
  filetype = "ans"
+++
