+++
linktitle = "dna_sys.ad"
title = "dna_sys.ad"
url = "bbs/ADS/dna_sys.ad.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DNA_SYS.AD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
