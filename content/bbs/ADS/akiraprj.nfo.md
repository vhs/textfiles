+++
linktitle = "akiraprj.nfo"
title = "akiraprj.nfo"
url = "bbs/ADS/akiraprj.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AKIRAPRJ.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
