+++
linktitle = "logo_collection_3.txt"
title = "logo_collection_3.txt"
url = "bbs/ADS/logo_collection_3.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LOGO_COLLECTION_3.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  split = true
+++
