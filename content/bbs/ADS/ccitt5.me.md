+++
linktitle = "ccitt5.me"
title = "ccitt5.me"
url = "bbs/ADS/ccitt5.me.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CCITT5.ME textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
