+++
linktitle = "pbm49.mag"
title = "pbm49.mag"
url = "bbs/PASCO/pbm49.mag.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PBM49.MAG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
