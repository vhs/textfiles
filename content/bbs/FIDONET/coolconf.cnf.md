+++
linktitle = "coolconf.cnf"
title = "coolconf.cnf"
url = "bbs/FIDONET/coolconf.cnf.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COOLCONF.CNF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
