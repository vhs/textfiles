+++
linktitle = "apply.dxn"
title = "apply.dxn"
url = "bbs/FIDONET/apply.dxn.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download APPLY.DXN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
