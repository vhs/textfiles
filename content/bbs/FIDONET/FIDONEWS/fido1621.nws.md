+++
linktitle = "fido1621.nws"
title = "fido1621.nws"
url = "bbs/FIDONET/FIDONEWS/fido1621.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO1621.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
