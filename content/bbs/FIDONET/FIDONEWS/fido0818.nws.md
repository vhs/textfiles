+++
linktitle = "fido0818.nws"
title = "fido0818.nws"
url = "bbs/FIDONET/FIDONEWS/fido0818.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO0818.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
