+++
linktitle = "fido1447.nws"
title = "fido1447.nws"
url = "bbs/FIDONET/FIDONEWS/fido1447.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO1447.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
