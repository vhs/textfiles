+++
linktitle = "fido1039.nws"
title = "fido1039.nws"
url = "bbs/FIDONET/FIDONEWS/fido1039.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO1039.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
