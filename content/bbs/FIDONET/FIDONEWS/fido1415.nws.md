+++
linktitle = "fido1415.nws"
title = "fido1415.nws"
url = "bbs/FIDONET/FIDONEWS/fido1415.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO1415.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
