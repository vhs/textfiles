+++
linktitle = "fido0319.nws"
title = "fido0319.nws"
url = "bbs/FIDONET/FIDONEWS/fido0319.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO0319.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
