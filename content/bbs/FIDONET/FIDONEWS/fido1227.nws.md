+++
linktitle = "fido1227.nws"
title = "fido1227.nws"
url = "bbs/FIDONET/FIDONEWS/fido1227.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO1227.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
