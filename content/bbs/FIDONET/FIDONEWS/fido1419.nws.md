+++
linktitle = "fido1419.nws"
title = "fido1419.nws"
url = "bbs/FIDONET/FIDONEWS/fido1419.nws.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO1419.NWS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
