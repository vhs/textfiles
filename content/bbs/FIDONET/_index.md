+++
title = "BBS Textfiles: FIDONET"
description = "Files, News and Information from the FidoNet Experience"
tabledata = "bbs_fidonet"
tablefooter = "There are 182 files for a total of 8,570,945 bytes.<br>There are 4 directories."
[cascade.ansilove]
  render = true
+++

In 1984, when an awful lot of BBS programs were popping up and trying to garner a base of boards to run them, one particular program started to get a bit of attention: FidoNet, by Tom Jennings. This software had something that other BBSes didn't; the ability to network and transfer files, e-mail and messages throughout many other BBSes spread across the country, and later the world.

This dream, of a people-run network of BBSes that would communicate, depended on a real interesting idea; every night, an hour would be reserved for all the Fidos to talk among each other, with some BBSes declared to be the head of their "Zones" and transfer the information between other heads, cutting down on traffic. A very fascinating approach, and one that has held out with relatively few bumps... which brings up the other facet of FidoNet.

FidoNet has also had an interesting seconardy aspect; the politics and technical issues of running a network, especially one where there were hundreds of different nodes with many different ideas about the whole process. This bears itself out in the hundreds of issues of <a href="fidonews">FidoNews</a>, the FidoNet newsletter that has come out pretty much every week since 1984 (and continues to). You see the whole gamut of issues that such an organization has faced, and how these issues have been dealt with.
