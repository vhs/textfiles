+++
linktitle = "interview.1.txt"
title = "interview.1.txt"
url = "bbs/FIDONET/JENNINGS/HISTORY/interview.1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download INTERVIEW.1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
