+++
linktitle = "cfpc.art.txt"
title = "cfpc.art.txt"
url = "bbs/FIDONET/JENNINGS/UNRELATED/cfpc.art.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CFPC.ART.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
