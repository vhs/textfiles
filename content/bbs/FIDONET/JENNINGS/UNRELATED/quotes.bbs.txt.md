+++
linktitle = "quotes.bbs.txt"
title = "quotes.bbs.txt"
url = "bbs/FIDONET/JENNINGS/UNRELATED/quotes.bbs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download QUOTES.BBS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
