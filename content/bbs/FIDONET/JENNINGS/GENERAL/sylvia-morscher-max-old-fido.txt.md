+++
linktitle = "sylvia-morscher-max-old-fido.txt"
title = "sylvia-morscher-max-old-fido.txt"
url = "bbs/FIDONET/JENNINGS/GENERAL/sylvia-morscher-max-old-fido.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYLVIA-MORSCHER-MAX-OLD-FIDO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
