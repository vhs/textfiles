+++
linktitle = "lostmail.txt"
title = "lostmail.txt"
url = "bbs/FIDONET/JENNINGS/GENERAL/lostmail.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LOSTMAIL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
