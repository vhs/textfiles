+++
linktitle = "fido815.doc.txt"
title = "fido815.doc.txt"
url = "bbs/FIDONET/JENNINGS/GENERAL/fido815.doc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO815.DOC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
