+++
linktitle = "fidonews.ver.txt"
title = "fidonews.ver.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/fidonews.ver.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDONEWS.VER.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
