+++
linktitle = "ufgate.how.txt"
title = "ufgate.how.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/ufgate.how.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UFGATE.HOW.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
