+++
linktitle = "policy.doc.txt"
title = "policy.doc.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/policy.doc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POLICY.DOC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
