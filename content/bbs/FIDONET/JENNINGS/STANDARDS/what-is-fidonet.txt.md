+++
linktitle = "what-is-fidonet.txt"
title = "what-is-fidonet.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/what-is-fidonet.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WHAT-IS-FIDONET.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
