+++
linktitle = "fido-src.art.txt"
title = "fido-src.art.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/fido-src.art.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO-SRC.ART.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
