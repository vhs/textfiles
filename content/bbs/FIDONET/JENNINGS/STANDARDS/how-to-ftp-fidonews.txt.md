+++
linktitle = "how-to-ftp-fidonews.txt"
title = "how-to-ftp-fidonews.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/how-to-ftp-fidonews.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOW-TO-FTP-FIDONEWS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
