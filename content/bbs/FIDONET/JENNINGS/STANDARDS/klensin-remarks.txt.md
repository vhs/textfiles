+++
linktitle = "klensin-remarks.txt"
title = "klensin-remarks.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/klensin-remarks.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KLENSIN-REMARKS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
