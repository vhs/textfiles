+++
linktitle = "fido-fidonet-software-policy.txt"
title = "fido-fidonet-software-policy.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/fido-fidonet-software-policy.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO-FIDONET-SOFTWARE-POLICY.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
