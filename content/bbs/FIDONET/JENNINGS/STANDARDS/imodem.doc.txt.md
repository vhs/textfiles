+++
linktitle = "imodem.doc.txt"
title = "imodem.doc.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/imodem.doc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IMODEM.DOC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
