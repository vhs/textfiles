+++
linktitle = "fidonet.jnm.txt"
title = "fidonet.jnm.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/fidonet.jnm.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDONET.JNM.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
