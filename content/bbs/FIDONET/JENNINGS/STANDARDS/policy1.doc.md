+++
linktitle = "policy1.doc"
title = "policy1.doc"
url = "bbs/FIDONET/JENNINGS/STANDARDS/policy1.doc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POLICY1.DOC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
