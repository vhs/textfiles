+++
linktitle = "ftsc.txt"
title = "ftsc.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/ftsc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FTSC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
