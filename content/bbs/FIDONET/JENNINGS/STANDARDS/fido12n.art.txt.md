+++
linktitle = "fido12n.art.txt"
title = "fido12n.art.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/fido12n.art.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO12N.ART.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
