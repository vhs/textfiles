+++
linktitle = "num-nine.txt"
title = "num-nine.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/num-nine.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NUM-NINE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
