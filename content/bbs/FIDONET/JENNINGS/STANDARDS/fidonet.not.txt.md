+++
linktitle = "fidonet.not.txt"
title = "fidonet.not.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/fidonet.not.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDONET.NOT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
