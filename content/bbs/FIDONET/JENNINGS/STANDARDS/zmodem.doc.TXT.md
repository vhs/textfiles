+++
linktitle = "zmodem.doc.TXT"
title = "zmodem.doc.TXT"
url = "bbs/FIDONET/JENNINGS/STANDARDS/zmodem.doc.TXT.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZMODEM.DOC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
