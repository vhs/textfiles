+++
linktitle = "sendsync.tbl.txt"
title = "sendsync.tbl.txt"
url = "bbs/FIDONET/JENNINGS/STANDARDS/sendsync.tbl.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SENDSYNC.TBL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
