+++
linktitle = "policy2.doc"
title = "policy2.doc"
url = "bbs/FIDONET/JENNINGS/STANDARDS/policy2.doc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POLICY2.DOC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
