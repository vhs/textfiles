+++
linktitle = "menu_control.c.txt"
title = "menu_control.c.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/UNIX_VERSION/menu_control.c.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MENU_CONTROL.C.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
