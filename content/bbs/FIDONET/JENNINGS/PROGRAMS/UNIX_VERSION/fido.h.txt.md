+++
linktitle = "fido.h.txt"
title = "fido.h.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/UNIX_VERSION/fido.h.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO.H.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
