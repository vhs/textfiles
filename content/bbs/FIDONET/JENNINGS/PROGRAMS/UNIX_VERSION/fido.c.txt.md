+++
linktitle = "fido.c.txt"
title = "fido.c.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/UNIX_VERSION/fido.c.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO.C.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
