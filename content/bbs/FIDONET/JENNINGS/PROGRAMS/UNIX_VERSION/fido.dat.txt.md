+++
linktitle = "fido.dat.txt"
title = "fido.dat.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/UNIX_VERSION/fido.dat.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO.DAT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
