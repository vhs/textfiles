+++
linktitle = "datasht.sty.txt"
title = "datasht.sty.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/FIDOSHEETS/datasht.sty.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DATASHT.STY.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
