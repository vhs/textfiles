+++
linktitle = "ifna.tex.txt"
title = "ifna.tex.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/FIDOSHEETS/ifna.tex.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IFNA.TEX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
