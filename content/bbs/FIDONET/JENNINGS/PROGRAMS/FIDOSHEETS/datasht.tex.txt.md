+++
linktitle = "datasht.tex.txt"
title = "datasht.tex.txt"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/FIDOSHEETS/datasht.tex.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DATASHT.TEX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
