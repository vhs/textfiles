+++
linktitle = "guide.asc"
title = "guide.asc"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/FIDOSHEETS/guide.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUIDE.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
