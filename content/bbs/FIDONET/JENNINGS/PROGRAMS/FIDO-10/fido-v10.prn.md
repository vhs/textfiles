+++
linktitle = "fido-v10.prn"
title = "fido-v10.prn"
url = "bbs/FIDONET/JENNINGS/PROGRAMS/FIDO-10/fido-v10.prn.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIDO-V10.PRN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  split = true
+++
