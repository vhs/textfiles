+++
linktitle = "eff.art.txt"
title = "eff.art.txt"
url = "bbs/FIDONET/JENNINGS/POLITICS/eff.art.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EFF.ART.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
