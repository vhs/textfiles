+++
linktitle = "oops.txt"
title = "oops.txt"
url = "bbs/FIDONET/JENNINGS/CONTROVERSY/oops.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OOPS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
