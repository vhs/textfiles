+++
linktitle = "measure9.txt"
title = "measure9.txt"
url = "bbs/FIDONET/JENNINGS/CONTROVERSY/measure9.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MEASURE9.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
