+++
linktitle = "pcinfo.txt"
title = "pcinfo.txt"
url = "bbs/FIDONET/JENNINGS/CONTROVERSY/MWSTAR/pcinfo.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PCINFO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
