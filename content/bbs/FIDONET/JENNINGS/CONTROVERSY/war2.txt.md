+++
linktitle = "war2.txt"
title = "war2.txt"
url = "bbs/FIDONET/JENNINGS/CONTROVERSY/war2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WAR2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
