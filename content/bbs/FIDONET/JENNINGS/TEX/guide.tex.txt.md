+++
linktitle = "guide.tex.txt"
title = "guide.tex.txt"
url = "bbs/FIDONET/JENNINGS/TEX/guide.tex.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUIDE.TEX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
