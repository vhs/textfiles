+++
linktitle = "coolnet.app"
title = "coolnet.app"
url = "bbs/FIDONET/coolnet.app.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COOLNET.APP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
