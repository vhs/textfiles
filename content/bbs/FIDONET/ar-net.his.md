+++
linktitle = "ar-net.his"
title = "ar-net.his"
url = "bbs/FIDONET/ar-net.his.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AR-NET.HIS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
