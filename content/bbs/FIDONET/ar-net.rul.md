+++
linktitle = "ar-net.rul"
title = "ar-net.rul"
url = "bbs/FIDONET/ar-net.rul.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AR-NET.RUL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
