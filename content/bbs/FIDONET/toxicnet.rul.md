+++
linktitle = "toxicnet.rul"
title = "toxicnet.rul"
url = "bbs/FIDONET/toxicnet.rul.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TOXICNET.RUL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
