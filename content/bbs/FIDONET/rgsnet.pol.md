+++
linktitle = "rgsnet.pol"
title = "rgsnet.pol"
url = "bbs/FIDONET/rgsnet.pol.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RGSNET.POL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
