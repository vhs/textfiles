+++
linktitle = "rimeconf.txt"
title = "rimeconf.txt"
url = "bbs/FIDONET/RIME/rimeconf.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RIMECONF.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
