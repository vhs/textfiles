+++
linktitle = "ftprules.txt"
title = "ftprules.txt"
url = "bbs/FIDONET/RIME/ftprules.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FTPRULES.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
