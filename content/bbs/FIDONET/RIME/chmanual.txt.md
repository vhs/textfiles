+++
linktitle = "chmanual.txt"
title = "chmanual.txt"
url = "bbs/FIDONET/RIME/chmanual.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHMANUAL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
