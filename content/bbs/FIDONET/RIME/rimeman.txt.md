+++
linktitle = "rimeman.txt"
title = "rimeman.txt"
url = "bbs/FIDONET/RIME/rimeman.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RIMEMAN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
