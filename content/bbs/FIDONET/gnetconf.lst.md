+++
linktitle = "gnetconf.lst"
title = "gnetconf.lst"
url = "bbs/FIDONET/gnetconf.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GNETCONF.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
