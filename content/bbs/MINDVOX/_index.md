+++
title = "BBS Textfiles: The Mindvox Collection"
description = "The MindVox Collection: Menus, Files and Forums of MindVox BBS, First Edition"
tabledata = "bbs_mindvox"
tablefooter = "There are 6 files for a total of 108,006 bytes.<br>There are 6 directories."
+++

In 1992, Patrick Kroupa (Lord Digital of the Legion of Doom) released a document called "Voices in My Head", which announced to the world the forming of a new BBS/ISP called "Mindvox". In this document (reprinted below), Kroupa created a mind-blowing narrative of his own journey from the dawning days of BBSes to the present, discussing not only his own personal experiences, but the conclusions and ideas he had come up with for how to improve the world online and offline. What had begun as a simple "we're open" seemed to encompass many of the issues and ideas of an online generation.

As befitting its tribute to the BBS days of old, the file was quickly passed through a multitude of internet and dial-up sites, showing up in file directories the country over. It was even reprinted in a number of publications, including Wired. Through this spreading word, the New York City-located MindVox (also known as Phantom Access Technologies) began to bring on users and grow in size. The peak user base was about three thousand people, considered by some to be the cream of the online crop: old-school hackers, writers, artists, law enforcement agents, and a cross-section of New Yorkers using Mindvox to get on the Internet and get their e-mail at mindvox.com.

The MindVox BBS closed its doors in July of 1996, a victim of a number of forces, including the encroachment of huge-scale players like MCI, Mindspring and AT&T into their market. But while MindVox itself had shut down, the mystique around the BBS never went away. Users (and non-users) continue to remember the name and the  high-quality online experience it hinted at and often delivered. 

This section is a collection of various-era MindVox documents, including Frequently Asked Questions Lists, menu captures, and sections of message bases. Together, they give a small glimpse into the unique world that was MindVox.

All materials are (C) Copyright 1991-2001, MindVox, Inc. and are posted on textfiles.com with their permission. Please contact MindVox if you wish to duplicate them.
