+++
title = "BBS Textfiles: The Mindvox Collection"
description = "Mindvox Frequently Asked Questions Files"
tabledata = "bbs_mindvox_faq"
tablefooter = "There are 3 files for a total of 66,736 bytes."
+++

A Collection of a number of MindVox Frequently Asked Questions and System Status files, providing as many answers as possible to questions that users might have. In many ways, the Version 2.43 Frequently Asked Questions is even more of a "VOX Manual" than the manual included in this collection. Besides giving further insight into the MindVox Philosophy, deeper explanations of all aspects of MindVox are presented.

All materials are (C) Copyright 1991-2001, MindVox, Inc. and are posted on textfiles.com with their permission. Please contact MindVox if you wish to duplicate them.
