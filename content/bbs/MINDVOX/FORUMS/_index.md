+++
title = "BBS Textfiles: The Mindvox Collection"
description = "Captures of MindVox Public Sub-Boards, 1993"
tabledata = "bbs_mindvox_forums"
tablefooter = "There are 6 files for a total of 359,704 bytes."
+++

Pure Gold. These are captures of message bases from roughly 1993 from a few MindVox message bases. In them, you see the sort of users and ideas that MindVox attracted during its peak. Names that had gained infamy or accolade in other arenas appear in these files as members of huge, intelligent conversations. At the same time, the messages are littered with historical (1980's) BBS references. 

Of special interest is the round table, hosted by an Secret Security agent and featuring some of the more famous names in hacking and the people who tracked them down and saw them incarcerated. Fascinating.

All materials are (C) Copyright 1991-2001, MindVox, Inc.  and are posted on textfiles.com with their permission. Please contact MindVox if you wish to duplicate them.
