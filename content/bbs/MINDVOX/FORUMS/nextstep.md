+++
linktitle = "nextstep"
title = "nextstep"
url = "bbs/MINDVOX/FORUMS/nextstep.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEXTSTEP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
