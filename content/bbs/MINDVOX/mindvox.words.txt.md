+++
linktitle = "mindvox.words.txt"
title = "mindvox.words.txt"
url = "bbs/MINDVOX/mindvox.words.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MINDVOX.WORDS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
