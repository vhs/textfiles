+++
title = "BBS Textfiles: The Mindvox Collection"
description = "Logs of Mindvox from Unseen (1993)"
tabledata = "bbs_mindvox_unseen"
tablefooter = "There are 7 files for a total of 3,039,557 bytes."
+++

This is a separate set of log files from 1993, created by a user named "Unseen". The logs were edited at some previous date to remove personal information. What's left is pristine snapshots of how Mindvox looked in the first half of 1993, with user lists, message bases, some of the online games, and some of the system writings available in other parts of this collection. 
