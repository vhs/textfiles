+++
title = "BBS Textfiles: The Mindvox Collection"
description = "First Draft of an Unfinished Mindvox Manual by Patrick Kroupa"
tabledata = "bbs_mindvox_manual"
tablefooter = "There are 17 files for a total of 52,364 bytes."
+++

An insight into the thinking process of the author of "Voices in My Head", this is an aborted attempt at an all-inclusive "MindVox Manual" that would explain all aspects of the BBS to users. Most sections are extremely incomplete (with the exception of the Internet section, which is rather encompassing all its own) and contain interesting notes of where the writing was going to lead.

All materials are (C) Copyright 1991-2001, MindVox, Inc. and are posted on textfiles.com with their permission. Please contact MindVox if you wish to duplicate them.
