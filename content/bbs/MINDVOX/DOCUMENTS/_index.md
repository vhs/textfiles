+++
title = "BBS Textfiles: The Mindvox Collection"
description = "Mindvox System Documents (Information Files)"
tabledata = "bbs_mindvox_documents"
tablefooter = "There are 8 files for a total of 21,962 bytes."
+++

A smattering of MindVox related documents that appeared on the BBS, including explanations of the chat system, user agreement, and the systems that MindVox was hosted on. The "shell" file, which was shown to users who had been given access to the UNIX shell of MindVox, is particularly amusing and full of a number of in-jokes.

All materials are (C) Copyright 1991-2001, MindVox, Inc. and are posted on textfiles.com with their permission. Please contact MindVox if you wish to duplicate them.
