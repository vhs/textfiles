+++
title = "BBS Textfiles: The Mindvox Collection"
description = "Mindvox System Menus for Users and Administrators"
tabledata = "bbs_mindvox_menus"
tablefooter = "There are 28 files for a total of 23,166 bytes."
+++

These files are the menus that would be displayed to users at various points on MindVox. Most give a hint as to the different features that MindVox made available, including customized chat systems and unusual commands. There are a number of unusual "~" strings sprinkled throughout these files; they were used by the system software to replace them with the user's name, password, or other information.

Note that in some cases, several revisions of the same general menu appear here, snapshots into the feature set of Mindvox over the years.

All materials are (C) Copyright 1991-2001, MindVox, Inc. and are posted on textfiles.com with their permission. Please contact MindVox if you wish to duplicate them.
