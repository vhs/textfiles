+++
linktitle = "guestmenu"
title = "guestmenu"
url = "bbs/MINDVOX/MENUS/guestmenu.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUESTMENU textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
