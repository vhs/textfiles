+++
linktitle = "current-menu.rev"
title = "current-menu.rev"
url = "bbs/MINDVOX/MENUS/current-menu.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CURRENT-MENU.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
