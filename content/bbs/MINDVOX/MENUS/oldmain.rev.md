+++
linktitle = "oldmain.rev"
title = "oldmain.rev"
url = "bbs/MINDVOX/MENUS/oldmain.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OLDMAIN.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
