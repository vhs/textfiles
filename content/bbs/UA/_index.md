+++
title = "BBS Textfiles: Unauthorised Access BBS"
description = "Messages, Logos and Captures from the Unauthorised BBS (1990-1995)"
tabledata = "bbs_ua"
tablefooter = "There are 54 files for a total of 2,982,146 bytes."
+++

Founded in 1990, Unauthorised Access BBS (or UABBS) was one of the most popular phreaking and hacking boards in the UK. The ex-sysop of UA BBS has been kind enough to donate these captures, logos and messages from throughout UABBS's history.
