+++
title = "The Ministry of Knowledge"
description = "Files from the 401 BBS \"The Ministry of Knowledge\""
tabledata = "bbs_tmok"
tablefooter = "There are 14 files for a total of 345,059 bytes."
[cascade.ansilove]
  render = true
+++

> "...in the beginning there was a guy named Daver, who was bored in the summer of nineteen-and-ninety-two, and decided he'd "hack" FrontDoor and post some fake messages to a local BBS message network, called New England Net.

> "Daver went a step further and threw a BBS up on his Amiga 2000, running software he himself had coded, called The Ministry of Knowledge....

> "The third and possibly most important action on Daver's part was to start up an e'zine called Rhode Island Computer Underground Society (RICUS), which gave all of the people who had been reading his BBS and supporting R0bin H00d in the various local Networks a place to put whatever crap they submitted.

> "This archive is representative of only about 1/3rd (if that) of the text files that were written during the period, which I would estimate runs from Early Summer 1992-Late 1995. A lot, and I mean A LOT, of stuff is missing. If any of you out there have it, please, submit it, so that I may be humiliated even further."
