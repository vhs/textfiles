+++
linktitle = "fuzzylogic.txt"
title = "fuzzylogic.txt"
url = "bbs/FILELISTS/fuzzylogic.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUZZYLOGIC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
