+++
linktitle = "wfobbs.lst"
title = "wfobbs.lst"
url = "bbs/FILELISTS/wfobbs.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WFOBBS.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
