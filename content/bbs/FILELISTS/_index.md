+++
title = "File Listings"
description = "Lists of Files Available on BBSes Long Past"
tabledata = "bbs_filelists"
tablefooter = "There are 31 files for a total of 8,703,216 bytes."
+++

Many BBSes had "file sections", where you could use XMODEM (and later YMODEM, ZMODEM, KERMIT, and a bunch of other specifications) to download programs and files to your local machine, and run them. This was amazing; use the modem, get a program! Of course, over time, it became a point of pride to say how many programs you had online, and file sections grew to enormous sizes. Some captured file listings are presented below.

Why include such a mundane aspect of the BBS world? Well, for the same reason that it was great that people took random street shots of cities thoughout the last century. Even though they seemed boring and uneventful, the progression of time away from when the picture was taken gives it interest. Maybe someone will browse a file list and see a program that was their favorite; maybe they'll see that there was a embryonic version of a program they use to this day, that was billed a lot different. No matter; there's a thousand stories buried in these listings. You might hear them tell one to you.
