+++
linktitle = "tales.13.yr.old"
title = "tales.13.yr.old"
url = "bbs/tales.13.yr.old.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TALES.13.YR.OLD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
