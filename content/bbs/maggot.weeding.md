+++
linktitle = "maggot.weeding"
title = "maggot.weeding"
url = "bbs/maggot.weeding.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MAGGOT.WEEDING textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
