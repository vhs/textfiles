+++
linktitle = "tfbv14.txt"
title = "tfbv14.txt"
url = "bbs/BBSLISTS/tfbv14.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TFBV14.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
