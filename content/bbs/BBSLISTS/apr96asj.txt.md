+++
linktitle = "apr96asj.txt"
title = "apr96asj.txt"
url = "bbs/BBSLISTS/apr96asj.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download APR96ASJ.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
