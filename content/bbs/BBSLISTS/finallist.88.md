+++
linktitle = "finallist.88"
title = "finallist.88"
url = "bbs/BBSLISTS/finallist.88.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FINALLIST.88 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
