+++
linktitle = "evvin.lst"
title = "evvin.lst"
url = "bbs/BBSLISTS/evvin.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EVVIN.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
