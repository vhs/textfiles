+++
linktitle = "os2world.bbs"
title = "os2world.bbs"
url = "bbs/BBSLISTS/os2world.bbs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OS2WORLD.BBS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
