+++
linktitle = "819bbslist.txt"
title = "819bbslist.txt"
url = "bbs/BBSLISTS/819bbslist.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 819BBSLIST.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
