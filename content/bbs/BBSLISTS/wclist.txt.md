+++
linktitle = "wclist.txt"
title = "wclist.txt"
url = "bbs/BBSLISTS/wclist.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WCLIST.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  split = true
+++
