+++
linktitle = "pdxbbs-p.txt"
title = "pdxbbs-p.txt"
url = "bbs/BBSLISTS/pdxbbs-p.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PDXBBS-P.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
