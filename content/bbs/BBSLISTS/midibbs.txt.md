+++
linktitle = "midibbs.txt"
title = "midibbs.txt"
url = "bbs/BBSLISTS/midibbs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIDIBBS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
