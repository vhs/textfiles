+++
linktitle = "apr95asj.lst"
title = "apr95asj.lst"
url = "bbs/BBSLISTS/apr95asj.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download APR95ASJ.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
