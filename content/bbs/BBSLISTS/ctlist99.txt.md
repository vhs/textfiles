+++
linktitle = "ctlist99.txt"
title = "ctlist99.txt"
url = "bbs/BBSLISTS/ctlist99.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CTLIST99.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
