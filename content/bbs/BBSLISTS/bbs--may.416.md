+++
linktitle = "bbs--may.416"
title = "bbs--may.416"
url = "bbs/BBSLISTS/bbs--may.416.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BBS--MAY.416 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
