+++
linktitle = "tgsites1990.txt"
title = "tgsites1990.txt"
url = "bbs/BBSLISTS/tgsites1990.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TGSITES1990.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
