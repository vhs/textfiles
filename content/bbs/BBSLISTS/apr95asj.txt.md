+++
linktitle = "apr95asj.txt"
title = "apr95asj.txt"
url = "bbs/BBSLISTS/apr95asj.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download APR95ASJ.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
