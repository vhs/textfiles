+++
linktitle = "dfw-e.lst"
title = "dfw-e.lst"
url = "bbs/BBSLISTS/dfw-e.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DFW-E.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
