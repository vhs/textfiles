+++
linktitle = "bbs05rev.txt"
title = "bbs05rev.txt"
url = "bbs/BBSLISTS/bbs05rev.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BBS05REV.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
