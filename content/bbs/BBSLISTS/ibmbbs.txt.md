+++
linktitle = "ibmbbs.txt"
title = "ibmbbs.txt"
url = "bbs/BBSLISTS/ibmbbs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IBMBBS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
