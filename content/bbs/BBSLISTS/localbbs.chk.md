+++
linktitle = "localbbs.chk"
title = "localbbs.chk"
url = "bbs/BBSLISTS/localbbs.chk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LOCALBBS.CHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
