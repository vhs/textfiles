+++
linktitle = "thelist9011.txt"
title = "thelist9011.txt"
url = "bbs/BBSLISTS/thelist9011.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THELIST9011.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  split = true
+++
