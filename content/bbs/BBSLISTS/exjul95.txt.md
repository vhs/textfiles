+++
linktitle = "exjul95.txt"
title = "exjul95.txt"
url = "bbs/BBSLISTS/exjul95.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EXJUL95.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
