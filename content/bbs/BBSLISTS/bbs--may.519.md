+++
linktitle = "bbs--may.519"
title = "bbs--may.519"
url = "bbs/BBSLISTS/bbs--may.519.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BBS--MAY.519 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
