+++
title = "BBS Lists"
description = "Lists of Telephone Numbers of BBSes Long Past"
tabledata = "bbs_lists"
tablefooter = "There are 444 files for a total of 13,215,272 bytes.<br>There are 2 directories."
[cascade.ansilove]
  render = true
+++

BBS Lists provided a vital way for a new BBS to make itself known to the community, a way that was much less annoying than posting your BBS Ad on other, unwilling BBSes (Although a lot of BBSes had special sub-boards for the posting of such ads). Usually run by a third party, these BBS Lists help give a real feel for what the landscape unfolded into for the average user.

Additionally, in the case of ASCII Express lines, these BBSes provided not just the phone number, but the password you'd have to type in to actually enter once you connected.
