+++
linktitle = "rcpm61.txt"
title = "rcpm61.txt"
url = "bbs/BBSLISTS/rcpm61.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RCPM61.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
