+++
linktitle = "mdmnum.txt"
title = "mdmnum.txt"
url = "bbs/BBSLISTS/mdmnum.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MDMNUM.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
