+++
linktitle = "bbs_mnrs.txt"
title = "bbs_mnrs.txt"
url = "bbs/bbs_mnrs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BBS_MNRS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
