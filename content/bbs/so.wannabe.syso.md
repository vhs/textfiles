+++
linktitle = "so.wannabe.syso"
title = "so.wannabe.syso"
url = "bbs/so.wannabe.syso.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SO.WANNABE.SYSO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
