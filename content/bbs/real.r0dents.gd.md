+++
linktitle = "real.r0dents.gd"
title = "real.r0dents.gd"
url = "bbs/real.r0dents.gd.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download REAL.R0DENTS.GD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
