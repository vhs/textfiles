+++
title = "BBS Textfiles: CBBS: 1992 Messages"
description = "Logs of CBBS, Circa 1992"
tabledata = "bbs_cbbs_1992"
tablefooter = "There are 127 files for a total of 658,262 bytes."
+++

These are logs of CBBS during the year of 1992, as seen from the point of view of the sysop, Ward Christensen. While some of it may be hard to follow, the general sense of the BBS should come through.
