+++
title = "BBS Textfiles: CBBS: 1989 Messages"
description = "Logs of CBBS, Circa 1989"
tabledata = "bbs_cbbs_1989"
tablefooter = "There are 153 files for a total of 1,602,489 bytes."
+++

These are logs of CBBS during the year of 1989, as seen from the point of view of the sysop, Ward Christensen. While some of it may be hard to follow, the general sense of the BBS should come through.
