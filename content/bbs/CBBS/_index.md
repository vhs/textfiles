+++
title = "BBS Textfiles: CBBS"
description = "CBBS: The Last Days of the First BBS"
tabledata = "bbs_cbbs"
tablefooter = "There are 3 files for a total of 32,330 bytes.<br>There are 8 directories."
+++

From Ward Christensen's private collection, here are logs of the last 5 years of CBBS, the original Bulletin Board System (founded in February of 1978). This is a treasure trove of insight into how the first BBS had grown up and spent its last few years before closing up for good. The logs themsleves are captures of sessions of Ward logging into the BBS, reading messages (there were no private messages), communicating with the users, and generally doing his sysop duties. 

While many of these files will be of interest to only the most die-hard of BBS historians, it provides some real insight into how the role and use of the BBS had matured across CBBS's 16-year history.
