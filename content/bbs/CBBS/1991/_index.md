+++
title = "BBS Textfiles: CBBS: 1991 Messages"
description = "Logs of CBBS, Circa 1991"
tabledata = "bbs_cbbs_1991"
tablefooter = "There are 106 files for a total of 727,058 bytes."
+++

These are logs of CBBS during the year of 1991, as seen from the point of view of the sysop, Ward Christensen. While some of it may be hard to follow, the general sense of the BBS should come through.
