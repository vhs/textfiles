+++
title = "BBS Textfiles: CBBS: The Dead CBBS Scrolls"
description = "" # TODO
tabledata = "bbs_cbbs_scrolls"
tablefooter = "There are 7 files for a total of 90,678 bytes."
+++

After conducting interviews with Ward Christensen in association with my BBS Documentary, Ward entrusted me with a very special and unique artifact: A roll of thermal printer paper that contained captures of all the keys typed by the users of CBBS (the original BBS) in the first few months of the BBS's existence. I kept the roll safe in a sealed bag as I travelled home, and set about transcripting them to an online form for posterity. The results of this effort are below.

Each roll transcription has two versions: RAW and RECONSTRUCTED. RAW is an exacting, character by character copy of the text as it appeared on the original rolls, mistakes, deletes and all. RECONSTRUCTED is an attempt to recreate the relevant text messages that are captured, and the chats that occured. In other words, they're human readable but are
no longer exact transcriptions. 
