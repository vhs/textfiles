+++
title = "BBS Textfiles: CBBS: 1990 Messages"
description = "Logs of CBBS, Circa 1990"
tabledata = "bbs_cbbs_1990"
tablefooter = "There are 167 files for a total of 1,153,586 bytes."
+++

These are logs of CBBS during the year of 1990, as seen from the point of view of the sysop, Ward Christensen. While some of it may be hard to follow, the general sense of the BBS should come through.
