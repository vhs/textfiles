+++
title = "BBS Textfiles: CBBS: 1993 Messages"
description = "Logs of CBBS, Circa 1993"
tabledata = "bbs_cbbs_1993"
tablefooter = "There are 122 files for a total of 675,257 bytes."
+++

These are logs of CBBS during the year of 1993, as seen from the point of view of the sysop, Ward Christensen. While some of it may be hard to follow, the general sense of the BBS should come through.
