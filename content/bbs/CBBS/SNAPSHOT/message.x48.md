+++
linktitle = "message.x48"
title = "message.x48"
url = "bbs/CBBS/SNAPSHOT/message.x48.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MESSAGE.X48 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
