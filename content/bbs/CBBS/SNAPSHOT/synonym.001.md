+++
linktitle = "synonym.001"
title = "synonym.001"
url = "bbs/CBBS/SNAPSHOT/synonym.001.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYNONYM.001 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
