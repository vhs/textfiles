+++
linktitle = "940219.u"
title = "940219.u"
url = "bbs/CBBS/SNAPSHOT/940219.u.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 940219.U textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
