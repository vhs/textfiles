+++
linktitle = "help.idx"
title = "help.idx"
url = "bbs/CBBS/SNAPSHOT/help.idx.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HELP.IDX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
