+++
linktitle = "message.x70"
title = "message.x70"
url = "bbs/CBBS/SNAPSHOT/message.x70.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MESSAGE.X70 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
