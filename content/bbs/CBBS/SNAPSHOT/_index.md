+++
title = "BBS Textfiles: CBBS: A Snapshot"
description = "A Snapshot of CBBS's System Files in its Last Days"
tabledata = "bbs_cbbs_snapshot"
tablefooter = "There are 76 files for a total of 781,956 bytes."
+++

This is a collection of files that represent the text portion of the CBBS program as it was running some time before the end of its life (possibly up to a year before). The only thing missing are the binaries and batch files that comprised the actual program. 

You can see files that maintain the help system, which will give better information as to the capabilities of CBBS, and there are a number of other files that include synonyms for commands and information for specific BBS functions.
