+++
linktitle = "message.x52"
title = "message.x52"
url = "bbs/CBBS/SNAPSHOT/message.x52.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MESSAGE.X52 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
