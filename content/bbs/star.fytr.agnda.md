+++
linktitle = "star.fytr.agnda"
title = "star.fytr.agnda"
url = "bbs/star.fytr.agnda.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download STAR.FYTR.AGNDA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
