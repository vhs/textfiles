+++
linktitle = "sincit1"
title = "sincit1"
url = "bbs/DELAM/MESSAGES/sincit1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SINCIT1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
