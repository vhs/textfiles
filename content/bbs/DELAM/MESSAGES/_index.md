+++
title = "BBS Textfiles: Dr. Delam's Collection"
description = "Selections of Message Bases from Various BBSes"
tabledata = "bbs_delam_messages"
tablefooter = "There are 22 files for a total of 75,659 bytes."
+++

Dr. Delam contributes this collection of captures, messages, and strange files from his past. Some of these have been seen elsewhere; most of it is not and is from his own buffers and terminal program as he browsed around the Internet and BBS world in the early 1990's. Some files are very short, while others are significant. Through them all, one sees the day to day text that came through his screen. 
