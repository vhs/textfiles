+++
title = "BBS Textfiles: Dr. Delam's Collection: CAPTURES"
description = "Random Captures and Buffers of Hacking, Reading, and Posting Online"
tabledata = "bbs_delam_captures"
tablefooter = "There are 105 files for a total of 1,497,593 bytes."
+++

The CAPTURES collection ranges from straight logins of now-gone BBSes, to banners and internals of infrastructure computers. In some cases, there is corruption or broken buffering, and in others it is not entirely clear what is going on. It is not up to me to decide what history needs to know.
