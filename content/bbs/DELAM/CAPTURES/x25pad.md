+++
linktitle = "x25pad"
title = "x25pad"
url = "bbs/DELAM/CAPTURES/x25pad.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download X25PAD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
