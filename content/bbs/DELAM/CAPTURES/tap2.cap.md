+++
linktitle = "tap2.cap"
title = "tap2.cap"
url = "bbs/DELAM/CAPTURES/tap2.cap.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAP2.CAP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
