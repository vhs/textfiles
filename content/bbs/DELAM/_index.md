+++
title = "BBS Textfiles: Dr. Delam's Collection"
description = "Dr. Delam's Collection of Hacker Captures, Messages, and Files"
tabledata = "bbs_delam"
tablefooter = "There are 26 files for a total of 281,268 bytes.<br>There are 2 directories."
+++

Dr. Delam contributes this collection of captures, messages, and strange files from his past. Some of these have been seen elsewhere; most of it is not and is from his own buffers and terminal program as he browsed around the Internet and BBS world in the  early 1990's. Some files are very short, while others are significant. Through them all, one sees the day to day text that came through his screen. 

The CAPTURES collection ranges from straight logins of now-gone BBSes, to banners and internals of infrastructure computers. In some cases, there is corruption or broken buffering, and in others it is not entirely clear what is going on. It is not up to me to decide what history needs to know.
