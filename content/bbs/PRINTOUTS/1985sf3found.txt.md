+++
linktitle = "1985sf3found.txt"
title = "1985sf3found.txt"
url = "bbs/PRINTOUTS/1985sf3found.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1985SF3FOUND.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
