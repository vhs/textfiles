+++
linktitle = "1985bbsboard.txt"
title = "1985bbsboard.txt"
url = "bbs/PRINTOUTS/1985bbsboard.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1985BBSBOARD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
