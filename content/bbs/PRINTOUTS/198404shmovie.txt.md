+++
linktitle = "198404shmovie.txt"
title = "198404shmovie.txt"
url = "bbs/PRINTOUTS/198404shmovie.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 198404SHMOVIE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
