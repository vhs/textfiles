+++
title = "BBS Textfiles: The Jason Scott / TEXTFILES.COM Printout Collection"
description = "The Jason Scott / TEXTFILES.COM BBS Printout Collection"
tabledata = "bbs_printouts"
tablefooter = "There are 24 files for a total of 296,653 bytes."
+++

During my time using BBSes in the 1980's, I would often make use of the Epson MX-80 line printer connected to my father's IBM PC XT, and print out messages and files for later reading. Usually, this was to be able to read the files in a place different than the computer room, like the backyard deck or in a car. How well this usually worked out escapes me, but it meant that I slowly built a very large collection of printouts, ranging from 1983 through to 1985.

In the early 1990s, to divest myself of my childhood, I gave the box of these printouts to a friend, and never expected to see them again. In 2001, that friend noted the amount of work I'd been doing with BBS textfiles and asked me if I wanted  the box back, which I gladly accepted.

Many of these printouts are of files already existent on TEXTFILES.COM, and are already saved. But many others are not; they are printouts of the message bases of BBSes I was calling at the time, right from the screen as they scrolled by into the printer. They are therefore entirely unabridged handfuls of historical data from these BBSes, some of which were busted, others completely gone in memories and time. In some cases, these printouts, 20 years after the fact, may be the remaining evidence the board ever existed.

This collection consists of those printouts, captured using Optical Character Recognition (OCR) software and then hand-edited to the best of my ability to accurately represent the printout they came from.  As these writings of events decades past come to light, they give context, information, and insight.
