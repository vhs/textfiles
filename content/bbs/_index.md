+++
title = "BBS Textfiles"
description = "Running and Using Bulletin Boards"
tabledata = "bbs"
tablefooter = "There are 377 files for a total of 6,813,345 bytes.<br>There are 15 directories."
+++

While the vast majority of the textfiles on this site came from Bulletin Board Systems (BBSes), a much smaller percentage concern themselves with the actual nuts-and-bolts issues, heartaches, triumphs, and wonder of the BBSes themselves. This section covers those files specifically.

Imagine a simple home computer, maybe an Apple II or Commodore 64, sitting in a basement of a suburban house, or maybe in the bedroom of a young  computer kid, waiting eagerly for the next calling to come down the second phone line (or even the only phone line, late at night) so that the single user could read and post messages, upload and download files, or ask the SysOp (system operator) if they wanted to chat. This was the single node of power which gave hundreds of kids the platform to express themselves or affect the world around them, one caller at a time. 

These files rank among my favorites; reading the rants about the rules of the BBSes, the distant but emotionally powerful networks that rose between groups of people who never met, and the glimpses you are given into how it was to have your machine running a message base, hoping that you were going to get the most "posts" of any of the boards running in your neighborhood. Fantastic.
