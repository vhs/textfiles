+++
linktitle = "screwbbs.txt"
title = "screwbbs.txt"
url = "bbs/DESTRUCTION/screwbbs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCREWBBS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
