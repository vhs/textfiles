+++
title = "BBS Destruction and Hacking"
description = "Files on or how to Crash and Destroy BBSes"
tabledata = "bbs_destruction"
tablefooter = "There are 53 files for a total of 422,390 bytes."
+++

By doing this, of course, the users would bring to a crashing halt the only line of communication that community had; the single machine that everyone's words and thoughts were being kept. Taken from this perspective, it seems foolish, but in some locations there were a lot of BBSes, and in many  cases, the BBS would be back up, shaken but not gone, by the next morning.

One can make an argument for the feeling of power this might give a young modem user. One also can't discount the scads of bland, uninteresting BBSes that populated the landscape, who only provided an interest to those who could penetrate its meager defenses and shake it by the scruff of the neck.

Of course, the software designed for bulletin boards in the 1980's was often the work of programmers using their spare time, so while the security would be existent, it would not be bulletproof. In the case of some programs (like the GBBS software), the code would be in something like the BASIC computer language, and certain Back Doors that had been put into the program were then known to all.  By giving these BBS programs the wrong information at the right time, users could make the system die, or worse, get access to the command prompt of the BBS computer, causing havok all over the place.
