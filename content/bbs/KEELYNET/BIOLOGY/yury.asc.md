+++
linktitle = "yury.asc"
title = "yury.asc"
url = "bbs/KEELYNET/BIOLOGY/yury.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download YURY.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
