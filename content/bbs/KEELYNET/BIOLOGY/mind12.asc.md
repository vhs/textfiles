+++
linktitle = "mind12.asc"
title = "mind12.asc"
url = "bbs/KEELYNET/BIOLOGY/mind12.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIND12.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
