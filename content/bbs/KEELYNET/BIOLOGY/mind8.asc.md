+++
linktitle = "mind8.asc"
title = "mind8.asc"
url = "bbs/KEELYNET/BIOLOGY/mind8.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIND8.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
