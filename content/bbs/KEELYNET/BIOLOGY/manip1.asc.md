+++
linktitle = "manip1.asc"
title = "manip1.asc"
url = "bbs/KEELYNET/BIOLOGY/manip1.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MANIP1.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
