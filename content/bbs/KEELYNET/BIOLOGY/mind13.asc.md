+++
linktitle = "mind13.asc"
title = "mind13.asc"
url = "bbs/KEELYNET/BIOLOGY/mind13.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIND13.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
