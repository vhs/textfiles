+++
linktitle = "hoxsey1.asc"
title = "hoxsey1.asc"
url = "bbs/KEELYNET/BIOLOGY/hoxsey1.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOXSEY1.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
