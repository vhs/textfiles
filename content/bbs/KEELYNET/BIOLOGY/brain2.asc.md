+++
linktitle = "brain2.asc"
title = "brain2.asc"
url = "bbs/KEELYNET/BIOLOGY/brain2.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BRAIN2.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
