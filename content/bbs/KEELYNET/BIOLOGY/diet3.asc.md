+++
linktitle = "diet3.asc"
title = "diet3.asc"
url = "bbs/KEELYNET/BIOLOGY/diet3.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIET3.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
