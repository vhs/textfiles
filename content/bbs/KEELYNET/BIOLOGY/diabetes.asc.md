+++
linktitle = "diabetes.asc"
title = "diabetes.asc"
url = "bbs/KEELYNET/BIOLOGY/diabetes.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIABETES.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
