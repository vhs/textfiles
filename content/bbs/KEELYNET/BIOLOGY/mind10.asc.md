+++
linktitle = "mind10.asc"
title = "mind10.asc"
url = "bbs/KEELYNET/BIOLOGY/mind10.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIND10.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
