+++
linktitle = "mindpolr.asc"
title = "mindpolr.asc"
url = "bbs/KEELYNET/BIOLOGY/mindpolr.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MINDPOLR.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
