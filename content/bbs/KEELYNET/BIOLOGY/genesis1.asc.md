+++
linktitle = "genesis1.asc"
title = "genesis1.asc"
url = "bbs/KEELYNET/BIOLOGY/genesis1.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GENESIS1.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
