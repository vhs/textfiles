+++
linktitle = "nemes2.asc"
title = "nemes2.asc"
url = "bbs/KEELYNET/BIOLOGY/nemes2.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEMES2.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
