+++
linktitle = "ldmon122.asc"
title = "ldmon122.asc"
url = "bbs/KEELYNET/BIOLOGY/ldmon122.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LDMON122.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
