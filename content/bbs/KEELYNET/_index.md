+++
title = "BBS: Jerry W. Decker's Keelynet BBS"
description = "The File Collection from the Incredible KeelyNet BBS"
tabledata = "bbs_keelynet"
tablefooter = "There is 1 file for a total of 225,395 bytes.<br>There are 11 directories."
+++

Located in Texas, the Keelynet BBS was dedicated to delving into the paranormal or "odd" science, whether it be new medical therapies, UFOs, psychic happenings, or someone finding old articles which brought up a topic not covered in recent times. The SysOp, Jerry Decker, has always encouraged skepticism but never takes the excuse of "if someone hasn't done anything with it yet, it's not worth doing" that has stopped so much exploration in new ideas.

These files have often shown up on lots of BBSes, stripped of their headers, or alone without any companion files that included skeptics or additional information. We're happy to have pretty much the whole collection from the BBS days of Keelynet up in this section.

The collection here is arranged as it was in the file archives that Keely makes available for users to download. Some files could arguably be in other sections, but they all pretty much stay on topic.

KeeleyNet has gone on to find a new life as a website located at <a href="http://www.keelynet.com">www.keelynet.com</a>. It's worth checking out, as jerry expands his reach and quest for information onto the Internet.
