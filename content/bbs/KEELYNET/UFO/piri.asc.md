+++
linktitle = "piri.asc"
title = "piri.asc"
url = "bbs/KEELYNET/UFO/piri.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PIRI.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
