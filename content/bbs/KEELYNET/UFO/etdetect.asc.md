+++
linktitle = "etdetect.asc"
title = "etdetect.asc"
url = "bbs/KEELYNET/UFO/etdetect.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ETDETECT.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
