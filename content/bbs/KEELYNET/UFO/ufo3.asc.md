+++
linktitle = "ufo3.asc"
title = "ufo3.asc"
url = "bbs/KEELYNET/UFO/ufo3.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UFO3.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
