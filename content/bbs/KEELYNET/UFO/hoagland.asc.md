+++
linktitle = "hoagland.asc"
title = "hoagland.asc"
url = "bbs/KEELYNET/UFO/hoagland.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOAGLAND.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
