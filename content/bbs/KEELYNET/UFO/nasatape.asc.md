+++
linktitle = "nasatape.asc"
title = "nasatape.asc"
url = "bbs/KEELYNET/UFO/nasatape.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NASATAPE.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
