+++
linktitle = "lotto45.asc"
title = "lotto45.asc"
url = "bbs/KEELYNET/UNCLASS/lotto45.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LOTTO45.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
