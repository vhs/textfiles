+++
linktitle = "file_sys.asc"
title = "file_sys.asc"
url = "bbs/KEELYNET/UNCLASS/file_sys.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILE_SYS.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
