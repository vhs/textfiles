+++
linktitle = "filelist.1995"
title = "filelist.1995"
url = "bbs/KEELYNET/filelist.1995.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILELIST.1995 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
