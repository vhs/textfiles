+++
linktitle = "theplan.asc"
title = "theplan.asc"
url = "bbs/KEELYNET/HUMOR/theplan.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THEPLAN.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
