+++
linktitle = "sphere1.asc"
title = "sphere1.asc"
url = "bbs/KEELYNET/ENERGY/sphere1.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SPHERE1.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
