+++
linktitle = "coal2gas.asc"
title = "coal2gas.asc"
url = "bbs/KEELYNET/ENERGY/coal2gas.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COAL2GAS.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
