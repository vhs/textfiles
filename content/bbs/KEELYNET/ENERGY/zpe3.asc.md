+++
linktitle = "zpe3.asc"
title = "zpe3.asc"
url = "bbs/KEELYNET/ENERGY/zpe3.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZPE3.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
