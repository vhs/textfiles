+++
linktitle = "hydeplea.asc"
title = "hydeplea.asc"
url = "bbs/KEELYNET/ENERGY/hydeplea.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HYDEPLEA.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
