+++
linktitle = "the_wave.asc"
title = "the_wave.asc"
url = "bbs/KEELYNET/ENERGY/the_wave.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THE_WAVE.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
