+++
linktitle = "zpepaper.asc"
title = "zpepaper.asc"
url = "bbs/KEELYNET/ENERGY/zpepaper.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZPEPAPER.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
