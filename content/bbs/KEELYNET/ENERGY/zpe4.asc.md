+++
linktitle = "zpe4.asc"
title = "zpe4.asc"
url = "bbs/KEELYNET/ENERGY/zpe4.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZPE4.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
