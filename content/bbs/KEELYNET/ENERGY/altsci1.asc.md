+++
linktitle = "altsci1.asc"
title = "altsci1.asc"
url = "bbs/KEELYNET/ENERGY/altsci1.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ALTSCI1.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
