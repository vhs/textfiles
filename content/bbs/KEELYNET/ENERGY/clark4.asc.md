+++
linktitle = "clark4.asc"
title = "clark4.asc"
url = "bbs/KEELYNET/ENERGY/clark4.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CLARK4.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
