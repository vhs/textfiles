+++
linktitle = "newman5.asc"
title = "newman5.asc"
url = "bbs/KEELYNET/ENERGY/newman5.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEWMAN5.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
