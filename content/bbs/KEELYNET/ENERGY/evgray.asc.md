+++
linktitle = "evgray.asc"
title = "evgray.asc"
url = "bbs/KEELYNET/ENERGY/evgray.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EVGRAY.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
