+++
linktitle = "zenneck.asc"
title = "zenneck.asc"
url = "bbs/KEELYNET/ENERGY/zenneck.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZENNECK.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
