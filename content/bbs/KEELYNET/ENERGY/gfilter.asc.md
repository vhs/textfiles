+++
linktitle = "gfilter.asc"
title = "gfilter.asc"
url = "bbs/KEELYNET/ENERGY/gfilter.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GFILTER.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
