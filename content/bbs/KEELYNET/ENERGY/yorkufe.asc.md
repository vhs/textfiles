+++
linktitle = "yorkufe.asc"
title = "yorkufe.asc"
url = "bbs/KEELYNET/ENERGY/yorkufe.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download YORKUFE.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
