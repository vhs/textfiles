+++
linktitle = "scalar3.asc"
title = "scalar3.asc"
url = "bbs/KEELYNET/ENERGY/scalar3.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCALAR3.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
