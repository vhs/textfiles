+++
linktitle = "gnostic.asc"
title = "gnostic.asc"
url = "bbs/KEELYNET/KEELY/gnostic.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GNOSTIC.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
