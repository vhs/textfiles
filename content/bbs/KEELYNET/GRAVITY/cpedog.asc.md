+++
linktitle = "cpedog.asc"
title = "cpedog.asc"
url = "bbs/KEELYNET/GRAVITY/cpedog.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CPEDOG.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
