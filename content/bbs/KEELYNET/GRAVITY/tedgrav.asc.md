+++
linktitle = "tedgrav.asc"
title = "tedgrav.asc"
url = "bbs/KEELYNET/GRAVITY/tedgrav.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TEDGRAV.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
