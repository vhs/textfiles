+++
linktitle = "smc_spli.txt"
title = "smc_spli.txt"
url = "games/CONSOLES/smc_spli.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SMC_SPLI.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
