+++
linktitle = "trs_coco.lst"
title = "trs_coco.lst"
url = "games/CONSOLES/trs_coco.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TRS_COCO.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
