+++
linktitle = "ufo-pass.txt"
title = "ufo-pass.txt"
url = "games/CONSOLES/ufo-pass.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UFO-PASS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
