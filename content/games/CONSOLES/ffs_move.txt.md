+++
linktitle = "ffs_move.txt"
title = "ffs_move.txt"
url = "games/CONSOLES/ffs_move.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FFS_MOVE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
