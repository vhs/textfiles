+++
title = "Home Gaming Consoles"
description = "Files about home-based \"Console\" systems made to hook up to a TV and play"
tabledata = "games_consoles"
tablefooter = "There are 136 files for a total of 3,711,782 bytes.<br>There is 1 directory."
+++

Starting with the home edition of Pong put out by Atari in the 70's, there has been a massive industry of home-based video games, games which are designed to hook up to TVs and bring the experience of the arcade to the living room. Of course, this wasn't really the case for a long time, because the home consoles would always be a little (or a lot) less powerful than what you'd be dumping quarters into at home. To keep the games fresh and new, a game-player would have to purchase cartridges, CDs, or other 'gaming packs' that are sold in stores. Naturally, it becomes a challenge to be the best at every new game that comes out, and people would show their prowess or abilities by solving or beating games and then going out to report their victories in "how-to" or "FAQ" files about the games. You can see many examples below.

There was never as much copy protection on home console games because it was never thought easy to duplicate the cartridges. This became a sub-industry in itself and many files about how to copy games, how to program in the gaming systems, and how to make the systems do things they were never supposed to do are below. 
