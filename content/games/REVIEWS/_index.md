+++
title = "Game Reviews"
description = "Reviews of Game Software from Many Sources"
tabledata = "games_reviews"
tablefooter = "There are 428 files for a total of 2,985,163 bytes."
+++

A large collection of reviews of different games for many different platforms. After buying and playing a given game, many people like to write reviews of their experience with the software so that others are informed/forewarned. Like all reviews, your opinion of the games might differ.

As technology barrels ahead and games become more and more graphically and audibly intense, it helps to have perspective as to what parts of a game were important before there was any hope of graphics being photo-realistic or 3-D routines being built-in on the CPU. Also, you can see the beginning strains of game lines (sequels) that continue to today.
