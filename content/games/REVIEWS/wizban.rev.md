+++
linktitle = "wizban.rev"
title = "wizban.rev"
url = "games/REVIEWS/wizban.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WIZBAN.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
