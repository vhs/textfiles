+++
linktitle = "ijlc-2.rev"
title = "ijlc-2.rev"
url = "games/REVIEWS/ijlc-2.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IJLC-2.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
