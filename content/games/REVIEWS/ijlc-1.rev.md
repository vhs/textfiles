+++
linktitle = "ijlc-1.rev"
title = "ijlc-1.rev"
url = "games/REVIEWS/ijlc-1.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IJLC-1.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
