+++
linktitle = "ufo.rev"
title = "ufo.rev"
url = "games/REVIEWS/ufo.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UFO.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
