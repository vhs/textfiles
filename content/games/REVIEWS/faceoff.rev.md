+++
linktitle = "faceoff.rev"
title = "faceoff.rev"
url = "games/REVIEWS/faceoff.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FACEOFF.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
