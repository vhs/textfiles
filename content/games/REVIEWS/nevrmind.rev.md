+++
linktitle = "nevrmind.rev"
title = "nevrmind.rev"
url = "games/REVIEWS/nevrmind.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEVRMIND.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
