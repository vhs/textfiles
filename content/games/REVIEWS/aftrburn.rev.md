+++
linktitle = "aftrburn.rev"
title = "aftrburn.rev"
url = "games/REVIEWS/aftrburn.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AFTRBURN.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
