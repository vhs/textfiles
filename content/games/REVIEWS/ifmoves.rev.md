+++
linktitle = "ifmoves.rev"
title = "ifmoves.rev"
url = "games/REVIEWS/ifmoves.rev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IFMOVES.REV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
