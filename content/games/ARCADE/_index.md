+++
title = "Arcade Games"
description = "Files about full-size \"Arcade\" Video Games including social commentary, instructions on using/caring for them, and beating them"
tabledata = "games_arcade"
tablefooter = "There are 101 files for a total of 1,224,784 bytes."
+++

Since the first Microcomputers, Arcade Games and gaming have always been linked in some fashion to the world of BBSs.  It would make sense that if people used computers to play, they'd be attracted to machines who were dedicated to nothing but playing games. 

It is important to mention, however, that dead-on collecting of Arcade Games for the home didn't really take off until the 90's, except for a very small group of people with the gumption and floor space. Therefore, the files in this directory, that deal with the technical issues anyway, have a more recent vintage to them.
