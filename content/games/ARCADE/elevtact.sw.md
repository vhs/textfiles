+++
linktitle = "elevtact.sw"
title = "elevtact.sw"
url = "games/ARCADE/elevtact.sw.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ELEVTACT.SW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
