+++
linktitle = "centiped.sw"
title = "centiped.sw"
url = "games/ARCADE/centiped.sw.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CENTIPED.SW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
