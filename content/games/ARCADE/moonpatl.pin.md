+++
linktitle = "moonpatl.pin"
title = "moonpatl.pin"
url = "games/ARCADE/moonpatl.pin.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MOONPATL.PIN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
