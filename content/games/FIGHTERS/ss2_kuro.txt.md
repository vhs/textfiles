+++
linktitle = "ss2_kuro.txt"
title = "ss2_kuro.txt"
url = "games/FIGHTERS/ss2_kuro.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SS2_KURO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
