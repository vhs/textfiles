+++
linktitle = "mk2moves.txt"
title = "mk2moves.txt"
url = "games/FIGHTERS/mk2moves.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MK2MOVES.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
