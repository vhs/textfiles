+++
title = "Fighter Video Games"
description = "The Over-The-Top world of Fighting Games"
tabledata = "games_fighters"
tablefooter = "There are 85 files for a total of 2,355,958 bytes."
+++

Like most of the major "Genres" of video games, the Fighter, or Fighting-themed video game, has roots almost from the beginning of the industry (one could think of air hockey and games like "pong" as fighters, if you were feeling charitable). and had a few classic examples all through the first decade of video games, but then a game (it could have been Virtua Fighter, Mortal Kombat, or Street Fighter) caught the imagination and exploded into a massive industry, with media tie-ins, dozens of sequals and modifications, and, most important to this site, a near monastic-like dedication to the specific moves, actions, and rules of the different fighting games.

In this section you see people who have meticulously recounted the exact  combinations and moves for all the characters in these fighter games, as well as speculation about the best general strategies, the easter eggs and secrets that have been discovered revealed before you with pride, and even recounting of the histories of the different characters, trying to find the plots and sub-plots behind why these characters fight. The craftsmanship in the files is sometimes quite amazing, as is the willingness to life the information wholecloth from any source they can find, never checking for accuracy. This is, after all, a subculture built on bravado and style, and whoever can claim to know the newest and the best information wins. This is as indulgent and involved as you can get.
