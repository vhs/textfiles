+++
linktitle = "sf2moves.txt"
title = "sf2moves.txt"
url = "games/FIGHTERS/sf2moves.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SF2MOVES.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
