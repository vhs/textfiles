+++
title = "10 Years of Atari/Atari Games VaxMail"
description = "10 Years of Atari/Atari Games VAXMAIL (Messages Between Employees) (1983-1992)"
tabledata = "games_atarimail"
tablefooter = "There are 10 files for a total of 3,882,256 bytes."
+++

Jed Margolis got his hands on something precious: a decade of internal mail from the now-defunct Atari Games corporation, makers of some of the more beloved arcade games in history and one of the more amazing stories in computer history. Buried among these large collections of e-mails from the Atari Corp. VAX are discussions of programming, trivia, jokes, and some real insights into the day-to-day concerns of this company.
