+++
linktitle = "1882vien.xtb"
title = "1882vien.xtb"
url = "games/CHESS/1882vien.xtb.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1882VIEN.XTB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
