+++
linktitle = "1968ber.xtb"
title = "1968ber.xtb"
url = "games/CHESS/1968ber.xtb.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1968BER.XTB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
