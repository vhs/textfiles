+++
linktitle = "44gijon.xtb"
title = "44gijon.xtb"
url = "games/CHESS/44gijon.xtb.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 44GIJON.XTB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
