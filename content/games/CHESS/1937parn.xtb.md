+++
linktitle = "1937parn.xtb"
title = "1937parn.xtb"
url = "games/CHESS/1937parn.xtb.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1937PARN.XTB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
