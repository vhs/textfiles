+++
title = "Hacking Textfiles: VAX and VMS"
description = "Textfiles About Exploring or Exploiting VMS and VAXes"
tabledata = "hacking_vms"
tablefooter = "There are 48 files for a total of 1,403,312 bytes."
+++

The Operating System and Machine Family created by Digital has its own interesting legion of fans and stadium of haters. It's weird, it's old iron, and some people swear it's amazingly stable. Regardless, it garnered a lot of interest from hackers for decades, and files that implore the community to check out these grand and fascinating machines were sprinkled quite liberally on BBSes.

Most of the big names of hacking in the 1980's had at least one VAX or VMS file to offer. Also, many people wrote humor files about VAXes; those are in the <a href="/humor/COMPUTER">Computer Humor</a> section.
