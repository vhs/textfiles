+++
linktitle = "pingdumb.txt"
title = "pingdumb.txt"
url = "hacking/INTERNET/pingdumb.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PINGDUMB.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
