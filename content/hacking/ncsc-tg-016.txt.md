+++
linktitle = "ncsc-tg-016.txt"
title = "ncsc-tg-016.txt"
url = "hacking/ncsc-tg-016.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NCSC-TG-016.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
