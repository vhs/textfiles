+++
title = "Hacking Textfiles: Conventions"
description = "Information about different Hacking Conventions"
tabledata = "hacking_conventions"
tablefooter = "There are 9 files for a total of 153,901 bytes."
+++

It was always natural for people who used a BBS who were all located nearby to have gatherings and parties. In some cases, people might travel a long distance to see the faces behind the text. There was nothing better than hanging out for a while with the folks you'd talked to for months, having some great times and eating some pizza or drinking soda while the hours went on. 

You can trace back the idea of "conventions" a long way back; there have been science fiction conventions, computer conventions, even phone phreak conventions... why not hacker conventions? Some started out small and have grown to thousands of attendees, with rented hotels and huge events accompanying them. Others are small and will remain so, the same small group of friends meeting regularly to share stories and get insanely, blindly drunk. 

Here are some of the textfiles that have accompanied these events.
