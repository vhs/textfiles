+++
title = "Hacking Textfiles: The Color Books"
description = "Purported Department of Defense Computer Security Guidelines"
tabledata = "hacking_colorbooks"
tablefooter = "There are 27 files for a total of 3,364,096 bytes."
+++

Somehow I missed this series of files when I was on BBSes in the 1980's, but they seem legitimate enough, so now you have them too. These are the "Color Books", books about computer security guidelines purported to be written for the Department of Defense from the 1980's to the 1990's. They are assigned arbitrary colors, and each one goes into great detail about a specific aspect of computer security or, more likely computer security administration. I've seen these files make a lot of appearences in computer security textfile archives, so I assume the combination of Department of Defense conspiracy and seemingly in-depth (if boring) recounting of principles reach out to something in the archivist's gut. Judge for yourself.
