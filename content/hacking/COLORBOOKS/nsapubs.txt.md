+++
linktitle = "nsapubs.txt"
title = "nsapubs.txt"
url = "hacking/COLORBOOKS/nsapubs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NSAPUBS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
