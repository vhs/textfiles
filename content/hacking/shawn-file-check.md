+++
linktitle = "shawn-file-check"
title = "shawn-file-check"
url = "hacking/shawn-file-check.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAWN-FILE-CHECK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
