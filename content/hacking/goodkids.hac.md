+++
linktitle = "goodkids.hac"
title = "goodkids.hac"
url = "hacking/goodkids.hac.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOODKIDS.HAC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
