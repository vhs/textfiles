+++
title = "Hacking Textfiles"
description = "The seamy underside of, well, Everything"
tabledata = "hacking"
tablefooter = "There are 691 files for a total of 18,779,213 bytes.<br>There are 9 directories."
+++

Around the 1970's, the term "hacking" meant any deep interest in computers that manifested itself in programming or learning arcane aspects of the  machinery or operating systems. By the early 1980's, this meaning morphed into a general term of fear to describe anyone who did anything even remotely evil using computer equipment. The people who considered themselves non-malicious "Hackers" decried this bastardization of the term they took with pride, and the new name "cracker" arrived, years after that name applied to people who removed copy protection from home video games. By the late 80's to early 90's, no one cared too much, except for the  people who care about everything too much. 

In other words, these textfiles are all about hacking.

In accordance with this spirit, anything involving subverting any technology, for good or evil, is placed here. Unless, of course, you're talking about telephones, at which point you should go over to
the <a href="/phreak">phreaking</a> section.

If you're looking for electronic hacking magazines, go check out the <a href="/magazines">magazines</a>. Additionally, you might also check the <a href="/groups">groups</a> section if your file was written by someone who was part of a group.
