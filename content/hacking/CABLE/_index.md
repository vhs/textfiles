+++
title = "Hacking Textfiles: Cable TV"
description = "Textfiles about Cable TV and Cable TV Piracy"
tabledata = "hacking_cable"
tablefooter = "There are 68 files for a total of 902,324 bytes."
+++

Free Cable TV has always been a prized holy grail by people of all kinds in this country, and throughout the years a cottage industry has risen up to give the people What They Want. Beyond the immediate satisfaction of free TV, of course, are the fascinating lengths that the industry goes to to scramble signals so only authorized customers can have the precious signals. 
