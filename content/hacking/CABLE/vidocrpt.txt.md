+++
linktitle = "vidocrpt.txt"
title = "vidocrpt.txt"
url = "hacking/CABLE/vidocrpt.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VIDOCRPT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
