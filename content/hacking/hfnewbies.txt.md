+++
linktitle = "hfnewbies.txt"
title = "hfnewbies.txt"
url = "hacking/hfnewbies.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HFNEWBIES.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
