+++
linktitle = "sirsunix.hac"
title = "sirsunix.hac"
url = "hacking/UNIX/sirsunix.hac.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SIRSUNIX.HAC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
