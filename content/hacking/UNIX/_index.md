+++
title = "Hacking Textfiles: UNIX"
description = "Textfiles About Exploring or Exploiting UNIX"
tabledata = "hacking_unix"
tablefooter = "There are 79 files for a total of 3,311,861 bytes."
+++

The UNIX operating system has gone through many different and interesting incarnations since it was invented in the early 70's, but one thing is clear: It's a real fun system for a hacker to play with. Once you get past the initial halting steps of learning what you can do with it, UNIX provides many avenues of exploration, and many opportunities to get it to do things that the owners of the machine didn't expect you (as a user) to be able to do.

What's interesting about technical files like those below is that the authors will often give you a basic set of skills with UNIX before immediately delving into the arcana and weirdness necessary to do the trickery they're attempting. It must have made for a mighty odd way to learn about an operating system for a young person on a BBS. Maybe it was even the better way.

rm -rf .
