+++
title = "Computer Usage Policies at Universities and Institutions"
description = "Various Ethics and Policies of Computing Facilities Nationwide"
tabledata = "hacking_policies"
tablefooter = "There are 28 files for a total of 217,760 bytes."
+++

These policy statements came to textfiles.com off a hacking CD purchased online; whoever had the idea to save this, thank you. They give great insight into how different universities dealt with hundreds of incoming students, each more computer-savvy than the previous class, who were twisting and bending the computer networks to their whim. You can almost tell on a school-by-school basis what horrifying event had happened, from the administrators' point of view, and what they were trying to account for.
