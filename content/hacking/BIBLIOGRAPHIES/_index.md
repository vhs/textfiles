+++
title = "Hacking Textfiles: Biblographies"
description = "Lists of Articles and Books about Hackers and Hacking"
tabledata = "hacking_bibliographies"
tablefooter = "There are 15 files for a total of 658,413 bytes."
+++

Before news articles were widely available online, it was very difficult to track down what publications had done stories on hacking and general computer security subjects. These biblographies came from two main sources: Actual computer professionals seeking research materials, and hackers tracking down the same.
