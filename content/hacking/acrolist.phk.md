+++
linktitle = "acrolist.phk"
title = "acrolist.phk"
url = "hacking/acrolist.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ACROLIST.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
