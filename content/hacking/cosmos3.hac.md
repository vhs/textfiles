+++
linktitle = "cosmos3.hac"
title = "cosmos3.hac"
url = "hacking/cosmos3.hac.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COSMOS3.HAC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
