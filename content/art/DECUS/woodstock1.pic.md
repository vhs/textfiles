+++
linktitle = "woodstock1.pic"
title = "woodstock1.pic"
url = "art/DECUS/woodstock1.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WOODSTOCK1.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
