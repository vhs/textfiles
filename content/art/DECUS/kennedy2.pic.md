+++
linktitle = "kennedy2.pic"
title = "kennedy2.pic"
url = "art/DECUS/kennedy2.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KENNEDY2.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
