+++
linktitle = "christmas_6.pic"
title = "christmas_6.pic"
url = "art/DECUS/christmas_6.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHRISTMAS_6.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
