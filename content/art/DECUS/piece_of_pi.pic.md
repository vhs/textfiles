+++
linktitle = "piece_of_pi.pic"
title = "piece_of_pi.pic"
url = "art/DECUS/piece_of_pi.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PIECE_OF_PI.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
