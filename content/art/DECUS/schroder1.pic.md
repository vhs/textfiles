+++
linktitle = "schroder1.pic"
title = "schroder1.pic"
url = "art/DECUS/schroder1.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCHRODER1.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
