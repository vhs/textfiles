+++
linktitle = "christmas_16.pic"
title = "christmas_16.pic"
url = "art/DECUS/christmas_16.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHRISTMAS_16.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
