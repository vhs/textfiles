+++
linktitle = "christmas_18.pic"
title = "christmas_18.pic"
url = "art/DECUS/christmas_18.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHRISTMAS_18.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
