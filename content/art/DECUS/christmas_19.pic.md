+++
linktitle = "christmas_19.pic"
title = "christmas_19.pic"
url = "art/DECUS/christmas_19.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHRISTMAS_19.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
