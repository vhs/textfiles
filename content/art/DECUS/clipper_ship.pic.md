+++
linktitle = "clipper_ship.pic"
title = "clipper_ship.pic"
url = "art/DECUS/clipper_ship.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CLIPPER_SHIP.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
