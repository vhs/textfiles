+++
title = "Text Artwork: The DECUS Poster Collection"
description = "The DECUS Art Collection"
tabledata = "art_decus"
tablefooter = "There are 203 files for a total of 14,021,117 bytes."
+++

Comet has sent along the DECUS Posters Collection, circa 1985. Some of these show up elsewhere, but together they represent the sort of designs and posters one could print out to hang on the wall of your computer lab or home. Overprinting plays a part in some of these files, meaning there were no linefeeds so the printer would print them even stronger, giving rise to near-true black and dark gray squares.

It is not fair to call these "ASCII", even though they are converted to ASCII; many of them were in other forms first.
