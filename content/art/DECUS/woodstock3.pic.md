+++
linktitle = "woodstock3.pic"
title = "woodstock3.pic"
url = "art/DECUS/woodstock3.pic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WOODSTOCK3.PIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
