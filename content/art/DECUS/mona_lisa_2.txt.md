+++
linktitle = "mona_lisa_2.txt"
title = "mona_lisa_2.txt"
url = "art/DECUS/mona_lisa_2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MONA_LISA_2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
