+++
linktitle = "dean_nixon.txt"
title = "dean_nixon.txt"
url = "art/DECUS/dean_nixon.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEAN_NIXON.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
