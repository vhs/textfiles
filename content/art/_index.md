+++
title = "ASCII Artwork"
description = "Various ASCII Artwork/Illustrations"
tabledata = "art"
tablefooter = "There are 167 files for a total of 2,394,718 bytes.<br>There are 4 directories."
pagefooter = "<blockquote>My aunt knew William Shatner's daughter, and she got him to sign a printout of <a href=\"kirk.art\"><b>kirk.art</b></a>. It hung on my wall proudly for many years. (Printer paper is also inferior for long-term autographs)</blockquote>"
+++

Not content to be limited by the text of ASCII, people since the beginning of computer communication have used the given character set of a system to express themselves artistically. Ranging from the ubiquitous smiley :) to extreme photo-realistic digitizations of portraits, this art continues to this day and shows no sign of slowing down. Some of these are clunky, and some are too intense to be believed. But this whole genre shows that trying to force people into small standard boxes is never really successful.

Of course, this approach has blossomed into pure insanity within the world of the software pirates, who use ASCII artwork to herald their presumed greatness. Their files are in the [Piracy](/piracy/) section.
