+++
title = "ASCII Artwork: The ASCIIPR0N Collection"
description = "The ASCII PR0N Collection, Courtesy of ASCIIPR0N.COM"
tabledata = "art_asciipr0n"
tablefooter = "There are 212 files for a total of 1,304,733 bytes."
+++

A group of creative individuals at a site called
[www.asciipr0n.com](http://www.asciipr0n.com) have collected an excellent cross-section of different types of ASCII Art, ranging from pornographic to pop cultural. While some of them seem very recent, others appear to have been rescued from long-forgotten collections elsewhere on the Internet. The descriptions you see below were written by them, as I believe they have a greater knowledge of the origins.

There is a good chance some people reading this page have never heard the term "pr0n". As far as I can discern, it is a play off of the mistyping of the shorthand word for pornography, "porn", with a transposed set of letters inside, "pron". This, ostensibly, is because the writer is some sort of spastic loser who can't spell correctly. The 0 for the o in "pron" cements this reputation.

The ASCIIPR0N site not only contains this entire collection (as well as other excellent ASCII creations outside the scope of textfiles.com), but is filled with other creative works in video and music, many haunting and some sublime. It has my highest recommendation.
