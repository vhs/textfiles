+++
linktitle = "facegirl.pix"
title = "facegirl.pix"
url = "art/RTTY/facegirl.pix.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FACEGIRL.PIX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
