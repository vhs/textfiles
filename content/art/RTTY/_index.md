+++
title = "ASCII Artwork: The RTTY Collection"
description = "A Collection of RTTY Art from RTTY.COM"
tabledata = "art_rtty"
tablefooter = "There are 32 files for a total of 199,196 bytes."
+++

This is a collection of ASCII Art files from a site called [rtty.com](http://www.rtty.com), run by George Hutchison (W7KSJ) and William Bytheway (AA6ED). This site is dedicated to a machine called a "Radio Teletype", and the amateur operatiors of these machines. Besides the technical aspects and the lore, there are these picture files, most of them from the 1970's, that represent ASCII art predating the BBS.

From the pages these were taken from: *"The Teletype Artwork on these pages is the product of many different authors and artists over the years. Recent efforts on the part of Bob Roehrig, John Foust, Tom Jennings, and John Sheetz have sparked new interest in this art form.... We have found the easiest way to print the pictures is to save the files to disk, and using the editor in DOS, call each one into the edit mode and then print it. The DOS editor is also fairly straightforward in enabling corrections and modifications."*

There are two different types of files here: PIX and POX. The .PIX files are easier to decode: they are one line of text for every line printed. The .POX files use what are called "overstrike" characters: some of the lines are meant to be printed over several times (by not putting in a carriage return) so that the characters will be much darker than would normally be seen in this situation. Work has been done at [rtty.com](http://www.rtty.com) to have Java viewers of the final files.
