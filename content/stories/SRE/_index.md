+++
title = "Stories: SRE: The Solar Realms Elite Stories"
description = "The Solar Realms Elite, by Josh Renaud"
tabledata = "stories_sre"
tablefooter = "There are 15 files for a total of 346,077 bytes."
+++

Josh Renaud writes"It's been 10 years since I first wrote these stories and uploaded them to the file sections of several St. Louis BBSes. I was about 13 or 14 years old at the time. The first three stories (SRE I-III) were narrative descriptions of an actual game of Solar Realms Elite played on the Knight's Armor BBS. It really was a great game, though these early stories didn't do it much justice.  People did read and download the stories and a few of them encouraged me to write more. So I did. The subsequent stories came solely from my imagination, except "Feqh Galaxy" and "Galaxy Sei," which were both based on games I participated in."

"Since I was a kid, my writing had a lot of room for improvement. Equally funny as the writing itself is the massive imaginary fan base I addressed in almost every story. The first three stories were clearly an attempt to exalt myself, since I felt my deserved glory had been stolen. I also wrote a document about trade and economics in the game itself. This was mostly a theoretical document, and doesn't have much practical value. In the years after I wrote all this stuff, I actually did discover a few tricks beyond the standard 'two empires at different economic levels trade and make money' strategy. Some day I'll write those down."

"SRE is still one of my favorite online games. Unfortunately, the author of the game (Amit Patel) lost the source code for SRE, and it never developed farther. One other note: I recently learned that "Va fan culo puttana" (the empire name used by a SEE player who became a character in one of the stories) is actually an Italian curse phrase. Ah yes, the ignorance of youth is bliss."
