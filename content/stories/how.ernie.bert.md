+++
linktitle = "how.ernie.bert"
title = "how.ernie.bert"
url = "stories/how.ernie.bert.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOW.ERNIE.BERT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
