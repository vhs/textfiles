+++
linktitle = "textfile.primer"
title = "textfile.primer"
url = "stories/textfile.primer.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TEXTFILE.PRIMER textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
