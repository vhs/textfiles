+++
linktitle = "pepsi.degenerat"
title = "pepsi.degenerat"
url = "stories/pepsi.degenerat.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PEPSI.DEGENERAT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
