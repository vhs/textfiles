+++
linktitle = "3sonnets.vrs"
title = "3sonnets.vrs"
url = "stories/3sonnets.vrs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 3SONNETS.VRS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
