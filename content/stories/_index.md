+++
title = "Stories and Fiction"
description = "BBS User-Written Fiction"
tabledata = "stories"
tablefooter = "There are 452 files for a total of 12,702,027 bytes.<br>There are 2 directories."
+++

Given than BBSes were an unlimited potential of cross-country publishing, it's no wonder that hundreds of would-be authors took it upon themselves to fill file directories with fiction, prose, and poetry. In the first few years, you normally had stories that were being told with a specific purpose, such as to bring awareness to an important issue of self-expression or computers. But as time went on, and there was more space available, you could see all sorts of literary work make itself known. There were even a few BBSes dedicated to nothing but fiction and writing.

Also buried here are transcriptions of old fables or stories, dating back who knows how many decades.

Direct transcriptions of classic novels and stories are generally located in the [Electronic Text](/etext) section.
