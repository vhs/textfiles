+++
title = "Stories: TRISTAN FARNON"
description = "The Stories of Tristan Farnon"
tabledata = "stories_farnon"
tablefooter = ""
pagefooter = "At the request of Tristan Farnon, this directory has been removed.<p>This saddens me no end, because his stories were truly wonderful. He'd post them on the Dark Side of the Moon AE, and while there were always interesting files being posted there, Tristan's always caused a rush of excitement through the place. You could tell he'd posted one, because the system would be really busy.<p>Tristan's stories were filled with irony, interesting situations, and well-written dialogue, all pleasant changes from what else you would find. They weren't interested in causing destruction or insulting others; they just entertained.<p>Tristan Farnon has gone on to do excellent web-based work on his site <a href=\"http://www.leisuretown.com\">Leisuretown</a>, which continues to display his excellent sense of humor, irony, characterization and flat-out quality. I give it my highest recommendation."
+++
