+++
title = "Politics: Guns and Gun Control"
description = "Files about the Topics of Guns and Gun Control"
tabledata = "politics_guns"
tablefooter = "There are 73 files for a total of 1,221,693 bytes."
+++

The classic debate online and off, but especially online. There are guns, which are weapons that kill from a distance. In some places, you can have one, in other places, you cannot. Sometimes they do good things, and sometimes, they do bad things. What should be done? A lot of folks have a lot of ideas.
