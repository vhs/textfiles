+++
linktitle = "gun-ctrl.txt"
title = "gun-ctrl.txt"
url = "politics/gun-ctrl.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUN-CTRL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
