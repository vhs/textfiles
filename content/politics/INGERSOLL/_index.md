+++
title = "Politics: The Works of Robert G. Ingersoll"
description = "Files Written by or with Robert G. Ingersoll"
tabledata = "politics_ingersoll"
tablefooter = "There are 126 files for a total of 9,639,507 bytes."
+++

Who is Robert G. Ingersoll? Good question. For whatever reason, I'm now in the possession of dozens of this man's writings; they span from reflections on friends he has known to debates about religion and ideas for the future and the position of man in the universe. Since this gentleman lived back around the late 1800's to early 1900's, there are some more recent folks who believe he should live again online. Never let it be said I didn't have space for him.
