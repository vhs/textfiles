+++
linktitle = "presid1.d"
title = "presid1.d"
url = "politics/INGERSOLL/presid1.d.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRESID1.D textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
