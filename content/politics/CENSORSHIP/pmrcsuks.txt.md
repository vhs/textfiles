+++
linktitle = "pmrcsuks.txt"
title = "pmrcsuks.txt"
url = "politics/CENSORSHIP/pmrcsuks.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PMRCSUKS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
