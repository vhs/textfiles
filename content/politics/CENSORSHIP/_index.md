+++
title = "Politics: On Censorship and Free Expression"
description = "Files about the topic of Censorship and Free Expression"
tabledata = "politics_censorship"
tablefooter = "There are 21 files for a total of 451,560 bytes."
+++

All of these files deal in some way with free expression and the banning of it, i.e. Censorship. Usually, they come in the manner of appeals to your sensibility to allow something to continue to be expressed without being unduly limited for political or religious reasons.

Obviously, this subject is very important to textfiles.com, as it is to any place which provides any sort of information that could potentially be questioned by other factions of the world. However, the nature of this site is that many files which deal in some way with censorship will often be in completely unrelated sections, so look around. The idea that someone tells you what you can and can't read runs as a common theme through a lot of the files here, so you can be guaranteed that you'll find plenty of text to read on the subject, even if it's not in this particular directory.
