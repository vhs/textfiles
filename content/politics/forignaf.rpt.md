+++
linktitle = "forignaf.rpt"
title = "forignaf.rpt"
url = "politics/forignaf.rpt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FORIGNAF.RPT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
