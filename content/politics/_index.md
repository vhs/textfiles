+++
title = "Textfiles Of a Political Nature"
description = "Files of a Political Nature"
tabledata = "politics"
tablefooter = "There are 665 files for a total of 28,198,348 bytes.<br>There are 5 directories."
+++

Ah, politics. Well, on first glance, this is about the political process in the United States (where the majority of the textfiles on this site come from), but on closer inspection, it gets a little more blurred. Some of these files talk about issues involving political actions in Religion, some talk about drugs in a political (lawmaking) context, and other files are simply transcription of important speeches or laws made by government.

In other words, you will find a lot of political files here, but no less than you might find in the Conspiracy, Drugs, Occult, or Law sections. It's all very hard to classify, what with files that rant against the intrusion of the government while others demand more intrusion and intervention to stop a wrong. As you might expect, my advice is to browse.
