+++
linktitle = "f-fascis.lyr"
title = "f-fascis.lyr"
url = "politics/f-fascis.lyr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download F-FASCIS.LYR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
