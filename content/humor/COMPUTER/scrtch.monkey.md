+++
linktitle = "scrtch.monkey"
title = "scrtch.monkey"
url = "humor/COMPUTER/scrtch.monkey.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCRTCH.MONKEY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
