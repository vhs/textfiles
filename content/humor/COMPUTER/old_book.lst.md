+++
linktitle = "old_book.lst"
title = "old_book.lst"
url = "humor/COMPUTER/old_book.lst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OLD_BOOK.LST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
