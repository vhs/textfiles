+++
linktitle = "goldi.flisp"
title = "goldi.flisp"
url = "humor/COMPUTER/goldi.flisp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOLDI.FLISP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
