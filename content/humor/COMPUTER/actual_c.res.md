+++
linktitle = "actual_c.res"
title = "actual_c.res"
url = "humor/COMPUTER/actual_c.res.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ACTUAL_C.RES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
