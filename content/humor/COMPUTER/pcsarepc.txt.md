+++
linktitle = "pcsarepc.txt"
title = "pcsarepc.txt"
url = "humor/COMPUTER/pcsarepc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PCSAREPC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
