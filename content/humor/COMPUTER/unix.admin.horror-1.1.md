+++
linktitle = "unix.admin.horror-1.1"
title = "unix.admin.horror-1.1"
url = "humor/COMPUTER/unix.admin.horror-1.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UNIX.ADMIN.HORROR-1.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
