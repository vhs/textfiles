+++
linktitle = "how_to_p.c"
title = "how_to_p.c"
url = "humor/COMPUTER/how_to_p.c.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOW_TO_P.C textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
