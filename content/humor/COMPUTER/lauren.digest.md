+++
linktitle = "lauren.digest"
title = "lauren.digest"
url = "humor/COMPUTER/lauren.digest.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LAUREN.DIGEST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
