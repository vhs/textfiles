+++
linktitle = "cpuns.c"
title = "cpuns.c"
url = "humor/COMPUTER/cpuns.c.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CPUNS.C textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
