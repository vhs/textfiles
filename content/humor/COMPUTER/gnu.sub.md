+++
linktitle = "gnu.sub"
title = "gnu.sub"
url = "humor/COMPUTER/gnu.sub.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GNU.SUB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
