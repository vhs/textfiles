+++
linktitle = "zippy_eliza"
title = "zippy_eliza"
url = "humor/COMPUTER/zippy_eliza.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZIPPY_ELIZA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
