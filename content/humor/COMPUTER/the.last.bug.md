+++
linktitle = "the.last.bug"
title = "the.last.bug"
url = "humor/COMPUTER/the.last.bug.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THE.LAST.BUG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
