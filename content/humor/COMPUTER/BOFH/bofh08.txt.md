+++
linktitle = "bofh08.txt"
title = "bofh08.txt"
url = "humor/COMPUTER/BOFH/bofh08.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BOFH08.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
