+++
linktitle = "bofh09.txt"
title = "bofh09.txt"
url = "humor/COMPUTER/BOFH/bofh09.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BOFH09.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
