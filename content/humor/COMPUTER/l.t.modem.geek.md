+++
linktitle = "l.t.modem.geek"
title = "l.t.modem.geek"
url = "humor/COMPUTER/l.t.modem.geek.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download L.T.MODEM.GEEK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
