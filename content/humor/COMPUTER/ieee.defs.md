+++
linktitle = "ieee.defs"
title = "ieee.defs"
url = "humor/COMPUTER/ieee.defs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IEEE.DEFS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
