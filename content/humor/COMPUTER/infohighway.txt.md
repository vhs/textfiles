+++
linktitle = "infohighway.txt"
title = "infohighway.txt"
url = "humor/COMPUTER/infohighway.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download INFOHIGHWAY.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
