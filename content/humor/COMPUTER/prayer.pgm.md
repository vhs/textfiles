+++
linktitle = "prayer.pgm"
title = "prayer.pgm"
url = "humor/COMPUTER/prayer.pgm.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRAYER.PGM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
