+++
linktitle = "hp-fonts.txt"
title = "hp-fonts.txt"
url = "humor/COMPUTER/hp-fonts.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HP-FONTS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
