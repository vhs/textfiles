+++
linktitle = "customer.sup"
title = "customer.sup"
url = "humor/COMPUTER/customer.sup.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CUSTOMER.SUP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
