+++
linktitle = "win95.txt"
title = "win95.txt"
url = "humor/COMPUTER/win95.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WIN95.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
