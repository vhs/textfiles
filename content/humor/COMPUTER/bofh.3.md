+++
linktitle = "bofh.3"
title = "bofh.3"
url = "humor/COMPUTER/bofh.3.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BOFH.3 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
