+++
linktitle = "alices_r.str"
title = "alices_r.str"
url = "humor/COMPUTER/alices_r.str.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ALICES_R.STR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
