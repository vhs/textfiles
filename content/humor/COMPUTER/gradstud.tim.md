+++
linktitle = "gradstud.tim"
title = "gradstud.tim"
url = "humor/COMPUTER/gradstud.tim.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GRADSTUD.TIM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
