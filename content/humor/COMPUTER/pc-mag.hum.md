+++
linktitle = "pc-mag.hum"
title = "pc-mag.hum"
url = "humor/COMPUTER/pc-mag.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PC-MAG.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
