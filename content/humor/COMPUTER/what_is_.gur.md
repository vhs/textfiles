+++
linktitle = "what_is_.gur"
title = "what_is_.gur"
url = "humor/COMPUTER/what_is_.gur.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WHAT_IS_.GUR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
