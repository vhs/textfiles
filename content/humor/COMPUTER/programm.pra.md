+++
linktitle = "programm.pra"
title = "programm.pra"
url = "humor/COMPUTER/programm.pra.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PROGRAMM.PRA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
