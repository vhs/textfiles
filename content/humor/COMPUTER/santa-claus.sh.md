+++
linktitle = "santa-claus.sh"
title = "santa-claus.sh"
url = "humor/COMPUTER/santa-claus.sh.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SANTA-CLAUS.SH textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
