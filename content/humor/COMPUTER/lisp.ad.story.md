+++
linktitle = "lisp.ad.story"
title = "lisp.ad.story"
url = "humor/COMPUTER/lisp.ad.story.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LISP.AD.STORY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
