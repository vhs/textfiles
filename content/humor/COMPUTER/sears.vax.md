+++
linktitle = "sears.vax"
title = "sears.vax"
url = "humor/COMPUTER/sears.vax.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SEARS.VAX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
