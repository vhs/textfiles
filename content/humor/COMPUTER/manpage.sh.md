+++
linktitle = "manpage.sh"
title = "manpage.sh"
url = "humor/COMPUTER/manpage.sh.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MANPAGE.SH textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
