+++
linktitle = "_vax.bugs"
title = "_vax.bugs"
url = "humor/COMPUTER/_vax.bugs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download _VAX.BUGS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
