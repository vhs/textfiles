+++
linktitle = "uzi&comp.txt"
title = "uzi&comp.txt"
url = "humor/COMPUTER/uzi&comp.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UZI&COMP.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
