+++
linktitle = "dr.seuss"
title = "dr.seuss"
url = "humor/COMPUTER/dr.seuss.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DR.SEUSS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
