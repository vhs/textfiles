+++
linktitle = "toilet.mil"
title = "toilet.mil"
url = "humor/COMPUTER/toilet.mil.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TOILET.MIL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
