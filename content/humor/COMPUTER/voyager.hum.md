+++
linktitle = "voyager.hum"
title = "voyager.hum"
url = "humor/COMPUTER/voyager.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VOYAGER.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
