+++
linktitle = "mumps.messag"
title = "mumps.messag"
url = "humor/COMPUTER/mumps.messag.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MUMPS.MESSAG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
