+++
linktitle = "unisys.jok"
title = "unisys.jok"
url = "humor/COMPUTER/unisys.jok.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UNISYS.JOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
