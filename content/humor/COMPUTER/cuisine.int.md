+++
linktitle = "cuisine.int"
title = "cuisine.int"
url = "humor/COMPUTER/cuisine.int.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CUISINE.INT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
