+++
linktitle = "lblbjoke.txt"
title = "lblbjoke.txt"
url = "humor/COMPUTER/lblbjoke.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LBLBJOKE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
