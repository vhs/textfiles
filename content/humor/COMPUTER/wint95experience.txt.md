+++
linktitle = "wint95experience.txt"
title = "wint95experience.txt"
url = "humor/COMPUTER/wint95experience.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WINT95EXPERIENCE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
