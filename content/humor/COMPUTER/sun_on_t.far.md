+++
linktitle = "sun_on_t.far"
title = "sun_on_t.far"
url = "humor/COMPUTER/sun_on_t.far.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUN_ON_T.FAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
