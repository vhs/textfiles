+++
linktitle = "nerd_te.st2"
title = "nerd_te.st2"
url = "humor/COMPUTER/nerd_te.st2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NERD_TE.ST2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
