+++
linktitle = "sweep.stakes"
title = "sweep.stakes"
url = "humor/COMPUTER/sweep.stakes.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SWEEP.STAKES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
