+++
linktitle = "manifest.pc"
title = "manifest.pc"
url = "humor/COMPUTER/manifest.pc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MANIFEST.PC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
