+++
linktitle = "mr.prtocl"
title = "mr.prtocl"
url = "humor/COMPUTER/mr.prtocl.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MR.PRTOCL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
