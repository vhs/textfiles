+++
linktitle = "wisdom.talk"
title = "wisdom.talk"
url = "humor/COMPUTER/wisdom.talk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WISDOM.TALK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
