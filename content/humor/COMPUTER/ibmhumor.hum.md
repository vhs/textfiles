+++
linktitle = "ibmhumor.hum"
title = "ibmhumor.hum"
url = "humor/COMPUTER/ibmhumor.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IBMHUMOR.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
