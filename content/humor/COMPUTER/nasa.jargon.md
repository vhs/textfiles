+++
linktitle = "nasa.jargon"
title = "nasa.jargon"
url = "humor/COMPUTER/nasa.jargon.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NASA.JARGON textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
