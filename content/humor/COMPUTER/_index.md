+++
title = "Humor Files: Computer"
description = "Computer-Related Humor"
tabledata = "humor_computer"
tablefooter = "There are 666 files for a total of 8,423,597 bytes.<br>There is 1 directory."
+++

Herein are the Humor files least likely to be 'gotten' in terms of humor. The subjects are obscure computer languages, puns based on coding techniques, and references to marketing and software engineering long since buried under the sands of time. These are also most likely some of the oldest textfiles on this site, with many dating to the 70's and early 80's, and maybe the occasional 60's document retyped and uploaded as networks became more prominent.

If you don't laugh, it's not you. if you do laugh, it is. Geek.
