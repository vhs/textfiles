+++
linktitle = "smart.basic?"
title = "smart.basic?"
url = "humor/COMPUTER/smart.basic_.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SMART.BASIC? textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
