+++
linktitle = "unix_laf.txt"
title = "unix_laf.txt"
url = "humor/COMPUTER/unix_laf.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UNIX_LAF.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
