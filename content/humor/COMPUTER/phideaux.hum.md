+++
linktitle = "phideaux.hum"
title = "phideaux.hum"
url = "humor/COMPUTER/phideaux.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PHIDEAUX.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
