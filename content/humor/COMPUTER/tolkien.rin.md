+++
linktitle = "tolkien.rin"
title = "tolkien.rin"
url = "humor/COMPUTER/tolkien.rin.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TOLKIEN.RIN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
