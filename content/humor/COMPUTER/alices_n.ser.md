+++
linktitle = "alices_n.ser"
title = "alices_n.ser"
url = "humor/COMPUTER/alices_n.ser.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ALICES_N.SER textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
