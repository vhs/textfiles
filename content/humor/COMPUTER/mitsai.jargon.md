+++
linktitle = "mitsai.jargon"
title = "mitsai.jargon"
url = "humor/COMPUTER/mitsai.jargon.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MITSAI.JARGON textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
