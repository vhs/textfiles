+++
linktitle = "jargon.tex"
title = "jargon.tex"
url = "humor/COMPUTER/jargon.tex.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JARGON.TEX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
