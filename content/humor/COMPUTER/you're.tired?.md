+++
linktitle = "you're.tired?"
title = "you're.tired?"
url = "humor/COMPUTER/you_re.tired_.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download YOU'RE.TIRED? textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
