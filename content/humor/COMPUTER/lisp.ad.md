+++
linktitle = "lisp.ad"
title = "lisp.ad"
url = "humor/COMPUTER/lisp.ad.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LISP.AD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
