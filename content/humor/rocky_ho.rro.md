+++
linktitle = "rocky_ho.rro"
title = "rocky_ho.rro"
url = "humor/rocky_ho.rro.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ROCKY_HO.RRO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
