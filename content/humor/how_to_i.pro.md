+++
linktitle = "how_to_i.pro"
title = "how_to_i.pro"
url = "humor/how_to_i.pro.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOW_TO_I.PRO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
