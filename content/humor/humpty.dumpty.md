+++
linktitle = "humpty.dumpty"
title = "humpty.dumpty"
url = "humor/humpty.dumpty.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HUMPTY.DUMPTY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
