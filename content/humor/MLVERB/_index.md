+++
title = "Humor: M.L. Verb"
description = "The Humor of M.L. Verb (From Compuserve)"
tabledata = "humor_mlverb"
tablefooter = "There are 25 files for a total of 328,880 bytes."
+++

M.L. Verb was a columnist for the National Satirist, a Compuserve-only humor magazine that may or may not exist. A web search shows that M.L. Verb hasn't come onto the WWW at large, which is a shame, considering.  Well, that's fixed until Compuserve decides they don't want this out here.
