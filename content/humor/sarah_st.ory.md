+++
linktitle = "sarah_st.ory"
title = "sarah_st.ory"
url = "humor/sarah_st.ory.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SARAH_ST.ORY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
