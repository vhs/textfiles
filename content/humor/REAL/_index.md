+++
title = "Humor: \"Real\" Guides"
description = "The \"Real\" Guides"
tabledata = "humor_real"
tablefooter = "There are 62 files for a total of 430,968 bytes."
+++

The theory holds that the concept of "Real" in the BBS Scene rose from the bestselling book "Real Men Don't Eat Quiche", published in the early 1980's. The audacity of this title, along with the easy copy-cat books that surrounded it, are probably what led to a massive rush of textfiles dedicated to listing or describing examples of "Real" Pirates, "Real" Phreakers, "Real" Users, and a host of other modem personalities.

In some cases, the action gets extremely metaphysical, with a Guide to Real Pirate's Guides, and Guides to types that no-one outside of the author probably knew existed.
