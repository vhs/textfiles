+++
linktitle = "jrrt.riddle"
title = "jrrt.riddle"
url = "humor/jrrt.riddle.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JRRT.RIDDLE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
