+++
linktitle = "dinosaur.jok"
title = "dinosaur.jok"
url = "humor/JOKES/dinosaur.jok.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DINOSAUR.JOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
