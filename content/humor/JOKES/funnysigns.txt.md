+++
linktitle = "funnysigns.txt"
title = "funnysigns.txt"
url = "humor/JOKES/funnysigns.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUNNYSIGNS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
