+++
linktitle = "taglines.mr"
title = "taglines.mr"
url = "humor/JOKES/taglines.mr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAGLINES.MR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
