+++
linktitle = "iraqjoke2.txt"
title = "iraqjoke2.txt"
url = "humor/JOKES/iraqjoke2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRAQJOKE2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
