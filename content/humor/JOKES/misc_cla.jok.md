+++
linktitle = "misc_cla.jok"
title = "misc_cla.jok"
url = "humor/JOKES/misc_cla.jok.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MISC_CLA.JOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
