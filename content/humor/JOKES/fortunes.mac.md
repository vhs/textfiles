+++
linktitle = "fortunes.mac"
title = "fortunes.mac"
url = "humor/JOKES/fortunes.mac.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FORTUNES.MAC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
