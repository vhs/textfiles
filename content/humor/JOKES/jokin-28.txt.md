+++
linktitle = "jokin-28.txt"
title = "jokin-28.txt"
url = "humor/JOKES/jokin-28.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JOKIN-28.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
