+++
linktitle = "fortun.cookie"
title = "fortun.cookie"
url = "humor/JOKES/fortun.cookie.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FORTUN.COOKIE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
