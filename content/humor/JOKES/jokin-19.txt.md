+++
linktitle = "jokin-19.txt"
title = "jokin-19.txt"
url = "humor/JOKES/jokin-19.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JOKIN-19.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
