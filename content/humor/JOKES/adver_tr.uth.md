+++
linktitle = "adver_tr.uth"
title = "adver_tr.uth"
url = "humor/JOKES/adver_tr.uth.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ADVER_TR.UTH textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
