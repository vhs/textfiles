+++
linktitle = "fortun_c.ook"
title = "fortun_c.ook"
url = "humor/JOKES/fortun_c.ook.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FORTUN_C.OOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
