+++
linktitle = "superman.jok"
title = "superman.jok"
url = "humor/JOKES/superman.jok.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUPERMAN.JOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
