+++
title = "Humor Files: Jokes and Shaggy Dog Stories"
description = "Standard Jokes"
tabledata = "humor_jokes"
tablefooter = "There are 421 files for a total of 10,111,551 bytes."
+++

If a joke is funny, it gets spread. If a person gets a bunch of jokes, they tack them together and they get spread. And if a subject happens (a scandal, a tragedy, an election) then the jokes about that event get tacked together and spread as well. 

What you end up with, then, are these massive files of very disparate joke clusters, bound only because someone thought they were funny or someone had a thing for light-bulb jokes. Therefore, there are probably tons of doubles and triples and what have you in this collection, and there's just not enough time in the day to sort them. So this whole directory is a bit of a crapshoot. On the other hand, it IS funny.
