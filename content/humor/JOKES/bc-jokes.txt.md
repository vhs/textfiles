+++
linktitle = "bc-jokes.txt"
title = "bc-jokes.txt"
url = "humor/JOKES/bc-jokes.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BC-JOKES.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
