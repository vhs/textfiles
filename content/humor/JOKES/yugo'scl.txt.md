+++
linktitle = "yugo'scl.txt"
title = "yugo'scl.txt"
url = "humor/JOKES/yugo_scl.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download YUGO'SCL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
