+++
linktitle = "t-shirts.jok"
title = "t-shirts.jok"
url = "humor/JOKES/t-shirts.jok.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download T-SHIRTS.JOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
