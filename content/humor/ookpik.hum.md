+++
linktitle = "ookpik.hum"
title = "ookpik.hum"
url = "humor/ookpik.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OOKPIK.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
