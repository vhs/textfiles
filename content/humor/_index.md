+++
title = "Textfiles of a Humorous or Funny Nature"
description = "Many, many attempts at being Funny"
tabledata = "humor"
tablefooter = "There are 812 files for a total of 7,442,399 bytes.<br>There are 5 directories."
+++

This directory is filled with files that I considered funny, or trying to be funny. In some cases, the files might better belong in other sections because of their chosen subject matter, but usually, someone is just trying to make others laugh. Hopefully you'll laugh too, or at least marvel that someone actually thought the chosen subject was humorous.

The sense of humor that people show online is pretty well varied; some people write fantastic diatribes on stupidity or weirdness they experience in their lives, some parody other common text files, and some just transcribe funny columns or jokes they heard. The range in this directory is, for the moment, beyond easy categorization.
