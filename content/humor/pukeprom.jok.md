+++
linktitle = "pukeprom.jok"
title = "pukeprom.jok"
url = "humor/pukeprom.jok.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PUKEPROM.JOK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
