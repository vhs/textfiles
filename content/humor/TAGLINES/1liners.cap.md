+++
linktitle = "1liners.cap"
title = "1liners.cap"
url = "humor/TAGLINES/1liners.cap.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1LINERS.CAP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
