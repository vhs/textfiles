+++
title = "Humor Files: Tag Lines and One-Liners"
description = "Collections of One-Liners"
tabledata = "humor_taglines"
tablefooter = "There are 33 files for a total of 957,352 bytes."
+++

Among the most popular approaches to humor in the BBS world and Internet has been the clever, witty, gets-you-in-the-back one-liner or "tag line", which contains an amount of humor way out of proportion to the statement that generates it. It's one thing to chuckle a few times when you read a multi-page story, but it's impressive to laugh out loud at a single sentence. Here's a collection of those single sentences.
