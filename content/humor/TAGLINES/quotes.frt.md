+++
linktitle = "quotes.frt"
title = "quotes.frt"
url = "humor/TAGLINES/quotes.frt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download QUOTES.FRT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
