+++
linktitle = "oscar.wil"
title = "oscar.wil"
url = "humor/TAGLINES/oscar.wil.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OSCAR.WIL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
