+++
linktitle = "socrmgr2.nfo"
title = "socrmgr2.nfo"
url = "piracy/HYBRID/socrmgr2.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SOCRMGR2.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
