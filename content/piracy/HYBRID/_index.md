+++
title = "Piracy Textfiles: HYBRID"
description = ".NFO and Information Files from the Hybrid Pirating Group"
tabledata = "piracy_hybrid"
tablefooter = "There are 84 files for a total of 725,054 bytes."
pagefooter="Important Credit: A portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
+++

A swedish piracy group that offered PC-based games from 1995 on.
