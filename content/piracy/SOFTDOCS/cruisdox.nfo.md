+++
linktitle = "cruisdox.nfo"
title = "cruisdox.nfo"
url = "piracy/SOFTDOCS/cruisdox.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRUISDOX.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
