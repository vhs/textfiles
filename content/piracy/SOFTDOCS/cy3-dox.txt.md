+++
linktitle = "cy3-dox.txt"
title = "cy3-dox.txt"
url = "piracy/SOFTDOCS/cy3-dox.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CY3-DOX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
