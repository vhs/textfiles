+++
linktitle = "civiliz.dox"
title = "civiliz.dox"
url = "piracy/SOFTDOCS/civiliz.dox.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIVILIZ.DOX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
