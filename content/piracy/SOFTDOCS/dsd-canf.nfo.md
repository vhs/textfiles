+++
linktitle = "dsd-canf.nfo"
title = "dsd-canf.nfo"
url = "piracy/SOFTDOCS/dsd-canf.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DSD-CANF.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
