+++
linktitle = "bic-sol2.dox"
title = "bic-sol2.dox"
url = "piracy/SOFTDOCS/bic-sol2.dox.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIC-SOL2.DOX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
