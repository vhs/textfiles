+++
title = "Piracy Textfiles: Soft Documentation"
description = "On-Line Documentation, usually for pirated programs"
tabledata = "piracy_softdocs"
tablefooter = "There are 144 files for a total of 3,697,829 bytes."
+++

Why many software programs are self-evident or contain help sections within the program itself, many others are complicated enough that the use of them is not self-evident. To counter this, piracy groups would transcribe the manual of the program so that anyone who got a copy of the program would know how to use it. In this say, Software-Based Documentation was born. It was shortened in name to "Soft Documentation" and then "Soft Docs", and even "dox" in some cases.

An interesting side-effect of this process, which was pretty tedious by any standard, was that the writer/transcriber would include a lot of interesting side-comments about the program, or the BBS world as they saw it, or any amount of gossip/opinion they could fit in.

This is not the only section with Soft Documentation; there is a very large collection of Apple II-Based Documentation in the
[Apple II](/apple) section.
