+++
linktitle = "sq5coord.txt"
title = "sq5coord.txt"
url = "piracy/SOFTDOCS/sq5coord.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SQ5COORD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
