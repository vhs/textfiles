+++
title = "Piracy Textfiles"
description = "All Hail the Warez"
tabledata = "piracy"
tablefooter = "There are 91 files for a total of 857,172 bytes.<br>There are 15 directories."
pagefooter = "Important Credit: A majority portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
[cascade.ansilove]
  render = true
+++

As soon as software became available for a price, it's been copied and distributed for free. The debate rages on about this facet of the computer world, but there's no denying it exists.

There exists the phenomenon of the Piracy Culture, where badly spelled messages offer the newest and best software for free or for trade. People brag about what they have and what they want, and young-sounding scofflaws promise the world. Examples of these files are below.

Note that there are a lot of similar files in the [Apple II](/apple) and [BBS](/bbs) sections; if you don't see something you'd expect to see below, look there as well.
