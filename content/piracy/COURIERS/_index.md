+++
title = "Piracy Textfiles: Courier Membership Lists and Introductions"
description = "Membership Lists of Couriering Teams"
tabledata = "piracy_couriers"
tablefooter = "There are 40 files for a total of 208,913 bytes."
pagefooter="Important Credit: A portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
+++

Of of the more intruiging aspects of the microcomputer piracy subculture has been the rise of "Courier Groups", which are dedicated to the sole purpose of transporting pirated games from the groups that pirate to high-traffic web sites and BBSes, often available with a membership fee. This more shark-like piracy approach rose in the early 1990s with the PC world, and by the middle of the decade, it was an established near-industry.
