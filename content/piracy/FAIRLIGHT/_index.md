+++
title = "Piracy Textfiles: Fairlight"
description = ".NFO and Information Files from the Fairlight Pirating Group"
tabledata = "piracy_fairlight"
tablefooter = "There are 41 files for a total of 336,489 bytes."
pagefooter="Important Credit: A portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
+++

"Where Dreams Come True", until they were busted in 1992.
