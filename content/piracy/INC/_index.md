+++
title = "Piracy Textfiles: The International Network of Crackers"
description = ".NFO and Information Files from the International Network of Crackers"
tabledata = "piracy_inc"
tablefooter = "There are 121 files for a total of 350,790 bytes."
pagefooter="Important Credit: A portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
+++
