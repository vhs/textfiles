+++
linktitle = "lexi-x.inc"
title = "lexi-x.inc"
url = "piracy/INC/lexi-x.inc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LEXI-X.INC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
