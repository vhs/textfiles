+++
title = "Piracy Textfiles: Unprotects"
description = "IBM-Based Software Unprotection Schemes"
tabledata = "piracy_unprotects"
tablefooter = "There are 104 files for a total of 280,925 bytes."
+++

The raging debate aside, in these files you find instructions on how to unprotect various pieces of software so they could be copied freely. In some cases, the attempt is to make it so the program can be run on a hard drive instead of the 256kb floppy that came with the software. In others, people try to make it easier to install among multiple machines or make a needed backup copy of programs that cost them hundreds of dollars.

An interesting subtext is the myriad ways that are used to prevent copying the software. A battle still being waged.
