+++
linktitle = "printshp.unp"
title = "printshp.unp"
url = "piracy/UNPROTECTS/printshp.unp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRINTSHP.UNP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
