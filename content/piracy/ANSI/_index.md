+++
title = "\"Tag\" Files"
description = "ANSI BBS and Group Tag Files"
tabledata = "piracy_ansi"
tablefooter = "There are 26 files for a total of 177,023 bytes."
+++

In the case of a lot of program collections out there, as well as archives of any computer-related sort, you will often find little one-page advertisements from the Group or BBS that distributed or created the file. In these small files, the entire essence of a BBS has to be presented and offered, with a plea or request to join in the fun. In some cases, there is no BBS to call, just a wish for acknowledgement of the superiority of that Group or Network. In these short files, you get offered the world.
