+++
linktitle = "bwatrloo.nfo"
title = "bwatrloo.nfo"
url = "piracy/PRESTIGE/bwatrloo.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BWATRLOO.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
