+++
linktitle = "settlrs2.nfo"
title = "settlrs2.nfo"
url = "piracy/PRESTIGE/settlrs2.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SETTLRS2.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
