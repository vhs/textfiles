+++
linktitle = "warcad95.nfo"
title = "warcad95.nfo"
url = "piracy/PRESTIGE/warcad95.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WARCAD95.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
