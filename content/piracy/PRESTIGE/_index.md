+++
title = "Piracy Textfiles: PRESTIGE"
description = ".NFO and Information Files from the Prestige Pirating Group"
tabledata = "piracy_prestige"
tablefooter = "There are 105 files for a total of 1,618,532 bytes."
+++

Prestige offered a cascade of PC-based games in the year of 1996. They also went out of their way to write long explanations of what the games were, and to invite you to join their ranks.
