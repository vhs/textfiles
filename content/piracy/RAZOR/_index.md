+++
title = ".NFO and Information Files from Razor 1911"
description = ".NFO and Information Files from the Razor 1911 Pirating Group"
tabledata = "piracy_razor"
tablefooter = "There are 404 files for a total of 4,943,893 bytes."
pagefooter="Important Credit: A portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
+++

Razor 1911 is one of the oldest, and probably the longest-living pirating, demo, and couriering groups in the history of software piracy. Along their more than 15 year history (they were founded in 1985) they have produced demos and pirated games for the Commodore 64, Amiga, and Intel PC, and through a near rototiller-like churning of members, have been able to stay current with advances in technology, including changing platforms, the CD-ROM revolution, and the Internet. 

A lot of the important details about Razor 1911 can be found in their [Official History File](rzrhistory), which is located in the middle of this directory. It was written by founding member Sector 9, spans over 13 years of the group's existence, and provides a much better sense of where this group came from and how they conducted themselves. Spectacular.
