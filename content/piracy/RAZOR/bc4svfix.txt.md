+++
linktitle = "bc4svfix.txt"
title = "bc4svfix.txt"
url = "piracy/RAZOR/bc4svfix.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BC4SVFIX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
