+++
linktitle = "cfoot.diz"
title = "cfoot.diz"
url = "piracy/RAZOR/cfoot.diz.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CFOOT.DIZ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
