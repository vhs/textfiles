+++
linktitle = "sextris.nfo"
title = "sextris.nfo"
url = "piracy/DREAMTEAM/sextris.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SEXTRIS.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
