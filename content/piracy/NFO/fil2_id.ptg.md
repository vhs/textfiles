+++
linktitle = "fil2_id.ptg"
title = "fil2_id.ptg"
url = "piracy/NFO/fil2_id.ptg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIL2_ID.PTG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
