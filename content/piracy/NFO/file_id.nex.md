+++
linktitle = "file_id.nex"
title = "file_id.nex"
url = "piracy/NFO/file_id.nex.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILE_ID.NEX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
