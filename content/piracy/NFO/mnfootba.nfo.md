+++
linktitle = "mnfootba.nfo"
title = "mnfootba.nfo"
url = "piracy/NFO/mnfootba.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MNFOOTBA.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
