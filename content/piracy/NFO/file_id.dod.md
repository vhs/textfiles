+++
linktitle = "file_id.dod"
title = "file_id.dod"
url = "piracy/NFO/file_id.dod.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILE_ID.DOD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
