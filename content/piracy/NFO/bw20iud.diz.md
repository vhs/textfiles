+++
linktitle = "bw20iud.diz"
title = "bw20iud.diz"
url = "piracy/NFO/bw20iud.diz.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BW20IUD.DIZ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
