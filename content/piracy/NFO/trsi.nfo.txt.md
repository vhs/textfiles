+++
linktitle = "trsi.nfo.txt"
title = "trsi.nfo.txt"
url = "piracy/NFO/trsi.nfo.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TRSI.NFO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
