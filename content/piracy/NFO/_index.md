+++
title = "\"Tag\" Files"
description = "A Very Large Selection of .NFO Files From Many Groups, 1990-Present"
tabledata = "piracy_nfo"
tablefooter = "There are 965 files for a total of 5,980,679 bytes."
pagefooter="Important Credit: A portion of these files came from a site called the <a href=\"http://www.defacto2.net/scene-archive/\">Defacto2 Scene Archive</a>, and were acquired and saved by the folks of that group. They've done a wonderful job on the site, and it is highly reccommended that you check it out."
+++

In the case of a lot of program collections out there, as well as archives of any computer-related sort, you will often find little one-page advertisements from the Group or BBS that distributed or created the file. In these small files, the entire essence of a BBS has to be presented and offered, with a plea or request to join in the fun. In some cases, there is no BBS to call, just a wish for acknowledgement of the superiority of that Group or Network. In these short files, you get offered the world.
