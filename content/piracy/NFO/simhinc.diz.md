+++
linktitle = "simhinc.diz"
title = "simhinc.diz"
url = "piracy/NFO/simhinc.diz.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SIMHINC.DIZ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
