+++
linktitle = "school.nfo"
title = "school.nfo"
url = "piracy/NFO/school.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCHOOL.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
