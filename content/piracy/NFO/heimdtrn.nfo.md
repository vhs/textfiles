+++
linktitle = "heimdtrn.nfo"
title = "heimdtrn.nfo"
url = "piracy/NFO/heimdtrn.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HEIMDTRN.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
