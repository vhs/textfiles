+++
linktitle = "file_id.cip"
title = "file_id.cip"
url = "piracy/NFO/file_id.cip.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILE_ID.CIP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
