+++
linktitle = "fil2_id.tdt"
title = "fil2_id.tdt"
url = "piracy/NFO/fil2_id.tdt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIL2_ID.TDT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
