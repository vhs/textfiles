+++
linktitle = "baddude.ptl"
title = "baddude.ptl"
url = "piracy/NFO/baddude.ptl.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BADDUDE.PTL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
