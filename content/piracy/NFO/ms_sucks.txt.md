+++
linktitle = "ms_sucks.txt"
title = "ms_sucks.txt"
url = "piracy/NFO/ms_sucks.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MS_SUCKS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
