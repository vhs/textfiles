+++
linktitle = "rigormor.tiz"
title = "rigormor.tiz"
url = "piracy/NFO/rigormor.tiz.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RIGORMOR.TIZ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
