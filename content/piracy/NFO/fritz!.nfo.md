+++
linktitle = "fritz!.nfo"
title = "fritz!.nfo"
url = "piracy/NFO/fritz_.nfo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FRITZ!.NFO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
