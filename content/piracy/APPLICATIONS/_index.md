+++
title = "Piracy Textfiles: Applications to Join"
description = "Applications to Join Piracy Groups"
tabledata = "piracy_applications"
tablefooter = "There are 31 files for a total of 97,732 bytes."
+++

Perhaps one of the wierdest sets of textfiles to have on here, these are applications to join Piracy groups. Achieving this level of organization both confirms some peoples' fears, but also gives real insight into what the priorities of the groups were. Personally, I find it more encouraging and fear-weakening than anything else: You see in here the typical clubhouse mindset of any small group; come join us, they say, and get some glory. Insightful.
