

     |||||| |||||| ||  || |||||| ||||||
     ||     ||     ||| ||   ||   ||
     || ||| ||||   ||||||   ||   ||||               Your
     ||  || ||     || |||   ||   ||
     |||||| |||||| ||  || |||||| ||||||             GEnieLamp Computing

     ||    |||||| ||    || ||||||                   RoundTable
     ||    ||  || |||  ||| ||  ||
     ||    |||||| |||||||| ||||||                   RESOURCE!
     ||    ||  || || || || ||
     ||||| ||  || ||    || ||


                    ~ WELCOME TO GEnieLamp APPLE II! ~
                      """"""""""""""""""""""""""""""
            ~ PD_QUICKVIEW:  Loadpall XCMD for HyperCard IIgs ~
           ~ FOCUS ON...:  The Finder as ProSel-16 Substitute ~
                 ~ PAUG NEWSLETTER:  January 1996 Report ~
                   ~ HOT NEWS, HOT FILES, HOT MESSAGES ~

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
 GEnieLamp Apple II     ~ A T/TalkNET Publication ~      Vol.5, Issue 46
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Publisher................................................John F. Peters
 Editor...................................................Douglas Cuff
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
          ~ GEnieLamp IBM ~ GEnieLamp ST ~ GEnieLamp PowerPC ~
        ~ GEnieLamp A2Pro ~ GEnieLamp Macintosh ~ GEnieLamp TX2 ~
         ~ GEnieLamp Windows ~ GEnieLamp A2 ~ LiveWire (ASCII) ~
            ~ Member Of The Digital Publishing Association ~
 GE Mail: GENIELAMP                       Internet: genielamp@genie.com
////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

           >>> WHAT'S HAPPENING IN THE APPLE II ROUNDTABLE? <<<
           """"""""""""""""""""""""""""""""""""""""""""""""""""
                            ~ January 1, 1996 ~


 FROM MY DESKTOP ......... [FRM]        HEY MISTER POSTMAN ...... [HEY]
  Notes From The Editor.                 Is That A Letter For Me?

 HUMOR ONLINE ............ [HUM]        REFLECTIONS ............. [REF]
  Flowchart Fun.                         Museum Home Pages.

 PD_QUICKVIEW ............ [PDQ]        FOCUS ON... ............. [FOC]
  Loadpall HyperCard XCMD.               Finder and ProSel-16.

 PAUG MEETING REPORT ..... [PMP]        LOG OFF ................. [LOG]
  Report on December's Meeting.          GEnieLamp Information.

[IDX]""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

READING GEnieLamp   GEnieLamp has incorporated a unique indexing system
"""""""""""""""""   to help make reading the magazine easier.  To utilize 
this system, load GEnieLamp into any ASCII word processor or text editor.  
In the index you will find the following example:

                   HUMOR ONLINE ............ [HUM]
                    GEnie Fun & Games.

     To read this article, set your find or search command to [HUM].  If 
you want to scan all of the articles, search for [EOA].  [EOF] will take 
you to the last page, whereas [IDX] will bring you back to the index.

MESSAGE INFO   To make it easy for you to respond to messages reprinted
""""""""""""   here in GEnieLamp, you will find all the information you 
need immediately following the message.  For example:

                    (SMITH, CAT6, TOP1, MSG:58/M475)
        _____________|   _____|__  _|___    |____ |_____________
       |Name of sender   CATegory  TOPic    Msg.#   Page number|

     In this example, to respond to Smith's message, log on to page 475 
enter the bulletin board and set CAT 6.  Enter your REPly in TOPic 1.

     A message number that is surrounded by brackets indicates that this 
message is a "target" message and is referring to a "chain" of two or more 
messages that are following the same topic.  For example:  {58}.

ABOUT GEnie   GEnie's monthly fee is $8.95 which gives you up to four hours
"""""""""""   of non-prime time access to most GEnie services, such as 
software downloads, bulletin boards, GE Mail, an Internet mail gateway, and 
chat lines.  GEnie's non-prime time connect rate is $2.00.  To sign up for 
GEnie service, call (with modem) 1-800-638-8369 in the USA or 
1-800-387-8330 in Canada.  Wait for the U#= prompt.  Type:  JOINGENIE and 
hit RETURN.  When you get the prompt asking for the signup/offer code, 
type:  DSD524 and hit RETURN.  The system will then prompt you for your 
information.  Need more information?  Call GEnie's customer service line 
(voice) at 1-800-638-9636.

SPECIAL OFFER FOR GEnieLamp READERS!   If you sign onto GEnie using the
""""""""""""""""""""""""""""""""""""   method outlined above you will 
receive $50.00 worth of credit.  Want more?  Your first month charge of 
$8.95 will be waived!  Now there are no excuses!

GET GEnieLamp ON THE NET!   Now you can get your GEnieLamp issues from
"""""""""""""""""""""""""   the Internet.  If you use a web browser, 
connect to "gopher://gopher.genie.com/11/magazines".  When using a gopher 
program, connect to "gopher.genie.com" and then choose item 7 (Magazines 
and Newsletters from GEnie's RoundTables).

                        *** GET INTO THE LAMP! ***
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""



           //////////////////////////////////////// GEnie_QWIK_QUOTE ////
          / Remembered an interesting quote this afternoon: "Of course /
         /  everything has already been said.  But since no one was   /
        /   listening, we must begin again."                         /
       ///////////////////////////////////////////  STRACZYNSKI  ////



[EOA]
[FRM]//////////////////////////////
                 FROM MY DESKTOP /
/////////////////////////////////
Notes From The Editor
"""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



     Apple Computer stopped making its last Apple II in 1983.  As I 
pointed out last year this time, 1994 was still not the year the Apple II 
died.  Neither was 1995.

     In fact, we made out like bandits in 1995.  We got new hardware:  the 
SecondSight card and, along with the rest of the micro world, IOmega ZIP 
drives.  We got new software:  Quick Click Morph, TimeOut Statistics, 
Convert 3200, Quick Click TIFF Reader, Deja II (AppleWorks 5.1 for the 
Mac), Opening Line, TouchTwo AppleWorks macros, Print 3200, and PMPFax, not 
to mention shareware/freeware efforts such as II Not Disturb, Blockade 
(game from Brutal Deluxe), and Pix Whiz (New Print Shop color pix editor).

     We also got significant updates and upgrades for some of our 
software:  Spectrum v2.0, Balloon v2.0, AppleWorks v5.1, rSounder v3.0, 
AutoArk v1.1, TimeOut ShrinkIt v5, One Touch Commands 5, GEnie Master 5, 
CoPilot for GEnie v2.5.5, The Tinies (with a new construction set), an 
improved variable-time SHR screen saver, and a patch for the HFS FST.

     Sure, it wasn't all good news.  There were some new products we 
haven't gotten yet--the TurboRez card, Wolfenstein 3-D, SimCity, Tracer 
Sanction, Mind Shadow--and some updates that still haven't arrived: 
GraphicWriter III v2.0, Kangaroo, TransProg III, SuperConvert, and a 
decidedly unofficial System 6.0.2 from Brutal Deluxe.  We even suffered a 
few losses in 1995:  Your Money Matters and free Apple IIc motherboard/ROM 
upgrades, to name just two.

     Our biggest losses in 1995 were our magazines and newsletters:  
A2-Central, TimeOut-Central, Script-Central, Studio City, GS+ Magazine, 
Softdisk (but not Softdisk G-S), and AppleWorks Forum all ceased 
publication.  In March/April 1995, we were told that II Alive "is not going 
quarterly any time soon".  As of December, it seems that this is exactly 
what's going to happen.  There are about four more issues to come, so II 
Alive will probably finish in late 1996 or early 1997.

     So we lost ICON (International Computer Owners Network) and NAUG 
(National AppleWorks Users Group)... Softdisk Publishing and EGO Systems 
are still with us, not to mention Shareware Solutions II and PAUG 
(Planetary Apple User Group).  Two replacement newsletters have come along:  
Apple Blossom by Steve Cavanaugh and II Something by Clark Stiles, both of 
which are distributed freely.  Thanks to the efforts of Joe Kohn and Will 
Nelken, Script-Central back issues and TimeOut-Central back issues are 
again available.

     (It's encouraging to see new Apple II magazines being announced, but 
I was also cheered by the news that Steve Disbrow, formerly editor of GS+, 
is going to be writing for Shareware Solutions II.  Small one-person 
newsletters are great, but I'd like to see a group of people get together 
to produce a larger magazine.)

     The HyperCard books came back in print, thanks to Mike Westerfield of 
the Byte Works, and ABC Direct is supposedly selling AE-like hardware.  In 
1995, we enjoyed not only KansasFest 1995, but also a videotape of same for 
those of us too poor to make it to Kansas City.  Auri Rahimzadeh published 
the Woz issue of PowerGS.  GEnie continued to be the true home of the Apple 
II community, with over 1700 uploads in 1995.  The Golden Orchard CD-ROM 
finally shipped.

     What's coming in 1996?  Two newsletters have been announced:  
AppleWorks Gazette from Howard Katz and Chris Serreau, and Juiced.GS from 
Max Jones.  It looks as though we might finally get Wolfenstein 3-D, now 
that another programmer has taken over Burger Bill's task.  Joe Kohn has 
promised Studio City back issues for sometime this year, as well as his 
expose on the ROM 4 Apple IIgs.  Tony "Hexman" Morales is continuing work 
on his "Hindenburg" Apple IIgs emulation project.

     Okay, so the future isn't so bright that we gotta wear shades.  And 
we don't want to don the rose-colored specs either, because fool's 
paradises just aren't our style.  But it's still true, my friends, that on 
a clear day, you can see forever.

     Happy 1996.

                                 [*][*][*]


     This issue of GEnieLamp A2 is 5 days late.  My apologies for the 
inconvenience.  This is something that hasn't happened since I became 
editor.  Once I pressed the deadline just a shade, releasing the magazine 
in the PM of the first of the month, rather than early in the AM, but other 
than that, my record has been spotless.

     Since I took over the editorship of GEnieLamp A2, my parents have 
given my wife and I the magnificent present of airfare home for the 
Christmas holidays.  This great joy carries with it the knowledge that I 
have to work like the devil to finish the January issue of GEnieLamp A2 
before I leave my "home office" (generally 2-3 days before Christmas).  
I've done that two years in a row.  This year, the PAUG newsletter wasn't 
ready.  I toyed with the idea of publishing the January 1996 issue without 
the column, but I didn't want to run without PAUG two months in a row, and 
I knew that the issue would run too short.

     It broke my heart to do it--and put a small damper on my Christmas 
vacation--but I decided I had to postpone publication until my return.  I'm 
back now, and I crave your kind indulgence for the delay.

-- Doug Cuff

GEnie Mail:  EDITOR.A2                       Internet:  editor.a2@genie.com



        __________________________________________________________
       |                                                          |
       |                   REPRINTING GEnieLamp                   |
       |                                                          |
       |   If you want to reprint any part of GEnieLamp, or       |
       |   post it to a bulletin board, please see the very end   |
       |   of this file for instructions and limitations.         |
       |__________________________________________________________|



                                                           ASCII ART BEGINS

     _____ ______       _      _                              ___  ___  
    / ____|  ____|     (_)    | |                            / _ \|__ \ 
   | |  __| |__   _ __  _  ___| |     __ _ _ __ ___  _ __   | |_| |  ) |
   | | |_ |  __| | '_ \| |/ _ \ |    / _` | '_ ` _ \| '_ \  |  _  | / / 
   | |__| | |____| | | | |  __/ |___| (_| | | | | | | |_) | | | | |/ /_ 
    \_____|______|_| |_|_|\___|______\__,_|_| |_| |_| .__/  |_| |_|____|
                                                    | |                 
                                                    |_|                 

                                                             ASCII ART ENDS


[EOA]
[HEY]//////////////////////////////
              HEY MISTER POSTMAN /
/////////////////////////////////
Is That A Letter For Me?
""""""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]

        o A2 POT-POURRI

                o HOT TOPICS

                        o WHAT'S NEW

                             o THROUGH THE GRAPEVINE

                                  o MESSAGE SPOTLIGHT



                           >>> A2 POT-POURRI <<<
                           """""""""""""""""""""

DEPT. OF NICE GUYS FINISH LAST   Did anyone notice the November 29 initial 
""""""""""""""""""""""""""""""   public stock offering of a company called 
Pixar, Inc?  That's the company that did all the computer animation for the 
new Disney movie, TOY STORY.  The stock was initially offered at $22/share, 
it opened at $47, and closed at $39.  What's my point?  Well, it just so 
happens that Steven Jobs heads the company and retains 80% of the 37.4 
million outstanding stock shares.  On paper, that makes him a billionaire.

 - Tony
                   (A2.TONY, CAT2, TOP7, MSG:67/M645;1)


PRINT 3200 BUG FIX   A week or so ago, I uploaded a program named Print 
""""""""""""""""""   3200.  Since then, a relatively minor (but 
disconcerting) bug has surfaced.

     Launching Print 3200 updates your hidden Desktop file.  IF you have 
your Finder set up to view items by anything other than Icons, you will 
notice that several filetype designations will appear to be blank.

     I just spoke to John Wrenholt, and he figured out what was causing 
the problem, and I was able to come up with a "quick and dirty" fix for it.

     However, since the fix involves using Foundation and not everyone has 
Foundation, I've asked John to release a Print 3200 v1.0.1 (or v1.1) after 
he's had a chance to confirm that the quick fix didn't break something 
else.  Also, we both agreed that it would be a real good idea to change the 
version number of Print 3200.

     Here's the "quick and dirty" fix...

     In Print 3200, there is a "dummy" rBundle.  It's not really there for 
any particular reason, but it is the cause of the problem as the "dummy" 
file is not quite empty.

     So, fire up Foundation, load Print 3200, and use the Hex Editor to 
enter 00 for the 5 or 6 bytes that display something other than 00.  Save 
Print 3200 back to disk, and it'll be OK.

     You will also have to delete your Desktop file.

     For those of you who don't own Foundation, look for Print 3200 v1.0.1 
in the next couple of days.

     We're hoping (and I'll test this in a little bit) that if we also 
change the rVersion number, that the Desktop file will be updated and that 
it will "fix itself."

Joe Kohn
                  (JOE.KOHN, CAT28, TOP4, MSG:306/M645;1)


LIFE UNFAIR IN OUR FAVOR FOR ONCE!   You're going to love this.
""""""""""""""""""""""""""""""""""

     In the current January issue of _Details_ magazine, there is an 
article on pages 52-53 about the evolution of the media industry along with 
technology.  At the bottom of the spread is a timeline which begins in the 
70's and proceeds through the next two-and-a-half decades.

     So I'm scanning this timeline and I see "Apple II Computer" in and 
about the '77 range.  Good.  They didn't forget.

     Passing other techno blurbs like "Sony Walkman" and "Gates / Allen 
MS-DOS," I continue and see "IBM PC" with a little picture of a vintage PC.  
Harumph!  It was unfair that Big Blue merited a picture of its progeny 
while the Apple II only received a mere mention.

     I keep reading and come to the "Apple Macintosh" with its own little 
picture.  Except that the picture is not of a Mac, but of a IIgs!  It's the 
old, familiar publicity photo of a Woz GS with the chemical factory screen!  
Ain't that a kick?

     Is it subversion or is it an accident?  Who knows?  But I got a good 
laugh out of it because despite everything Apple Computer has done to 
distance itself from the image of its once successful Apple II line, the 
Apple II just won't go away :)

Michael
                  (ANIMASIA, CAT2, TOP7, MSG:105/M645;1)


TURBO ASB FIX   Thanks to Ron Higgins, I found out why I was having trouble 
"""""""""""""   running the Turbo ASB at the Ext./16 baud rate.  I had 
SW1-1 UP which gives 230,400 bps but my modem (most modems other than the 
Hayes Optima) can only go 115,200 bps.  D'OH!  I could have slapped myself 
for not figuring that one out.

     Now I've got SW1-2 UP instead and I'm flying at 115,200 bps.  It 
works great for file transfers, but ProTERM's text display cannot keep up 
with speeds like that so I end up with some messy looking screens (sort of 
like using Spectrum SHR mode at high baud rates.)  Apparently, ProTERM's 
top limit (at least on my Zip 8/64 GS) is 57,600 bps.  Anything faster than 
that and the screen display can't keep up.

     I'll probably end up using 38,400 bps for standard telecom and I'll 
hike it up to 115,400 bps for file transfers.  Man, this thing is a speed 
demon!  I only wish you could setup the Turbo ASB to provide 57,600 and 
115,200 bps simultaneously.  But I can live with 38,400.

 - Tony

                  (A2.TONY, CAT12, TOP39, MSG:26/M645;1)


                            >>> HOT TOPICS <<<
                            """"""""""""""""""

PATCH FOR HFS FILE SYSTEM TRANSLATOR (FST)   HFS Patcher
""""""""""""""""""""""""""""""""""""""""""   
Copyright 1995, Procyon Enterprises Inc.

Permission hereby granted to distribute this package far and wide, 
including in SoftDisk and other publications, so long as only the original 
archive (or files) is transmitted unmodified.

Written by Jawaid Bazyar because Dave Lyons didn't have time to write it.

The Bug   The HFS FST (Macintosh HFS File System Translator) that ships
'''''''   with the IIGS System 6.0.1 has a simple but catastrophic bug in 
it.  There is an error in a "multiply two numbers" subroutine.  This 
subroutine is called by the part of the FST that determines where on a disk 
device a "logical block" resides.

     Much like ProDOS, the HFS file system can only have 65536 blocks on a 
disk device.  Unlike ProDOS, however, HFS can have varying "logical block" 
sizes - anywhere from 512 bytes (like on a floppy disk) to 10K or more (on 
very large hard disk drives).  Thus, even though there can only be 65536 
blocks on a disk, large hard disks can still be used by making each of 
those blocks bigger.  When HFS goes to read or write to a hard disk, it 
must determine a physical block number, given the logical block size and 
HFS block number.  The routine that does this in the IIGS' HFS FST is the 
buggy routine mentioned above.

     This bug can/will cause random trashing of HFS volumes on a GS, and 
incorrect reading of HFS volumes created on a Mac (such as CD-ROMs, hard 
disks, optical disks, any large media).  This is almost certainly the bug 
that caused Jim Maricondo great amounts of grief and delayed the Golden 
Orchard CD by some months, and this was causing many files on the Compton's 
1995 CD disc to be read as garbage.

     A single byte change will correct the bug.  To fix the bug, as well 
as change the version numbers and modification date of the FST, I have 
provided a simple patch program.  Simply run this program, and it will 
automatically patch the file HFS.FST that resides in your system folder.  
You need only run the patch program once -- the patch is permanent, but be 
careful not to overwrite the patched version with an old one from your 
System Disks or elsewhere.  The Modification date of the patched FST will 
be 4-OCT-95, 12:00am

     Many, many, many, many thanks (thousands!) go to Dave Lyons, who took 
his own time to track down and correct this bug. I'll quote from his email 
to me:

-- begin quote --

There is an INC A instruction at offset +$2CE0 in the FST, in RAM. (I 
believe it's easily patchable on disk, as well.)  This INC A ($1A) should 
be changed to a NOP ($EA).  -- I simulated all 8 billion cases on a Power 
Mac (4 billion with the INC, 4 billion with the NOP), and the NOP is 
correct. (Bad algorithm comes from "65816/65802 Assembly Language 
Programming" my Michael Fischer, by the way, page 353.)

So...if someone were to write a little patcher program to fix the FST on 
disk, I would suggest the following, in addition to the INC-->NOP:

(These are in-RAM offsets.)

+$0044 change $31 to $32 (changes string "v01.01" to "v01.02"
+$0010 change $01 to $02 (changes binary version number from $0101 to 
$0102)
+$005A change $32 to $35 (changes copyright date)

And finally, change the file's last-mod date to 12:00am (midnight), 
4-OCT-95, just for consistency (the version number isn't displayed to the 
user, so they deserve a way to see if they have the fix).

-- end quote --

     The provided patch program (PatchHFS) does all of the above as Dave 
suggests.

     Three Cheers to Dave Lyons for helping our IIGS's be a little bit 
more reliable than they already are!

Jawaid Bazyar
bazyar@hypermall.com

Interlink Advertising Services
Affordable & Professional Web Site Services
PO Box 641
Englewood, CO 80151-0641
(303) 781-3273 

=================

     The above is the PatchHFS.Docs file out of the archive.  I'll be 
uploading it to the A2 library shortly.

Dave
                  (JUST.DAVE, CAT9, TOP7, MSG:118/M645;1)

>>>>>   Does anyone know if patching the HFS FST after a disk has been in
"""""   use with the original FST will screw up any data?  Or if the disk 
is still good, it will continue to be good?

 Joat

                   (A2.TIM, CAT9, TOP7, MSG:128/M645;1)

>>>>>   ATTENTION ALL IIgs HFS USERS!!
"""""

     A =VERY= good question was recently asked here:

> Does anyone know if patching the HFS FST after a disk has been in use
> with the original FST will screw up any data? Or if the disk is still
> good, it will continue to be good?

     From my snOOping of things, I am now fully convinced that it is 
entirely possible, infact highly probable, that an HFS volume that 
-appeared- stable and trouble free prior to the patch will have -severe- 
problems -after- installing the patch.

     This is =NOT= due to any flaw in the patch, or any undiscovered bugs 
in the HFS.FST, and infact should have been totally predictable!

     The problem is that the patch fixes a flawed logical -> physical 
block number conversion routine in the FST, and it was the originally 
flawed results of this routine that were used to write any existing files 
onto an HFS volume.  It should be fairly obvious that if you use a flawed 
routine to do this calculation, and then later use a fixed routine, chances 
are great that you won't wind up accessing the same physical block on a 
volume.  The results of this can easilly be a trashed volume.

     I would =STRONGLY= urge that the following procedure be followed in 
ALL cases, =regardless= of HFS volume size, simply to be on the safe side:

1) Make a FILE-BY-FILE backup of all your existing HFS volumes, regardless 
   of volume size BEFORE applying the patch. (GSHK can do this, saving the 
   resultant archive onto a ProDos volume. Works fine for me)
2) Apply the patch to the HFS.FST
3) Reboot the machine (needed to bring the patched FST into memory)
4) Using the Finder, either Erase or Initialize all of your HFS volumes 
   (critical step, this 'nukes' any existing, incorrect information!!)
5) Restore your files from the backups made in step one.

Note: Do =NOT= try to get away with using an "image" type backup! Those 
      backups will restore the =original= and likely flawed directory 
      structures to the reformatted HFS volume, leaving you =EXACTLY= where 
      you would have been had you not done the above series of steps!!!

     This should provide you with a properly functioning HFS volume, 
assuming that no further bugs in the HFS.FST are discovered.

-Harold
Resident Solder Slinger
                  (HAROLD.H, CAT9, TOP7, MSG:137/M645;1)

HFS PATCH AND GOLDEN ORCHARD CD   Jim has been saying on comp.sys.apple2 
"""""""""""""""""""""""""""""""   lately that the patched FST will let him 
create a new, one partition CD with much less hassle and a lower price.  He 
is planning on setting up an ftp site in January so people can send him 
files for the new CD.

     He also mentioned that he had "rescued" the best of what is in the 
AOL Apple II library (about 210 MB compressed).  More details as they 
arrive. 
                (S.CAVANAUGH1, CAT9, TOP7, MSG:181/M645;1)


INITIAL SECONDVIEW PRO REPORT   SecondViewPro's interface is apparently 
"""""""""""""""""""""""""""""   identical to SecondView's, which can best 
be described as "minimalist".

     I have had good luck getting SVP to deal with JPEGs, GIFs, 3200s (at 
last!), Targas (the sample ones), AST Vision Plus raw files, fair (no bit 
depth whatsoever) results with PICTs, and no luck with TIFFs.

     I will continue doing some testing throughout the next few days.

Ryan
                (R.SUENAGA1, CAT20, TOP14, MSG:116/M645;1)

<<<<<   Well, after much deliberation, I believe that the JPEG viewing with
"""""   SV Pro is indeed 24 bit. :)

     I also did some comparison between the AST Vision Plus raw file 
loading with the SV Pro vs. SuperConvert software, and SV Pro is _much_ 
more realistic :)

     More to come. . .

Ryan
Check out the Late Nite Luau
Late Friday nights and early Saturday mornings at 645;2!
ANSITerm and CoPilot v2.55
                (R.SUENAGA1, CAT20, TOP14, MSG:123/M645;1)


THOUGHTS ABOUT A WEB BROWSER FOR THE IIGS   > ...I don't know of any 
"""""""""""""""""""""""""""""""""""""""""   > programmer writing a web
> browser...

     Me either, so don't start rumors.

     One of the problems with an Apple II web browser is that it takes two 
specific kinds of talent/knowledge base to do it:  comm skills and language 
skills.

     I've been dabling with the idea of writing an HTML interpreter subset 
for another project that may or may not ever get done.  The subset could 
easily become a full implementation.

     If that interests any comm types, get in touch with me.

Mike Westerfield
                  (BYTEWORKS, CAT28, TOP4, MSG:42/M645;1)

>>>>>   It was leaked to me that a graphical web browser -WAS- indeed being
"""""   explored for the IIgs.  This from a reliable programmer who would 
neither confirm nor deny that he had any direct connection to the progect.  
Why such secrecy is beyond me.  It would seem that an accelerator would be 
a must for it to work as most graphic programs take a good deal of time to 
convert graphic images on the GS anyway.  Any way that is the rumor mill at 
work.  Anybody heard anything current?

 Dave
                  (D.HAND1, CAT10, TOP15, MSG:17/M645;1)

>>>>>   I think a GS Web Browser will be =crucial= to the =future= of the
"""""   IIGS.  One of the major directions of computing is information 
retrieval, such as the Internet's WWW.  Lynx is nice (and fast) for text 
but with increasing emphasis on graphical web pages, you can see what will 
happen to the IIGS if someone doesn't come up with graphic capability real 
soon.  Not only for browsing but also for web page creation.  Even kids in 
school are putting up their own pages on the web.  There's an outfit "out 
there" that will host your web page for only $5 a month.  Can the software 
for this be all THAT difficult to write?

 - Joachim

 ][ Everyone who needs a graphical browser for the GS, raise your hand! ][
                 (J.NELSON56, CAT10, TOP15, MSG:18/M645;1)

>>>>>   > This from a reliable programmer who would neither confirm nor
"""""   > deny that he had any direct connection to the progect.  Why such
        > secrecy is beyond me.

     Because we have found (applies to us hardware types with new 
developments too) that once we let it get out to public knowledge that "I'm 
working on a new whatsit" then we get no peace.  Whoever the programmer is 
that is thinking about the project doesn't want to have 90% of their 
available time taken up answering questions, and making excuses.. they may 
not even be sure the project will be successful, and don't want to 
disappoint people, or damage their own reputation by saying "I'm writing a 
new whatsit -- (which gets folks all excited) -- followed 6 months later by 
"I couldn't do whatsit".

     Been there :(, so I understand the feeling.

Doug Pendleton, Apple II hardware help
A2 Promotions and Public Relations Manager
Delivered by: ProTerm 3.1 and CoPilot v2.5.5 Offline GEnie Message Manager
Internet: Doug.P@GEnie.com
                   (DOUG.P, CAT10, TOP15, MSG:19/M645;1)

>>>>>   For anyone who is seriously considering writing a web browser for
"""""   the IIGS...

     Brutal Deluxe's Convert 3200 is, without a doubt, the fastest GIF 
converter/viewer ever released for the Apple IIGS.  The source code for 
Convert 3200 is going to be available any day now, and I _think_ that 
Brutal Deluxe would be willing to license that source code for use with a 
commercial IIGS web browser.

     Obviously, I am not authorized to speak for Brutal Deluxe, but as the 
world wide distributor for Convert 3200, let me just say that I have some 
influence with them.

     BTW, the source code for Convert 3200 is Merlin 16+.

     Because of the French postal strike, I haven't yet actually seen it, 
but Brutal Deluxe told me just a week or two ago that they were going to 
fully comment it before releasing it.

     Just a thought....

Joe Kohn
                  (JOE.KOHN, CAT10, TOP15, MSG:51/M645;1)

>>>>>   The major stumbling block of a GS graphical web browser is speed.
"""""   A 28,800 bps modem is an absolute must, but the real slowdown would 
be decompressing and displaying the graphics.  It's easier (and faster) 
with a Second Sight card because the graphics do not have to be "converted" 
for display on the standard GS screen, but it would still be slow.  GIFs 
wouldn't be too bad unless they're really big, but JPGs take a long time to 
decompress even on a fast GS.  Heck, even Mac and PC power users often turn 
off the graphics because they're so slow.

     That said, I'd still pay any reasonable amount of money for a GS 
graphical web browser.  GNO/ME 2.0.6 (I think Mike incorrectly said 1.0.6) 
will have the ability to do TCP/IP, which has been written by a third party 
and is supposed to be available shortly.  SLIP should also be available 
soon.  All the parts are there, someone just has to write the browser.

 - Tony
                  (A2.TONY, CAT10, TOP15, MSG:52/M645;1)


                            >>> WHAT'S NEW <<<
                            """"""""""""""""""

BRUTAL DELUXE'S CONVERT 3200   Since bursting onto the IIGS scene in 1992, 
""""""""""""""""""""""""""""   the French programming team Brutal Deluxe 
has thrilled the IIGS community with one smash hit freeware game after 
another.  In the past 3 years, Brutal Deluxe has excited, delighted and 
enchanted the Apple IIGS community with such wonderful instant-classics as 
The Tinies, Cogito, Blockade, Opale Demo and The Tinies Construction Kit.

     Not content to rest on their laurels, Brutal Deluxe, in association 
with Shareware Solutions II, is pleased to announce the release of their 
very first commercial software product for the Apple IIGS.

     Under development for 2+ years, Convert 3200 was used by Brutal 
Deluxe as their in-house graphics conversion utility to help port graphics 
to all of their freeware games.  Now sporting a user-friendly interface 
similar to that found in Brutal Deluxe's games, Convert 3200 is a fun to 
use program that contains amusing Easter Eggs and sophisticated machine 
language algorithms that makes it the fastest graphics conversion program 
ever released for the Apple IIGS.  As an example of its efficiency, Convert 
3200 can convert a large 1280 * 800 GIF graphic and scale it down to a 
Super Hi Res 320 * 200 format using 16, 256 or 3200 colors in 15 seconds or 
less!  Conversions of foreign format graphics into the standard 16 color 
format available in the 320 * 200 mode can be accomplished in as little as 
2 seconds.

     Convert 3200 can load in, display and convert the following graphic 
formats:

Apple IIGS formats:
'''''''''''''''''''
- DYA               3200 colors              320*200
- PaintWorks Gold        16, 256 colors           up to 640*400
- PackBytes         16, 256, 3200 colors          up to 640*200
- ApplePreferredFormat   16, 256, 3200+ colors         limit of 1280*800
- FrenchFormats (APP, NRL) 3200 colors            320*200
- Unpacked screen        16, 256, 3200 colors          up to 640*200

Foreign Formats:
''''''''''''''''
- Windows-OS/2 BMP  up to 256 colors         limit of 1280*800
                   (compressed or uncompressed)
- Compuserve GIF         up to 256 colors         limit of 1280*800
- Amiga .IFF/ PC .LBM    up to 256 colors         limit of 1280*800
               (compressed or uncompressed)
- Paintbrush PCX    up to 256 colors         limit of 1280*800
- TIFF(with restrictions)     up to 256 colors         limit of 1280*800
               (MacPackBits compressed or uncompressed)
- Binary PC         256 colors          limit of 1280*800


     Once converted, Convert 3200 can save files in the following formats:

- ApplePreferredFormat   16, 256, 3200 colors
- Windows BMP            256 colors
- Paintbrush PCX         256 colors
- TIFF                   256 colors
- Binary PC              256 colors
- Print Shop GS          Standard PSGS

     Convert 3200 is incredibly fast.  While Prism takes 2 minutes and 35 
seconds to convert an IFF graphic into 3200 color mode, Convert 3200 takes 
just 15 seconds.  Additionally, Convert 3200 allows you to load in all 
types of foreign format graphics such as PCX, IFF, GIF, TIFF, BMP and it 
even allows you to save your pictures in foreign formats including PCX, 
TIFF, and BMP.

     Convert 3200 is very flexible and easy to use.  There is only one 
convert button and no complicated options; however, Convert 3200 allows you 
to select your 320 * 200 area as you want, and it provides power users with 
the ability to modify RGB tones, remove some colors and it even comes with 
an automated script conversion mode.  Best of all, its "convert all" option 
allows you to convert dozens of graphics at once, in an unattended mode.

     Convert 3200 only operates in the 320 * 200 mode, and does not 
support True Color graphics in the JPEG, TGA, QRT, or TIFF formats.  At the 
current time, it does not support the Second Sight VGA Card.

     Convert 3200 is currently available from Shareware Solutions II for 
the low price of $15, which includes postage and shipping/handling to 
anywhere in the world.  The complete Merlin 16+ source code is also 
available to owners of Convert 3200 for an additional $5.

     Convert 3200 is professional quality software that costs less than 
many shareware programs.  In an effort to make the pricing as low as 
possible, the complete and extensive documentation manual is supplied as a 
file on the disk.

     Despite its low cost, technical support is available 24 hours a day 
via Internet e-mail and if any updates are released, they will be available 
free of charge to all Convert 3200 owners.

     Convert 3200 requires a IIGS with 1.25 megabytes of RAM memory; 4 
megabytes of memory is recommended to convert large 1280 * 800 graphics.  
Convert 3200 has been tested on Apple IIGS ROM 01 and ROM 03 versions, and 
it operates under System 6.0.1.  Although not required, a hard disk drive 
and an accelerator card is recommended.

     To purchase Convert 3200, send checks or money orders, in US Funds 
only to:

          Joe Kohn
          Shareware Solutions II
          166 Alpine St
          San Rafael, CA 94901-1008
          USA

     Sorry, but no credit cards or school purchase orders can be accepted. 
 Please make all check payable to Joe Kohn.

     Apple II Forever!
                  (JOE.KOHN, CAT28, TOP4, MSG:298/M645;1)

<<<<<   I just wanted to mention that the Convert 3200 source code isn't
"""""   available yet.

     Olivier Zardini is currently writing an informational text file that 
he hopes will explain the overall design of the program and how all the 
various segments are linked.  As you'll see from the Convert 3200 program 
documentation, Olivier believes in being complete and going into as much 
detail as possible.

     Because Olivier's native language is French, I imagine that his text 
will need quite a bit of editing to make sense to English speakers.  As it 
turned out, I spent many days working on the Convert manual, and that was 
simply just to make it more readable in English.

     A second problem exists because I would prefer to distribute the 
source code disk from a master disk that is received via snail mail, as 
opposed to e-mail.  It's not that I don't trust e-mail, but I'd just feel a 
lot more secure distributing a disk that Brutal Deluxe mailed me.

     And, all of France seems to be on strike, including the mail service.

     So, for those of you who sent in $20 to receive the Convert 3200 
program and the source code, I'll get out the program disk to you today.  
When I receive the source code, I'll then mail a second disk to you.

     I hope that is acceptable to you.

Joe Kohn
                  (JOE.KOHN, CAT28, TOP4, MSG:324/M645;1)


TIMEOUT STATISTICS FOR APPLEWORKS   TO,Statistics is a disk with 3 TimeOut 
"""""""""""""""""""""""""""""""""   Applications.  Together they cover a 
full spectrum of Statistical Analysis.  Your data is stored in an AWKs Data 
Base.  From the Data Base you invoke the TimeOut menu the navigate thru 
some moe more menus to choose the function you wish to perform and define 
parameters to be used.  Function include:  Estimation, Hypothesis Testing 
(1 or 2 samples), Linear Regression, Independence, Analysis of Variance, 
Histgrams, Sign Test, Rank Sums, and Signed Rank.  The disk also includes 
many Statistical Tables in the form of AWKs Spreadsheet files.

     TO.About Time is a single TimeOut desk accessory for calculating the 
difference between to dates or the period between to times of day.  Date 
range is from Jan 1, 0100 AD to Dec 31, 9999.

     TO.Disk Tools is a set of TimeOut applications for backing up ProDOS 
volumes and/or files.  Also includes a Find-A-File function and a way to 
display to different AWP documents on a split screen so you can compare 
them.
                   (D.GUM, CAT13, TOP7, MSG:104/M645;1)


PMPFAX(TM) FACSIMILE SOFTWARE SHIPS!   December 12, 1995
""""""""""""""""""""""""""""""""""""

             PMPFax(tm) Facsimile Software for the Apple IIGS

                NOW AVAILABLE FROM PARKHURST MICRO PRODUCTS

NOTE:  This press release may be freely distributed, but not altered in any 
       way.

     Parkhurst Micro Products is proud to announce PMPFax Facsimile 
Software for the Apple IIGS!  PMPFax turns your Apple IIGS into an 
integrated fax machine, capable of sending and receiving faxes in any 
desktop application!  PMPFax gives you the power and flexibility in fax 
software that you've been waiting for!

FEATURES INCLUDE:
'''''''''''''''''

o   Send faxes in any desktop application that supports printing.
o   Create faxes using the Page Setup environment for your printer, instead 
    of having to reformat your document for a special "fax printer."
o   Choose between printing and faxing your documents by simply holding 
    down the Option key when selecting "Print."
o   Schedule fax sends for any time and date, or for immediate faxing.
o   Save faxes for later transmission.
o   Add "printed" fax documents to existing faxes, or create a new fax.
o   User-definable fax page titles for identifying fax pages, includes 
    system variables in titles.
o   Fax to multiple locations in a single session.
o   Change the quality and speed of fax sends.
o   Receive faxes in any desktop application.
o   Automatically print received faxes when they are received.
o   View faxes in any desktop application with four scaling factors and 
    page-flip.
o   User-definable cover sheets with full page-layout capability, allowing 
    text, shapes, and imported pictures and over 15 available system 
    variables (including a memo).
o   Multiple View and Cover Sheet windows can be open at the same time.
o   Forward received faxes to send them again.
o   Supports TrueType fonts in both fax pages and cover sheets.
o   Quick Fax support for faxing a quick message.
o   Multiple phone book support, including individual entries and groups.
o   All faxes can be individually named.
o   Fax pages can be renamed, copied, and deleted.
o   Plug-in fax page translators allow open-ended support for importing and 
    exporting fax pages, future support for scanners and input devices, and 
    expandability through third-party vendors.
o   Detailed logs of all fax activity, including printed or text-file fax 
    activity reports.
o   Access the FaxCenter through a convenient NDA, or through a seperate 
    application.
o   Support for the Apple IIGS modem port, printer port, and Super Serial 
    Card.
o   Support for Class 1, 2, and 2.0 fax modems.
o   Auto-selection of fax Class, or choose the Class yourself.

     And much more!

SYSTEM REQUIREMENTS:   PMPFax requires an Apple IIGS with about 1.5 
''''''''''''''''''''   megabytes of free RAM available (4 megabytes of RAM 
or more recommended), a hard drive, and a fax-compatible modem.  
Accelerator such as a ZipGS or TransWarp GS highly recommended.

PRICING   PMPFax is only $89 direct from Parkhurst Micro Products.  Please 
'''''''   add $3 for shipping within the United States, $8 outside the 
United States.  California residents, please add state and local sales tax.

                           *** SPECIAL OFFER ***

     If you have purchased another Apple IIGS fax software package, you 
can purchase PMPFax for 50% off!  Send a COPY of the invoice you received 
with your other fax program, or a COPY of your diskette and the first page 
of your manual to Parkhurst Micro Products to receive PMPFax for only $45 
plus shipping!  This offer expires March 1, 1996.


ORDERING INFORMATION:   To order with credit card (Visa or MasterCard), 
'''''''''''''''''''''   call:

                              (510) 837-9098.

             Or send check or money order for total amount to:

                    Parkhurst Micro Products
                    2491 San Ramon Valley Blvd
                    Suite 1-317
                    San Ramon, CA, 94583.

     Please allow two weeks for delivery.  Payment must must be made in US 
funds drawn on a US bank. 

EMAIL:   If you wish to contact Parkhurst Micro Products electronically, 
''''''   you may use any of the following email addresses:

   Internet:  pmp@genie.com  - OR -
              pmp@delphi.com

   GEnie and Delphi:  PMP
                    (PMP, CAT38, TOP15, MSG:274/M645;1)

<<<<<   Btw, I will shortly be uploading some cover sheets for PMPFax here. 
"""""   PMPFax comes on 2 diskettes and they're both pretty packed, so I 
could only include 2 cover sheets with the system.  Once people start to 
use it, hopefully we'll get some more cover sheets uploaded :)

Paul

PS:  The first shipment goes out tomorrow, the 18th.
                    (PMP, CAT38, TOP15, MSG:277/M645;1)


TOUCH TWO APPLEWORKS ADD-ONS   Shipping now...!
""""""""""""""""""""""""""""   
                __________________________________________
               |\ ______________________________________ /|
               | |                                      | |
               | |               TouchTwo               | |
               | |  (a new set of AppleWorks add-ons)   | |
               | |______________________________________| |
               |/________________________________________\|

     Marin MacroWorks is pleased to offer TouchTwo, a collection of 
thirteen (13) TimeOut add-on utilities.  They include:

     Clipper -- Clip any portion of a word processor document to save as a 
                file (or append to an existing file) or send to a printer.

     ColumnCalc -- Sum an entire column or range of rows within a column in 
                   a data base, a spreadsheet, or a word processor file. 
                   Display the result and/or enter the result in any 
                   desktop file.

     DB Hilighter -- Visually highlight individual categories onscreen. 
                     Highlight follows the cursor.

     DB Marker -- Visually frame individual categories onscreen. Frame 
                  follows the cursor.

     Desktop File Mover -- Move files from one desktop to another.

     FlexiCalc -- Calculate only specific row(s) of an ASP - much faster 
                  for large spreadsheets!

     Hilighter Plus -- Like DB Hilighter, but adds auto-dialing of the 
                       highlighted number.

     MultiColumnDJ -- Print a word processor file in multiple columns (2, 
                      3, or 4) on DeskJet (sheet-fed) printer.

     MultiColumnIW -- Same as above for the ImageWriter (and other 
                      tractor-fed printers).

     NumConverter -- Convert numbers between decimal and hexadecimal. 
                     Display the result and/or enter in a desktop file.

     Print Months -- Enter the names of the 12 months, full or abbreviated, 
                     to rows or columns in a word processor, data base, or 
                     spreadsheet file.

     Report Transporter -- Copy data base reports between files.

     SS WordWrap -- Provide auto word wrap in spreadsheet columns.

     Each program's function is documented, with screen shots, where 
applicable, in AppleWorks files on the disk.

     TouchTwo is available in two versions:  one for AppleWorks 4 and one 
for AppleWorks 5.  You must specify which version you require when 
ordering.

     Product                                     Price  (Shipping included)
     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~     ~~~~~~~~~~~~~~~~~~~~~~~~~~
     TouchTwo (for AppleWorks 4)   3.5" disk           $12.95
     TouchTwo (for AppleWorks 5)   3.5" disk           $12.95

     Outside North America, please add $3.00 per disk order for airmail 
delivery.

     To order, please send prepayment (check or money order in U.S. funds) 
to:

          Marin MacroWorks
          1675 Grand Avenue, Suite 1
          San Rafael, CA  94901-2211

     To contact by phone, please call (415) 459-0845.
                 (W.NELKEN1, CAT13, TOP11, MSG:111/M645;1)


QUICK CLICK CALC 1.3
""""""""""""""""""""

 GS-21  Quick Click Calc                   $60
        Update from any earlier version    $10
        Update with international disk     $11

     Quick Click Calc is our spreadsheet and business graphics package for 
the Apple IIGS.  It's a full desktop program, featuring formatting and 
graphics features you won't find in any other Apple II spreadsheet, plus 
publish and subscribe for linking spreadsheets to one another.

     And unlike other spreadsheets, this one is still actively maintained. 
 Quick Click Calc 1.3 is now available.  Of course it fixes all bugs 
reported in earlier versions.  But it also includes speed improvements, 
more options for customizing how you work, and an international edition 
with versions localized for Germany and France!

     The complete program is just $60 plus shipping.  Updates are only 
$10, or $11 if you want the international version, which includes the 
complete English release plus the French and German versions.  The update 
price includes worldwide surface shipping.

     And you can even get the update free!  If you ordered Quick Click 
Calc after October 1st, 1995, all you have to do to get the update is ask 
for it.  If you order any other product (except another disk update) at the 
same time, we'll give any registered owner of Quick Click Calc a free 
update!

     To order, or to ask for more information or our latest catalog, get 
in touch with us at:

          Byte Works, Inc.
          8000 Wagon Mound Dr. NW
          Albuquerque, NM  87120

          AOL:          MikeW50
          GEnie:        ByteWorks
          Internet:     MikeW50@AOL.COM
          Phone:        (505) 898-8183
                 (BYTEWORKS, CAT45, TOP2, MSG:142/M645;1)

<<<<<   David, the snappyness is an interesting story--to me anyway--that I
"""""   won't bore all of you with.  The bottom line is that QCCalc isn't 
really any faster, I just found and fixed two bugs that were causing speed 
bottlenecks.  The result is a program that often feels a lot faster, even 
though the actual work is being done at the same speed.

Mike Westerfield
                 (BYTEWORKS, CAT45, TOP2, MSG:136/M645;1)


                       >>> THROUGH THE GRAPEVINE <<<
                       """""""""""""""""""""""""""""

NEW APPLEWORKS MAGAZINE   WELCOME to the AppleWorks Gazette, the newest 
"""""""""""""""""""""""   Apple // magazine around.  Here, while we'll 
focus on AppleWorks and what you can do with it, we'll also offer you other 
information pertinent to your use of your Apple.  Of course, all material, 
where feasible, will be placed in AppleWorks format.

     AppleWorks is still one of the best written and supported integrated 
programs on the computer market.  Its combination of power plus ease of use 
is definitely not easily found on any of the other platforms.

     The demise of ICON's TimeOut Central and the National AppleWorks 
Users Group left a void that we plan on filling with a disk-based, 
bimonthly publication about AppleWorks and the Apple II world.  AppleWorks 
has had an almost mythical following.  Its allure has been hard for its 
fans to define, and we're not about to try to do that here, ourselves, 
since we also have trouble defining ineffable terms.  Suffice it to say 
that we enjoy using AppleWorks and want to continue helping others do the 
same, in a manner similar to the ease of use many of us became used to with 
the Forum.

     We are no trying to continue the work of NAUG; neither shall we try 
to become another TimeOut Central.  The Gazette's formation was to help all 
of us from going thru withdrawal without an AppleWorks publication.  
Typical columns will include news about the latest AppleWorks events, 
reviews of Apple II hardware, views of the program internal structure, tips 
and techniques in programming, and reports about possible uses of 
AppleWorks.  A special column for newcomers in the Apple II and AppleWorks 
world, and letters from readers are available.  The AppleWorks Gazette also 
welcomes notes, articles, and software submitted by readers.  The software 
section will include the best public-domain, freeware, and shareware 
AppleWorks-related programs.  We hope to have both familiar names and new 
authors in the Gazette bringing you the information necessary to continue 
using your Apple // for many years to come.

     The Apple // has proven itself to be a solid platform system, and 
capable of handling most if not all of your computing needs.  AppleWorks, 
of course, figures heavily into most of these needs for a great many of us.  
With the release of Deja ][, AppleWorks enters a whole new era -- that of 
truly portable AppleWorks usage.  Since Deja ][ is designed to run 
AppleWorks 5.1 on a Macintosh, including the PowerBooks; we promise that we 
won't mention the Mac unless it has to do with Deja ][ (well, we'll do our 
best!)

     There's a lot of life left in the Apple //, and we promise to help 
you get the most out of it.  We hope that you're planning on being in this 
with us for the long run.

     The AppleWorks Gazette is here to help the promise once made by Apple 
Computer Corp., but since forgotten by them:

     Apple ][ forever! ---- Send mail to Chris Serreau (100316.14) or 
Howard Katz (h.katz@genie.com) for details on how to subscribe.
                   (H.KATZ, CAT17, TOP18, MSG:1/M645;1)

>>>>>   Why not post here?   Is it a secret?
"""""

.Keith
                  (K.SAGALOW, CAT17, TOP18, MSG:2/M645;1)

<<<<<   No secret, of course.  We're offering a discount to people who
"""""   order prior to Jan. 10th, and after that the subscription price 
will be $35.00.  The discount will also depend on whether or not you were a 
NAUG member.  Rather than list a menu of prices, we figured it'd be easier 
(and so far it has been) to email the information to those who express an 
interest.  

     And if I can get my reviews done this weekend, the disk WILL be on 
time for the Jan. 20th mailing!  :)

Later............Howard
                   (H.KATZ, CAT17, TOP18, MSG:3/M645;1)


NEW APPLE IIGS NEWSLETTER!   Hello all, and Happy New Year!
""""""""""""""""""""""""""

     Discussions about the creation of Juiced.GS, an Apple IIGS-specific 
newsletter, have moved to this new topic.

     Feel free to drop in, ask questions, or otherwise offer comments 
about what you'd like to see included in this newsletter.

     Work on Volume 1, Issue 1 is well under way and is scheduled for 
release on or around Feb. 15.

     Plenty more info on Juiced.GS will be released in this topic in 
coming days.

     Stay tuned ...

Max Jones
Editor and publisher
Juiced.GS
                 (M.JONES145, CAT13, TOP43, MSG:1/M645;1)


QUICKDRAW II SUPPORT FOR THE SECONDSIGHT CARD?   > All I know is that it's 
""""""""""""""""""""""""""""""""""""""""""""""   > being worked on, albeit
> slowly, by at least one person (Mike Hacket, if I'm not mistaken.)

     Hey!  I'm working as fast as I can! :-)  I'm as anxious to see it 
finished as the rest of you.  Probably more so, since I've been working on 
it for so long!  (Oh, and BTW, that's two 't's in Hackett.)

     I'm not comfortable giving time estimates that I'm not sure I can 
keep, but I have been giving it my full attention, with the exception of 
the past week in which I have been working on finishing off another project 
(non-II related).  The delay is largely due to my very careful testing 
process and several design refinements.  (It's not just a QuickDraw/toolbox 
patch program, it offers an API for programs to break past QD's limitations 
as well.)

     All I can say is, I hope it is worth the wait.
                 (M.HACKETT, CAT20, TOP14, MSG:138/M645;1)


WOLFENSTEIN 3-D NOT DEAD YET!   For all who ordered Wolfen 3D last year, 
"""""""""""""""""""""""""""""   and those who haven't.  I got a call from 
Vitesse today to verify that I still wanted it!  Well my answer was an 
enthusiastic yes.  I asked her if anyone had said no, and Suzanne replied, 
why no!  For all who have not ordered yet, it looks like it's time!  $39.95 
plus $4.00 shipping.  She said to expect receipt in January.

     BTW, I saw Burger Bill demo this at Kansas Fest '95 and it looked 
real good, although an accelerator is probably a must.

     Wow!  After a few months of mixed to even a run of nothing but bad 
news, this bit of information has got my spirits up.  I love you man!  (As 
John gently pats his IIGS)  

John Stankowski
Delivered by:CoPilot 2.55 and AnsiTerm 2.12
Grateful IIGS User
                (J.STANKOWSKI, CAT40, TOP6, MSG:146/M645;1)

>>>>>   Yesterday I called Vitesse to request a catalog and ask for myself
"""""   about the status of Wolfenstein 3D...

     I talked to a nice lady who said they were in the process of 
contacting all the people who were on backorder to see if they still wanted 
it.  Probably to ship in January.

     Well, last night my anticapation got the better of me and I called 
Vitesse to place my backorder for Wolf and press them for a little more 
info.

     I talked to a nice man (Jay?) who said (in a nutshell) Wolf _will be_ 
released, and late January is the date they're shooting for.

     Now, I know lots of people have been waiting for a year (and more) 
for this, and I've been following the events very closely.  I must say that 
this is the best news I've heard on the subject in a long time.

     So good I put my money down.  And ordered Ultima to boot.

     And if you want Wolf, you should call Vitesse and experss interest 
(818/813-1270).

     Money (or the promise thereof) talks, boys and girls!  : )

John.
                (J.LAWRENCE9, CAT40, TOP6, MSG:149/M645;1)

>>>>>   Well, I just got off the phone with Vitesse about Wolf 3D.  The
"""""   lady told me that yes they _had_ started calling people to confirm 
who still wanted it, but had to stop calling people after two days.  The 
reason was that they were contracting out to _someone else_ to finish Wolf, 
and because of that, Id Software wants a _new_ contract.  We all remember 
how long that took the last time... :(  I didn't think to ask Vitesse who 
they were contracting to finish it.

     So the jist of all this is that Wolf will probably _not_ ship in 
January.  She didn't want to take my order right now but said to call later 
on in January to get the status.  Sorry to be the bearer of bad news.

-Doc
Internet: WEL378@prb.mhs.compuserve.com
                  (M.WELTE, CAT40, TOP6, MSG:152/M645;1)

>>>>>   Wolfenstein 3D for the Apple IIgs is indeed an ongoing project; I
"""""   have NO idea how the distribution is going to be arranged; all they 
did was tell me to finish the program -- Bill's been swamped with projects, 
and I didn't have any pending. :)

     At any rate, Wolf is making definite progress.  I can't say when it 
will be done for sure, since I don't know how long they'll let me keep 
working on it.  However, I've been working on it full-time for a week now, 
and it's looking pretty good.  It does need a good deal of work still.

     Keep in mind that the last 10% of a program requires 90% of the work. 
Although Wolf 3D for the IIgs is nearly finished, the part that's left to 
do is the hardest part -- making sure it works flawlessly and as fast as 
possible.  That's my job here.

     So please be patient. :)

     And please don't innundate me with email asking about Wolf 3D; if I 
get mail from you asking about it, I'll trash it and probably stop speaking 
to you, as every letter I get asking about it takes up time I could be 
working on Wolf. :)

Sheppy
                   (SHEPPY, CAT40, TOP6, MSG:151/M645;1)


SHAREWARE SOLUTIONS II IN 1996   Shareware Solutions II just keeps on 
""""""""""""""""""""""""""""""   growing and growing and growing, so I just 
wanted to let folks know (in one message) about some of the current and 
latest developments....

o Sometime after the 1st of the year, we'll be going to press with the 12th 
  issue of the newsletter. In that issue, Cynthia Field and I will be 
  joined by the newest Shareware Solutions II staff member...Steve Disbrow.

o Shareware Solutions II continues to distribute all the back issues of 
  Resource-Central's (ICON's) HyperCard based Script-Central.

o In early 1996, Shareware Solutions II will also start to distribute 
  Resource-Central's (ICON's) HyperStudio based Studio City.

o In early 1986, Shareware Solutions II will start to distribute the entire 
  NAUG collection; that includes 400 or so disks of AppleWorks related 
  materials, several AppleWorks related books, and all of the back issues 
  of "NAUG On Disk."

o Shareware Solutions II is currently distributing all of the IIe/IIc 
  commercial entertainment titles that had previously been available from 
  Big Red Computer Club. That inventory includes 15 classic games such as 
  Pac Man, Ms PacMan, Defender, Qix, Renegade, DragonWars and Neuromancer.

o In early 1996, Shareware Solutions II will start to distribute the 
  following Big Red Computer Club software: Shanghai II, Labels Labels 
  Labels, Print Shop Lover's Utiltity Set, Print Shop Lover's Utiltity Set 
  GS.

o Shareware Solutions II continues to publish Bill Heineman's Contacts GS 
  name and address NDA database.

o Shareware Solutions II recently became the worldwide distributor of 
  Brutal Deluxe's Convert 3200.

o Shareware Solutions II has recently acquired a prototype ROM4 IIGS, and 
  will be lifting the lid on "the computer that could have changed the 
  world" in Volume 3, Issue 1 of the newsletter.

o The size of the Shareware Solutions II library of freeware/shareware 
  disks just keeps growing and growing, and currently offers some 
  exclusives such as Symbolix GS and SheppyWare '95.

o Shareware Solutions II is the exclusive distributor of Cynthia Field's 
  "A+ Home Organizer" set of AppleWorks GS templates and her HyperCard IIGS 
  based "Better Safe Than Sorry" disk that teaches children about safety.

Joe Kohn
Publisher, Shareware Solutions II

                     (      e-mail JOE.KOHN for subscription info        )
                     (       or point your favorite web browser to       )
                     (        http://www.crl.com/~joko/ssii.html        )
                  (JOE.KOHN, CAT28, TOP4, MSG:330/M645;1)


JOHN LINK SOFTWARE TO BE PLACED IN PUBLIC DOMAIN?   In the last NAUG 
"""""""""""""""""""""""""""""""""""""""""""""""""   AppleWorks Forum 
Newsletter, John Link posted an announcement that he was putting all the 
Apple II software he has written into the public domain, and invited people 
to trade it, hack it or whatever.  In followup email, I learned that the 
last version of SuperPatch was supposed to be posted on a private bbs 
called "The Boardworks" (14400, PTSE, 8N1) at (313) 421 9144, and run by 
Joe Connelly.

     I tried several times to download from there, and it kept locking up 
on me, then I'd get a 'no carrier' message and have to try again.  Has 
anyone else been able to download teh last version of SuperPatch?  Is it 
posted here somewhere?  What about the other software John wrote, including 
Last Patch, Reporter, Tester, etc.?  John says its on that bbs also.  
SuperPatch is supposed to be in the 'exfer library 13' there....

     For what its worth, the http reported for John's home page in the 
AppleWorks Forum article was wrong.  It should be:  //graphics.wmich.edu or 
//graphics.wumich.edu/home John can be reached at:  
link@orchid.art.wumich.edu

     Any chance of getting SuperPatch, and/or John's other programs into 
the "Lost Classics" collection and posted in the library here?
                (J.COUNTRYMAN, CAT7, TOP3, MSG:176/M645;1)


WORDPERFECT TRANSLATOR?   Was a GW3 translator for WordPerfect files ever 
"""""""""""""""""""""""   finished? I remember rumors of this from AOL, I 
think.

     Thanks,

---Brian
                 (B.CLEMMONS, CAT43, TOP6, MSG:68/M645;1)


GRAPHICWRITER III AND SUPERCONVERT UPDATES   I just ordered some things 
""""""""""""""""""""""""""""""""""""""""""   from Seven Hills, and was told 
that we're probably looking at a month or so for the GWIII and SC updates. 
. . 

Ryan--new preferred email: rsuenaga@kestrok.com
                 (R.SUENAGA1, CAT43, TOP6, MSG:71/M645;1)

>>>>>   I had email from Seven Hills about 2 weeks ago, and they said they
"""""   were aiming for January.  In addition, they were hoping for a March 
release of SuperConvert 4.0.  I, too, am eagerly awaiting this upgrade.  It 
_is_ in active beta testing, so we should have a solid product when it is 
released.
                (S.CAVANAUGH1, CAT43, TOP6, MSG:72/M645;1)


TALK IS CHEAP SOURCE CODE TO BECOME AVAILABLE?   Another program may be 
""""""""""""""""""""""""""""""""""""""""""""""   available for preservation 
as a "Lost Classic"....TIC.

     I got the following message via a pro-line newsgroup

     Is there any interest in getting this included in the Lost Classics?  
If so, someone should follow up with the author.....?!

Path:  pro-carolina!delton
From:  delton@pro-carolina.cts.com (Don Elton)
Date:  24 Dec 95 23:55:23 EST
Newsgroups:  pro.tic
Distribution:  world
Reply-To:  delton@pro-carolina.cts.com
Subject:  TIC source code
Lines:  8

     TIC has been on the market for about 9 years now.  Sales are in the 
range of 1-3 copies per month right now as the Apple II series continues to 
wind down in terms of machines remaining in active use.  I've considered 
releasing the source code to TIC for those who like to timker with this 
sort of things and would welcome comments as to whether there would be 
interest in this.  If so, I'll put the code, in ORCA/M format, on the file 
server of my BBS where you can access the files via email commmands to the 
server.
                (J.COUNTRYMAN, CAT7, TOP3, MSG:179/M645;1)


                         >>> MESSAGE SPOTLIGHT <<<
                         """""""""""""""""""""""""

Category 2,  Topic 7
Message 87        Fri Dec 08, 1995
W.RASCHER [William]          at 22:26 EST
 
Bob,

     Things are a little different from my view point.  I have so much 
Apple // stuff to dig through, experiment with, and overall learn about 
that my time is tied up with exploring the GS. :)  There sure is an 
enormous amount to learn and do on other platforms, but I have found that 
my Apple //s take up all the time I can afford to give to computing.  Of 
course nobody I work with can do a fraction on their hot new machines that 
I can do with my GS.  It is years of experience, interest, and money that 
separate us.  OCR on a Mac has the GS beat, but what teacher or family can 
afford the high price equipment and software.  My kids are still arguing 
over who gets to use the Lode Runner or Captain Goodnight disks tonight.  
Its hard for me to believe the number of years we've been playing those 
games.  These kids can play SNES games if they want, but they play Apple // 
games more then SNES.  My 4th grader has written so many stories with the 
Storybook Weaver series over the years that she has almost filled up a 32 
meg partition!  Matthew keeps his NES (and Apple // games) codes in several 
AppleWorks files, and prefers Out of this World on the GS.  There is no 
doubt that games and simulators like Flight Sim. on a Mac or Clone are 
outstanding,  but I find the cost isn't justifiable for my family (total 
platform cost).  Hopefully Pippen will prove to be just the right machine 
for the family and school.  One thing I can say for sure is I'm glad I 
didn't follow all that great advise others gave so cheaply and move to 
another platform.  Remember when the 286 (or SE/30) was a hot item?  I 
think the best medicine right now is patients.  Unless, of course, you have 
to have something that the Apple can't give you.

William

                                 [*][*][*]


    While on GEnie,  do  you spend most of your time  downloading files?
If so, you may be missing out some excellent information in the Bulletin
Board  area.   The messages  listed above  only scratch  the surface  of
what's available and waiting for you in the bulletin board area.

    If you are serious about your Apple II, the GEnieLamp staff strongly
urge  you to give the  bulletin board area a try.   There are  literally
thousands  of messages  posted  from people  like you from  all over the
world.



[EOA]
[HUM]//////////////////////////////
                    HUMOR ONLINE /
/////////////////////////////////
Flowchart Fun
"""""""""""""
by Alan Meiss
   [ameiss@indiana.edu]



               >>> THE MALE GUIDE TO SELECTING AN OUTFIT <<<
               """""""""""""""""""""""""""""""""""""""""""""

 ----------      -----------             -------------------       -------
| Are there| No | Are there | "What's a | Are there clothes | No  |  Buy  |
|clothes in|--->|clothes in |  hamper?" | strewn in random  |---->|  more |
| dresser? |    |the hamper?|---------->|piles on the floor?|     |clothes|
 ----------      -----------             -------------------       -------
     | Yes            | Yes                       | Yes
     +---------------------------------------------
     |
     V
 ---------------
| Take whatever |
|   is on top   |
 ---------------           ------------------------
     |                    |                        |
     V                    V                        |
 --------  No         ---------               -----------
|   Is   |---------->| Perform | "Ohmigosh"  |  Spray    |
|   it   | Not sure  |  smell  |------------>|   with    |
| clean? |---------->|  test   |             | deodorant |
 --------             ---------               -----------
     | Yes                | "Not bad"
     +--------------------
     |
     V
 --------------                    ---------                -------------
|For underwear:| "Which ones are  |Will they| "I may get   |Place item on|
|Are there many|   for my legs?"  |   be    |  arrested."  | dirty pile; |
|    holes?    |----------------->| visible?|------------->| start over  |
 --------------                    ---------                -------------
     | No                              | No
     +---------------------------------
     |
     V
 ---------        ------------        -----------------------------------
|  Is it  | Yes  |   Do you   | Yes  |But would you rather have a tick on|
|wrinkled?|----->|really care?|----->|  your eyeball than iron a shirt?  |
 ---------        ------------        -----------------------------------
     | No              | No                           | Yes
     +------------------------------------------------
     |
     V
 --------  Kinda             -------       ---------
|  Does  |----------------->| Is it | No  | Seek the|
|   it   | "Does it what?"  |  dark |---->|advice of|
| match? |----------------->|  out? |     | a female|
 --------                    -------       ---------
     | Yes                      | Yes
     +--------------------------
     |
     V
 ----------
|  Put on  |
| clothes! |
 ----------

  Copyright 1995 by Alan Meiss.  Reprinted with the author's permission.
               Visit Alan's archive of his humor files at: 
            http://www.cs.indiana.edu/hyplan/ameiss/humor.html


[EDITOR'S NOTE:  The editor would like to receive submissions for a 
possible female's guide to selecting an outfit, BUT he insists it be 
written by a female.  Anyone offended by this editorial policy is invited 
acquire both a sense of humor and a sense of proportion, and also requested 
to go sit on a tack.]



[EOA]
[REF]//////////////////////////////
                     REFLECTIONS /
/////////////////////////////////
Thinking About Online Communications
""""""""""""""""""""""""""""""""""""
by Phil Shapiro
   [pshapiro@aol.com]



      >>> SOME THOUGHTS ON THE RAMIFICATIONS OF MUSEUM HOME PAGES <<<
      """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

     In my free time I do some volunteering at the Capital Children's 
Museum, here in Washington DC.  I was excited to hear last week that the 
museum will be soon setting up a home page on the world wide web.

     The ramifications of such a web page could bring a profound new 
dimension to the museum's work.  This museum already has a large following 
and fan club.  With a public presence on the web, the museum's fan club is 
bound to grow exponentially.

     This particular children's museum happens to be much more than a 
museum.  The museum actively involves youth in the city in various creative 
projects, including the creation of video documentaries (amongst other 
things).  The museum gives teens access to a full video production facility 
that was donated to the museum.  Students plan, shoot, and edit their own 
documentaries--sometimes even adding their own computer generated 
animations.

     The museum is everything a museum should be, and more.  It takes 
seriously its mission to celebrate human culture and creative expression.  
It excels at helping young minds awaken to the wonders of the world.

     Yesterday I was explaining to a friend how hugely significant it is 
for the museum to set up a presence on the web.  The very existence of a 
home page will let the metropolitan community know that the museum is 
comfortable extending its presence into cyberspace.  And the existence of 
this home page is an implicit invitation to all interested persons to help 
support the further construction of the page.  (And to help support the 
mission of the children's museum in general.)

     To be sure, it's a bit scary to contemplate how much incoming e-mail 
this web page might generate.  The museum's hard-working staff already puts 
in long hours of work.  Having to answer an extra 30 to 40 e-mail messages 
per day is no small undertaking.  And the volume of e-mail arriving at the 
museum could rise to 80 to 90 messages per day within just a few months.

     I admire the leadership of the museum for their foresight and courage 
in setting up a web page.  Since the web is such a new creation, there are 
few precedents about what happens when a popular museum sets up a web page.

     For families and schools in the metro area, the museum's web page is 
sure to be a treasure.  People will be able to plan their visits to the 
various exhibits at the museum, whetting children's interest before the 
kids even set foot in the museum.

     To help you picture the Capital Children's Museum, imagine a huge 
three story red brick structure with dozens of exhibit rooms and oodles of 
nooks and crannies.  This museum is so large that I quite regularly get 
lost in traveling from one end of the museum to the other.

     What makes this museum so interesting to me is that the museum has a 
certain magical aura.  Soon after the museum's doors open each morning the 
sound of children's laughter and giggling fills the air.  Almost all the 
exhibits include discovery-based learning activities.  And now this museum 
is on the web.

     What will the museum's home page help the museum accomplish?  In some 
respects a web page is an ongoing, ever growing newsletter.  News about 
events and happenings at the museum can be easily disseminated to anyone 
interested in such.  Solicitations for specific volunteer help can be 
posted on the museum's web page each and every week.

     If the museum needs help with a project involving desktop publishing, 
the museum staff can post an appeal for help on the museum's web page.  
Chances are good that some kind soul in the greater metropolitan community 
will step forward to assist with such an appeal.

     The opportunities for "remote volunteering" abound when a museum sets 
up a web page.  The desktop publishing volunteer mentioned above is just 
one service that remote volunteers could offer to the museum.  And the nice 
thing is that the museum can express their appreciation for completion of 
volunteer tasks by expressing their gratitude right there on the museum's 
web page--in public, for all the world to see.

     Do you think local businesses might take a greater interest in 
supporting the museum once the museum has a web page set up?  I suspect 
they might.

     The aspect of this story that fascinates me the most, though, is the 
possibility that the museum could choose to exhibit local youth's creative 
work on the museum's web page.  Since a web page is theoretically a museum 
with unlimited wall space, this possibility tickles my imagination.

     It would not be difficult at all for the museum to put out an appeal 
to all school art teachers in the metropolitan area to submit (in 
electronic form) the best of their students' graphic art work.  A similar 
appeal could go out to school music teachers in the metro area to submit 
digitized samples of some of their students' most creative work.  And 
English teachers could be asked to submit in ASCII form some of the most 
creative writings of their students.

     The museum could assemble a large collection of these submissions and 
exhibit them for all the city (and all the world) to appreciate.  Imagine 
that.  A museum with limitless wall space celebrating the creative spirit 
of the city's youth.

     The social ramifications of having a museum on the web are just 
beginning to be understood.  I'm quite sure, though, that many positive 
things are going to be happening soon at this museum.  The museum's 
original mission, which it has succeeded so well in fulfilling, is going to 
explode into a whole new dimension.

Phil Shapiro

                                 [*][*][*]


          The author takes a keen interest in the social and psychological 
          aspects of online communication.  This is the 29th essay in the 
          "Thinking About Online Communications" series.  Information about 
          earlier essays in this series can be found on the author's home 
          page, at http://users.aol.com/pshapiro/

          Feedback about this current essay is invited at: pshapiro@aol.com



[EOA]
[PDQ]//////////////////////////////
                    PD_QUICKVIEW / 
///////////////////////////////// 
Yours for the Asking
""""""""""""""""""""
by Steve Cavanaugh
    [S.CAVANAUGH1]



         Program Name    :  Loadpall
         Filename        :  LOADPALL.BXY
         Library Area    :  30
         Program Number  :  24604
         File Size       :  132992
         Program Type    :  XCMD for HyperCard IIgs
         Author          :  Brian Gillespie, Jaunt! Software
         Version Reviewed:  1.02
         Requirements    :  HyperCard IIgs

                               [*][*][*]


                     .                       __ 
                    (^)        <^>         /~  ~\ 
                     \-=======_/"\_======-/     \) 
PD_Q RATING           "\.   FOUR LAMPS   ./" 
"""""""""""              "\._    _   _./" 
                             (_____)         (1-5)

DOCUMENTATION   N/A (a demonstration stack outlines use of the XCMD)
"""""""""""""

PD_Q COMMENTS   LoadPall is a stack by Brian Gillespie that has been in the 
"""""""""""""   Apple II library for several months now.  I don't know if 
other people are getting much use from it, but I have found it to be an 
amazingly useful and rock-solid XCMD for HyperCard.

     HyperCard IIGS, like its Macintosh cousin, is an application that 
lets any individual create their own multimedia stacks.  Brian's LoadPall 
helps extend the native abilities of HyperCard IIGS by allowing the display 
of 16, 256, and 3200 color 320-mode graphics in HyperCard by allowing the 
loading of other palettes (hence the name, Loadpall).

     To use the XCMD (pronounced ex-command) is very easy.  Brian's stack 
has a button that allows you to install the XCMD in any stack you like (so 
you do have to create the "target" stack first).  Then, to call up the XCMD 
you simply insert the line "loadpall xxxx.pal" where xxxx.pal is the name 
of a 320 mode pallete that has been saved out of a paint program.

     For instance, I took a picture of a bull moose that had been a TIFF 
graphic on a Mac CD-ROM.  I converted the TIFF to a 320-mode 256-color 
graphic using PRISM, then loaded the picture into DreamGraphix.  From 
DreamGraphix, I saved the pallete of the picture in the same directory as 
my stack (a fledgling encyclopedia of North America), naming it moose.pal.  
Then I quit to HyperCard IIGS, opened up the encyclopedia stack, and 
imported the moose picture to a card.  I then created a card script (by 
holding down Option-Command-Shift-C) and typed in the following script:

          on opencard
          hide menubar
          loadpal "moose.pal"--load the pallete for the moose picture
          end opencard

          on closecard
          loadpal "def.pal"--load the default pallete
          show menubar
          end closecard

     Next, I created a button on the card with text describing Moose that 
opened the card with the picture.  Upon presenting that card, HyperCard 
hides the menubar (giving us a full screen for the picture), loads in the 
palette, and the picture, in near photographic quality, is presented for 
viewing.  A mouse click sends us back to the original picture (that's a 
different script).

     I have used LoadPall with many pictures, and with external windows 
for 16 color pictures, and it works great!  If you do any work with 
HyperCard IIGS, and don't yet have LoadPall, you should definitely download 
it.

EDITOR'S NOTE   To get the most recent version of Loadpall, you must 
"""""""""""""   download file #24604, LOADPALL.BXY and file #25531, 
LOADPALL102.BXY.  I think that the second file is just an update to the 
first, not a stand-alone application.  At least, when I tried to use v1.02 
on its own, it wouldn't work for me without the palette files in the 
earlier version.--Ed.



[EOA]
[FOC]//////////////////////////////
                     FOCUS ON... /
/////////////////////////////////
The Finder as ProSel-16 Substitute
""""""""""""""""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



                  >>> CONFESSIONS OF A PROSEL-16 USER <<<
                  """""""""""""""""""""""""""""""""""""""

     I've been using ProSel 8 since it was called ProSel, and graduated to 
ProSel-16 almost as soon as it became available.  ProSel, in all its 
incarnations, is a program launcher and suite of utilities.

     I happen to like ProSel.  Some say that its complexity can be a bit 
intimidating at first.  I never found it so.  When I first bought ProSel, I 
used just those parts of it that I could understand.  As I gained the 
confidence to explore, I added other parts of the program to my repertoire.  
I confess that there are still some functions that remain a mystery to me, 
even after three years of a stable program.  (ProSel-16 v8.84 has a 1992 
copyright date.)

     Those of us who made the move from DOS 3.3 to ProDOS in 1984 were 
appalled by FILER, the file manipulation utility that Apple Computer 
provided.  ProSel's Cat Doctor was (and is) like a breath of fresh air.  
The other core utilities were equally invaluable:  Backup, Restore, and 
Recover (for backing up hard drives), Beach Comber (for optimizing), Block 
Warden (sector editor), Find File (which also finds strings within files), 
Info Desk (print catalogs to disk, printer, or the screen), Mr. Fixit (disk 
volume repair) and Volume Copy (which uses as much memory as it could find 
to copy a disk, thus reducing disk swaps).

     None of this addresses either the central purpose of ProSel nor how 
it gets its name:  at heart, ProSel is a PROgram SELector... or a program 
launcher.  All of a sudden it makes sense why I should want to compare the 
Finder and ProSel, doesn't it?  ProSel's interface is text-based (ProSel-16 
uses 320-mode graphics, but it's still a text-based interface), dividing 
the screen into three columns of 20 items, with each of the 60 items being 
24 characters wide.  A sample might look like this:

AppleWorks GS v1.1       CoPilot v2.5.5            Finder
GraphicWriter III v1.1   SnowTerm v2.06            GNO/ME v2.0.4
Quick Click Calc v1.3    Spectrum v2.0             ORCA
WordPerfect v2.1e        TimeMaster IIgs v2.1

HyperStudio v3.1         ShrinkIt GS v1.1          Graphics Exchange v2.1
MD-BASIC v2.0            PMPunZIP v2.0             Platinum Paint v1.0.5

     You can use the mouse, the arrow keys, or the alphabetic keys to 
select one of these options, then press Return or click the mouse to launch 
the program associated with the option.

     Notice how this differs from the Finder:  in ProSel, the lists above 
are arranged first by general function and then alphabetically--because 
that's how _I_ like it--and the selection names on the screen do not 
(necessarily) equal the names of the program on disk.  Also, the screens 
you construct in ProSel are HARD-CODED... that is, they only show you the 
programs for which you have constructed links.  If you think this is a 
disadvantage, then my guess is that you are a confirmed Finder user.  It 
can also be a great advantage... ProSel can keep track of the programs that 
you use on a regular basis.  (And it can show you up to 60 on a screen, 
without having to scroll through a long list with a mouse, which takes 
time.)

     ProSel also has an "immediate" mode, for launching programs that 
don't appear on any of the lists you have created.  Press 1 and ProSel will 
show you the launchable (SYS and S16) programs--plus any subdirectories--on 
your first drive or partition.  (Other partitions are also available.)  
ProSel doesn't clutter up your screen with documents and other things you 
can't launch.

     Perhaps because I view my computer as a tool for beating back the 
tide of information _until I ask for it_, ProSel suits me just fine.  
ProSel's primary function is to keep me organized.

     Another advantage is that ProSel doesn't slow me down my demanding 
that I use a mouse.  (ProSel does support the mouse for those that prefer 
it, though.)  I don't care how convenient you find it when learning how a 
program works, pointing, dragging, and clicking will never match the speed 
of keyboard input.

     Now that I stand revealed before you as a person who despises a mouse 
and gets irritated when he is given information before he wants it, you'll 
understand why the Finder is not my launcher of choice.  However, I am 
compelled to admit that I have started using it.  Now that you Finder types 
have been turned on to the wonders of ProSel, maybe you should go clean 
your closets while I hip my fellow ProSel users to the when actually launch 
the dang Finder.

     Cat Doctor's ability to batch-process files sure came as a boon, 
didn't it?  It you have a whole bunch of files on need to do the same thing 
to all of them, ProSel's Cat Doctor is the way to go.  Even if you want to 
delete all but two of a files in a given folder and rename the remaining 
two, then you can probably hold the details in your short-term memory.  But 
if you have a slew of files that need to be copied to various destinations, 
renamed, moved, appended, and deleted, then use the Finder.

     ProSel's excellent at letting you do one thing a time.  But with the 
Finder, you can do lots of things.  You can interrupt your tasks without 
interrupting your train of thought.

     For example:  I file all my electronic mail.  Once a month, I archive 
the whole shootin' match on the assumption that I might need to retrieve it 
someday.  Well, that's the theory, anyhow.  In practice, I archive almost 
all of it.  Now that junk E-mail is with us, I just throw that out.  
(Already the task is beginning to diversify.)  And I subscribe to a few 
E-mail groups and lists, and of course I don't archive those as 
correspondence.  What's more, I occasionally forget my archive names, and 
file the same information under two similar but different names.  Or two 
letters from the same person will end up in two different folders... and 
therefore the letters need to be combined into one file.

     It is possible to go through my monthly mail-archiving ritual with 
ProSel, but I keep losing my train of thought.  If I stop moving files to 
rename, I lose my place in the list of files to be processed.  I either 
finish with largely illegible notes scrawled on myriad scraps of paper, or 
I find I go through all the files several times to be sure I haven't missed 
anything.

     Since I began using the Finder for this and other similar operations, 
my headaches have stopped.  With the Finder, I can do all the file 
management I need, look into files, combine files, and do all manner of 
things without once losing my place in the list.  The Finder isn't as good 
as ProSel at cramming information onto a single screen, but the fact that 
the Finder lets you choose how to view (as opposed to sort/organize) makes 
most tasks much easier.  And of course, the Finder lets you have windows 
representing various destinations on the desktop simultaneously, which 
means you don't have to retype pathnames.

     For years, I have been saying tolerantly, in public, that both ProSel 
and the Finder had their uses, but privately believing that the Finder was 
only for those who need pretty-picture icons and don't loathe a mouse as 
much as I do.  I've known all along how to use the Finder.  At long last, I 
know why to use it.  Hallelujah!



[EOA]
[PNL]//////////////////////////////
                 PAUG NEWSLETTER /
/////////////////////////////////
January 1996 Report
"""""""""""""""""""
by Ray Pasold
   [R.PASOLD]



                       >>> VOLUME III, NUMBER 3 <<<
                       """"""""""""""""""""""""""""

WHAT IS PAUG?   The mission of the Planetary Apple User Group (PAUG) is to 
"""""""""""""   serve as the online heart of the worldwide Apple II 
community.  PAUG's goals include providing help and support for anyone who 
may not have a local user group nearby, and for user groups that want a 
virtual link to the Heart of the Apple II world.  Creating a link between 
PAUG and both the online and offline user group community is one of our 
priorities.  PAUG sponsorship of an online user group is focused on 
promoting the fact that the Apple II is not only still alive, but doing 
very well.


WHAT DOES PAUG DO?   There _is_ support!  PAUG will provide it, or help you 
""""""""""""""""""   find it.  We meet the third Sunday of every month at 
7:00 p.m. Eastern Time in the Apple II RoundTable (A2) Real Time Conference 
(RTC) area.  With no long miles to drive or time away from home, PAUG 
offers the Apple II user a friendly and comfortable association with others 
of similar computing interests.  There is plenty of accurate computer 
information, hints, tips, and just plain old fashioned fun conversation.  
Along with this, you also get the security of knowing that all the 
computing support you could possibly need is right at your fingertips.  
What could be more convenient?


ANNOUNCEMENTS
"""""""""""""

                  H A P P Y   N E W   Y E A R  ! ! ! ! !

Resignation   I am sad to announce my resignation as editor of the PAUG 
'''''''''''   newsletter.  Pressing financial matters, a job, and personal 
things have forced me to resign.  It is with great apology I do so.  On 
another note, though, I have fallen in love and have a girlfriend!  So be 
happy for me!

The Next Meeting   The next meeting of PAUG is at 7 p.m. Eastern Time on 
''''''''''''''''   Sunday, January 21.  This will be the first meeting 
ushering in a new year of Apple II fun and productivity.  We should have 
some news at this meeting about the official status of PAUG as a listed 
user group, and what benefits and discounts this means to you!  Be there!


THE MAIN EVENTS   "Sights and Sounds of the Season" was the theme for the 
'''''''''''''''   December meeting.  What are your favorite holiday 
graphics and desktop pictures?  What are the best Apple II programs to 
display them?  How about sounds, like music and rSounds?  All of these 
topics were covered during the meeting.  The esteemed guests were Pat Kern 
and Clay Juniel.  Pat is the graphics wizard and always says, you can't 
have enough graphics!  Clay is the author of some fine programs that will 
help you enjoy the power of your Apple II.


THE LIBRARY   A revised Eamon game list is here, since the previous list 
'''""""""""   was not complete.

     DOS 3.3:     Library 42
     ProDOS 8:    Library 33
     GS specific: Library 21

Eamon Stuff
'''''''''''
 DOS 3.3 Masters, Samples, and Some of TomZ's Favorites:

 24544 E.DOS33.001.BXY DOS 3.3 Eamon Master Disk (required to play Eamon)
 24677 E.DOS33.042.BXY DOS 3.3 Eamon "Alt. Beginners Cave"  (easy)
 24883 E.DOS33.124.BXY DOS 3.3 Eamon "Assault on Dolni Keep (intermediate)
 24911 E.DOS33.138.BXY DOS 3.3 Eamon "Starfire"             (easy)
 24976 E.DOS33.169.BXY Dos 3.3 Eamon "The Black Phoenix"    (sf)
 25052 E.DOS33.194.BXY DOS 3.3 Eamon "Attack of the Kretons (funny!)
 25094 E.DOS33.204.BXY DOS 3.3 Eamon "Sanctuary"            (advanced)

 24542 E.DOS33.D62.BXY DOS 3.3 Eamon Dungeon Designer v6.2
 24535 E.DOS33.D71.BXY DOS 3.3 Eamon Dungeon Designer v7.1
 24543 E.DOS33.GMH.BXY DOS 3.3 Eamon Graphics Main Hall
 25421 E.DOS33.UTL.BXY DOS 3.3 EAG Eamon Utilities

 ProDOS Masters, Samples, and Some of TomZ's Favorites:

 16219 EAMON.001.BXY   ProDOS Eamon Master/Main Hall (required to play)
 15698 EAMON.124.BXY   80-col. P8 'Assault on Dolni Keep' (intermediate)
 15699 EAMON.138.BXY   80-col. P8 'Starfire'              (easy)
 17043 EAMON.169.BXY   80-col. P8 'The Black Phoenix'     (sf)
 11767 EAMON.P.191.BXY ProDOS  'Enhanced Beginners Cave'  (easy)
 16034 EAMON.194.BXY   80-col. P8 'Attack of the Kretons' (funny!)
 18011 EAMON.204.BXY   80-col. P8 'Sanctuary'             (advanced)

 24532 EAMON.DDD71.BXY ProDOS Eamon Dungeon Designer v7.1
 15702 EAMON.GMH.BXY   ProDOS 'Graphics Main Hall'
 24534 EAMON.UTL.BXY   Desc: ProDOS EAG Eamon Utilities

 16750 STARTER.KIT.BXY Very Best role playing system!
 16728 BEST.EAMONS.BXY An incredible role-play experience!

 General Eamon Information Files:

 25212 EAMON.KEYWD.TXT Describes the Eamon keyword scheme
 25505 EAMON.LISTS.BXY Updates of six Eamon Lists
 11842 EAMON.MAPS.BXY  Misc. Eamon maps
 23000 EAMON.REV1.BXY  Reviews of Eamon Adventures
 23001 EAMON.REV2.BXY  Reviews of Eamon Adventures
 23002 EAMON.REV3.BXY  Reviews of Eamon Adventures

Have FUN!


PLEASE FEED ME!   We have had some great letters and comments about PAUG 
"""""""""""""""   and it appears everyone wants us to go official with 
Apple.  So, we have filed for that status!  More about that real soon!

GOOD BUY   Willis Poole (W.POOLE) has done a good job on reviewing an 
""""""""   extended keyboard for the IIgs.  Here it is.

     OK!! Here is the Interex, Mac-105A Extended keyboard.

     Apple 150 key, ADB compatible keyboard with adjustable height legs,  
sold at Computer City for $34.99. Some of the extra keys will work by 
themselves after you plug it in and play just like a GS KB with the rest of 
the main board/keypad fully functional. To make use of the F-1 thru F-4 and 
F-5 thru F-12, you will need the programs called  Keyboard Extender by Bill 
Tudor in the library, and Hotkeys from GS+ in that order.

     Many KB's have built in clickers but this one doesn't. I have added a 
NDA called Keyclicker to adjust the click to what I want, which is a very 
faint "tic" due to the light touch keys. The keys feel very solid like a 
good car door slamming shut and quiet, only the space bar has a slight 
rattle to it, I can feel the two positions on each key when I push fully 
down for the repeater, the keys have a very nice easy touch (depression 
approx 1/32) and may work well with a skin cover.

     I really like the keys, ie. on the main keyboard the "return key" is 
enlarged at the top so the |\ key is next to the return key, this is good 
when you get to the Net and start using some UNIX commands when some of 
these rarely used keys are put in use. I also like the large delete key 
above the RTN key, look closely at the other KB's you  can  see that the |\ 
key is next to the little delete key.....when its down next to the RTN key 
I can assign things to it and reach it with one hand, its out of place next 
to the delete key.

     The ADB ports are located at the top left corner and top right corner 
on the ends, indented not flush, some KB's have these ports located top 
center, the mouse  is simply connected like the GS KB. It has 4 lights 
..option numbers, numbers, caps, scroll. This KB is made in Taiwan and 
again has a solid feel to it.

     I think you really need to do a hands on so you can feel these keys.

     If you want this one here is some more info...

     Computer City..1-800-THE CITY  will tell you the closest store  to 
you.

          Interex Computer Products
          2971 S. Madison
          Wichita, Ks 67216

          Tech support 316-524-4747 Ext. 232  0800 to 1700 M-F
          FAX 316-524-4636..24hrs

     Willis Poole.

     Thank you Willis!!

     Also a reminder, if you are serious about your Apple II, and of 
course at this time you have to be serious is you still use it, then 
subscribe to Shareware Solutions II.  It is the best thing happening in the 
Apple II world Today!


IN CONCLUSION   The PAUG Newsletter is an intrinsic part of GEnieLamp A2. 
"""""""""""""   If this is your first time reading the GEnieLamp A2, be 
sure to get it every month and take note; all the text file (TXT) back 
issues are in the DigiPub Library and the AppleWorks Word Processor (AWP) 
back issues are in the A2 Library, awaiting your reading pleasure.  I will 
miss all of you!

                HAPPY NEW YEAR, and thanks for the support!

     Remember, the goal of the Planetary Apple User Group is to be _your_ 
primary Apple II resource!  If you have any suggestions, insights, or ways 
to help us help you...let us know!  Future plans include working with 
groups on the "outside" to create a stronger bond within the Apple II 
community.  Just email c.adams11@genie.com or a2.gena@genie.com, or if you 
are already online with GEnie (yes, still a good move!) make a post in 
Category 3, Topic 34 (Planetary Apple User Group) in the A2 RoundTable 
Bulletin Board (m645;1).








           ////////////////////////////////////////////// QWIK_QUOTE ////
          /  I love being a writer.  What I can't stand is the         /
         /   paperwork.                                               /
        ////////////////////////////////////////  Peter De Vries  ////



[EOA]
[LOG]///////////////////////////////
                          LOG OFF /
//////////////////////////////////
GEnieLamp Information
"""""""""""""""""""""

   o   COMMENTS: Contacting GEnieLamp

        o   GEnieLamp STAFF: Who Are We?



GEnieLamp Information   GEnieLamp is published on the 1st of every month
"""""""""""""""""""""   on GEnie page 515.  You can also find GEnieLamp on 
the main menus in the following computing RoundTables.


RoundTable      Keyword  GEnie Page     RoundTable      Keyword  GEnie Page
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
DigiPub         DIGIPUB      1395        Atari ST        ST          475
Macintosh       MAC          605         IBM PC          IBMPC       615
Apple II        A2           645         Apple II Dev.   A2PRO       530
Macintosh Dev.  MACPRO       480         Geoworks        GEOWORKS    1050
BBS             BBS          610         CE Software     CESOFTWARE  1005
Mini/Mainframe  MAINFRAME    1145        Programming     PROGRAMMING 1445
Data Comm.      DATACOMM     1450        IBM PC Prog     IBMPCPRO     617
PowerPC         PPC          1435        PowerPCProg     PPCPRO      1440



    GEnieLamp is also distributed on CrossNet and many public and 
commercial BBS systems worldwide.

    o To reach GEnieLamp on Internet send mail to genielamp@genie.com

    o Back issues of GEnieLamp are available in the DigiPub RoundTable 
      Library #2 on page 1395 (M1395;3).

    o GEnieLamp pays for articles submitted and published with online GEnie 
      credit time.  Upload submissions in ASCII format to library #31 in 
      the DigiPub RoundTable on page 1395 (M1395;3) or Email it to 
      GENIELAMP.  On Internet send it to: genielamp@genie.com

    o We welcome and respond to all E-Mail.  To leave comments, suggestions 
      or just to say hi, you can contact us in the DigiPub RoundTable 
      (M1395) or send GE Mail to John Peters at [GENIELAMP] on page 200.

    o If you would like to meet the GEnieLamp staff "live" we meet every 
      Wednesday night in the Digi*Pub Real-Time Conference at 9:00 EDT 
      (M1395;2).

    o The Digital Publishing RoundTable is for people who are interested in 
      pursuing publication of their work electronically on GEnie or via 
      disk-based media.  For those looking for online publications, the 
      DigiPub Software Libraries offer online magazines, newsletters, 
      short-stories, poetry and other various text oriented articles for 
      downloading to your computer.  Also available are writers' tools and 
      'Hyper-utilties' for text presentation on most computer systems.  In 
      the DigiPub Bulletin Board you can converse with people in the 
      digital publishing industry, meet editors from some of the top 
      electronic publications and get hints and tips on how to go about 
      publishing your own digital book.  The DigiPub RoundTable is the 
      official online service for the Digital Publishing Association.  To 
      get there type DIGIPUB or M1395 at any GEnie prompt.


                         >>> GEnieLamp STAFF <<<
                         """""""""""""""""""""""

 GEnieLamp  o John Peters         [GENIELAMP]    Publisher
 """""""""  o Mike White          [MWHITE]       Managing Editor

  APPLE II  o Doug Cuff           [EDITOR.A2]    EDITOR
  """"""""  o Ray Pasold          [R.PASOLD]     A2 Staff Writer
            o Charlie Hartley     [A2.CHARLIE]   A2 Staff Writer

     A2Pro  o Tim Buchheim        [A2PRO.GELAMP] EDITOR
     """""

     ATARI  o Sheldon H. Winick   [GELAMP.ST]    ATARI EDITOR
     """""  o Bruce Smith         [B.SMITH123]   EDITOR/TX2
            o Jeffrey Krzysztow   [J.KRZYSZTOW]  EDITOR/ST-Guide
            o Mel Motogawa        [M.MOTOGAWA]   Atari Staff Writer
            o Al Fasoldt          [A.FASOLDT]    Atari Staff Writer
            o Lloyd E. Pulley     [LEPULLEY]     Atari Staff Writer

       IBM  o Sharon La Gue       [SHARON.LAMP]  IBM EDITOR
       """  o Tika Carr           [LAMP.MM]      MULTIMEDIA EDITOR
            o Susan M. English    [S.ENGLISH1]   Multimedia Graphics Artist
            o Wayne & Chris Ketner[C.KETNER]     IBM Staff Writers

 MACINTOSH  o Richard Vega        [GELAMP.MAC]   MACINTOSH EDITOR
 """""""""  o Tom Trinko          [T.TRINKO]     Mac Staff Writer
            o Robert Goff         [R.GOFF]       Mac Staff Writer
            o Ricky J. Vega       [GELAMP.MAC]   Mac Staff Writer

  POWER PC  o Ben Soulon          [BEN.GELAMP]   POWER PC EDITOR
  """"""""  o Eric Shepherd       [SHEPPY]       Power PC Staff Writer

   WINDOWS  o Bruce Maples        [GELAMP.WIN]   EDITOR
   """""""  o Tika Carr           [LAMP.MM]      Windows Staff Writer

      ETC.  o Jim Lubin           [J.LUBIN]      Add Aladdin Scripts
      """"  o Scott Garrigus      [S.GARRIGUS]   Search-ME!
            o Mike White          [MWHITE]       (oo) / DigiPub SysOp
            o John Peters         [GENIELAMP]    DigiPub SysOp
            o Phil Shapiro        [P.SHAPIRO1]   Contributing Columnist
            o Sanford E. Wolf     [S.WOLF4]      Contributing Columnist
            o Douglas Parks       [DELUXE]       Contributing Columnist



\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
 Opinions expressed herein are those  of the individual authors, and do
 not  necessarily  represent  opinions of  GEnie Information  Services,
 GEnieLamp Online Magazines, or T/TalkNet  Online Publishing.  Bulletin
 board messages are reprinted verbatim, and are included in this publi-
 cation with permission  from GEnie Information Services and the source
 RoundTable.   GEnie Information Services,  GEnieLamp Online Magazines,
 and T/TalkNet Publishing  do not guarantee the accuracy or suitability
 of any information included herein.   We reserve the right to edit all
 letters and copy.

 Material  published in  this edition may be  reprinted under the  fol-
 lowing terms only. Reprint permission granted, unless otherwise noted,
 to  registered computer  user groups and  not for profit publications.
 All articles  must remain unedited  and include  the issue  number and
 author  at the top of each article reprinted.  Please include the fol-
 lowing at the end of all reprints:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////
 The  preceeding article  is reprinted  courtesy of  GEnieLamp  Online
 Magazine.  (c) Copyright 1995 T/TalkNET  Publishing and  GEnie Infor-
 mation Services.  Join GEnie  now and receive $50.00 worth  of online
 credit. To join GEnie, set your modem to 9600 baud (or less) and half
 duplex (local echo). Have the modem dial 1-800-638-8369 in the United
 States or 1-800-387-8330  in Canada.  After  the CONNECT message, you
 should see a U#= prompt.  At the  U#= prompt, type: JOINGENIE and hit
 the RETURN key.  When you get  the prompt asking for the signup code,
 type DSD524 and hit RETURN. GEnie will then prompt you for your sign-
 up information. For more information call (voice) 1-800-638-9636.
////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

[EOF]
�