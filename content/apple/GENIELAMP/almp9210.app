

     |||||| |||||| ||  || |||||| ||||||
     ||     ||     ||| ||   ||   ||             
     || ||| ||||   ||||||   ||   ||||               Your
     ||  || ||     || |||   ||   ||            
     |||||| |||||| ||  || |||||| ||||||             GEnie Lamp A2/A2Pro
                                     
     ||    |||||| ||    || ||||||                   RoundTable
     ||    ||  || |||  ||| ||  ||               
     ||    |||||| |||||||| ||||||                   RESOURCE!
     ||    ||  || || || || ||             
     ||||| ||  || ||    || ||         


                           ~ Joe Kohn On A2 ~
                        ~ Softdisk Publishing ~
                           ~ Apple Vs. IBM ~
                       ~ Apple History, Part 5 ~
           ~ HOT FILES ~ HOT MESSAGES ~ HOT ROUNDTABLE NEWS ~

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
 GEnie Lamp A2/A2Pro ~ A T/TalkNET OnLine Publication ~  Vol.1, Issue 7
 """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
   Publisher............................................Kent Fillmore
    Senior Editor........................................John Peters
     Editor.............................................Tom Schmitz

            ~ GEnieLamp Macintosh ~       ~ GEnieLamp IBM ~
  ~ GEnieLamp ST ~    ~ GEnieLamp Elsewhere ~   ~ GEnieLamp A2/A2Pro ~
////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

       >>> WHAT'S HAPPENING IN THE APPLE II/A2Pro ROUNDTABLE? <<<
       """"""""""""""""""""""""""""""""""""""""""""""""""""""""""
                          ~ October 1, 1992 ~

 FROM MY DESKTOP ......... [FRM]        HEY MISTER POSTMAN ...... [HEY]
  Notes From The Editor.                 Is That A Letter For Me?

 HUMOR ONLINE ............ [HUM]        APPLE_TALK .............. [APP]
  Operator From Hell.                    Apple Vs. IBM.

 ONLINE FUN .............. [FUN]        WHO'S WHO ............... [WHO]
  Search-ME!                             Who's Who In Apple II.
 
 THE MIGHTY QUINN ........ [QUI]        FOCUS ON ................ [FOC]
  Milliseconds With Mark.                Thinking Out Loud.

 GAMES PEOPLE PLAY ....... [GAM]        CowTOONS! ............... [COW]
  Apple II Fun.                          Moooooo Fun!

 CONNECTIONS ............. [CON]        APPLE II ................ [AII]
  Joe Kohn On A2.                        Apple II History, Part 6.

 THE ONLINE LIBRARY ...... [LIB]        SOFTDISK PUBLISHING ..... [SOF]
  Yours For The Asking.                  Softdisk Publishing On GEnie.

 GEnie ONLINE ............ [GEN]        HACK'N ONLINE ........... [HAC]
  En guarde!                             HST Modem Upgrade.

 ELSEWHERE ............... [ELS]        LOG OFF ................. [LOG]
  Connecting The World.                  GEnieLamp Information.

[IDX]"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

READING GEnieLamp   GEnieLamp  has  incorporated  a  unique   indexing
"""""""""""""""""   system to help make  reading the  magazine easier.  
To  utilize this system, load GEnieLamp into any ASCII  word processor
or text  editor.  In the index  you will find the  following  example:

                   HUMOR ONLINE ............ [HUM]  
                    [*]GEnie Fun & Games.

   To read this  article, set your  find or search command to [HUM].  If  
you want to scan all of the articles, search for [EOA].  [EOF] will take  
you to  the last page,  whereas [IDX]  will bring you back to the index.

 
MESSAGE INFO   To make it easy for you to respond to messages re-printed  
""""""""""""   here in GEnieLamp, you will find all the information you  
need immediately following the message.  For example:

                    (SMITH, CAT6, TOP1, MSG:58/M475)
        _____________|   _____|__  _|___    |____ |_____________  
       |Name of sender   CATegory  TOPic    Msg.#   Page number|

    In this  example, to  respond to  Smith's  message, log  on to  page
475 enter the bulletin board and set CAT 6. Enter your REPly in TOPic 1.

    A message number that is surrounded by brackets indicates  that this  
message  is a "target" message and is  referring  to  a "chain"  of  two   
or more  messages that are following the same topic.  For example: {58}.


ABOUT GEnie   GEnie costs only $4.95 a month for  unlimited evening  and
"""""""""""   weekend  access  to  more  than  100  services   including
electronic mail,  online encyclopedia,  shopping,  news,  entertainment,
single-player games,  multi-player chess and bulletin  boards on leisure
and  professional  subjects.   With  many other services,  including the
largest  collection of files  to download and the best online games, for
only  $6  per hour  (non-prime-time/2400  baud).   To sign up for  GEnie
service,  call (with modem) 1-800-638-8369.  Upon  connection  type HHH.
Wait for the U#= prompt. Type: XTX99368,GENIE and hit RETURN. The system
will then prompt you for your information.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


         ///////////////////////////////////// GEnie_QWIK_QUOTE ////
        / "If your only tool is a hammer....perhaps every problem /
       / becomes a nail......."                                  /
      //////////////////////////////////////////  H.WESSEL3  ////

  

[EOA]
[FRM]//////////////////////////////
                 FROM MY DESKTOP /
/////////////////////////////////
Notes From The Editor
"""""""""""""""""""""
By John Peters
   [GENIELAMP]



TOP OF THE PAGE   GEnieLamp writers and contributors are paid for their
"""""""""""""""   efforts with online time here on GEnie.  In order for me
to credit their accounts, I ask them to send me their GEnie ID number.
Recently, a new contributor sent me his password.  GASP!  I blinked a few
times, took a deep breath and wrote a reply to his message explaining to
him how to go about changing his password.

     Afterwards, I got to thinking that maybe it was because of my position
here on GEnie that he assumed it would be okay to give me his password.  Or
maybe it was just an oversight.  Regardless of the reason, it brings up the
point that it can happen.  Don't let it happen to you.  Never, I repeat,
_NEVER_ let your password out into the hands of someone else!  There is
absolutely _no_ reason what-so-ever for anyone but yourself to know your
password.  Period.

     Sooooo.... how long has it been since you've last changed your
password?  Last week?  A month ago?  Never?  If it has been awhile, perhaps
today would be a good day to change it.  Odds are your account will never
be the victim of some unscrupulous member, but why take chances?  Changing
your password takes only a few seconds to do, plus, it is part of your
GEnie*Basic package.

     To change your password, type SET or M900;2 at any GEnie main prompt.
You can use any character from A through Z, all digits from 1 through 9,
asterisks, periods and dollar signs.  GEnie will ask you to enter your old
password, then to enter the new password.  It will then ask you to re-enter
your new password for verification.  Pick a password is easy for you to
remember, but next to impossible for someone else to figure out.  Commit it
to memory or write it down and put it in a _secure_ place.

     If you are using Aladdin while on GEnie, changing your password is
even easier.  Just choose "Change Password," follow the prompts and let
Aladdin do the dirty work.

     Take the time and effort to do this.  Your pocketbook may thank you
someday!


                     >>> GEnieLamp Odds & Ends <<<
                     """""""""""""""""""""""""""""
 
Standing Ovation   Tom Schmitz, the chief editor of the Apple II/A2PRO
""""""""""""""""   GEnieLamp, has informed us that he's accepted a large
promotion at his daytime job and will consequently have to relinquish his
role as chief editor of the Apple II/A2PRO GEnieLamp.  Since April, 1992,
Tom coordinated the production of the first seven Apple II/A2PRO
GEnieLamp's, setting high standards in every facet of his work.  We will
deeply miss Tom's pioneering leadership.

    Taking over as chief Apple II editor is frequent GEnieLamp contributor
Darrel Raines [D.RAINES].  Darrel has broad interests in the Apple II, and
hopes to give the Apple II GEnieLamp his own signature leadership in the
months ahead.  Persons interested in contributing articles to the Apple
II/A2PRO GEnieLamp are requested to contact co-editor Phil Shapiro
[P.SHAPIRO1] until Darrel familiarizes himself with the GEnieLamp ropes."
-Phil Shapiro
 

THOUGHT YOU SHOULD KNOW...   I downloaded the new (ALADDIN) 1.62 version
""""""""""""""""""""""""""   from a local BBS. After that I had all kinds
of problems, like it would download a long time and when I went to read the
messages, there was only one available to read. Another time it downloaded
a few messages and gave the 'end' beep and said I've been idle too long.
Another time I didn't get me mail at all even though I had 4 letters
waiting.

     So, last night I downloaded 1.62 again, this time from the Aladdin
library and everything seems to be working the way it is suppose to now. So
I want to warn people, sometimes you may get a bad download especially if
it is from a local BBS, it may have had a bad upload if you know what I
mean...  -Leska  [V.WRIGHT]

                               [*][*][*]

     Have you ever wondered what it's like to be a SysOp?

To the tune of "White Rabbit" By Jefferson Airplane...

      Some folks like the BB's,
      And some folks like the Mall.
      And the ones who start up flamewars,
      Don't like anything at all.
      Go ask Sysop, when he climbs the wall.
     
      And if you go, chasing lurkers,
      And you know you're going to fall.
      Tell 'em hookah... smoking modem,
      Has given you the call.
      Poor Sysop, he deals with it all.

      When the posts in the deadmail,
      Get up and tell you where you go.
      And you've just had some kind of linenoise,
      And your node is moving on...

      Go ask Sysop, I think he'll know.
      When the pointers, and topic markings,
      Are lying sloppy dead.
      And the cursor is typing backwards,
      And you can't read what you've just said.
      Remember, what the Doormouse said,
      Park your head... park your head.

     (Reposted with the author's permission)
                  (UHH.CLEM, CAT30, TOP27, MSG:36/M470)

                               [*][*][*]

  Until next month...

                                                 John Peters
                                             GEnieLamp E-Magazine


         ///////////////////////////////////// GEnie_QWIK_QUOTE ////
        / "I've visited Tim in his computer room - we had to take /
       / turns breathing.  :)"                                   /
      ////////////////////////////////////////////  A2.BILL  ////


 
[EOA]
[HEY]//////////////////////////////
              HEY MISTER POSTMAN /
/////////////////////////////////
Is That A Letter For Me?
""""""""""""""""""""""""
by Thomas M. Schmitz
       [TOM.SCHMITZ]

     o   Apple II ODDS & ENDS 
 
          o   WHAT'S NEW?
 
               o   THROUGH THE GRAPEVINE 
                         
                    o   APPLE HEADS WANT TO KNOW 
 
                         o  MESSAGE SPOTLIGHT 
 


                      >>> Apple II ODDS & ENDS <<<
                      """"""""""""""""""""""""""""
 
CALL TO ARMS!!!!!!!   According to some reports I've seen Apple won't be
"""""""""""""""""""   distributing version 7.1 of the Mac operating system
via bulletin boards--such as GEnie-- or user groups.  While its not been
stated that sounds like Apple is going to charge for system 7.1.  I
strongly recommend you call Apple and tell them that you don't want to have
to pay both a premium for their hardware and for relatively minor updates
to the System software.

     I'm not suggesting that Apple should be a charitable institution.
However I do feel that Mac owners should be able to get the latest version
of the basic operating system for free.  So it's ok in my mind for Apple to
charge for new functionality such as DAL or AppleScript-- although charging
for AppleScript will probably keep it from being accepted due to the high
costs for developers to redo their software to support it-- but they
shouldn't charge us for a .1 version upgrade.

     Further I don't expect Apple to provide printed documentation, media,
or support for those who get the latest system via an electronic, ie free,
channel.  It's perfectly reasonable for Apple to charge for tech support
once you're Mac is out of its one year warranty period.   In any case
you'll probably get all the support you need here on GEnie.  The Apple
support people are good but Apple can't afford the type of people who
provide answers here out of the kindness of their hearts.

     Finally I would be willing to pay for system 8 if it had enough new
features and/or improvements to warrant the cost.  So start writing now
folks let Apple know that they can't charge more for the hardware and also
charge for the basic system software.  -Tom Schmitz

 
HELP!!!!!   Denver, CO (PNS)  Jawaid Bazyar of Procyon, Inc. reported
"""""""""   today that an AWGS gone amuck caused non-trivial damage to
Procyon's customer database.  Since he seemed about to have a breakdown of
some sort, this reported offered a suggestion- ask around and people would
probably help in the reconstruction effort.  At that point, he put the
grenade down and wrote the following for me to publish, in this fine
gazette:

"Dear Valued GNO Customers, unfortunately a certain sequence of events
occurred that caused us to lose our records for customers with GNO serial
number from 000160-000183, and from 000201-000220.  If you're one of these
good folk, please send me (via email, GEnie: Procyon.Inc) the following
information to help us reconstruct our database.

 Name
 Address
 City, State, Zip, Country
 Phone
 GNO Serial Number

     We're going to be mailing announcements of our new products soon, and
we don't want anyone to miss out!"

     After Mr. Bazyar handed me this note, he proceeded to tell me all
about the Unicorn collection at the Zoo, and how he loves to visit them
wearing anti- radiation DEVO clothing.  White-clothed men then came to
assist Mr. Bazyar away for treatment.

     [p.s. This really happened, and we really need your help! ]

 Matt Gudermuth
 Procyon News Services
                  (PROCYON, CAT30, TOP5, MSG:1/M530)
 


                           >>> WHAT'S NEW <<<
                           """""""""""""""""" 
              >>> A2 UNIVERSITY RETURNS BETTER THAN EVER <<<
              """"""""""""""""""""""""""""""""""""""""""""""

IT'S BACK!   A2 and A2Pro's educational arm, A2Pro, has been undergoing
""""""""""   serious renovation during summer vacation -- and now it's
back, bigger and better than ever!

     Our new A2U Dean, Professor Steve Gunn, has been renovating the
university all summer.  You'll now find A2U in A2Pro, where the programming
discussions it spawns can peacefully coexist with all the other programming
discussions with programmers of all skill levels and interests.  A2U lives
in the A2Pro bulletin board as category #18, and also in A2Pro's library
#18.

     There you can find all the past A2U courses plus sign-ups and
discussion for two of the most exciting courses we've been pleased to
offer.

     Starting September 15th, Professor Will Nelken (professor for a
previous A2U course on UltraMacroWorks and author of numerous Ultra
reference materials, including columns in A+/inCider) starts a brand-new
12-part course on JEM Software's latest, most powerful offering to date:
Ultra 4.0.  "Ultra 4.0 to the Max!" can teach _you_ how to squeeze all the
power possible from this fantastic new offering.

     And starting in October, Professor Andy McFadden, author of NuLib and
YankIt archiving programs -- both of which deal with ShrinkIt (NuFX)
archives and often do so faster than ShrinkIt -- begins a fantastic course
on data compression.  If you as a programmer have ever wondered how
compression works, how specific compression algorithms work, how to choose
the right compression for you or how the most popular Apple II compression
schemes work in theory or in practice, this course is for YOU!  Prof.
McFadden's course starts with how compression works and continues, in
English and with sample code, through some advanced topics in byte
squeezing.

     How much does it cost?  Nothing more than the time it takes to read
the messages and download the files -- it's part of A2 and A2Pro's service!
No additional charges whatsoever!

     Stop by new A2Pro category 22 and take a look around, and sign up for
more Apple II programming knowledge from A2 and A2Pro -- _THE_ place to be!
               (M.DEATHERAGE, CAT1, TOP17, MSG:29/530)
 
What Is A2 University?   A2 University is a special area in A2PRO that
""""""""""""""""""""""   provides for programmers and non-programmers
alike to increase their programming skills via free classes that are taught
online. A2 University (A2U) has the following goal: To increase the number
and the quality of programmers in the Apple II world. For you see, that is
the only way that we can prolong the life of the Apple II.

What is Involved?   A2U was designed to serve the needs of all levels of
"""""""""""""""""   programming experience. Whether you want to get
started in basic, or optimize some of your assembly routines, A2U is the
place for you. Four times a year, we will have a course registration for 2
weeks right before the next term starts. In this time you will need to
register for any courses that you wish to study. Please note that there is
no extra charge to participate, but we do need you to register.

     Then when the course begins, you will be able to download the weekly
lessons that are uploaded by your professor, and attend the RTCs held here
in A2PRO to ask them questions live. There will also be topic available
here in the Bulletin Board to ask questions if you are unable to attend the
conference.

How Do I Register?   When each teaching term in A2U is about to begin, all
""""""""""""""""""   students who have sent in a general A2U registration
form will receive a short Email reminder as to what courses are being
taught this next term, and how to register. There will also be plenty of
reminders on the front door of A2PRO when the registration period is going
on. Simply fill out the registration form in the registration Topic for
each course that you want to participate in, and post it as a reply in that
topic. That simple.

How Do I Get Mailings?   In this category, there is a topic called
""""""""""""""""""""""   "Student Directory" which contains a form. When
you fill out this form, you will receive Email announcements from A2U.
These will announce new courses, new Professors, important information etc.
Please note that you must fill out this form to be placed on the mailing
list, but you only have to register for a course to participate.

What Happens When The Course Is Over?   After each 12 week term in A2U,
"""""""""""""""""""""""""""""""""""""   the messages concerning each of
the courses will be archived and uploaded to the A2PRO library. Then a
topic will be started for the discussion of the Old Course. These topics
should be used for any discussion of A2U courses that are not currently
going on, and will contain all information on what is required to take the
course.

Who Teaches The Courses?   A2 University Professors are volunteers that
""""""""""""""""""""""""   teach the classes. The classes that they teach
are of their own creation.  Their only compensation is a Free Flag in both
A2 and A2PRO for the duration of their course. If you are interested in
teaching a course, please send a letter to that effect to me (A2PRO.STEVE)
and carbon copy it to A2PRO$. Then we will let you know what is involved
and get you started on your course!

Is There Anything Else?   Well, in addition to the time here on GEnie,
"""""""""""""""""""""""   some courses may require a textbook, or a
special piece of software. But most of the courses will use the "standard"
tools for Development. If you have a compiler for the language you will be
using, the Toolbox Manuals, and the GS/OS manuals, then you are probably
set to tackle most of the courses offered in A2U. But there have been
courses that required only AppleWorks and TimeOut UltraMacros. Each course
is unique, and its requirements are equally unique.

Closing   Thank you for reading this, and your interest in A2 University.
"""""""   We hope to see your face in a classroom next course session.

How To Take an A2U Course   Taking an A2U course here on GEnie is actually
"""""""""""""""""""""""""   very simple. All you need to do is say that
you want to take it. Then you can participate in several areas.  All of
which are _OPTIONAL_. I suggest that you download all of the lessons for
the course you are interested in.  It usually helps to have them around.
Plus, the Bulletin Board (Right Here!) is another great place to ask
questions.  Then finally, each class will have an RTC (Conference) that you
can attend to ask questions in real time! And that is about it! Take time
to post a message about yourself in Topic 4, and there is nothin else that
you need to do! You are a full fledged A2U Student.  -Steve
 
Fall Term Courses   I am happy to announce that we will have two new
"""""""""""""""""   courses for the Fall Term here in A2U. They will be on
Ultra 4.0 (Will Neklen) and Data Compression (Andy McFadden). Here are the
topics that have been created for these courses, post anything pertinent to
the course there.

   22   ULTRA 4.0 - To The MAX!         (Ultra 4.0 Course)
   23   A pinch of Data Compression     (Data Compression Course)

-Steve  A2 University Dude 
                (A2PRO.STEVE, CAT22, TOP2, MSG:1/MA2PRO)

 
 
    A2PRO COMPANY SUPPORT GROWS WITH GS+ MAGAZINE AND JEM SOFTWARE
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
     The list of companies providing direct support for programmers in
A2Pro grows to five with two sterling new Apple II developer-supporting
companies opening their own categories and libraries in A2Pro.

     GS+ Magazine, published by EGO Systems of Tennessee, is famous for the
with, technical information and no-holds-barred style of the publishers and
authors, mostly Steve Disbrow and Joe Wankerl.  Steve and Joe have opened a
new category in A2Pro's bulletin board and a new library (both #33) to help
programmers who want to use GS+ utilities, or maybe who want to write and
become famous by publishing in GS+ Magazine!  Check out category and
library 33 to learn more about GS+ Magazine.  Steve and Joe have also
opened a category in the A2 bulletin board (on page 645) for non-programmer
concerns, such as subscriptions and back issues.

     JEM Software, Randy Brandt's AppleWorks super-charging company, has
just released the most powerful AppleWorks 3.0 enhancement ever, Ultra 4.0.
Not only does A2U offer a course on how to use this fantastic product --
JEM Software and Randy Brandt are now in Category #34 of A2Pro to support
Ultra programmers as well as programmers who use all kinds of JEM products.
Drop by and have a look -- if you want to know how to make AppleWorks do
more for you than before, this is for you.  A2Pro supports all kinds of
programmers, from assembly language to HyperTalk to Ultra 4.0!

     Please join A2Pro in welcoming these fine companies to our online
family.  Drop by their bulletin board categories and say hi!
             (M.DEATHERAGE, CAT1, TOP17, MSG:30/M530)
 

MORE APPLE HISTORY   If you caught (or will catch, depending on how you
""""""""""""""""""   are reading these messages today) my post in Cat 3,
Top 4 (New Uploads), I've uploaded five more pieces of the Apple II History
(finally!)  These:

     #19320    Apple II History, Part 18 -- Software
     #19339    Appendix A, Part 1 -- Software 1977-80
     #19340    Appendix A, Part 2 -- Software 1981-82
     #19341    Appendix A, Part 3 -- Software 1983-86
     #19342    Appendix A, Part 4 -- Software 1987-92

     Now, even though I've uploaded an appendix, that does not mean that I
am finished with this puppy yet.  Actually, I have several more segments to
polish up and add to, but the appendix that dealt with software seemed
appropriate to upload at this time.  Also, I had another reason for
uploading it right now.

     The appendix, in its four parts, gives an annotated listing of popular
Apple II software that was released over the years.  For the years
1977-1983, this is taken directly from the Softalk Top Thirty lists
published each April (and voted on by their readers).  Many of the
descriptions that go along with each program are also taken directly from
Softalk's "Fastalk" column.  In some cases I added comments of my own, or
created comments where Softalk never got around to it.

     The place where I find I need help is in the years after 1984.  I
have had to rely on posted ads for the various software packages during the
years late 1984-1992, since no magazine has taken on the task of compiling
a list of top software on a yearly basis since then.  Consequently, the
list is rather scarce during some years.

     What I would like from you, the users of A2, is this:  If you have an
interest in reviewing the lists in the Appendix parts and see if there are
either errors (author's name misspellings, program name , publisher, or
description) or omissions, I would be most pleased to know so I can make
the list better.  If you know the name of an author that I have not
included, please let me know.  I feel that I am particularly weak on the GS
side of software, not having gotten into the 16-bit world until about 2
months ago.

     I don't feel that I can include EVERY piece of software that has ever
been released for the Apple II, but if there is anything that was clearly a
popular program, or an innovative program, that I have missed, here is your
chance to make me aware of it.

     What I probably cannot do is make the appendix files, in toto,
shorter.  Trying to find a way to present information about the gargantuan
pile of programs that have come out over the years is part of what made
this part of the History take SO-O-O-O long to get out.  I've had
information for Parts 19-22 actually completed for months, but I had this
stumbling block of how to organize this stuff that slowed me down so
badly.

     If the info presented in Appendix A (parts 1-4) is already
acceptable, I would appreciate hearing about it also.  Oh, yes; the stuff
for 1991 and 1992 I did more annotation on, since it is current an d still
available.  If there are some other programs recently released that I
should include, please tell me about that also. Steve Weyhrich  <IX0YE>--<
     (S.WEYHRICH [Historian], Cat. 2,  Top. 16, Msg.  114, M645;1)


SOFTDISK PUBLISHING ONLINE IN A2PRO   Softdisk Publishing, creators of
"""""""""""""""""""""""""""""""""""   Softdisk and Softdisk G-S magazines
on disk, now have their own online support category _just for programmers_
in A2Pro!

     Softdisk has long been a valued contributor to A2 and A2Pro, and they
continue to provide customer service and support for the subscribers in
A2, as they have for a long time.  But now, here in A Pro, Softdisk has
support for you, the _programmers!_

     Want to know how to submit to Softdisk?  You should, because Softdisk
pays good money for your work -- sometimes more than you can get from
shareware fees.  Do you subscribe and admire the produce on values?  You can
find submission guidelines that describe all these things in Softdisk's
new A2Pro category and library, both #31.

     We're pleased to welcome Softdisk to a formal level of support for
programmers and potential submitters here in A2Pro.  Look for even more
companies supporting programmers here in A2Pro -- the development
information you want all in one convenient place!  Drop by category 31 and
check it out!
       (M.DEATHERAGE [A2Pro Leader], Cat. 3,  Top. 7, Msg. 9, M645;1)


WARP SIX NOW PD   Warp Six BBS is now in the public domain, with the
"""""""""""""""   release of public domain version 1.0, right here on A2 as
file number 19178, filename W6BBS.BXY.

     I'm not going to completely stop working with the system, but I plan
on spending a lot less time with it. For this reason, I felt it would be
best to place it in the public domain.

     For those interested in the source code to the modem drivers, I'll be
uploading those to A2Pro very soon. You will need Merlin to edit and re-
assemble the SSC.Driver, and Merlin 16 or Merlin 16+ to edit and
re-assemble the GS.Hski and GS.GPi drivers. The Xmodem module and Xloader
can be assembled with Merlin 8. (If you have Merlin 16 or 16+ you can
assemble the lot.)

     Best wishes in the future and with this message I'm departing from
this topic.   -Jim Ferr, creator of Warp Six BBS.
            (Category 41,  Topic 10, Message  51, M645;1)


ASTRONOMER GS V2.3.2 IS HERE!   I just complete the upload of the latest
"""""""""""""""""""""""""""""   version.  With luck (and the Sysops help)
it should be available on-line very soon.  Along with that, I uploaded two
other separate files that will 'tell you about the program.  First, for
those who don't have the program the file "About Astronomer" in the
Education library contains a text file and some screen snapshots showing
what the program screens look like.  The text file explains what the
program's capabilities are.  The second file, Astro..Changes in the same
library will describe the changes since version 2.3.1. Since the new
version corrects several bugs everyone should update as soon as possible.
             (L.BELL13 , Cat. 13,  Top. 14, Msg. 11, M645;1)

 
MDG PROGRAMMERS EXCHANGE   The Morgan Davis Group is proud to unveil the
""""""""""""""""""""""""   MDG Programmers Exchange.   Many of our
products are geared toward software development for both 8- and  16-bit
Apple II platforms.  This is the place to go when you have questions  about
working with our tools.  Use the TOPIC command to get a complete  listing
of c 1 is for MDG news and announcements.  Here, you'll find notices about
new products, upgrades, new uploads in our software library, and so on.

     Topic 2 contains summary information about our entire product line,
including pricing, order information, etc.

     The first message in each product topic is a press release describing
current features and availability.

     The last topic is the MDG Hackers' Lounge.  Post messages here that
are not appropriate in any of the other topics.

     In the MDG Software Library, you'll find files related to using
Morgan  Davis Group development products.  Uploads consist of either binary
programs and data files, or lengthy text documents describing some aspect
of programming with our software.

     Occasionally, we will upload "freebie" software -- utilities to help
around the hard disk.  We also have a few demos of some of our titles, like
MD-BASIC.  With ten minutes of informative and humorous running time, the
MD-BASIC demo is really fun to watch.

     Thank you for joining the MDG Programmers Exchange here on GEnie.
Enjoy!    
  /\/\
 / /__\  Morgan Davis (M.DAVIS42, CAT32, TOP1, MSG:1/2/M530)
 
 
FOUNDATION IS BACK   Well Folks, I'm finally back here on GEnie and you're
""""""""""""""""""   going to start seeing my face (so to speak) around
here to answer any Foundation related questions and keep you up to date on
Lunar Productions.

     To start things off, here is the current state of Foundation:

     We released it!  Yes, version 1.0 was released at KansasFest, but...
(you knew there was a but :) ...at the last minute we decided to pull
ScriptEdit from the release.  We couldn't convince Uncle DOS or Event
Specialists to postpone KansasFest for a month, and Jim "no sleep" Murphy
gave it his best shot anyways.  The bottom line is that Foundation 1.0.1
will begin shipping in a couple weeks with a VERY nice ScriptEdit module
(including the dynamic ScriptBuilder!)  Anyone who has already purchased
Foundation will get 1.0.1 as a Free update (please keep those registration
forms coming :)

     Foundation 1.0.1 is $79.95 and can be ordered directly from us at:

     Lunar Productions 1808 Michael Drive Waukesha, WI 53186 414/549-9261
(evenings please - central standard time)

     Marc Wolfgram Lunar Productions -
 


                     >>> THROUGH THE GRAPEVINE <<<
                     """""""""""""""""""""""""""""
           ___
GSP CARD    |om, like the GSP Card for the Apple II that was rumoured to
""""""""    be in development by Apple, the Apple /// on a card (codenamed
"Rub a dub dub") is actually in development by a ///rd party.  A prototype
is expected to be shown off running one of the cash registers and acting as
a coin-op videogame simultaneously at PizzaFest.  (:  -= Lunatic     (:
            (LUNATIC, Cat. 44,  Top. 11, Msg. 33, M645;1)


ANNOUNCING: THE MACRO PROJECT (?)   Several months ago I floated the idea
"""""""""""""""""""""""""""""""""   that we macro writers consider forming
some sort of association or alliance for the purpose of promoting
macro-based programs, exchanging information and ideas, etc.  Although some
expressed a general interest in the idea, I did not find  much enthusiasm
for a formal association, largely because of the bad taste left in all of
our mouths by the untimely demise of the AppleWorks Programmers
Association.

     While I agree that there may not be a need or a desire for a formal,
dues-paying organization like APA, I still believe that those of us who
write macros for commercial consumption could take som eps to increase the
visibility of the AppleWorks/UltraMacros combination in  general and to
promote our own products in particular.

     As independent programmers writing for a necessarily limited (and
perhaps dwindling) universe (those who use AppleWorks _and_ UltraMacros),
I believe we need to make special efforts to reach our entail customers. An
ad in, say, inCider (aside from its cost) may not be the most effective
marketing tool because many of the Apple // users who would see it would
not be UltraMacros owners. Advertising in NAUG's AppleWorks Forum might be
more effective in reaching the targeted audience, but only a small
percentage of AppleWorks users are NAUG  members, and again, many of them
either do not own UltraMacros or do not have enough memory or disk storage
space to use some macro programs  effectively.

     Most marketing experts will tell you that the best potential
customers for most products are people who have previously bought a
similar item or an item carrying the same brand name. That is, the people most
likely to buy our macros are those who have already bought our macros --
or someone else's macros.

     With that idea in mind, I would like to propose what I call The Macro
Project.

     The Macro Project would produce, at least as its initial offering, a
comprehensive catalog of UltraMacros-based programs, task files, macro
sets,  useful macro tips, hints and/or instructions. The catalog would
provide sufficient space for each macro programmer to list and briefly
describe all the macro sets/programs/taskfiles that the programmer is
selling. The catalog would have a consistent format and would include a
table of contents listing offerings by programmer name and/or company name
(for example, Dan's Macro City would have a section, Marin MacroWorks
another, etc.). It would also contain an index of the individual macros or
macro sets.

     The Macro Project catalog would, at the very minimum, be mailed to
everyone on the combined mailing lists of all participating macro writers.
This could be handled in several ways. Either sufficient copies would be
provided to each individual so that he could mail his own list (which would
result in a fair amount of duplication) or our lists could be combined (and
the duplicates winnowed out). The details of how the mailing would work
would have to be handled through discussion and negotiation.

     I would hope that eventually we could obtain other useful mailing
lists from sources that have a high percentage of customers who would be
likely to own both AppleWorks and UltraMacros. The names companies that
might have those lists are fairly obvious. I have not contacted any  of
them yet, so the availability and/or cost of these lists is unknown. Of
course, all Apple // publications would b e notified of the catalog's
availability and asked to publish an address that anyone could write to
for a free catalog. I'm sure there are other distribution strategies that
we could explore.

The immediate goals of this project would be:

 1) To provide an appropriate and effective advertising medium for
    macro-based software.

 2) To promote and increase the visibility of the AppleWorks/UltraMacros
    software combination.

 3) To demonstrate the range of macro-based productivity software
    available.

 4) To establish an ongoing publication that can be updated at regular
    intervals.
 
     I know that there are a lot of issues to be worked out between
stating the concept and mailing out the finished catalog; I don't intend
to go into all of them here. I will state that I am willing devote my time
and energy to overseeing this project if there is sufficient interest
among macro writers.

     If the interest is there, I would like to see the project finished
before Christmas, so I need feedback on this soon. I would appreciate it
if the discussion of this could be kept public and in t forum so everyone
can read the responses. But if there is something you would  rather
express to me privately, please feel free to email me.  -=* Dan *=-
         (D.CRUTCHER [Dan], Cat. 17,  Top. 12, Msg. 84, M645;1)


APPLE CLINIC UPDATE   I was kind of sad to see that Cecil Fretwell will no
"""""""""""""""""""   longer be writing Apple Clinic in inCider, myself.
Fretwell was an Apple II pioneer, from the very earliest days.  His column
was always well written and accurate.  It's a shame he isn't doing it
anymore.

     On the other hand, I expect good things from Cynthia Field, so we'll
see I guess!

     In other news, I got word last week that Dan Muse had left inCider to
work at Byte.  At the moment the editing duties are being taken care of by
one Eileen Terrill, though it's not clear if that's to be permanent or not.

     What with a change of hands at Apple Clinic, and my new column, and
Matt Deatherage's new column, and with more columns for Joe Kohn, and a new
editor, inCider is starting to look like a whole new magazine!  Should be
interesting to see how much it changes with all these new faces and new
directions. -Dean Esmay
         (A2.DEAN [Chief Sysop], Cat. 28,  Top. 3, Msg. 38, M645;1)



                    >>> APPLE HEADS WANT TO KNOW <<<
                    """"""""""""""""""""""""""""""""

PIZZAFEST SUCCESS!   \/\/ell, the first Silicon Valley PizzaFest has now
""""""""""""""""""   come off, and I can say that it was quite a success.
In attendance were:

           Matt Deatherage
           Dave Lyons
           Andy Nicholas
           Matt Ackeret
           Andy McFadden
           Lunatic E'Sex
           Joe Kohn
           Jim Merritt
           Kent Keltner
           John Ferreira
           Moses Ferreira
           Shirley Hill

 (Vaguely in order of appearance)

    |\/|uch food was consumed, and much fun was had by all!

    |)
    |lans are already under way for another Apple II social get together
and hang out in the SF Bay Area, sometime next month.  It was agreed that
the next one will be held at a Chevy's restaurant (Mexican food), and that
the location will be farther up the peninsula towards San Francisco.  All
attending were highly enthusiastic about making it to the next one.

      _
     (_
     __)o, if you're in the Bay Area, or you're going to BE in the Bay
Area, near the end of September, and would like to go out to dinner with
this cool group of people, keep watching this space for further details!
                                                    -= Lunatic     (: 
          (LUNATIC, Cat. 44,  Top. 11, Msg. 42, M645;1)


From our Foreign Desk          PROTOCOL
"""""""""""""""""""""          (summary by Henrik Gudat)
Pizza sponsored by: Apple dealer U. Brunner Organizer: H.Gudat

     The first meeting in Reinach, Switzerland was a real success. 9
programmers (8 assembly language, 1 Pascal), the local Apple dealer and two
other interested people attended the "show". Besides exchanging information
and eating tons of peanuts, the most recent releases of some programs were
shown.

     The meeting ended in a pizzeria. Mr. Brunner, the Apple dealer,
expressed his support and belief in the Apple II by generously sponsoring
the complete evening. Thank you very much!

     The equipment - provided by H. Gudat - consisted of two Apple IIGS, a
52MB and a 20 MB HD, two 3.5" drives, one 5.25" drive, a Revox amplifier,
Bose 301 III speakers, and other stuff.

Present   Urs Brunner, Joerg and Valerie Kienzle, Yann Le Tensorer,
"""""""   Laurence, Urs Hochstrasser, Marc Schweizer, Andre Horstmann,
Michael Born, Andreas Furrer, Dominik, Henrik Gudat.

U. Brunner   confirmed his faith in the Apple IIGS, donated a 6.0 CD and
""""""""""   suggested interesting marketing strategies.

Joerg Kienzle   he demonstrated SpaceFox because one person didn't know the
"""""""""""""   program! (Okay, he played in the cheat mode)

Andre Horstmann   showed off the brand-new ShadowDial II 2.1. It's a
"""""""""""""""   Videotex/Btx decoder with great capabilities.
Unfortunately, there wasn't a modem connected to the GS so he could scratch
the surface only. What a shame! Andre added tons of new macro commands and
features supporting the new system tools. In addition you're now able to
copy graphic directly out of the Vtx/Btx screen into the system clipboard.

Michael Born and Andre   explained their new project. They are building a
""""""""""""""""""""""   hardware/software package that will allow you to
control any external device via phone (!) and much, much more. As far as I
understood the whole thing, it will turn the GS into a intelligent
telephone answering machine.

Andreas Furrer and Dominik   they started a demo with sensational
""""""""""""""""""""""""""   70-line-scrolling (!!!) and NoiseTracker
music. The quality was outstanding! Both had also some great ideas
regarding new programs. They are now working on a new action adventure
game with smooth animation.

Urs Hochstrasser   ...launched ChemiGS, a desktop application for molecular
""""""""""""""""   design. It's similar to an object oriented drawing
program but features some functions for "simulating" 3D structures. A prot
ocol between GSymbolix (see below) and ChemiGS is planned. (ChemiGS will be
released in 1993.)

Henrik Gudat   ...double-clicked on GSymbolix 1.7. This nearly-running
""""""""""""   prototype has been modified so that it supports 6.0
features ( especially the Sound CDev and menu icons). A short 3D animation
of a cosine could be seen as well as the built-in debugger (...which was
definitely better than GSBug...) The most important change is GSymbolix's
new capability of working with complex formulae. The program recognized
even complicated expressions (such as 4*i/3*i^3). A button bar _might_
also be part of v1.7. GSymbolix 1.7 will ship in a couple of months.

Marc Schweizer   demonstrated LinReg, a desktop program written in
""""""""""""""   ORCA/Pascal for linear regression. It worked flawlessly
but it is not yet finished.

New action game   Bright Software will soon start with a new, funny
"""""""""""""""   action/strategy game for IBM (by Yann Le Tensorer), Mac
(Joerg Kienzle), Atari (?) and IIGS (Andre Horstmann and Henrik Gudat). We
promise you: the IIGS version will be the best!

     There are also plans for an application-independent, object oriented,
graphic based programming language. Let's face it when the new game hits
the shelves...

     To make a long story short, this afternoon was plain fun. Obviously
we all spoke the same language (except for one - Urs is using APW -
uuuuhhhh).  Though the evening ended with a lot of confusion regarding
negative fixed values, I'm about to call this meeting the Swiss KansasFest.

     And next year I'll take a big pizza. -Henrik

PS  for more info, please contact Bright Software, P.O. Box 18, 4153
""   Reinach 2, Switzerland.
           (A.HORSTMANN, Cat. 13, Top. 13, Msg. 13, M645;1)


Ultra Macro Printing   A while back I saw a message requesting a macro
""""""""""""""""""""   that would print all of the odd pages in a
document, then pause to allow the paper to be turned over, then print the
even pages.  I also needed to print on both sides of the paper and, unable
to find a ready made macro, I was "forced" to write my own.  Actually there
are two macros; one for the odd pages and one for the even.


 <ba-O>:<awp                            {macro to print odd pages}
      oa-1:                             {move to beginning of file}
      oa-k rtn:                         {calculate # of pages in doc}
      oa-9:                             {goto last page}
      up:                               {move to status line}
      P = peek $00b4:                   {get last page #}
      C = 1:                            {start page counter @ 1}
      begin:                            {start loop}
         oa-1:                          {move to top of file}
         oa-P>P<rtn :                   {go to printer options, select page}
         print C: rtn :print C: rtn: rtn: {print the page in C}
         C = C + 2:                     {increment counter by 2}
         if C > P then exit             {check for last page}
 else rpt:!                        {end loop}


 <ba-E>:<awp                            {macro to print even pages}
      oa-1:                             {move to beginning of file}
      oa-k rtn:                         {calculate # of pages in doc}
      oa-9:                             {goto last page}
      up:                               {move to status line}
      P = peek $00b4:                   {get last page #}
      C = 2:                            {start page counter @ 2}
      begin:                            {start loop}
         oa-1:                          {move to top of file}
         oa-P>P<rtn :                   {go to printer options, select page}
         print C: rtn :print C: rtn: rtn: {print the page in C}
         C = C + 2:                     {increment counter by 2}
         if C > P then exit             {check for last page}
      else rpt:!                        {end loop}

     I hope that someone will find these useful.  Although these macros
are not very long (or elegant) they took several hours of head scratching
and reading the excellent Ultra Macros series by Willen.  Enjoy! Karl R.
           (K.RONEY [Karl],Cat. 15,  Top. 35 Msg. 85, M645;1)


Quiet Your MDIdeas Card   re long cables on the MDIdeas card,  I had the
"""""""""""""""""""""""   same problem basically the thing wants to
oscillate,  so on the MDIdeas end I put a 3 ft cable before the 20 ft
cable, and inbetween a little RC net work.  I _think_ I put about a 100 pf
cap across the line on the MDIdeas side,  and about a 100 ohm resistor in
series with the line. Used a little piece of perfboard.

     Worked fine then.  This is my standard procedure for lines that go
from a computer into the real world.  Actually a .01 cap will just round
the edges of a 10khz square wave,  and it kills noise dead. Violates all
kinds of people's sensibilities, but works.   :)
        (J.IMIG [Bit Picker] , Cat. 6,  Top. 7 Msg. 155, M645;1)


DRIVE TALK   Yes the Vulcan is IDE and you can replace it with any Quantum
""""""""""   or Connor mechanism and I would expect the Maxtor drives to
work as well.  Current state of the art IDE drives use a block translation
and are not designed to require an initialization routine to know the
parameters of the drive.  For example the Quantum drives will allow the
interface card to setup an arbitrary number of heads/cyl inders/sectors per
track. The drive simply stores these parameters at initialization time and
when the interface card issues a read command the drive will convert the
head/cylinder/sector number using the initialization parameters over into a
logical block number.  From the logical block number the drive will convert
over into a head/cylinder/sector number that fits with it's drive
parameters.  It has to be done this way because current state of the art
drives have variable number of sectors per track.

>>>>    Flopticals.....   We are looking into it and we make no promises
""""    at this time.  Further badgering on the subject will not be taken
in a positive light.


AS FAST AS IT GETS   The RamFAST does DMA at 1meg/sec.  This is as fast as
""""""""""""""""""   the IO bus can handle.  The RF could actually DMA at
about 1.7 meg/sec but the IO bus can't handle it so the RF runs as fast as
the IO bus can handle.  We read the drive at 1:1 interleave which is as
fast as the drive can manage.  The RamFAST is as fast as it gets.  It is
theoretically possible to make a DMA interface that plugs into the Zip or
the processor socket and is capable of DMA at 2.6 meg/sec (2.59xxx
actually) but it would not be compatible with the TWGS so we haven't
pursued it.  -Drew
     (CV.TECH [Tri.Stated], Cat. 11,  Top. 10, Msg. 77, M645;1)

TAPES   The mechanism in question is a 3M mechanism and it uses the DC-2000
"""""   tapes.  I've got one here and we use it for testing.  The tapes
will run you about $14 ish a piece in small quantity and the performance of
the drive is about 1.1 meg/sec.  A couple of drawbacks to the drive
include:

    o  The tapes only hold 40 meg (actually about 38000k)
    o  The tapes have to be formatted before use and that takes over half
       an hour per tape.
    o  The drive is slightly finicky about power consumption so make sure
       it comes with a box and power supply.  If you put it together
       yourself you will need at least a 60 watt power supply.
    o  The drive takes awhile to recognize a tape.  When you insert a
       tape the drive "goes away" for a couple of MINUTES.

Advantages of the 3M mechanism:

    o  It is the only tape drive I know that can do random access. This
       means that the RF card can tell the drive to go out into the tape
       and find block number xxx and read it.  The tape drive copes.
       This is a great improvement when it comes to file based restores
       using the RF.  -Drew
      (CV.TECH [Tri.Stated] , Cat. 11,  Top. 12, Msg. 82, M645;1)



SCRIPT-CENTRAL SUBSCRIBERS   A quick note to Script-Central subscribers....
""""""""""""""""""""""""""   I am sorry to say that I left two bugs in the
Doctor Who stack. Until the Companions and Villains Stacks are provided, do
_NOT_ select Slide Show from either the Companions or Villains popUp
menus.  If you wish to fix the 'bugs' you must add three lines to the
slideShow handler of both the All Companions button and the Some Villains
button.  The three lines are as follows:

   on slideShow
    checkIt                         --  <---- add this Line 1
    global F                        --  <---- add this Line 2
    if F is "NO" the exit slideShow --  <---- add this Line 3
    global STK

     If you do not wish to make these changes, the 'problem' will
disappear when the auxiliary stacks are provided. -Joseph
      (J.WEEKS4 [IANAN,IAAFM!], Cat. 23,  Top. 8, Msg. 101, M645;1)


AE HIGH-DENSITY 3.5 DRIVE INCOMPTIBLILITIES   We are indeed aware that
"""""""""""""""""""""""""""""""""""""""""""   there are incompatibilities
with the AE high-density 3.5" drive. We currently are working on an upgrade
to Salvation-Deliverance, Salvation-Renaissance, and Salvation-Bakkup.
However, our department has not yet been informed about the specifications
of these application. I do know that the new versions will be supporting
Apple's 1.44 MB high-density drive. I can't yet say that we are, or are
not, supporting the AE high-density 3.5" floppy disk drive.

     As soon as we know what the specifications are, we'll let everyone
know.  Thanks, Lowell Erbe Vitesse, Inc.
           (VITESSEINC., Cat. 40,  Top. 13, Msg. 39, M645;1)
 

LOOKING FOR...
""""""""""""""
 > Can anyone suggest or recommend software that will generate invoices and
 > statements?

     Sure.  First, there's an add-on of some sort for AppleWorks; I'll bet
someone's already posted that information.

     Myself, I like DB Master Pro for its flexibility.  And, with Barney's
going- out-of-business sale, it's fairly inexpensive.  (BusinessWorks is
fairly expensive, if I recall.)  I uploaded to the Library a demo of a
relational inventory/invoicing system for DB Master awhile back; all you
have to do is add the program itself :).

     If your group went that route, Bill, I could probably modify my
system to its needs, gratis, if it's not too involved.  You didn't tell us
anything about what the requirements are:  Is this for inventory,
fund-raising and contributions, or something else?  Does the system have to
add interest on an automatic basis (like credit cards)?  What sort of a
system does the group have -- hard drive, ram , platform? <<<Lloyd>>>
     (L.DEVRIES [Lloyd], Cat. 2,  Top. 5, Msg. 60, M645;1)

 
MODEM DRIVERS   TO: Everyone who uses a USR HST or one of the new
"""""""""""""       SupraFAXModems with GBBS Pro or another ACOS-based
system.  Paul Parkhurst has recently completed work on a set of drivers
which will run any variety of US Robotics HST modem and the SupraFAXModem
v.32bis from either the GS serial port or the Super Serial Card.  The cost
for the complete set of drivers is $15.00.  If you want just the Supra
driver, the cost is $10.00.  He is waiting for 40 orders before he will
ship.  He can be reached by voice at 510-837-9098, or on his BBS, the
Infinity's Edge BBS, at 510-820-9401. This is the only way I know of to use
the SupraFAXModem v.32bis with GBBS, but I'm open to other suggestions.
BTW, what's the story on the guy who was working on FAX software for the
GS?  --> Dan  <via GEM v4.20>
         (D.BROWN109 [Dan], Cat. 10,  Top. 2, Msg. 166, M645;1)


WAIT, THERE'S MORE...   Matt's right about 6.0 and CPS Follow, but there's
"""""""""""""""""""""   more. I wrote the patches that Zip is distributing
(actually they had a contract programmer adapt the code I gave them), and
you only need the AppleTalk patch for pre-6.0 AppleTalk. Since slowing down
to 81% worked, I have to ask if you are in fact running system 5, because
81% and CPS Follow disabled should not (under 6.0) make the printer
reappear -- 30% or so would.

     I have my own version of the init that not only fixes AppleTalk for
5.0 but also fixes the "disappearing cursor" flicker and patches the GS/OS
SET_SYS_SPEED vector for Future Driver Compatibility (ooh ahh). I sent the
entire init to Zip, and it looks like their contractor didn't change
anything (although they did convert the source from Orca/M to Merlin). The
system 5 version of the init is called ZIPTALK, and when system 6 came out
I removed the AppleTalk code and called the resulting init ZIPFIX. Both
inits have been available on internet.

     What Zip's documentation probably still doesn't tell you: AppleTalk
delay was a last minute compatibility hack. It is really an IRQ delay that
turns off the acceleration for 5 milliseconds every time an interrupt
occurs. Zip did all their prototyping and beta testing with boards that ran
just slow enough for AppleTalk to keep working, and when they finally got
the 8 mhz parts from WDC with weeks to go before the release date, they
scrambled to find a way to keep from breaking AppleTalk. Since AppleTalk
Delay kicks in on any type of interrupt, enabling it really slows down the
Zip -- heartbeat interrupts happen 60 times a second (16 ms), and the Zip
slows down for 5 ms each time...  so roughly a third of the time (or more)
is spent unaccelerated. For this reason the AppleTalk delay should ALWAYS
be disabled and an init (or system 6) should be used. Besides, as you've
probably discovered, AppleTalk delay doesn't fix AppleTalk under system 5
unless you slow the Zip down a bit yourself. Blearg. This is because long
AppleTalk packets take 14 ms to send, and the AppleTalk delay only makes
things work for 5 ms -- so if you are sending data to the printer and
running the Zip at full speed under system 5 the first third of the packet
makes it OK and then the Zip comes back on and you're toast. You may be
noticing something like this now; you can see the printer, but actually
printing doesn't always work. That's because you don't need to use long
packets to see the printer, but you do need them to print to it.

     Likewise, CPS Follow should ALWAYS be on. Turning it off may make the
Zip run a bit faster, but you won't be able to use Disk ]['s (big loss) and
system 6's AppleTalk driver requires it, as Matt said. Other things won't
work, like border color animation demos, and the Normal Speed setting in
the control panel won't do anything. There are probably other compatibility
risks but I haven't run into them because I play it safe and leave CPS
follow enabled.

     I suppose I should upload these inits to the software libraries. Which
category would be the best one, or it is obvious?  -Todd Whitesel toddpw
          (TODDPW, Category 26,  Topic 12, Message 99, M645;1)


RULE #1
"""""""
 > the only thing anybody is going to use the MSDOS FST for is writing out
 > files that are intended for an MSDOS system (or a unix system that can
 > access MSDOS, like the one I use at work). NOBODY IN HIS RIGHT MIND IS
 > GOING TO REGULARLY USE MSDOS FOR REAL IIGS FORKED FILE WORK. Get real!!

     Rule #1 of designing software:  NEVER assume what people are going to
do with it.

     90% of all the problems I've ever seen users have with developer's
work come about on things where the developer says "People won't want to do
that. Get real."

     If the FST is writable, people will try to save files to it in
Standard File, and if the file has a resource fork the file will then be
immediately unusable to the application that just saved it.

     This was an early problem with the HFS FST -- it treated files of
type $B0 (SRC) as files of type TEXT, so they got HFS creator 'pdos' and
file type 'TEXT'.  The problem was that when you read them, they had GS/OS
file type $04 (Text).  It had the advantage of making sense, and letting
Macintosh applications read the files with no tweaking.  It had the
disadvantage that if you copied an ORCA/C source file to an HFS partition,
ORCA/C would no longer compile it (wrong language type).

     Either it acts like a disk or it doesn't act like a disk.  If it
doesn't act like a disk, it shouldn't be an FST.  Anything less is imposing
even more "rules" for IIgs users to remember, and this is absolutely
positively not the goal. --Matt (I speak for myself, not for Apple)
     (M.DEATHERAGE [A2Pro Leader], Category 9,  Topic 7, Message 59)


WE'VE MOVED!!
"""""""""""""
     Our NEW physical address is:
          Econ Technologies, Inc.
          99 N Central Ave Ste B
          Oviedo, FL 32765

     Our business ours are: 9:30 AM - 6:00 PM EST

     Our mailing address is:
          P.O. Box 195356
          Winter Springs, FL 32719
    (ECON [D. Proni], Category 35,  Topic 2, Message 2, M645;1)


WHAT'S A COMPUTER?   ][t's a person who uses a slide rule, pencils, and
""""""""""""""""""   lots of paper to make complicated calculations.  At
least, that's how they're referred  to in the Lensman series of book by
E.E. "Doc" Smith.  ( :


INTERESTING TRIVIA   The first manned spacecraft to include an electronic
""""""""""""""""""   computer was the Apollo.  All previous manned
spacecraft (Mercury, Gemini, and the Soviet craft) were controlled merely
by various combinations of simpler electronics and mechanical devices.
Kinda scary, when you look around these days and practically EVERYTHING has
a computer in it just to make it work the way you want it to.  -= Lunatic
(:         (LUNATIC , Category 2,  Topic 5, Message 89,  M645;1)

>>>>>   Don't forget the pigeon bomb control system.  Pigeons were
"""""   conditioned to watch a small screen and peck one of four buttons
when a certain shape came on the screen (circle, square, triangle, and
diamond).  If the rocket started to get off course, the navigation system
would flash a symbol on the screen and the pigeon would peck the correct
button to respond.  I don't recall if this navigation system was ever used,
but I saw a number of times the pigeon training program to "program the
navigation computer".  Skinner would probably have loved it.
 

 
                       >>> MESSAGE SPOTLIGHT <<<
                       """""""""""""""""""""""""
Category 2,  Topic 8
Message 8         Sat Sep 12, 1992
R.COVINGTON2 ["Baron"]       at 21:01 EDT

  
APPLE II WARS   The following commentary was aired on Sound Bytes, a
"""""""""""""   public radio show originating in Rochester, NY.  It is
copyright (c) 1990 by Nick Francesco.  Permission is granted to disseminate
it in any form, as long as the wording is not changed, and this copyright
notice accompanies it.

     DOS and Mac people have been at each other's throats since the
introduction of the Mac in 1984.  Which machine is better;  which machine
is more fun.  As religious wars go, it's somewhere in the middle:  worse
than the Crusades, but not as bad as the Inquisition.  No one on either
side has been willing to take prisoners, and Silicon Valley is littered
with its victims.  Like the Crusades, no one was ever really persuaded to
change sides, and like the Inquisition, people who did change sides did so
only under extreme duress.

     Up until now, of course, most of the ammunition has been on the Mac
side.  Lots of studies have come out about how easy it is to learn to use a
Mac, and how Mac people know and use more different types of programs than
DOS people.  No less prestigious a company than Microsoft (all rise) has
released the information that their support lines prove that the Mac is
easier to learn and use.  They provide less support per package sold on the
Mac side, proving that a Graphical User Interface is better.  Of course,
they didn't release this information until they had their own Graphical
User Interface on the market, but I'm sure it takes a long time to compile
the results of this sort of study.

     The DOS people had to content themselves with intangibles (it's
slower), and appeals to the compu-macho in us all (real users don't use
mice).

     Now, however, from the hallowed halls of the University of Delaware,
comes Dr. Marcia Peoples Halio (trumpet sound).  Dr.  Halio (no relation to
the graphics package, I'm sure) has released the results of a five-year
study that suggests that Mac people are shallower, more illiterate, and
less likely to have sex than DOS users.

     She did this by looking at the grades of a basic composition course.
You see, as each student entered the University of Delaware, he or she was
required to take a writing course in basic composition.  Each student was
also allowed to decide if he or she would rather use a Mac or DOS.  And
over five years, Dr.  Halio discovered that Mac people got lower grades,
picked shallower topics, and (gasp!!!) had more spelling errors than DOS
people.  The obvious conclusion?  DOS people are fine, upstanding, moral,
right-thinking people you would be proud to call your neighbor.  And Mac
people... well, you wouldn't want your daughter to marry one.

     Of course, this begs a few questions.  Do the Mac people start out
stupid, or is it something to do with the user interface?  Is there
something about a DOS command line that builds strong minds twelve ways?

     And what about a control group?  If you took a few of these DOS
Ubermenchen and put them in front of Macs, would they turn into drooling
idiots?  And if you could prop some of these Mac people up in front of DOS
machines, would they suddenly start speaking in complete sentences and be
able to get dates?

     We clearly need more study here.  We need to delve deeper into this
obviously fascinating mystery - I sense a Time-Life Books series coming on.
Do DOS people eventually burn out and buy Windows?  Do Mac people find
themselves reduced to pointing at pictures in the menus at Denny's?

     And what about their future?  Do DOS people end up becoming dry
academicians, arguing about Edwin Newman's latest column and living on
stipends while praying for tenure?  Do Mac people spend their evenings
standing around fern bars in double-breasted Armani suits, talking about
convertible debentures and vacationing in the Azores?

     I am prepared to devote my life to this study.  All I need is about
five million dollars from Apple Computer, and about twenty million from
Bill Gates (that's a fair assessment, based on their respective net
worths).  John, Bill, are you guys listening? I can be bought.  Uhh...
rented.  Hired.  You know.

     This is Nick Francesco for Sound Bytes, a production of WXXI in
Rochester, NY - Hub of Civilization in the Western World.
 
                               [*][*][*]


    While on GEnie,  do  you spend most of your time  downloading files?
If so, you may be missing out some excellent information in the Bulletin
Board  area.   The messages  listed above  only scratch  the surface  of
what's available and waiting for you in the bulletin board area.

If you are  serious  about your APPLE II,  the GEnie Lamp staff strongly
urge  you to give the  bulletin board area a try.   There are  literally
thousands  of messages  posted  from people  like you from  all over the
world.


      //////////////////////////////////////////// GEnie_QWIK_QUOTE ////
     / "Nathan, I imagine Lee caught onto the meaning of "enthisiasm" /
    / right away.  He's very enthisiastic himself!"                  /
   //////////////////////////////////////////////  D.A.BRUMLEVE  ////



[EOA]
[HUM]//////////////////////////////
                    HUMOR ONLINE /
/////////////////////////////////
Operator From Hell
""""""""""""""""""
by Simon Travaglia



                   >>> B*ST*RD OPERATOR FROM HELL <<<
                   """"""""""""""""""""""""""""""""""
                               ~ PART 1 ~

     It's backup day today so I'm P.O.'d.  Being the BOFH, however, does
have it's advantages.  I assign the tape device to null - it's so much more
economical on my time as I don't have to keep getting up to change tapes
every 5 minutes.  And it speeds up backups too, so it can't be all bad.

     A user rings

     "Do you know why the system is slow?" they ask

     "It's probably something to do with..."  I look up today's excuse "..
      clock speed"

     "Oh"  (Not knowing what I'm talking about, they're satisfied)  "Do
      you know when it will be fixed?"

     "Fixed?  There's 275 users on your machine, and one of them is you.
      Don't be so selfish - logout now and give someone else a chance!"

     "But my research results are due in tomorrow and all I need is one
      page of Laser Print.."

     "SURE YOU DO.  Well; You just keep telling yourself that buddy!"  I
      hang up.

     Sheesh, you'd really think people would learn not to call!

     The phone rings.  It'll be him again, I know.  That annoys me.  I put
on a gruff voice

     "HELLO, SALARIES!"

     "Oh, I'm sorry, I've got the wrong number"

     "YEAH?  Well what's your name buddy?  Do you know WASTED phone calls
      cost money?  DO YOU?  I've got a good mind to subtract your wasted
      time, my wasted time, and the cost of this call from your weekly
      wages!  IN FACT I WILL!   By the time I've finished with you, YOU'LL
      OWE US money!  WHAT'S YOUR NAME - AND DON'T LIE, WE'VE GOT CALLER
      ID!"

     I hear the phone drop and the sound of running feet - he's obviously
going to try and get an alibi by being at the Dean's office.  I look up his
username and find his department.  I ring the Dean's secretary.

     "Hello?" she answers

     "Hi, SIMON, B.O.F.H HERE, LISTEN, WHEN THAT GUY COMES RUNNING INTO
      YOUR OFFICE IN ABOUT 10 SECONDS, CAN YOU GIVE HIM A MESSAGE?"

     "I think so..." she says

     "TELL HIM `HE CAN RUN, BUT HE CAN'T HIDE'"

     "Um. Ok"

     "AND DON'T FORGET NOW, I WOULDN'T WANT TO HAVE TO TELL ANYONE ABOUT
      THAT FILE IN YOUR ACCOUNT WITH YOUR ANSWERS TO THE PUURITY TEST IN
      IT..."

     I hear her scrabbling at the terminal...

     "DON'T BOTHER - I HAVE A COPY.  BE A GOOD GIRL AND PASS THE MESSAGE
      ON"

     She sobs her assent and I hang up.  And the worst thing is, I was just
guessing about the purity test thing.   I grab a quick copy anyway, it
might make for some good late-night reading.

     Meantime backups have finished in record time, 2.03 seconds.  Modern
technology is wonderful, isn't it?

     Another user rings.

     "I need more space" he says

     "Well, why don't you move to Texas?" I ask

     "No, on my account, stupid."

     Stupid?!?....  Uh-Oh..

     "I'm terribly sorry" I say, in a polite manner equal to that of Jimmy
Stewart in a Family Matinee "I didn't quite catch that.  What was it that
you said?"

     I smell the fear coming down the line at me, but it's too late, he's
a goner and he knows it.

     "Um, I said what I wanted was more space on my account, *please*"

     "Sure, hang on"

     I hear him gasp his relief even though he covered the mouthpiece.

     "There, you've got plenty of space now"

     "How much have I got"

     Now this REALLY *PISSES* *ME* *OFF*!  Not only do they want me to
give them extra disk, they want to check it, to correct me if I don't give
them enough.  They should be happy with what I give them *and that's it*!!!

     Back into Jimmy Stewart mode.

     "Well, let's see, you have 4 Meg available"

     "Wow!  Eight Meg in total, thanks!" he says pleased with his
bargaining power

     "No" I interrupt, savouring this like a fine red, at room temperature
"4 Meg in total..."

     "Huh?...  I'd used 4 Meg already, How could I have 4 Meg Available?"

     I say nothing.  It'll come to him.

     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagggggghhhhhH!"

     I kill me; I really do!

 +-----------+  Terminal Sticker: "My other terminal is a chunk of sh*t too"
 |+----+     |  +----------------------------------------------------------+
 ||    |     |  | Simon Travaglia, Computer Services, University of Waikato|
 |+----+VT100|  | Priv. Bag, Hamilton, New Zealand. spt@grace.waikato.ac.nz|
 +-----------+  +----------------------------------------------------------+

The telephone pole was approaching fast, I was attempting to swerve out of
it's path when it struck my front end.

                               [*][*][*]


 CS-ID: #1253.humor/tasteless@pro-friends, 4587 chars
 Date: 10 Jun 92 09:14:40 +1200
 From: spt@waikato.ac.nz (Simon Travaglia)
 Subject: b*st*rd OPERATOR FROM HELL #1
 Newsgroups: alt.tasteless
 Message-ID: <1992Jun10.091440.8536@waikato.ac.nz>
 Organization: University of Waikato Computer Centre
 Lines: 126

 
        //////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "If anyone wants any stamps send me a SASE and I will send /
      / you a stamp."                                              /
     /////////////////////////////////////////////  M.FARMER2  ////



[EOA]
[APP]//////////////////////////////
                      APPLE_TALK /
/////////////////////////////////
Apple Vs. IBM
"""""""""""""
by Stephen Kroese



     As I was walking down the street the other day, I noticed a man
working on his house. He seemed to be having a lot of trouble. As I came
closer, I saw that he was trying to pound a nail into a board by a window
--- with his forehead. He seemed to be in a great deal of pain. This made
me feel very bad, watching him suffer so much just to fix his window pane.
I thought, ``Here is an opportunity to make someone very happy simply by
showing him a better way to do things.'' Seeing him happy would make me
happy too. So I said, ``Excuse me sir, there is a better way to do that.''

     He stopped pounding his head on the nail and with blood streaming
down his face said, ``What?''

     I said, ``There is a better way to pound that nail. You can use a
hammer.''

     He said, ``What?''

     I said ``A hammer. It's a heavy piece of metal on a stick. You can
use it to pound the nail. It's faster and it doesn't hurt when you use
it.''

      ``A hammer, huh?''

     ``That's right. If you get one I can show you how to use it and
you'll be amazed how much easier it will make your job.''

     Somewhat bewildered he said, ''I think I have seen hammers, but I
thought they were just toys for kids.''

     ``Well, I suppose kids could play with hammers, but I think what you
saw were brightly colored plastic hammers. They look a bit like real
hammers, but they are much cheaper and don't really do anything,'' I
explained.

     ``Oh,'' he said. Then went on, ``But hammers are more expensive than
using my forehead. I don't want to spend the money for a hammer.''

     Now somewhat frustrated I said, ``But in the long run the hammer
would pay for itself because you would spend more time pounding nails and
less time treating head wounds.''

     ``Oh,'' he said. ``But I can't do as much with a hammer as I can with
my forehead,'' he said with conviction.

     Exasperated, I went on. ``Well, I'm not quite sure what else you've
been using your forehead for, but hammers are marvelously useful tools. You
can pound nails, pull nails, pry apart boards. In fact every day people
like you seem to be finding new ways to use hammers. And I'm sure a hammer
would do all these things much better than your forehead.''

     ``But why should I start using a hammer? All my friends pound nails
with their foreheads too. If there were a better way to do it I'm sure one
of them would have told me,'' he countered.

     Now he had caught me off guard. ``Perhaps they are all thinking the
same thing,'' I suggested. ``You could be the first one to discover this
new way to do things,'' I said with enthusiasm.

     With a skeptical look in his bloodstained eye he said,''Look, some of
my friends are professional carpenters. You can't tell me they don't know
the best way to pound nails.''

``Well, even professionals become set in their ways and resist change.''
Then in a frustrated yell I continued, ``I mean, come on! You can't just
sit there and try to convince me that using your forehead to pound nails is
better than using a hammer!''

     Now quite angry he yelled back, ``Hey listen buddy, I've been pounding
nails with my forehead for many years now. Sure, it was painful at first
but now it's second nature to me. Besides, all my friends do it this way
and the only people I've ever seen using {\Fit0 hammers} were little kids.
So take your stupid little children's toys and get the hell off my
property.''

     Stunned, I started to step back. I nearly tripped over a large box of
head bandages. I noticed a very expensive price tag on the box and a blue
company logo on the price tag. I had seen all I needed to see. This man had
somehow been brainwashed, probably by the expensive bandage company, and
was beyond help. Hell, let him bleed, I thought. People like that deserve
to bleed to death. I walked along, happy that I owned not one but three
hammers at home. I used them every day at school and I use them now every
day at work and I love them. A sharp pain hit my stomach as I recalled the
days before I used hammers, but I reconciled myself with the thought that
tonight at the hammer users club meeting I could talk to all my friends
about their hammers. We will make jokes about all the idiots we know that
don't have hammers and discuss whether we should spend all of our money
buying the fancy new hammers that just came out.  Then when I get home,
like every night, I will sit up and use one of my hammers until very late
when I finally fall asleep. In the morning I will wake up ready to go out
into the world proclaiming to all non-hammer users how they too could
become an expert hammer user like me.
-Bear               (A2-BEAR, CAT2, TOP8, MSG:2/M645)


      ///////////////////////////////////////////// GEnie_QWIK_QUOTE ////
     / "There's an Office Depot just up the street from me.  I'll give /
    / your recommendation a try (even though you told me not to trust /
   / anyone's recommendations ;^)."                                  /
  ///////////////////////////////////////////////////  OUTRIDER  ////



[EOA]#61
[FUN]//////////////////////////////
                      ONLINE FUN /
/////////////////////////////////
Search-ME!
""""""""""
Scott Garrigus
  [S.GARRIGUS]



SEARCH-ME!   Welcome to Search-ME, our monthly puzzle article.  Each
""""""""""   month we will have a different theme.  This month the
Search-ME! puzzle contains 21 keywords that are associated with
programming.


                          >>> PROGRAMMING! <<<
                          """"""""""""""""""""

             Q A O R R A G H S S F S P G S A K D F I B Q C 
             S T Q R M X J I D D M K U E A R E L M X G T G 
             W Z R M J X N P O V B O O W F K E X P S Q N D 
             Y Z I Z O W W G T F H C H K G S Y X N U S W J 
             D C R Y E W O T N I R P G B X T Q A H D G A B 
             Z T A P A T K D G M F H X L G P X V T Y L C U 
             X R B S O A D I U P Y A U I Z M F G X X H Q H 
             J S H P Z F A G G D Z T Y F U B C T E P D C G 
             G N F S K T C J Z S F P N R N W E P N N F O E 
             O T H Y Q G K B L O U J N A S Y C E S L W M Y 
             A S S E M B L Y R I F C M N T I Q G R O G P S 
             W D E V U O C W Z B N E W L S S K Z E O W I Z 
             R L Q Y F F P O E D M D H A G H N Y A P B L E 
             I G K V B O E A J O X O B O F L H O D U W E V 
             T P N V I Z U H R G Z W S Q J Q Y B C C J L O 
             E B J M O T C Y K U E U Q G Y Y A F A J A C M 
             S E U L Z R K H M M B S O D E U R E Z C A H Y 
             T E G C Q V B L N O V V A L G M R B S P S N E 
             V A R I A B L E P A B L E V P I A A B U D H F 
             I L M W K E V E J Z A Q P O M L P V Z Z X V W 

                                    
                 ARRAY           ASSEMBLY        BASIC
                 BIOS            COMPILE         CONSTANT
                 DOS             FOR             GEM
                 GFA             GOSUB           GOTO
                 LOOP            MEMORY          MOVE             
                 NEXT            PASCAL          PRINT            
                 READ            VARIABLE        WRITE            

                               [*][*][*]


GIVE UP?   You will find the answers in the LOG OFF column at the end of
""""""""   the magazine.  

          This column was created with a program called SEARCH ME,
          an Atari ST program by David Becker.


         ///////////////////////////////////////// GEnie_QWIK_QUOTE ////
        / "You will get the mag on Thursday because you sent them a   /
       / message asking if it was sent on Wednesday.  I know because /
      / I sent a message on Tuesday and just got my mag today.  I   /
     / wish I'd sent the message a week or two earlier so I would  /
    / have had the magazine. :-)"                                 /
   /////////////////////////////////////////////////  WAYNED  ////
 


[EOA]!!@ 
[WHO]//////////////////////////////
                       WHO'S WHO /
/////////////////////////////////
Who's Who In Apple II
"""""""""""""""""""""
by Phil Shapiro
   [P.SHAPIRO1]



                    >>> WHO'S WHO?  DEAN ESMAY <<<
                    """"""""""""""""""""""""""""""
                  ~ Head Apple II Roundtable Sysop ~


GEnieLamp>  Dean, how did you first get interested in the Apple II
"""""""""   computer?  Do you have any anecdotes you can share with us
            about your first experiences with the Apple II?

DEAN ESMAY> The first computer I messed with was a TRS-80 Model I.  A
""""""""""  neighbor upstairs had one when I was a kid.  It fascinated me
            and I quickly learned how to load game programs from its
            cassette tape - it was neat!

            Though I drifted away from that, years later in high school I
            started working extensively with TRS-80 Color Computers.  I
            also went through a brief love affair with the IBM-PC, but I
            spent most of my time with the CoCo, and later with a TRS-80
            Model XII, a pretty rare CP/M based business computer that
            never did too well for Radio Shack.

            In school I had almost unlimited time in front of the machines.
            The school was very small (it was a Special Education school
            with a student body of somewhere between 70 and 90) and I
            quickly gained a reputation for being great at word processing,
            so the people who ran the school let me spend a lot of time in
            front of the machine.  They had me doing office correspondence
            for them, typing in software from computer magazines, and just
            basically the computer whiz kid for the school.

            Anyway, to get to the point, one day the school got an Apple
            IIe to run some specialized vocational evaluation software.
            Well, it was right there in the office so I got to play with
            it, too.  I remember starting with Apple Writer and not really
            being sure what to think, but the demo disk that came with the
            machine was very intriguing.  It only had a green monochrome
            screen, but the graphics were still much nicer in many ways
            than what the Radio Shack TRS-80 Color Computer could do.

            Well I didn't get too much further into it right then.  But not
            long afterwards I started going back to hang out with some
            friends in my old neighborhood who I hadn't seen for a few
            years, and pretty quickly I found that a couple of them used
            Apple IIes in school. One of them in particular, a close friend
            named Thom Ryng, actually had one at home.  And wow, they had
            these neat disks with software to give me; mostly Applesoft and
            Integer BASIC stuff under DOS 3.3, just silly demo stuff that
            was used back then to teach programming. But it was wonderful!

            I had been messing with BASIC programs on the TRS-80 systems,
            but the Apple was just so neat!  It had disk drives that were
            actually useful!  Oh sure, the TRS-80 systems had disk drives,
            but they were a real pain to work with, especially under BASIC.
            I fell in love with the Apple because you could put in a disk,
            boot the machine, and then just type CATALOG to see what was
            there!  And just type RUN and a file name to run a program!
            Amazing!

            And you could just type LOAD and a file name to load a program
            up, and then you could LIST it, make changes to it, almost
            anything!

            And you could make a turnkey disk by just writing a program,
            putting in a blank disk, and typing INIT HELLO.  Your own
            custom Hello programs!  You could even write one BASIC program
            that would run another program!  Jeez, it was just so freaking
            AWESOME.

            It all sounds quaint and kind of silly now, but at the time it
            just blew me away.  I was just absolutely in love.

            Soon we got a copy of AppleWorks (I'm pretty sure it was 1.0 at
            that point) and I started doing all sorts of things with it.  I
            think that's when I finally stopped using the other systems
            completely.  The IIe was just way too much more powerful, more
            friendly, and more FUN than any system I'd used before.

GEnieLamp>  You've been working for Resource Central for several years
"""""""""   now, doing work in several different capacities.  How did you
            first link up with Tom Weishaar and his merry crew?

DEAN ESMAY> I think I picked up my first copy of Open-Apple (which is
""""""""""  what A2-Central used to be called, even before the company was
            named Resource Central) only a year or so after I started using
            the Apple IIe as my primary computer at the school.  I got a
            copy of that first "Issue #0," the one that talked in glowing
            terms about what a wonderful system the Apple II was, despite
            the way Apple ignored it.  That's pretty funny considering that
            this was, what, seven or eight years ago?  We Apple II'ers have
            been complaining about Apple and worrying about the Apple II's
            future for at least that long, but we're still here!

            Well anyway, Open-Apple hooked me immediately and I scraped up
            what meager funds I had to subscribe.  Come to think of it, I
            may have actually gotten a teacher to subscribe to it for me. I
            can't remember anymore.  (Chuckle.)

            That little newsletter changed my life.  I remember telling Tom
            Weishaar that once and he seemed to think that was a kind of
            weird thing to say, but when I started reading it I was still
            in my mid-teens, and it more than anything else was responsible
            for plunging me into a long-term love affair with computers.
            Let me tell you, the direction my life would have gone in if I
            hadn't developed a serious interest in computers is kind of
            scary for me to think about even today.  I'd probably be
            working at McDonald's or eking out a living on food stamps or
            something.  <grin>

            That newsletter made me an expert, gave me something I could
            both understand and enjoy, and something I actually enjoyed
            WORKING with.  It made me a serious computer fanatic - and not
            just computers in general, but the Apple II in specific.  I
            still like working with other computers, and we now have others
            around the house, but my Apple II is where I do all my real
            work.

            Well anyway, I kept reading Open-Apple for years, and wrote in
            a few letters now and then that Tom actually published, which
            was a big thrill for me.  Then one issue of Open-Apple came
            along where Tom talked about how he'd been talked into taking
            over the Apple II areas on GEnie.

            Well I was already a diehard modem user; I'd been calling local
            systems for years and eventually started running a bulletin
            board myself (the Apple Tree BBS, under the auspices of the
            Apple Tree Computer Club, south-suburban Chicago user group
            I've been involved with for many years).  I had never used a
            large service like GEnie, but I sure knew my way around modems,
            and had some amount of expertise in computers and in the Apple
            II.  So when I saw Tom mention he was taking over GEnie's Apple
            II areas, I thought, "What the hell."  I worked up a resume and
            mailed it to him, saying I'd be very happy to work as a
            volunteer to help manage the area.

            I never got a response, so a few months later I sent him
            another copy, slightly updated.  I had learned by then that if
            you wanted to make sure those Open-Apple guys saw something and
            actually remembered it, you had to send at least two copies.
            <grin>

            Well I still didn't hear anything for a few months. And then
            one day I got this hand-scrawled note from Tom saying, and I'm
            quoting almost exactly, "Got your resume.  Can you send me some
            references?  Why aren't you in college?  Tom Weishaar." (Which
            is pretty classic Tom Weishaar - much of his correspondence
            reads that way.  Short and to the point!)

            So I wrote him another letter, including another resume and the
            requested list of references.  I didn't hear anything for
            another few months, so I basically forgot about it.

            Then one day sitting at my desk (I was working as a secretary
            at the time) a friend of mine called me up and said, "Hey, Tom
            Weishaar just called me and asked me about you."  Then two
            other people called me and said the same thing within the space
            of maybe ten minutes.  And these were all people that I'd put
            down as references!  Well I didn't have any chance to really
            absorb that news, because right after I hung up with one of
            them, the phone rings again and this time it's Tom Weishaar
            himself!  I was almost speechless; he was something of a hero
            to me and here he was calling me to talk about working for him.

            It was more than that; he actually wanted to fly me out to meet
            the rest of the crew and we even talked about salary over the
            phone.  Within a week he'd flown me to Kansas City for the
            interview.

            For a guy in his early 20's with nothing but a High School
            diploma, working a dead-end job but who loved his Apple,
            well... it was just the biggest thrill I'd ever experienced.
            And though the salary he could afford to offer wasn't really
            much, it was much more than I was making at the time.

            The worst part was that I had to turn down the job.  I wanted
            it badly, but at the time I was trapped in a marriage; a very
            unhappy one that I had been thinking about terminating for a
            while, but I was still young and insecure and not sure what to
            do.  Anyway, she couldn't/wouldn't move to Kansas City.  I told
            Tom I might get a divorce, or maybe try to work something else
            out, but I just couldn't come work for him now.  I hoped he'd
            hold the job open for me but I didn't know if or when I could
            take it.  So I left back for Chicago very depressed and
            unhappy.

            Well a few days later Tom calls me again and tells me that for
            the projects he wants me for, he doesn't see any reason I can't
            just stay in Chicago.  He wouldn't pay me quite as much, but I
            could set my own hours and work from where I was!

            I eventually did get out of that marriage (no one should ever
            marry at 19!) but somehow it's always worked out that I'd just
            stay here in Chicago anyway.  I seem to have started a minor
            trend at Resource Central though; seems like more and more of
            the people who do stuff for Tom (such as Bo Monroe, and
            HangTime) live nowhere near Kansas.

            I see I've left a lot of little things out, but I do have a
            tendency to drone on and on once I get going, so I'd better
            stop.

GEnieLamp>  While just a few years old, the A2-Central Summer Developer's
"""""""""   Conference (nicknamed "KansasFest") has become a veritable
            Apple II institution.  As a person who has attended every
            KansasFest conference, can you share some thoughts about
            KansasFest?  What would you like to see added for next year's
            conference?

DEAN ESMAY> Well one correction - the first KansasFest was called the
""""""""""  A2-Central Developer's Conference.  I remember how we all sort
            of nicknamed it KansasFest the first year.  But anyway, all
            subsequent conferences have been called simply the A2-Central
            Summer Conference - note that the word "Developer" was removed.

            It's not a developer conference; there's lots of stuff for
            programmers and developers, but plenty to interest
            non-programmers.

            But anyway, what can I tell you about KansasFest?

            Basically, I think anyone who is an Apple II aficionado (not
            just a user, but someone who really ENJOYS working with it)
            really should go.  Not so much for what you'll learn (though
            you'll probably learn some things) but for who you'll meet and
            how much fun you'll have.

            I always have a wonderful time there.  Meeting other Apple II
            users from around the world is such a treat.  It's a double
            treat if you're on GEnie, because you usually find at least a
            few people who you've met in RTC's or on the bulletin board,
            but never actually seen face to face.

            One of the strongest observations I want to make about
            KansasFest, though, and one which I think far too few people
            realize, is that the real fun is not the conference - it's
            hanging out in the rooms during the evening with other Apple II
            users!

            What's weird is some folks who come never catch on to that.
            They actually try to find hotels, or they hole up in their
            rooms, or just stick with one or two friends and don't
            socialize much.  Oh, what a mistake!

           The fact is that there's just nothing more fun than getting to
           know the other Apple IIers in the world.  I remember that first
           KansasFest four years ago; I just swallowed my many insecurities
           and started walking around introducing myself.  Nobody knew who
           I was, but soon I had a bunch of friends; computer enthusiasts
           all share certain personality traits, and Apple II users are an
           unusually friendly bunch anyway.

           What would I like to see added to KansasFest?  I can't think of
           a thing; it's always one of the highlights of my year now.  This
           year was by far the worst KansasFest I ever attended - but only
           because I was quite ill with some kind of respiratory problem,
           and then some kind of stomach problem on top of it, for half the
           event.  And a bunch of things we'd planned to promote the
           goings-on here on GEnie fell through because I was under the
           weather, and also some other things we planned didn't come
           through.

           But I STILL had fun!  The only thing that was not-fun was seeing
           all the other people having even more fun than me.  <wink>

           You will make friends at KansasFest, you will learn things about
           your Apple II, and you will have fun.  That's what it's all
           about.

           So, the only thing I can think of that should be added to
           KansasFest is more Apple II users.  Those of you who don't come
           just don't know what you're missing.

           That's all KansasFest needs - more of you guys out there who
           say, "Ah, I don't know if it's for me."  Yes it is!

GEnieLamp>  Before ascending to the head sysop of the Apple II Roundtable
"""""""""   on GEnie, you worked for a while as the head Apple II
            Roundtable librarian.  What did you find most rewarding about
            the librarian job?  Least rewarding?

DEAN ESMAY> I didn't just work for "a while" as librarian.  It was the only
""""""""""  thing I ever did on GEnie!  <wink>  Well actually I did that
            and answered almost all A2.HELP mail at the same time.  I did
            both for something like four years before becoming chief sysop,
            which was only just a few months ago.

            I always tell people the same thing about being software
            librarian on GEnie.

            The greatest thing about it is getting to download and play
            with so many files!  It really is neat to see so much stuff and
            get to mess with all of it.  You have to make sure it all works
            and all that, right?  So when the latest new shareware game
            comes along, of course I have to spend some time playing it to
            make sure it works... right?

            But the worst thing about being file librarian is... having to
            download and mess with so many files!  Seriously, it's both the
            greatest plus and greatest drawback.  When you see EVERY file
            that goes up, and when you HAVE TO look at all of it, it starts
            to lose some of its attraction.  Some days you just can't stand
            the thought of having to deal with any more new files... but of
            course you have to, because it's your job.

            I kind of thought I'd miss the job after I turned it over to
            Tim Tobin a few months ago, but you know what?  I really
            haven't.  A2 gets anywhere from a few dozen K to a megabyte or
            even several megabytes (and I mean megabytes AFTER being
            compressed with ShrinkIt) every single day.  Having to wade
            through all that can be fun, but I find that after a having had
            a few years of it I don't miss it near as much as I thought I
            might!

GEnieLamp>  What do you consider your most proud accomplishment?  (In terms
"""""""""   of your work with the Apple II.)

DEAN ESMAY> My work on Studio City.  Studio City is a bi-monthly
""""""""""  magazine-on-disk that I edit.  It uses HyperStudio as its main
            environment.

            It used to be called Stack-Central, but we thought Studio City
            was more catchy and would help people more clearly distinguish
            it as a HyperSTUDIO product.  (We also have Script-Central, for
            HyperCARD, but I'm only involved peripherally with that.)

            I pour a lot of my soul into Studio City, and it's a lot of
            work.  I've been doing it for about 14 months now and, while
            I'm starting to feel a real strain to keep up with it, it's
            still the work I'm proudest of.  I look at those back issues
            since I took over, and I think that much of what is in them
            represent some of the best work I've ever done in my life.

            I'm also proud to say that we've attracted some people who
            aren't HyperStudio owners but who really like and can use the
            stuff we put on each issue!  I really like that; you can use
            HyperStudio stacks without owning HyperStudio, due to the free
            "runtime" module we include with each issue, but most people
            don't seem to have caught on yet.  We publish loads of good
            software that people can use every issue, much of it stuff you
            just DON'T need HyperStudio to use or enjoy.

            HyperStudio is such an exciting environment.  There's all
            sorts of opportunity it represents from the enterprising
            person; you don't have to be a "real" programmer to do some
            very exciting stuff with it.  Although if you are a "real"
            programmer there's stuff for you in it, too.  Again, since
            people DON'T have to own HyperStudio to use what you create
            with it, there's just lots of opportunities there if you want
            to develop products people might buy (or at least enjoy
            downloading from our library!  <wink>).

            Unfortunately, at the present time HyperStudio is in a new
            release stage (version 3.1) which has some significant bugs.
            This doesn't affect "average" users much, but those of us who
            push the environment to its limits keep coming across
            frustrating bugs that cause real problems.

            Fortunately it's been getting more stable, and I hope that
            Roger Wagner Publishing gets them all ironed out soon.

GEnieLamp>  Who do you look up to as your mentors?
"""""""""

DEAN ESMAY> I don't really have any mentors; I never really have had them.
""""""""""  I suppose you could say I am a self-made man.  I didn't go to
            college, and in fact I barely squeaked through a Special Ed
            High School (they made me Valedictorian but considering that it
            was a graduating class of about a dozen, with an average GPA of
            maybe 2.0 that's not saying much!).  I was always a loner who
            kept to himself and read.

            I suppose you could say my mentors were books.  I feasted on
            them; especially SF books, but other books of many types, not
            to mention magazines.  Certainly the Open-Apple newsletter and
            those who wrote in it (not just Tom Weishaar but Dennis Doms
            and the many people who wrote letters that were published in it
            over the years) had a big effect.

            But many others would include those authors who taught me ways
            of looking at life, taught me facts about the world, and even
            sometimes taught me how to think (especially how to think for
            myself).  As a kid I read three to five books a week, and even
            now I read at least one a month on the average (which isn't
            near as much as I'd like, but I'm too slow and too busy to
            handle much more).

            There are some names that come to mind though - Mark Twain,
            Robert Heinlein, Isaac Asimov... and Albert Einstein (not that
            he wrote much that I read, but I spent so much time and energy
            trying to teach myself Special and General relativity, he had
            an effect anyway, if you see what I mean).

            I guess this is a kind of weird answer but it's the only one I
            have.  I grew up reading, and books were my mentors; not just
            my mentors, but my friends, allies, and partners in crime.
            Guess that means I was a weird kid, but so what?  <grin>

GEnieLamp>  Dean, speaking as someone with quite a lot of
"""""""""   telecommunications experience, where do you see the future of
            telecommunications moving in the next five to ten years?  Do
            you think the new Internet capability of GEnie will be opening
            up a lot of doors?  (In terms of allowing GEnie subscribers to
            communicate with people on other information services.)

DEAN ESMAY> I think it's really hard to say where the future lies.
""""""""""  Certainly telecommunications will get faster and more
            efficient and more powerful.  We'll also see things that are
            much more advanced in terms of graphics, and even sound, being
            generated across modem lines.  How much so is very hard to say.

            Not to get all political (though I love to talk politics),
            much of it depends on how soon the government gets around to
            regulating the industry.  I'm confident they'll do it sooner or
            later.  No matter how much we fight it, they'll have their way
            with us eventually.  And once the on-line services are hostage
            to government control and restriction, then the technological
            advances we see, and what companies are allowed to offer
            consumers, will be slowed down significantly.

            So it really depends on how long we have until the government
            finally comes in and screws up a good thing.  <grin>  It's
            inevitable; I merely hope it's later rather than sooner.  If we
            have a decade or so more without much interference, I think
            what we'll see will be mind-blowing.  If we only have another
            year or two, don't expect too much more exciting than what we
            have now (which I'll admit is still pretty neat!  :-).

            The Internet links thousands of different computer systems
            around the world.  So yes, that makes it likely that there will
            be a lot of new opportunities for communicating with people
            on-line.  You won't have to require that other people you know
            have GEnie accounts before they can send messages to you;
            they'll be able to send mail to you from all kinds of other
            systems.  That should be very nice.

            I'm not really an expert on the Internet, so I can't say much
            else about it than the fact that it will give us access to
            quite a lot of information and quite a lot more people!

GEnieLamp>  What sorts of things do you like to do for fun (i.e.
"""""""""   non-computer hobbies)?

DEAN ESMAY> I'm a rabid music listener.  I have a fairly nice stereo and
""""""""""  several hundred CDs.  I listen to many different kinds of
            music, mostly rock and blues but a smattering of everything -
            jazz, pop, country, classical, industrial, rap, folk, and
            avant-garde stuff.

            I also still read quite a lot.  A majority of the fiction I
            read is Science Fiction, but I read lots of other things.  I
            seem to have a perverse interest in economics, as well as
            certain odd kinds of historical and sociological areas.

            I also used to love to go to places like the Jerry Pournelle
            RoundTable to talk about everything from politics to physics,
            but since taking over A2 I haven't had enough time for that.

GEnieLamp>  Along with everything else you do, Dean, you also edit
"""""""""   A2-Central On-Disk. Since some Apple II enthusiasts may not
            know what A2-Central On-Disk has to offer, can you give us a
            brief synopsis of its contents? Does editing A2-Central On-Disk
            take up a lot of time each month?

DEAN ESMAY> A2-Central On Disk (A2 On Disk for short) is a monthly
""""""""""  companion to the A2-Central newsletter.  Now, A2-Central
            (formerly Open-Apple) has won many awards and much acclaim as
            an Apple II periodical.  What doesn't get mentioned as often is
            the monthly disk that you can get with it as an option.

            Each issue of A2 On Disk is a single 800K 3.5 disk that
            contains, first, a copy of the A2-Central newsletter itself in
            a text file.  Each month I convert the newsletter into a usable
            text file.

            That may not sound all that exciting until you realize that we
            provide software to let you search through that text for items
            of interest.  One thing you can do is take all the text files
            from your back issues and use software such as Sneeze (which is
            free), or Fastdata Pro (which costs a little but is awfully
            nice) to search through back issues very quickly.  It becomes a
            highly useful technical reference, much more convenient and
            flexible than standard indexes.

            There's TONS of useful information in back issues of
            A2-Central, so it's neat to have it all quickly at your
            fingertips.

            Now, the newsletter text only takes up a small fraction of the
            disk.  So each month it is also crammed - and I do mean crammed
            - full of the best public domain, freeware, and shareware
            software I can find.  Not only do I get stuff from on-line, but
            people send me stuff all the time.  Many shareware authors send
            me their programs, and quite often subscribers from around the
            world will send me disks with neat stuff on them.  It's really
            cool when you get a disk from a place like Australia full of
            new software you've never seen before.

            While most of the software on A2 On Disk is IIgs-specific,
            every month I try to include as much quality 8-bit stuff as I
            can find. And it usually winds up being at least enough to fill
            up a 5.25 disk or two.

            You see, we compress all the files on each issue with
            ShrinkIt, which means your 800K disk actually has well over a
            megabyte worth of programs on it.  So there's lots of things I
            can do with it now.

            As for how long it takes - you know, it takes up a lot more
            time than you might think.  Usually I get the raw, unformatted
            text from Ellen Rosenberg (the current editor) about four days
            before it has to be done.  And it usually takes me about two
            working days to finish the issue at that point.  I spend a lot
            of time fitting stuff on there, trying to develop a theme, and
            trying to make sure everything on it is really interesting and
            that there's a variety of all kinds of things.  I put a lot of
            effort into that, and making sure there's stuff for all
            machines on there.

            I also work really hard to fit things together well; each
            issue usually has well under 10K of free space left on it by
            the time I'm done, and that's not as easy to do as it sounds.
            Let's say you have ten files to choose from; but if you use
            these two files, you can't use this other file because you'll
            be out of room, and so you dump those two for later and then
            you realize you've got a bunch of space left over and have to
            go find something to fill that gap, and so on.  It really does
            take some effort, and a sort of intuition that you develop for
            it over time.

            And there's work the rest of the month, mostly in collecting
            materials, weeding through what I've gotten lately for things
            that are appropriate, and contacting the authors of the
            programs we feature.

            Though the material is mostly shareware and freeware, it's
            important to us that we actually contact the author whenever
            possible to obtain permission to use it.  So there's a lot of
            chasing people down in e-mail (which not everybody answers
            unfortunately), trying to find their phone numbers and getting
            them on the phone, keeping track of who I've gotten permission
            from and who I haven't, who I still have to call, and so on.

            I've been doing it for years now and I've got most of it down
            to a system.  But it's not as easy as it might appear!

GEnieLamp>  As a person who works out of a home office, can you share any
"""""""""   special insights as to the benefits and drawbacks of working
            at home?

DEAN ESMAY> Working at home has obvious advantages.  No "boss" looking
""""""""""" over your shoulder or riding you to work on certain things at
            certain times.  No need to "look busy" when you have nothing
            to do.  And not near as much office politics to deal with as in
            a standard work environment.

            No need to dress up; I wear comfortable clothes at all times
            and NEVER wear a tie (heck, I don't even own one anymore).  I
            can listen to my stereo as loud as I want.  I can sleep late
            pretty much whenever I want.

            And I like to say that I have a thirty-second commute in the
            morning - that being about how long it takes me to stumble out
            of bed and stump over to my computer to check my morning mail!
            :-)

            It saves money, too.  You don't spend near as much on clothes,
            gasoline, or upkeep on your car.  Then again your phone bills
            shoot way up, so it kind of evens out.

            There are drawbacks; you don't get a lot of office
            socializing, chatting around the water cooler type stuff.  You
            spend a lot of time on your own, or without adult company
            anyway.  Sometimes there are interminable delays when you can't
            reach someone on the phone and have to wait 24 hours or more
            for e-mail to be returned on what should be a simple matter
            that would only normally take 30 seconds to get answered.

            You also tend to get fat I'm afraid.  Not only do you not even
            get the exercise it takes you to walk out to your car in the
            morning, but it's insidiously easy to run downstairs to the
            refrigerator to snack on stuff.  No one's going to notice or
            care if you eat at your desk, right?  Well that's nice but it's
            easy to do too much of it.

            Another drawback is that sometimes other people have a hard
            time believing that you actually do work much, or work very
            hard.  I've had people more or less tell me that they don't
            believe I really have a job, that I just sort of horse around a
            bit.  When you don't have a firm schedule, and don't have to GO
            somewhere to work, well, to some people it doesn't seem like
            WORK.

            This is a double problem for me because I generally keep a
            night schedule; many people who work at home don't do that, but
            I personally find that I work much better that way.
            Unfortunately, this leads some people to consciously or
            unconsciously think of you as "lazy" because you're usually
            asleep at 9 or 10 in the morning.  They don't realize that you
            were still working while they were snug in their beds.  (I
            usually don't go to sleep until around 5:00 a.m., sometimes
            later.)

            So, sometimes I have to put up with a little snootiness and
            sniffing because of the hours I keep.

            But overall it's great.  I much prefer it to working in a
            regular office; I did that for several years and sure don't
            miss it much.


          ////////////////////////////////////// GEnie_QWIK_QUOTE ////
         / "Wow, three new messages waiting for me when I posted my /
        / last message!"                                           /
       ////////////////////////////////////////////  OUTRIDER  ////


 
[EOA]
[QUI]//////////////////////////////
                THE MIGHTY QUINN /
/////////////////////////////////
Milliseconds With Mark
""""""""""""""""""""""
by Mark Quinn, DOA
   [M.QUINN3]



             >>> A WHOLE BUNCHA MILLISECONDS WITH MARK <<<
             """""""""""""""""""""""""""""""""""""""""""""

     They are out there.  Those logjams in the bit stream; those dropped
characters (there are a few characters I'd like to drop, but they seem to
be more prevalent than ever); those I/Os (I/O, I/O, so CompuServe I owe),
and those lows.  But what _really_ bugs a modem geek (or, as someone I know
chooses to refer to them, a "modemhead")?  (I rustle the card, and try to
screw my expression up, to the delight of the audience.)


          TOP TEN THINGS THAT GIVE A MODEMHEAD A HEADACHE
          """""""""""""""""""""""""""""""""""""""""""""""
    10.  Soap.
     9.  Hardware incompatibilities between modems.
     8.  The Danger Man from Domino's.
     7.  Those nasty things hooked to modems called
         "computers".
     6.  Actually paying to download a MIDI or GIF file
         after seeing its description, which begins,
         "This is my first attempt at this sort of
         thing . . .".
     5.  Rugrats belching into the family phone in the
         middle of a download.
     4.  When the 9600 bps modem gives out in the middle
         of the night, and the spare 2400 bps internal model
         is found at the bottom of a stack of dirty dishes.
     3.  Thinking that you bought a Hewlett Packard product,
         and discovering the next morning that it was
         manufactured by that _other_ company.
     2.  After you've moved, your spouse insists on unpacking
         inconsequential items like children's toys _before_
         the computer equipment.

    and (give us a digital drum roll, will you, Hal?) . . .

     1.  Getting "pulled into chat" by the sysop of a local
         BBS, and discovering that he/she is normal.


         ////////////////////////////////////////// GEnie_QWIK_QUOTE ////
        / "cmp.l LET_A,d0   That would compare the contents of address /
       / 65 with d0.  It would also generate an address error since   /
      / 65 is odd and you're looking for a long.  Also, bus error    /
     / since you may not be in supervisor mode.                     /
    /                                                              /
   / Keep at it, it'll come to you in a blaze of light one night  /
  / when you are just about to fall asleep....."                 /
 //////////////////////////////////////////////  C.WALTERS1  ////



[EOA]
[FOC]//////////////////////////////
                     FOCUS ON... /
/////////////////////////////////
Thinking Out Loud
"""""""""""""""""
by Phil Shapiro
   [P.SHAPIRO1]



      >>> THINKING ABOUT PEOPLE'S AFFECTION FOR THEIR HOME COMPUTER <<<
      """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

     The other day I got to thinking about people's affection for their
home computers.  These thoughts were raised when I took my computer in for
a motherboard upgrade.  As the dealer took my computer out of my clutches,
I hesitantly asked: "So how many days is this going to take?"

     If the upgrade were going to take five days, that would be an
eternity.  Three days would be barely manageable.  Two days would be
difficult, but I'd get over it.

     It turns out that putting in the new motherboard would take three
days.  Barely manageable.

     But as I walked out of the store it struck me how very similar my
feelings for my computer were to the feelings for my classical guitar when
I took it to the store to have an electronic pickup installed.  When I
handed over my cherished guitar, I hesitantly asked:  "So how many days is
this going to take?"

     If the upgrade were going to take five days, that would be an
eternity.  Three days would be barely manageable.  Two days would be
difficult, but I'd get over it.  The answer:  "Three days."  Arrgggh.
Barely manageable.

     The reason people's affection towards their home computers is so
similar to their affection towards their cherished musical instrument is
because both computer and instrument are extensions of the human
personality.  Both computer and instrument provide unlimited opportunities
for creative expression.  Both, too, provide unlimited opportunities for
creative exploration.

     Computers are sandboxes for the mind.  Musical instruments are
sandboxes for the soul.

     But the comparison doesn't stop there.  The parallels grow stronger on
closer investigation.

     About six years ago my older brother Ian showed me a new tuning for
guitars, the lovely-sounding "dropped-D" tuning.  The moment I heard that
tuning I knew it would open up vast new areas of musical exploration.  In a
sense the new tuning was a new "operating system" for the guitar.  It
allowed me to produce the same music I produced before, but in a whole new
and interesting way.

     The parallels between computers and instruments continues when you
consider that both computers and musical instruments provide opportunities
for a lifetime's worth of study and mastery.  After mastering the basics
you can go on to study endless intricacies.  You can stand in awe at what
virtuosos have accomplished in the past.  You can develop a fine
appreciation of the art and the craft.  You may even develop enough skill
to extend the boundaries of the craft yourself.

     Perhaps these parallels are all centered around the "appeal to the
creative temperament."  Could anybody doubt that if Wolfgang Amadeus Mozart
were alive today he'd be spending much of his free time sitting at a
computer keyboard --- playing, learning, composing, exploring?  Knowing
Mozart, he'd find a way to get Salierni's computer to print musical notes
backwards, from right to left, on Salierni's computer screen.

     Likewise, could anyone doubt that Da Vinci, were he alive today, would
be online three or four times a day with other artists/inventors in other
Renaissance villages?  Chances are Da Vinci would be at his computer so
much he'd never have a chance to take a good look at his screen saver.
(You know --- the one with sketches of parachutes, gliders, and other
mechanical contrivances.)

     Moving ahead five hundred years, it's intriguing to consider that Alan
Kay, an awesomely creative research fellow at Apple Computer, is himself a
world class pianist.  And it's hardly surprising to hear that his special
talent is extemporaneous composition.

     Other microcomputer visionaries have had similar musical passions.
Paul Allen, co-founder of Microsoft, likes to do nothing more than to jam
with his rock-and-roll band.  Microcomputer legend Steve Wozniak organized
and sponsored two large rock concerts in the early 1980's.

     People who don't use a computer regularly can sometimes have a
difficult time understanding how other people could get emotionally
attached to such an "inanimate object."  If you need a quick explanation of
your emotional attachment to your computer, drawing the comparison to
musical instruments can serve as a useful analogy.  If such non-computer
using persons had every experienced the joy and wonder of playing a musical
instrument, they'll nod their heads in understanding.  Some things in life
you just can't put into words.

                               [*][*][*]

NOTICE!   Last month GEnieLamp printed an article about how Apple II
"""""""   computers are being used for "cognitive therapy" in a
psychiatric hospital.  GEnieLamp would like to spotlight other inspiring
stories where Apple II computers are assisting and uplifting people with
special needs.
 
     If you happen to know of Apple II's in your area being used in a
children's hospital, adult literacy center, special needs school, or other
community organization, please contact GEnieLamp co-editor Phil Shapiro
(p.shapiro1) here on GEnie.
 
     We are also interested in putting together a story on how Apple II's
are the favored computer for public access use in our nation's libraries.
Anyone with anecdotes or interesting stories on this subject are invited to
contact us about that article.
 
-Phil Shapiro
                               [*][*][*]


     Phil Shapiro   Shapiro is the founder of Balloons Software, a
     """"""""""""   new Apple II educational software company. He can
     be reached  He can be reached via electronic mail on GEnie at:
     P.Shapiro1; on America Online at: pshapiro


        /////////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "I'm still sold on LiveMouse.  It's a one-size-fits-all kinda /
      / deal. If it's too small when you buy it, you just feed it     /
     / until it's the right size.  Then you pull the Freeze Tab      /
    / and voila...instant mouse."                                   /
   //////////////////////////////////////////////  J.EIDSVOOG1  ////



[EOA]
[GAM]//////////////////////////////
               GAMES PEOPLE PLAY /
/////////////////////////////////
Apple II Games
""""""""""""""
By Darrel Raines
      [D.Raines]


                         >>> HIDDEN TREASURE <<<
                         """""""""""""""""""""""
             ~ The Public Domain Eamon Adventure Game Series ~

Introduction   "Fade in to a dimly lit tunnel that has moss hanging on the
""""""""""""   walls.  The source of light is a few torches hung every 10
feet along the sides of the tunnel. On the floor of the tunnel you can see
a pile of diamonds that would buy you a kingdom and ensure a happy future
for the king into the foresee- able future. Now, there is only one little
problem with your plans for a glorious retirement.

     You swing your trusty sword, Trollsfire, at the giant orc and hit.
The giant orc is at death's door, knocking loudly!

     The giant orc swings his heavy axe and hits.  Your armor absorbs the
blow.  The lesser orc shoots an arrow at you and misses.

     The orc guard stabs at you with his sword and hits. You are badly
injured!

     The question of the moment is whether you should flee now and return
to fight another day. You have the giant orc on the floor, if you can just
finish him off before you are finished yourself.  Unfortunately, for the
giant orc, that pile of diamonds sure does look like your destiny.

     You thrust Trollsfire back at the giant orc and ...

     ... as you look around the tunnel that is now littered with the
bodies of fallen orcs, you find that you have no more enemies at hand.
With a sigh of relief, you put Trollsfire back into your belt until the
next battle.  Your body feels like you just tried out a new carriage by
letting the horses drag it over you.  With a coarse whisper you utter the
words of healing.  Immediately you can feel the wash of energy through your
body.  You are now in good shape.

     With a confident stride you walk over to the pile of diamonds.  This
was a rough dungeon to conquer, but it looks like the reward will make it
worth the effort.  As you bend down to scoop the diamonds, you meet an
unexpected resistance.  Something is wrong here.  The diamonds don't seem
to be laying loose in a pile like you first thought.  Instead, they seem to
be embedded in a tough hide!  As you draw your head back to look for the
edge of the seemingly endless stretch of hide, you see a large pair of
gleaming eyes inspecting you from the dark shadows of a tunnel recess.  You
have managed to irritate a very large dragon.

     As you draw out Trollsfire, you think to yourself that you should have
stayed back in that warm, cozy tavern with the sweet tasting ale.  Fade out
to the sounds of metal biting into bone and the roar of fire blazing out in
a hot blast."


History   The section of history related in the previous section could
"""""""   have easily come from one of the many adventures awaiting the
daring game player within the varied worlds of Eamon.  Eamon is a public
domain, adventure gaming system for the Apple II computer that has been
around for many years.  As best I can tell, it was created about 1980-1981.

     The original idea was brought to life by Donald Brown.  I was first
introduced to Eamon in 1983-1984.  Even at that time, no mention was ever
made of Donald Brown still being around to support his creation.  But the
wonderful thing about Eamon is that the software was written to be an
expandable, changeable, unrestricted environment for people to create their
own adventure games.  Eamon provides a shell that can be adapted by the
programmer to make an adventure of any variety.  Indeed, many different
styles of adventures already exist within the Eamon gaming system:
Tolkien-type adventures, science fiction, fantasy, Dungeon and Dragons, and
many more.

     Eamon adventures are written in Applesoft Basic and run under the
standard 40 or 80 column screen mode.  Don't let this fact fool you.  There
are many Infocom text adventures that outshine the graphic adventures
produced since then. Eamon adventures are as good or bad as the creators of
the individual games themselves.  Some are outstanding.  Others are at best
only fair.  When you get tired of playing a game, you can sit down and
create a game.  The possibilities are endless.

     Even if you do not want to write your own adventures, you can still
enjoy the more than two hundred games that have already been written.  All
of them can be run under ProDOS and many of them make use of 80 column text
screen to provide magnificent descriptions of the adventure creatures and
surroundings.

     It wouldn't be fair to describe the history of Eamon and not say
anything about the best thing to happen to Eamon since its creation:  Tom
Zuchowski.  Tom has been keeping Eamon alive and well now for some time.
He has written a number of games himself. But more importantly, he has
spear-headed many of the efforts to keep Eamon working on modern Apples
with modern operating systems. Eamon was first written on 5 1/4' disks
under DOS 3.3 .  After you play a few Eamon adventures, see if you don't
think that Tom's efforts have been worthwhile.


Next Time   Eamon is too big and too exciting to do justice with a single
"""""""""   article.  Therefore, I must ask you to look back in on this
column in the November issue of GEnieLamp.  We will then do some critical
analysis of the gaming system.

     Next month I will describe how to play a typical game of Eamon.  I
will go through the process of setting up the game on your hard drive or
3.5 inch floppies. I will give you a rating for the Eamon system itself and
a few of the better adventures.  Finally I will have a few parting words
about this wonderful freeware system.  Some of you will be itching to try
out Eamon before next month.  Therefore, I have listed below some good
starter files that are available on GEnie right now for your gaming
pleasure.  Until next time, happy exploring!


  No. File Name             Type Address      YYMMDD Bytes   Access Lib
----- ------------------------ - ------------ ------ ------- ------ ---
16728 BEST.EAMONS.BXY          X T.ZUCHOWSKI  910929  348544    100  36
      Desc: An incredible role-play experience!
16750 STARTER.KIT.BXY          X A2.DEAN      911002  331008    160  36
      Desc: Very Best role playing system!


Author   Darrel Raines (D.Raines) welcomes any feedback or comments via
""""""   electronic mail to the listed user name.


        //////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "In my experience GEnie management has always been very    /
      / attentive to my problems, and very helpful, and even       /
     / nice :) They give me the benefit of any doubts every time."/
    /////////////////////////////////////////////  S.JACQUES  ////



[EOA]
[COW]//////////////////////////////
                       CowTOONS! /
/////////////////////////////////
Moooooo Fun!
""""""""""""
                                                           *
                           *
                                 *
                 (________)         *
                    ()()               *
             /-------\/                          *
            / |     ||                *
           *  ||----||                        *
              ~~    ~~
 
                     " Extraterrestrial Longhorn "
                        ~~~~~~~~~~~~~~~~~~~~~~~~~
                                 in the
                         Space/Time Cowtinuum 
by Mike White
  [M.WHITE25]
 

               (__)
            __( oo )__             CowTOONS?  Mike and Robert took us up
            \  \  /  /             on our offer and sent us this month's
   =|========|  ''  |========|=    CowTOONS selection.  Thanks, guys!
     \       |\|><|/|       /
      \      | \  / |      /       If you have an idea for a CowTOON, we
       \     |  \/  |     /        would like to see it.  If we publish it
        \    |  /\  |    /         in GEnieLamp we will credit your account
         \___| |__| |___/          with 2 hours of GEnie non-prime time!
             (@)   (@)             
         COWnt Dracula             
         by Robert E. Santosuosso  
                   [R.SANTOSUOSS]


        ///////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "Trouble is, someone will BELIEVE that stuff.  You wouldn't /
      / believe how gullible some people are.  I will be reading on /
     / the nets that someone on GEnie said a full disk weighs more /
    / than an empty one... Sigh."                                 /
   ///////////////////////////////////////////////  NTACTONE  ////
 


[EOA]
[CON]//////////////////////////////
                     CONNECTIONS /
/////////////////////////////////
Joe Kohn On A2
""""""""""""""
by Joe Kohn
   [J.KOHN]



                         >>> CONNECTIONS <<<
                         """""""""""""""""""
                 ~ Copyright (c) 1992 by Joe Kohn ~

     Permission is hereby granted to non-profit Apple II User Groups to
republish this article, in whole or in part, in their newsletters.
Electronic re-distribution is encouraged via online network and/or BBS.
This article may not be re-published by any for-profit organization without
the written consent of Joe Kohn.)


Greetings Everyone   I am firmly convinced that the single most valuable
""""""""""""""""""   peripheral device that can be connected to a computer
is a modem. Once a modem is connected, it's possible for anyone to join
that huge group of Apple II users who frequent America Online, CompuServe,
GEnie, and the Internet. I'm so convinced of the importance of "going
online" that I will be writing a new column for inCider/A+ on the subject.
By now, many of you will have seen the first installment of "Grapevine",
and I hope that it's piqued your interest in owning a modem. Each month,
I'll be sharing interesting Apple II related tidbits found on the various
online services, and I'll also be sharing money saving hints and tips for
those of you who already have modems. Grapevine; coming monthly to
inCider/A+.

     * I'd like to mention a brand new $10 Shareware program that's one of
the best brain teasing, yet enjoyable, games I've ever played on the IIGS.
Kenrick Mock, the author of that fine game Columns GS, has just released
BoggleGS, and it's something that all fans of word games should have. When
first run, a colorful grid filled with letters appears. You have 3 minutes
to find words that can be made from adjacent letters in the grid. It's a
very colorful program and even has music. If you enjoy working crossword
puzzles, you should really enjoy it, and if you're a teacher, you'll love
BoggleGS.

     * Speaking of shareware, I'd like to let you know about a fantastic
new Apple IIGS shareware utility program that may change your life. Coming
all the way from New South Wales in Australia, John MacLean's $10 DOS 3.3
Launcher should be of great interest to long-time Apple II owners who have
a large library of older DOS 3.3 software. In short, DOS 3.3 Launcher
provides an easy-to-use way to store, and run, DOS 3.3 software on any hard
drive connected to an Apple IIGS. Even if your hard drive wasn't DOS 3.3
compatible before, it is now.

     DOS 3.3 Launcher is a GS/OS desktop based program that can be
launched from the Finder. It has a standard GS/OS interface complete with
pull down menus. Once run, it will allow you to copy DOS 3.3 Binary files,
or entire DOS 3.3 disks, to your hard drive, and it will let you launch
those files or disks from the Finder, and will return you to The Finder
when you're finished using the DOS 3.3 software. DOS 3.3 Launcher works
with single or double sided disks. It even slows down old games so that
they run at 1 Mhz, and returns you to the GS'es faster speed upon exiting
those programs. It does not work, of course, with copy protected software.

     John MacLean, who also wrote Roger Wagner's Graphic Exchange, has
written a very useful utility program that will soon have you dusting off
your old DOS 3.3 software.

     * Speaking of new software, I finally got around to installing the new
AppleWorks Classic enhancement TimeOut Grammar. This is a grammar checker
that works right from within AppleWorks, and I like it a lot. This TimeOut
version is based upon the old Sensible Grammar, and works in a similar
manner. It checks Appleworks word processing documents for grammar usage
and punctuation. Combining that with TimeOut Thesaurus, AppleWorks V3.0 is
a writer's best friend. TimeOut Grammar is available from Quality
Computers.

     * Quality Computers will soon be releasing Finder Helper, an
incredible collection of System 6.0 Finder Extensions and Desk Accessories
written by noted IIGS programmer Bill Tudor. I really like Finder Helper a
lot, but before I provide any details, allow me the liberty to stray, and
please be patient with me as I editorialize a little.

     Many of the utilities found in Finder Helper started out life as
shareware products. Bill Tudor must have been quite proud when he saw that
his programs had been downloaded hundreds of times from the various online
networks, and were in use on thousands of System 6 equipped GS'es; hardly a
day went by when I didn't hear someone rave about how great Bill Tudor's
shareware programs were. But, something was amiss. Many of the people that
used Bill Tudor's shareware never bothered to send in their shareware fees,
so he sought a more traditional outlet for his software. Now that it's a
commercial product, he'll at least be getting some monetary reward, but, in
some ways, I can't help but feel that the Apple IIGS community has lost
something.

     It's important to submit shareware fees for programs you use. By
sending in shareware fees, you'll be helping to prolong the life of the
Apple II, because you'll be encouraging those who program these computers.
Think about it, and then take the pledge to submit at least one shareware
payment to an author whose work you like.

     Getting back to Finder Helper, it's a collection of Finder Extensions
and New Desk Accessories that provide useful new tools that can be used
when using GS/OS. It includes a very well behaved Alarm Clock that appears
in the IIGS Menu Bar. It includes Cdev Alias that allows you to control
your Control Panel Devices from a New Desk Accessory. SuperDataPath allows
you to easily instruct the Finder where it can find your data files.
HotKeys allows you to launch your favorite programs directly from the
IIGS'es numerical keyboard. Catalog will save a disk catalog's contents to
a file on disk. File Peeker shows you the contents of Text, Teach,
Pictures, Sounds, Icons and Filetype documents. Workset allows you to
double click on one small icon and have AppleWorksGS, for example, launch
and load multiple documents. Crypt allows you to encrypt all your sensitive
personal files, and MoreInfo provides, among other things, the ability to
lock and unlock files right from the Finder's Extra Menu.

     Due to the fact that Apple has trademarked the word "Finder", when
this set of utilities is actually released, it may have a different name.
No matter what it's named, it's a great package of System 6 enhancements.

      * I spent a pleasant afternoon recently with Olivier Goguel, the
founder of the FTA, when he was visiting San Francisco. If you're not
already familiar with the FTA, make sure you pick up some of their freeware
disks from your local user group or download some from your favorite online
service. The France based FTA has created a stunning collection of GS
software, and it is not to be missed.

     The FTA disbanded late last year, and are no longer actively
programming for the IIGS, but Olivier Goguel still managed to bring me some
GS news from France. And, it's from France that we might eventually see a
MultiFinder. In any case, Olivier did give me a disk of his latest
software. Alas, it requires an IBM or compatible. I brought it over to a
friend's to see, and we were both mightily impressed.

     I was able to arrange what I think of as the "Summit Meeting of the
Century" between Olivier Goguel and that GS programming master, Bill
Heineman. The two spent a day together, impressing each other with their
programming abilities. It's just possible that we'll see a joint project
coming from that meeting.

       * In the rumor department, I've been hearing a lot recently about
One World Software Wizards, a new group of Apple IIGS programmers whose
plans include a freeware CAD program and a new version of NoiseTracker.
It's even rumored that the founder of the FTA is going to be involved. Stay
tuned, in future months, to see if anything comes from these great plans.

      ** Joe Kohn is a Contributing Editor for inCider/A+ Magazine, and
writes the monthly "Shareware Solutions" and "Grapevine" columns. He also
writes a monthly column for Softdisk G-S, and is the Founder and President
of Shareware Solutions: The User Group. Connections is his monthly column
that is distributed as Copyrighted Freeware. Write to Joe Kohn at 166
Alpine Street, San Rafael, CA 94901. Send a self addressed stamped envelope
if you'd like a personal reply. Or, contact Joe online. He shouldn't be too
hard to locate on America Online, CompuServe or GEnie.


        ////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "You'll laugh, you'll cry, you'll kiss 3 bucks goodbye!" /
      ///////////////////////////////////////////  D.MCNAMEE  ////



[EOA]
[AII]//////////////////////////////
                        APPLE II /
/////////////////////////////////
Apple II History, Part 5
""""""""""""""""""""""""
By Steven Weyhrich
      [S.WEYHRICH]

 

                        >>> APPLE II HISTORY <<<
                        """"""""""""""""""""""""
                Compiled and written by Steven Weyhrich 
                  (C) Copyright 1991, Zonker Software 
 
                          (PART 5 -- THE DISK II)
                            [v1.1 :: 12 Dec 91]

INTRODUCTION   The second most important device that Steve Wozniak
""""""""""""   invented, after the Apple II itself, was the Disk II drive.
It was the availability of the floppy disk that catapulted this computer
into prominence beyond its competitors of the day, and ensured that it
would survive for the long haul.  Certainly, something was necessary to
overcome the less-than-intuitive cassette interface built-in to the Apple
II.  That was one place where the TRS-80 had it over the Apple II:  A real
cassette operating system.  Of course, which computer survived the long
run...hmmm?


THE DISK II   Let's put some more trash into Mr. Fusion to fuel the next
"""""""""""   leg of our trip.  How about one of those KIM-1 computers
over there in the corner of the Computer Faire auditorium?  We might have
to break it up a bit to make it fit ... Okay, now we'll just make a small
jump, to December of 1977.  By this time the Apple II had been generally
available for about six months.  Most customers used their television as an
inexpensive color monitor, and used a cassette recorder to store and
retrieve their programs and data.  Apple's major competitors were the
TRS-80 and the Commodore PET.  The products made by these two companies,
together with Apple, could be considered as the second generation of
microcomputers; they all came fully assembled and ready to use out of the
box, with a keyboard and cassette interface.  The TRS-80 and the PET even
came with a monitors and cassette recorders.  The strength of the Apple was
expandability and graphics, while the strength of the others was cost (both
the TRS-80 and the PET sold for around $600, half the price of the Apple
II).

     By late 1977, Apple had introduced some enhancements to the II,
including their first version of a floating point BASIC (called
"Applesoft") on cassette, and a printer interface card to plug into one of
the slots on the motherboard.  But the Apple II still needed something to
make it more attractive to buyers, to stand out above the TRS-80 and the
PET.  One area that needed improvement was its program and data storage and
retrieval system on cassette; it was a continued source of frustration for
many users.  The cassette system used on the TRS-80 was more sophisticated
than that of the Apple II, allowing named files and easier storage of files
and data on the same tape.  On the Apple II it took VERY careful adjustment
of the volume and tone controls on the cassette recorder to get programs or
data to successfully load.  The Apple cassette system also needed careful
attention to the location on the tape where a program was stored, and was
no more accurate than the number on the recorder's mechanical tape counter
(if it had one).

     Apple president Mike Markkula was one Apple II user that was
dissatisfied with cassette tape storage.  He had a favorite checkbook
program, but it took two minutes to read in the program from the tape, and
another two minutes to read in the check files.<1>  Consequently, at the
executive board meeting held in December 1977 he made a list of company
goals.  At the top of the list was "floppy disk".  Although Wozniak didn't
know much about how floppy disks worked, he had once looked through a
manual from Shugart (a Silicon Valley disk drive manufacturer):

     "As an experiment Woz had [earlier] conceived a circuit that
     would do much of what the Shugart manual said was needed to
     control a disk drive.  Woz didn't know how computers actually
     controlled drives, but his method had seemed to him particularly
     simple and clever.  When Markkula challenged him to put a disk
     drive on the Apple, he recalled that circuit and began
     considering its feasibility.  He looked at the way other computer
     companies--including IBM--controlled drives.  He also began to
     examine disk drives--particularly North Star's.  After reading
     the North Star manual, Woz knew that his circuit would do what
     theirs did and more.  He knew he really had a clever design."<2>

     Other issues that Wozniak had to deal with involved a way to properly
time the reading and writing of information to the disk.  IBM used a
complex hardware-based circuit to achieve this synchronization.  Wozniak,
after studying how IBM's drive worked, realized that if the data was
written to the disk in a different fashion, all that circuitry was
unneeded.  Many floppy disks sold at that time were "hard sectored",
meaning that they had a hole punched in the disk near the center ring.
This hole was used by the disk drive hardware to identify what section of
the disk was passing under the read/write head at any particular time.
Wozniak's technique would allow the drive to do self-synchronization ("soft
sectoring"), not have to deal with that little timing hole, and save on
hardware.

     Wozniak asked Randy Wigginton for help in writing some software to
control the disk drive.  During their week of Christmas vacation in 1977
they worked day and night creating a rudimentary disk operating system,
working hard to get the drive ready to demonstrate at the Consumer
Electronics Show in the first week of 1978.  Their system was to allow
entry of single letter commands to read files from fixed locations on the
disk.  However, even this simple system was not working when Wozniak and
Wigginton left for the show.

     When they got to Las Vegas they helped to set up the booth, and then
returned to working on the disk drive.  They stayed up all night, and by
six in the morning they had a functioning demonstration disk.  Randy
suggested making a copy of the disk, so they would have a backup if
something went wrong.  They copied the disk, track by track.  When they
were done, they found that they had copied the blank disk on top of their
working demo!  By 7:30 am they had recovered the lost information and went
on to display the new disk drive at the show.<3>,<4>

     Following the Consumer Electronics Show, Wozniak set out to complete
the design of the Disk II.  For two weeks, he worked late each night to
make a satisfactory design.  When he was finished, he found that if he
moved a connector he could cut down on feedthroughs, making the board more
reliable.  To make that move, however, he had to start over in his design.
This time it only took twenty hours.  He then saw another feedthrough that
could be eliminated, and again started over on his design.  "The final
design was generally recognized by computer engineers as brilliant and was
by engineering aesthetics beautiful.  Woz later said, 'It's something you
can ONLY do if you're the engineer and the PC board layout person yourself.
That was an artistic layout.  The board has virtually no feedthroughs.'"<5>


THE DISK II: COST   The Disk II was finally available in July 1978 with
"""""""""""""""""   the first full version of DOS, 3.1.  It had an
introductory price of $495 (including the controller card) if you ordered
them before Apple had them in stock; otherwise, the price would be $595.
Even at that price, however, it was the least expensive floppy disk drive
ever sold by a computer company.  Early production at Apple was handled by
only two people, and they produced about thirty drives a day.<6>,<7>

     Apple bought the drives to sell with Woz's disk controller from
Shugart, right there in Silicon Valley.  To cut costs, however, they
decided to go to Alps Electric Company of Japan and ask them to design a
less expensive clone.  According to Frank Rose, in his book "West Of Eden":

     "The resulting product, the Disk II, was almost obscenely
     profitable:  For about $140 in parts ($80 after the shift to
     Alps) [not counting labor costs], Apple could package a disk
     drive and a disk controller in a single box that sold at retail
     for upwards of $495.  Better yet was the impact the Disk II had
     on computer sales, for it suddenly transformed the Apple II from
     a gadget only hard-core hobbyists would want to something all
     sorts of people could use.  Few outsiders realized it, but in
     strategic terms, Woz's invention of the disk controller was as
     important to the company as his invention of the computer
     itself."<8>

                               [*][*][*]


NEXT INSTALLMENT  The Apple II Plus
""""""""""""""""
NOTES
"""""
     <1> Gregg Williams and Rob Moore, "The Apple Story, Part 2: More
         History And The Apple III", BYTE, Jan 1985, pp. 167-168.

     <2> Paul Freiberger and Michael Swaine, "Fire In The Valley, Part Two
         (Book Excerpt)", A+ MAGAZINE, Jan 1985, p. 45.

     <3> Williams and Moore, "Part II", p. 168.

     <4> Freiberger and Swaine, (Part Two), p. 45.

     <5> Freiberger and Swaine, (Part Two), p. 46.

     <6> -----, "A.P.P.L.E. Co-op Celebrates A Decade of Service",
         CALL-A.P.P.L.E.,  Feb 1988, pp. 12-27.

     <7> -----,  "Apple and Apple II History", THE APPLE II GUIDE, Fall
         1990, pp. 9-16.

     <8> Frank Rose, WEST OF EDEN: THE END OF INNOCENCE AT APPLE COMPUTER,
         1989, pp. 62.

 
        /////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "I'm *sold* out & _all_ shipped out... Whew, 26 outa 32.. /
      / GEnie is a wonder!!!"                                     /
     ////////////////////////////////////////////  T.EVANS21  ////



[EOA]
[LIB]//////////////////////////////
              THE ONLINE LIBRARY /
/////////////////////////////////
Yours For The Downloading
"""""""""""""""""""""""""
By Mel Fowler 
    [MELSOFT]


            >>> TOP 10 LIST FROM THE AUGUST UPLOADS <<<
            """""""""""""""""""""""""""""""""""""""""""

     These are among the best UPLOADs to the A2 Software Library in the
past month.   Please check them out.   You will not be disappointed.

                               [*][*][*]

 
19253  Name: CASTLE.ARMS.BXY  V1.1      Castle Arms is a two player
game for the Apple IIgs.  The object is to volley a cannon shot across
a random terrain and strike the opponent's castle.

19246  Name: DYAINSTS1.BXY      This is a packed disk full of
instruments created by the DYA.  They can be used with SoundSmith and
NoiseTracker.

19244  Name: TIMELORDDEM.BXY     This is a demo version of TimeLord
IIGS a new adventure, role playing game from DreamWorld Software.
Great game.

19224  Name: COMP.KB.BXY  V4.01     This is an upgraded and fixed of
the V3.0 and allows for multiple users on a 3.5" disk or hard drive
only.   This version is also SHAREWARE.

19215  Name: DE.DEMO.BXY      Freeware demonstration of the Desktop
Enhancer V2.0 from Simplexity Software.  This is an excellent enhancer
for the System 6.0 Finder.  Check it out.

19205  Name: BOGGLED.GS.BXY      Boggled GS is a GS implementation of
the popular word game, Boggle (TM Parker Brothers).  In Boggled, a 4 by
4 grid of randomly generated letters is displayed, and you must find
words linked by connecting letters.   An excellent word game.

19188  Name: UMDEMO.BXY     This is a crippled version of Universe
Master v1.0 from Econ Technologies, Inc. UM is a fully integrated,
desktop based disk management utility written specifically for System
6.0. It includes backup/restore, volume repair, and a wide variety of
other essential functions.

19183  Name: STARTREK.FC.BXY      This is Star Trek: First Contact, a
game based on Star Trek: The Next Generation.  No two games are ever
exactly the same.  Planet locations and intruder intentions are
randomized before each game begins!  Written using Zbasic this version
of Star Trek will work on any Apple II with 128k of memory.

19177  Name: BJTUTOR.3.0.BXY     Use Blackjack Tutor to play the game
of blackjack, learn playing  strategies, and test those strategies over
hundreds of hands. Changes for version 3.0: keep detailed statistics
for each decision, and write to spreadsheet- compatible file;
optionally display # cards & tens left in deck; allow user to set
betting strategy.

19164  Name: APLWRTR.2.1.BXY     As of 24 July 1992, Paul Lutus has
allowed Applewriter 2.1 to be classified as Freeware. Applewriter 2.1
(NOT any other version!) may be freely copied and distributed.  This is
a 5.25 DISK archives.   This is the predecessor to AppleWorks.  It will
work on any Apple II with 64K of memory.

 
      ////////////////////////////////////////////// GEnie_QWIK_QUOTE ////
     / "Actually if you mail an envelope with nothing but a disk in it, /
    / it can pass for 29 cents.  But as soon as you put a note (so we  /
   / know what you want) and a check in it, it exceeds one ounce and  /
  / costs 52 cents."                                                 /
 /////////////////////////////////////////////////  J.EIDSVOOG1  ////



[EOA] 
[SOF]//////////////////////////////
             SOFTDISK PUBLISHING /
/////////////////////////////////
Softdisk Publishing On GEnie
""""""""""""""""""""""""""""



Welcome to Softdisk Publishing Online...

     Softdisk and Softdisk G-S are disk-based magazines for the Apple II
family of computers.  Each month we deliver useful applications, unique
AppleWorks(tm) templates, fun and challenging games, dazzling Print
Shop(tm) graphics, and informative commentaries on one 3.5-inch disk or two
5.25-inch disks (Softdisk only).

     If you are a programmer, Softdisk may have a special treat for you.
We offer the serious programmer an alternative to shareware offering prices
that range from 100.00 to...the skies the limit.  We may not make you rich
beyond your wildest dreams, but we have helped pay the bills of some of the
most deserving programmers, artists, and gurus in the software world.

     Please use this area provided by A2Pro for your support or programming
questions, as well as a two way commentary with the gang who puts the disk
together.  We are looking forward to making your Apple II soar!

 Lee Golden
 Managing Editor,
 Softdisk and Softdisk G-S

You can contact me at:

 Softdisk, Inc.
 Attn: Lee Golden
 606 Common St.
 Shreveport, LA 71130-0008
 GEnie: SOFTDISK.INC



ARE YOU A CLOSET A2 PROBRAMMER?   Are you a closet Apple II programmer, a
"""""""""""""""""""""""""""""""   pixel Picasso, or an Appleworks(tm)
template machine?  Do you wish there were a way to gain world wide
recognition for the work you do when you know it's high quality material?
Do you like money?

     If you answered yes to any of the above questions, please read on.
If you answered no, read it all the same.  If you answered no to the last
question we may need to have a serious talk.

     Seriously, Softdisk is in the business of providing a monthly
magazine on disk to over 10,000 subscribers and we need to get our material
somewhere.

     That somewhere could be you.  Everyday we receive disks full of
programs, graphics, and Appleworks templates for publication consideration.
Although the majority of programs don't make it onto the disk on the first
cut, most are honed to softdisk standards with the specs that we provide
after a complete review.

     Program prices range from 100.00 to more money than any human should
make on a 1 or 2  week fun programming project.  Most fall into the 350.00
to 550.00 range with those few exceptional programmers planning a vacation
to Las Vegas with gambling money!  Hey shareware is great, but we GUARANTEE
FULL PAYMENT on publication, so you know what you're going to make.  A wish
list will be provided here on GEnie and we have loads of routines that you
can use.  All we ask is that you sign our non-disclosure for our
routines...if this stuff fell into the wrong hands it could be the end of
civilization as we know it! :)

 Lee Golden
 Softdisk Publishing 
                 (SOFTDISK, CAT31, TOP2, MSG:1/M530)


 
INTERESTED?   If you are interested in submitting to Softdisk or Softdisk
"""""""""""   G-S, we've uploaded some documents you may be interested in:

   2826  Softdisk Submitter Guidelines              (Teach)
   2825  Softdisk G-S Programming Guidelines       (Teach)
   2824  Softdisk (8-bit) Programming Guidelines   (Teach)
   2823  Softdisk Submitter Guidelines              (ASCII Text)
   2822  Softdisk G-S Programming Guidelines       (ASCII Text)
   2821  Softdisk (8-bit) Programming Guidelines   (ASCII Text)

     The Teach documents use the Bookman and Courier font families and
look _very_ sharp when printed on a LaserWriter...

     Also, if you'd like, we'd be happy to mail you a copy of these
documents (in case you have trouble downloading, or just want the nice
Laser printed versions :)  -Zak
                  (SOFDISK, CAT31, TOP6, MSG:1/M530)
 

 
CHECK IT OUT...   I'm not sure when you saw your sample disk, but Softdisk
"""""""""""""""   G-S has undergone _radical_ changes in the last 18
months.  First, our rule of thumb is 10 programs per three issues (it used
to be 5 :).  So, here's a short listing of the last few issues (and of the
next couple to come):

Issue #32 ---------
   Word Search............ Just what the title indicates :)
   Home Refinancer........ I learned a lot doing this one :)
   World Tour: Australia.. Part of a series

Issue #33 ---------
   Crazy 8's.............. the card game, with variations too
   Lift-A-Gon............. 30 levels of Fox-and-Geese puzzle thingy (cool:)
   Text Wizard............ convert text formats including Softdisk issue text
   The Optimizer.......... er, ignore this one (see below)

Issue #34 ---------
   BlockWords............. spell words on a 4x4 grid, very well-done
   Easy Eyes.............. change the gs colors to be more pleasing
   The Analyst 1.1........ just what is in your system anyway? Issue #35 ------
---
   Nucleon........... arcade game that can be addicting as anything
   SeeHear............ NDA/Finder Extension to view graphics and hear sounds
   World Tour: Pacific another in the series
   The Optimizer v1.0.1 scrunch resource forks in programs and stacks

Issue #36 ---------
   Sound Wizard....... convert sounds between various formats
   Canasta............ a card game 
   Quizzical G-S...... an educational tool/game/program thing
   ZMaker............. Mass format/verify/copy/compare 3.5-inch disks

Issue #37 ---------
   Son of Star Axe.... the legend continues!
   Mintrel w/new songs MIDI Synth song player NDA
   QuickForms......... An app for dealing with form letters


     Ok, these are just the _programs_ on our issues (and don't blame me
for the weak descriptions, I'm just a programmer :).  Don't forget that we
are a "magazine-on -disk" with all that implies: editorials, columns,
reviews, reader's write, professor know-it-al, and what-not.  We also
publish clip-art, clip-sounds (new!) , awgs templates, print shop stuff and
more!  (really, there is more :)

     Not bad for $10 a month eh?  (less if you get it by the year :)
-Zak

(btw: All of the programs on Softdisk G-S have a _very_ professional look
and feel.  The Human Interface is something that is very important to me,
and I really nitpick on interface issues--just ask some of the submitters
that have dealt with me.  In the end, the subscriber is the winner though.)
                  (SOFTDISK CAT31, TOP3, MSG:7/M530)


      ////////////////////////////////////////////// GEnie_QWIK_QUOTE ////
     / "In 1 Second the eye sends 1 billion messages to the brain (it   /
    / has a 1 BIP, I/O port).:-)  Your eye can sense about ten million /
   / gradations of light and seven million shades of color. Whats     /
  / that mean as far as 24bit color?  I can only see 12 bits at a    /
 / time if I turn my brightness button up and down.:-)"             /
///////////////////////////////////////////////////////  REALM  ////



[EOA]
[GEN]//////////////////////////////
                    GEnie ONLINE /
/////////////////////////////////
En guarde!
""""""""""
by Pat Hart
  [P.HART4]



                          >>> ONLINE CHESS <<<
                          """"""""""""""""""""

Doing It Online   Some of you have probably heard that you can now play
"""""""""""""""   USCF-rated postal chess on Compuserve. This makes some
sense since it is the largest system, but it is also (probably) the most
expensive pay BBS around. But there is an alternative here on GEnie and it
is part of Basic*Service to boot. Imagine playing against players
throughout the country for twelve hours a day at no extra charge! (NOTE:
See end of article for information on new pricing structure for the RSCARDS
Multi-player games effective October 1, 1992. -ed.)  To get started, simply
go to page 875 to familiarize yourself with the RSCards games and how to
get your graphic front-end program for your particular computer model. Then
go to page 882.

     So how is the competition you ask? There are over four hundred thirty
GEnie-rated players as I write this and the list is growing. I have found
eight USCF Masters (including International Master Doug Root), eleven
Experts, sixteen A-players, eleven B-players, six C-players, a sprinkling
of lower-rateds and some who may not have ever played tournament chess, but
are none-the-less strong players. There are online tournaments played over
two weeks to minimize connect time since they cost $6/hour, with 100% of
the entries returned in prizes -- 80% in cash and 20% in free connect time.

     When you first enter the chess Room, the current Tables are displayed
along with the player's handles. (I use Golden Knight.) You can either
challenge another player in the Room or move to a Table to sit in the
Gallery and watch and kibitz. Once you sit down to play, you choose the
time limit first (5-minutes up to no clocks). When both agree, the game
begins automatically. If you get disconnected or have to leave, the game
can be saved and finished at another time. Colors are assigned randomly
when both are due the same color, otherwise you alternate.

     The chess program, written by John Weaver, Jr., is surprisingly
capable with many built-in automatic features: (a) makes your move if it is
forced; (b) moves a piece when selected if there is only one legal move;
(c) declares a game drawn when there is insufficient material to mate when
the clock expires, after 50 moves, or threefold repetition (but only if in
succession); (d) prevents resignation before the tenth move; and, (e) the
option to squelch messages from kibitzers or other games in progress.

     It has been an interesting experience playing unseen human opponents
on a computer. It is exactly like playing a computer chess program if you
use the graphic front-end program available. But, do not forget, it is not
a chess program! It is live. It is fun. Trust me. So, come on by and play
me a game. I am gentle. Ask anyone! <grin>

                               [*][*][*]


ATTENTION!   GEnieLamp has just received the following notice in regards
""""""""""   to the RSCARDS RoundTable.  RSCARDS will be changing their
pricing structure effective October 1, 1992.  GEnie subscribers will be
able to access RSCARDS games (Blackjack, Backgammon, Checkers, Poker,
Reversi, and Chess) via two options:

     1) The standard $6.00 per connect hour non-prime time rate,

        or

     2) As a member of the RSCARDS Club. RSCARDS Club membership
        will cost $30.00 per month, and entitles the member to a
        discounted rate of $3.00 per hour for unlimited non-prime
        time play of any RSCARDS games at speeds up to 2400 baud.

     The institution of the RSCARDS Club also marks the end of our seventh
RSCARDS game, Basic*Chess, which will be discontinued on 10/1/92. Full
details of the RSCARDS Club, and signup information will be available on
10/1/92. Type RSCARDS at any menu page prompt or move to GEnie page 875.

     As a bonus, anyone who played any of the RSCARDS games (including
Basic*Chess) between June 15, 1992 and September 14, 1992 will be entitled
to join the RSCARDS Club and have their first month's membership fee WAIVED
if they sign up for the RSCARDS Club between 10/1/92 and 10/15/92. Hourly
charges will still apply, and subsequent months' membership fees will be
charged at the standard $30.00 per month.

     The RSCARDS system allows you to play real-time multi-player games
with GEnie users from all over the world, in text or with an optional
graphics driver program (available online for the Atari ST, the Commodore
Amiga and 128 computers, the Apple Macintosh and //gs, and IBM
compatibles).

     Full details of the RSCARDS Club, and signup information will be
available on 10/1/92. Type RSCARDS at any menu page prompt or move to GEnie
page 875.

    NOTE: Rates quoted are for 300/1200/2400 baud access from the United
          States. A Club Plan will also be implemented for Canada, and
          details will be announced shortly. Non-Prime time is from 6:00 PM
          to 8:00 AM local time Monday through Friday, and all day on
          Saturday, Sunday and holidays. Communications surcharges, if
          applicable, still apply.

     Discussion of the RSCARDS Club is taking place in the Multi-Player
Games Bulletin Board, Category 29, Topic 10. (Type *MPGRT to get there, or
move to GEnie page 1045;1).

 Howard Rosenman 
 Product Manager 
 GEnie Games and Entertainment


    ///////////////////////////////////////////// GEnie_QWIK_QUOTE ////
   / "From a former Prodigy pawn ---- THANK GOODNESS FOR GENIE!!!!!" /
  ///////////////////////////////////////////////////  C.METHOD  ////


 
[EOA]
[HAC]//////////////////////////////
                   HACK'N ONLINE /
/////////////////////////////////
[*]HST Modem Upgrade
""""""""""""""""""""
by Bill Yung
   [W.YUNG1]



          >>> UPGRADING AN HST ONLY MODEM TO DUAL STANDARD <<<
          """"""""""""""""""""""""""""""""""""""""""""""""""""
    
PLEASE REMEMBER!   You are responsible for any damage or  liability when
""""""""""""""""   you  make  any  modifications  or  upgrades  to  your  
equipment.  Also keep  in mind that  opening your computer may void your  
guarantee.  If you  are unsure  of your  ability to  take on a  hardware  
project, find someone who isn't.

GIF ILLUSTRATIONS   To help you with this upgrade, you can find a series
"""""""""""""""""   of  GIF illustrations by the author in the GEnieLamp
RoundTable  on  page 515.  (Keyword: GENIELAMP)   Download:  HST_GIF.ARC

                               [*][*][*]

 
A v32.bis SOLUTION!   Times change quickly in the world of high-tech
"""""""""""""""""""   electronics and the world of telecommunications is
certainly no exception.  It wasn't long ago v.32bis was conceived. Now,
with the advent of modems supporting this type of modulation at very
affordable prices, new high speed users are coming on-line faster than ever
before. In the past, US Robobtics dominated the high speed market with
their HST modulation.  Gradually, the tide seems to be turning and HST
users are finding themselves unable to connect at high speed with the
increasingly popular v32.bis modems. If you've found yourself in this
predicament, read on and you may find an inexpensive solution to your
plight.


The Upgrade   The upgrade about to be described is not possible with all
"""""""""""   HST only modems. The particular upgrade pertains only to the
newer model Courier HST 14.4 modems. The newest 16.8k modems are quite
different from the previous 14.4 models and I have not had the opportunity
to investigate the possibility of performing a similar upgrade on those
nor on any older models.  Refer to the graphic illustration to determine if
you own an HST model suitable for this upgrade. I have personally performed
this upgrade and have verified it's reliability on my own Courier HST. The
largest benefit of doing this upgrade is the substantial savings involved.
The value of this upgrade is somewhere around $300-$400 (based on the cost
of a new v.32bis unit); however, the cost of required materials is
approximately $60. If you are handy with a soldering iron, the entire
process should take about 8 hours.

    The most difficult part of the operation was finding parts sources.  I
was able to obtain everything required in about 1 week. I was informed the
main DSP (Digital Signal Processor) chip could take up to 12 weeks to be
delivered since it is a "highly allocated" part.  This did not prove to be
the case as it showed up a mere 5 days after I placed the order.
Hopefully, everyone else will receive the same surprisingly fast delivery.
I'll include the sources for all necessary components below for your
convenience. Just to clarify USR's position on the matter of parts
procurement: They will not assist you in any way. They do not sell parts.
In fact, there is no upgrade kit available for the type of modem to which
this procedure is applicable.  USR will upgrade the unit for a fee of $350
according to Mark Eric of HST. This was the only information he was willing
to offer.


How Dey Do Dat?   The HST modulation is asymmetrical. Data travels at 14.4k
"""""""""""""""   bps in one direction while the back channel proceeds at
450 bps. In order to serve as a v.32bis modem, we must install the
necessary components to provide for 14.4k bps operation in both directions.
There are illustrations to accompany this text and they do aid in
determining if you have an appropriate model and in finding the correct
position to install the new chips. In the event you are unable to obtain
the graphic portions of this article, I will attempt to give a complete and
accurate enough description to facilitate the successful completion of the
project without them.

    The first step is to open the case by removing the rubber feet at the
rear of the case and the two phillips screws beneath them. The case can now
be opened. You are now looking at the guts of one the best modems in the
world. What? You're not impressed? Try removing the metallic shield that
isolates the digital from the analog. There, that's better. The area you've
just revealed is the focal point of our work and is shown in detail in
Figure 4. Immediately noticeable should be several spaces suitable for
mounting the necessary circuitry. If there are no unused spaces, you don't
have the proper model for upgrading. Welp, it was worth a shot, huh? Thank
you for your patience in bearing with me this far. I bid you farewell. If
you DO notice the aforementioned spaces, you're about to become the proud
owner of a USR Dual Standard modem. As you further inspect the unit, you
will notice the pc board is well marked. There will be very little doubt
concerning where the parts are to be mounted. If you notice empty spaces
that do not correspond to the details I'm about to present, you probably
have an older model. Drop me a line on GEnie and maybe we can come up with
a solution.

    Some desoldering is required to clear the holes for mounting our new
parts. This can best be done with a desoldering iron. Radio Shack carries
one for under $10 that does a good job. I would also recommend the use of
desoldering braid for the more stubborn spots.  This too is available at
any local electronics shop, Radio Shack included. The task of clearing the
solder out of all the necessary holes is the most tedious portion of this
upgrade as it involves clearing a couple hundred holes. As desoldering
goes, it's a straightforward operation because there are very few paths on
the bottom side of the pc board. In light of this fact, every effort should
be made to clear the holes from the bottom so as to avoid damaging traces.

    All of the desoldering having been completed and the pc board ready for
the new parts, refer to the following parts list with associated pc board
silkscreen labels. If you don't have the graphic portion of this article,
refer to these pcb labels to determine the correct location for parts
placement.


A Word Of Caution   Before installing an IC pay special attention to it's
"""""""""""""""""   orientation as marked on the pc board. Unlike many
circuits which have all chips oriented in the same direction, this circuit
follows no such convention. Pay particular attention to the new DSP which
is rotated 90 degrees from the existing DSP.


 PCB label       Description       Source           Part #         Price
 """""""""       """""""""""       """"""           """"""         """""
             68 pin PLCC socket   Easy Tech         PLC68          2.29
 U206        T/I 33MHZ DSP         Arrow        TMS320C25FNL33    25.00
 L8          6.8 uH RF Choke      Easy Tech          CH68          1.29
 U207-U208   8k 25ns Static ram   Easy Tech        6264BP25        8.25
 U209-U210   Octal buss xcvr      Digi-Key         74HCT245         .77
 U211-U212   Octal buffer/drvr    Digi-Key         74HCT541         .74
 R201,2,4    10K resistors        Digi-Key         P10ke-nd        5.99
 Cap Type 1  .01 uF SMD caps      Mouser        140-CC501B103K      .49
 Cap Type 2  .1 uf SMD caps       Mouser        140-CC502B104K      .69
 Cap Type 3  100pf SMD cap        Mouser        140-CC501N101J      .35
 """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Capacitors
""""""""""
Type 1
C201,203,211,213,215,217,223

Type 2
C204,212,214,216,218,221,222

Type 3
C224


Some Notes About Suppliers   The most important chip is the DSP. It's by
""""""""""""""""""""""""""   far the most expensive and the hardest to
find. I found a local Texas Instruments dealer who would order them for me
at ten dollars each but I would have to buy 20 of them. I also found them
in stock at Hamilton Avnet but there is a $100 minimum order. Arrow
Electronics is a national distributor with a $25 minimum and this turned
out to be the best source for a single part. Even though they did not have
the part in stock and warned of a long delay, the chip arrived within a
week in a 3X1X1 box. No, not 3 inch by 1 inch by 1 inch. Three feet by 1
foot by 1 foot! These guys really know how to pack a chip. The packaging
included a large, military spec desiccant, a humidity indicator, static
shielding barrier film (with label indicating relative humidity when
opened), a three foot plastic chip carrier and lots of packing popcorn.
Wow!  Needless to say, when installed, the chip worked fine.

     Supplier                    Phone Numbers           Terms       
 """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
   Arrow Electronics            1-800-321-3837        $25 minimum         
   Easy Tech                    1-800-582-4044         no minimum
   Digi-Key                     1-800-344-4539        <$25 = $5 fee
   Mouser                       1-800-346-6873        <$20 = $5 fee


Substitutions   The pc board is designed to accept surface mount resistors
"""""""""""""   and capacitors. These are sometimes hard to obtain in
small quantities.  The $5.99 price for the 10k resistors represents the
price for 200 of them.  Even so the plastic tube they came in is no bigger
than my little finger.  Although they're really tiny, I had little trouble
handling them and mounting them with conventional equipment.  It might not
be a bad idea to substitute conventional resistors and capacitors of the
appropriate values for the surface mount devices.  Such substitutes can be
found at any local electronics shop. There is no real space restriction to
prevent you from using the more common (albeit much larger) parts. If by
chance you have trouble finding the 6.8 uH rf choke, you may place a jumper
from L7 to provide the necessary B+ to the IC's.


Finally, The Fun Part   Reassemble the modem and type ATI7 from your
"""""""""""""""""""""   favorite terminal program. The modem should
                        respond with:

ATI7
Configuration Profile...

Product type           External
Options                HST,V32
Clock Freq             16.0Mhz
Eprom                  64k
Ram                    32k

Supervisor date        03/05/91
IOP date               10/11/90
DSP date               03/04/91

Supervisor rev         3.0
IOP rev                1.3
DSP rev                10

OK

     Did it work? Are you leaping with glee? Do I detect a tear of joy in
your eye? I thought so. Congratulations. You've done a fine job. You may
now be able to sell your modem for almost what you paid for it.  Isn't that
an enchanting thought? Thanks for coming along for the ride and have fun
with your new dual standard.


Testing   There's only one command set option needed to enable the V.32bis
"""""""   handshaking. It's ATB0. Change this parameter and write it to
NVRAM.  From now on, your modem will attempt to negotiate a v.32bis
connection with any modem it dials. If you call another dual standard, you
will connect as a v.32bis, NOT HST. Therefore, you may want to change back
to ATB1 when dialing duals. When dialing HST only modems, an HST connection
will be made after a brief attempt at v32.bis.  If you have trouble making
a v32.bis connection, try dialing the GEnie 9600 line in your area. I've
experienced no trouble connecting with GEnie at high speed.


WHAT? It Doesn't Work?   Here's some things to try if you didn't achieve
""""""""""""""""""""""   immediate success:

     If the modem is completely dead:
 
     Check the fuse. If it's blown, there's a reason why it blew.  Don't
just replace it and try it again. Try and find the cause of the trouble by
looking for:

    1) Shorts across leads of any capacitor.
    2) IC's which may have been installed with incorrect orientation.
    3) Globs of solder left from desoldering which may be shorting.
    4) A broken trace in a part of the modem you shouldn't be
       touching in the first place. Remember, we're adding a new
       circuit, not tampering with the old ones.
 

     If the modem lights up and echoes back characters but doesn't show
HST,V32 in the second line of the ATI7 response:

    1) Type ATI2 from your terminal program. This checks the ram.
       you now have two banks of ram and should subsequently receive
       two OK's as a response. Like this:
ATI2
OK

OK
       If you receive the correct response, the trouble is not in
       your ram. If you do not receive two OK's, make sure you have
       35ns or faster static rams.

    2) Did you remember to install L8? If you don't have a 6.8uh
       rf choke, did you install the jumper properly? The jumper should
       extend from the lead of L7 that is farthest from the edge of
       the modem to the through hole for L8 that is likewise, farthest
       from the edge.

    3) Did you install the DSP correctly in it's socket? Remember, it
       does not face the same way as the existing DSP.

    4) Did you make a parts substitution other than using conventional
       instead of SMD resistors and capacitors? For example:
       Did you use 74LS541's instead of 74HCT541's?
    5) Nearly all connections are made on the top side of the pcb.
       check the legs of the chips on the top side to verify the 
       establishment of a good solder joint.
    6) Also check for the items mentioned above in the "Completely
       Dead" section.


    Hopefully, any problems will be resolved using these methods. If not,
have yourself a good long cry because you trashed a $600 modem and your
wife will never let you buy another in a million years.  She may even
confiscate your tools to prevent you from destroying anything else around
the house. (And we couldn't blame her one bit) Go ahead, get it out of your
system. It's not good to keep that kind of emotional loss all bottled up.
Okay, alright, enough of that. Get a grip on yourself. Take a couple deep
breaths. Chant your mantra.  Relax.....

    Just kidding. In reality, if you've exhausted every other option, leave
me e-mail on GEnie and I'll try to bail you out but I can't be responsible
for your actions or the quality of your work. Take your time when you do
this upgrade.  It's not a race. There is no prize for he who completes it
the fastest. If you absolutely can't live without a modem for even one day
(like me), do the desoldering one night and the soldering the next. It took
me about 6-8 hours to do this, most of which was spent desoldering.


Performance   I'm a relative novice to v.32bis so I'm not sure how the
"""""""""""   modem should perform but I have noticed some shortcomings in
the v.32bis type modulation. First of all, there is one bbs with which I
have trouble making a connection. The problem is intermittent. Usually when
I DO have the trouble the modem will hang up while negotiating error
correction. As soon as the ARQ light comes on, I get a NO CARRIER.  This
doesn't happen all the time and only with that one bbs.  (The Woodworks.
Thanks, Tim) Secondly, the modem realizes cps rates around 1300 even though
the result string indicates I'm connected at 14400/V32. With HST
modulation, 1650 cps is typical.  Even so, 1300cps is noticeably faster than
280cps. (To say the least!) Other than that, I'm just ecstatic over the
whole episode.


Acknowledgements   I didn't figure this out without help from others.
""""""""""""""""   Those who contributed know who they are and I wish to
thank them sincerely.  Thanks to Atari for making a computer for those of
us without patience, money or a doctorate. To those who offered nothing but
disinformation and discouragement, I still think you make the best modem in
the world. With that said, I have no further axe to grind and nothing nice
to say either, leaving me with no alternative but to terminate our little
chat....
                                               -Bill Yung


        //////////////////////////////////////// GEnie_QWIK_QUOTE ////
       / "One of the continuing charms of the Apple II world is the /
      / kind of service you get from some of the the suppliers.    /
     / It's like being in a small town where people know and      /
    / trust each other.  I love it.  :)"                         /
   ///////////////////////////////////////////////  A2.BILL  ////



[EOA]
[ELS]//////////////////////////////
                       ELSEWHERE /
/////////////////////////////////
Connecting The World
""""""""""""""""""""
by Chris Carpenter
    [C.CARPENTER3]


                       >>> THE BBS ROUNDTABLE <<<
                       """"""""""""""""""""""""""
                        ~ Connecting the World ~
                               Page 610

     What's the difference between a local bulletin board (BBS) and GEnie?
Only size and accessibility of GEnie which allows real time interaction
between many users at the same time.  Now local BBS SysOps (as well as
anyone aspiring to possibly be a SysOp) have a place to talk, leave
messages or exchange files on GEnie.  It's the BBS Roundtable, page 610 and
although it has been around for quite a while it's being given a facelift,
the cobwebs are being swept away and Real Time Conferences (RTC's) are
starting up again every Thursday night at 7:00 o'clock PT.

     The newly appointed, helpful SysOps in this RT are:

                    Leonard Reed (BIBLIA), Chief SysOp
                   Dave Cole (MACLAMP), Macintosh SysOp
              Tony Newman (UHH.CLEM), PC/Clone and CP/M SysOp
                Chris Carpenter (C.CARPENTER3), Atari SysOp

     So if you are an active SysOp or an aspiring SysOp wanting to know
more about what running a BBS entails...stop by and read some messages or
leave a message of your own, check out the BBS related files for your
particular computer (upload some if you think they'll help someone else)
and be sure to show up on Thursday nights for the RTC's which focus on a
different computer platform each week.  You can talk with the SysOp's, all
of which have experience with local BBS's, or with other folks who might
stop in like you and have just the answer you've been looking for or might
need an answer that you can give...and after awhile every RTC turns into a
simple social affair where you can just talk with other people sharing the
same interests.

     The BBS RT is once again growing and all of the SysOps extend an
invitation to everyone to come by and discover an online community with
similar interests, mainly 'Connecting the World' through BBS's.  We hope to
make this the most active area on GEnie but it's up to you...by using this
resource that is available to you and contributing when you can.  With
your help we hope to establish a service on GEnie that will represent the
best storehouse of BBS information and related files in the world, and one
of the few places in the world where you can talk to others with similar
interests without leaving the comfort of your home.


 
[EOA]
[LOG]//////////////////////////////
                         LOG OFF /
/////////////////////////////////
GEnieLamp Information
""""""""""""""""""""""

    o   COMMENTS: Contacting GEnieLamp

         o   GEnieLamp STAFF: Who Are We?

              o   GET_THE_LAMP Scripts & Macros   

                    o   SEARCH-ME! Answers



GEnieLamp     GEnieLamp is monthly  online magazine  published  in  the
"""""""""     GEnieLamp  RoundTable  on page 515.   You can  also  find
GEnieLamp in the ST (475), the  Macintosh (605), the IBM (615) Apple II
(645),  A2Pro (530), Unix  (160),  Mac Pro (480), A2 Pro (530) Geoworks
(1050),  BBS (610)  CE Software  (1005) and  the  Mini/Mainframe  (1145) 
RoundTables.  GEnieLamp can also  be found  on CrossNet, (soon) Internet
America  Online  and  many public and commercial BBS  systems worldwide.

     We welcome and respond to all GEmail.To leave messages, suggestions
or just to say hi,  you can contact us in the GEnieLamp RoundTable (515)
or at the following GE Mail addresses:

      o  John F. Peters    [GENIELAMP]   Senior Editor/RoundTable SysOp
      o  Kent Fillmore     [DRACO]       Publisher/GEnie Product Manager


U.S. MAIL
"""""""""
                       GEnieLamp Online Magazine
                           Atten: John Peters
                       5102 Galley Rd. Suite 115/B
                       Colorado Springs, CO  80915


GEnieLamp STAFF
""""""""""""""""

  ATARI ST     o John Gniewkowski [J.GNIEWKOWSK] ST Editor
  """"""""     o David Holmes     [D.HOLMES14]   ST TX2 Editor
               o Fred Koch        [F.KOCH]       GEnieLamp [PR] Editor
               o Mel Motogawa     [M.MOTOGAWA]   ST Staff Writer
               o Terry Quinn      [TQUINN]       ST Staff Writer
               o Sheldon Winick   [S.WINICK]     ST Staff Writer
               o Richard Brown    [R.BROWN30]    ST Staff Writer
               o John Hoffman     [JLHOFFMAN]    ST Staff Writer
 

  IBM          o Peter Bogert     [P.BOGERT1]    IBM Editor
  """          o Mark Quinn       [M.QUINN3]     IBM Co-Editor
               o Mark Dodge       [M.DODGE2]     IBM Staff Writer
               o Brad Biondo      [B.BIONDO]     IBM Staff Writer

  MACINTOSH    o James Flanagan   [J.FLANAGAN4]  MAC Editor
  """""""""    o Richard Vega     [R.VEGA]       MAC Co-Editor
               o Tom Trinko       [T.TRINKO]     MAC Staff Writer
               o Bret Fledderjohn [FLEDDERJOHN]  MAC Staff Writer
               o Erik C. Thauvin  [MACSPECT]     Technical Consultant

  APPLE II     o Tom Schmitz      [TOM.SCHMITZ]  A2 Editor
  """"""""     o Phil Shapiro     [P.SHAPIRO1]   A2 Co-Editor
               o Mel Fowler       [MELSOFT]      A2 Staff Writer

  ELSEWHERE    o Brian Bradley    [TRS-ASST]     Staff Writer
  """""""""    o Jeffry Dwight    [JEFFREY]      Staff Writer

  ETC.         o Jim Lubin        [JIM.LUBIN]    Add Aladdin
  """"         o Scott Garrigus   [S.GARRIGUS]   Search-ME!
 
  CROSS-NET    o Bruce Faulkner   [R.FAULKNER4]  BBS SysOp
  """""""""


GEnieLamp CONTRIBUTORS
""""""""""""""""""""""

                   o Mike White        [M.WHITE25]
                   o Bill Yung         [W.YUNG1]
                   o Scott Garrigus    [S.GARRIGUS]
                   o Pat Hart          [P.HART1]
                   o Paul Sadowski     [LOONEY.TUNES]
                   o Joe Kohn          [J.KOHN]
                   o Steven Weyhrich   [S.WEYHRICH]
                   o Darrel Raines     [D.RAINES]
                   o Chris Carpenter   [C.CARPENTER3]
                   o Bill Garrett      [BILL.GARRETT]

 
"GET_THE_LAMP" SCRIPTS NOW ONLINE   GEnieLamp scripts are now available for
"""""""""""""""""""""""""""""""""   our IBM, Atari ST and Microphone
II/White Knight Macintosh readers.  These script files will allow you to
download all the issues, or just the issues you want.  As an added plus,
you can also have Aladdin grab the latest copy of GEnieLamp while you
sleep.  Where can you Get_The_Lamp script?  You'll find the Aladdin scripts
in the GEnieLamp RT, [m515], Aladdin ST RT, [m1000] and the PCAladdin
RT, [m110].  The Macintosh macros for White Knight and Microphone II are
available in the GEnieLamp RT [m515], the Mac RT [m605] and the Freesoft RT
[m585].  Search for LAMP to find the latest version.

           --> Get_The_Lamp.  Scripts and macros make it easy! <--

 
SEARCH-ME! ANSWERS
""""""""""""""""""
 
             + + + + + + + + + + + + + + + + + + + + + + +
             + + + + + + + + + + + + + + A + + + + + + + + 
             + + + + + + + + + + + + + + F + + + + + + + + 
             + + + + + + + G + + + + + + G + + + + + + + + 
             + + + + + + O T N I R P + + + + + + + + + + + 
             + + + + + T + + + + + + + + + + + + T + + + + 
             + + + + O + + + + + + + + + + + + + X + + + + 
             + + + + + + + + + + + T + + + + + + E + + C + 
             + + + + + + + + + S F + N + + + + + N + + O + 
             + + + + + + + + + O + + + A + + C + + L + M + 
             A S S E M B L Y R I + + M + T I + + R O + P + 
             W + + + + + + + + B + E + + S S + + E O + I + 
             R + + + + + + + + + M + + A G + N + A P + L E 
             I + + + + + + + + O + + B O + + + O D + + E V 
             T + + + + + + + R + + + S + + + Y + C + + L O 
             E + + + + + + Y + + + U + G + + A + + + A + M 
             + + + + + + + + + + B S O D E + R + + C + + + 
             + + + + + + + + + + + + + + + M R + S + + + + 
             V A R I A B L E + + + + + + + + A A + + + + + 
             + + + + + + + + + + + + + + + + P + + + + + + 

 
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
   Material  published in this  edition may  be reprinted  under  the
   following  terms  only.   All articles  must remain  unedited  and
   include  the issue  number and author  at the top of  each article
   reprinted.  Reprint permission granted, unless otherwise noted, to
   registered  computer user groups and not  for profit publications.
   Opinions  present herein  are those of the  individual authors and
   does  not necessarily  reflect those of  the publisher or staff of
   GEnieLamp.   We reserve  the right  to edit all  letters and copy.
   Material  published in this edition may be reprinted only with the
   following notice intact:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
  (c) Copyright 1992   T/TalkNET  OnLine  Publishing, GEnie,  and  the
  GEnie Computing  RoundTables.  To sign  up for  GEnie service,  call
  (with modem) 1-800-638-8369.  Upon connection type HHH. Wait for the
  U#= prompt.  Type: XTX99368,GENIE and hit  RETURN.  The system  will
  then prompt you for your information.
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
[EOF] 
 
