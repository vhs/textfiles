+++
title = "Apple II Textfiles: Genie Lamp Archives"
description = "Archive of the Genielamp A2, the GEnie Apple II Roundtable"
tabledata = "apple_genielamp"
tablefooter = "There are 66 files for a total of 8,988,221 bytes."
pagefooter = "Note: It appears I am missing two files from the archive I got, one of which is probably named <b>almp9709.asc</b> and the other is Volume 6, Issue 60. If you find them before me, let me know."
+++

Since April 1, 1992, a group of Apple II Enthusiasts on the GEnie Online Network have produced a newsletter covering all aspects of the Apple II series of computers. Well-written, thoughtful articles cover hardware, software, anecdotes, company news, and the occasional off-topic rant.

The current listed run covers all the issues of GEnie Lamp A2 from April 1992 to October, 1997. The newsletter supposedly moved onto the Delphi information service, although none of those newsletters are archived here.
