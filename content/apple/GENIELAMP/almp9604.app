

     |||||| |||||| ||  || |||||| ||||||
     ||     ||     ||| ||   ||   ||
     || ||| ||||   ||||||   ||   ||||               Your
     ||  || ||     || |||   ||   ||
     |||||| |||||| ||  || |||||| ||||||             GenieLamp Computing

     ||    |||||| ||    || ||||||                   RoundTable
     ||    ||  || |||  ||| ||  ||
     ||    |||||| |||||||| ||||||                   RESOURCE!
     ||    ||  || || || || ||
     ||||| ||  || ||    || ||


                    ~ WELCOME TO GENIELAMP APPLE II! ~
                      """"""""""""""""""""""""""""""
              ~ SOFTVIEW A2:  GraphicWriter III v2.0 update ~
             ~ FOCUS ON...:  The Apple Blossom and Juiced.GS ~
     ~ PD_QUICKVIEW:  "In The Breach of Centuries" HyperStudio stack ~
                   ~ HOT NEWS, HOT FILES, HOT MESSAGES ~

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
 GenieLamp Apple II     ~ A T/TalkNET Publication ~      Vol.5, Issue 49
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Publisher................................................John F. Peters
 Editor...................................................Douglas Cuff
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
          ~ GenieLamp IBM ~ GenieLamp ST ~ GenieLamp PowerPC ~
        ~ GenieLamp A2Pro ~ GenieLamp Macintosh ~ GenieLamp TX2 ~
         ~ GenieLamp Windows ~ GenieLamp A2 ~ LiveWire (ASCII) ~
            ~ Member Of The Digital Publishing Association ~
 Genie Mail:  GENIELAMP                  Internet: genielamp@genie.com
////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

           >>> WHAT'S HAPPENING IN THE APPLE II ROUNDTABLE? <<<
           """"""""""""""""""""""""""""""""""""""""""""""""""""
                             ~ April 1, 1996 ~


 FROM MY DESKTOP ......... [FRM]        FROM MY MAILBOX ......... [MAI]
  Notes From The Editor.                 Letters To The Editor.

 HEY MISTER POSTMAN ...... [HEY]        HUMOR ONLINE ............ [HUM]
  Is That A Letter For Me?               You Know You've Been on Genie.

 FOCUS ON... ............. [FOC]        PD_QUICKVIEW ............ [PDQ]
  The Apple Blossom & Juiced.GS.         In The Breach of Centuries.

 FILE BANDWAGON .......... [BAN]        THE ONLINE LIBRARY ...... [LIB]
  Top 10 Files for February.             February Arrivals on Genie.

 SOFTVIEW A2 ............. [SOF]        LOG OFF ................. [LOG]
  GraphicWriter III v2.0.                GEnieLamp Information.

[IDX]""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

READING GENIELAMP   GenieLamp has incorporated a unique indexing system
"""""""""""""""""   to help make reading the magazine easier.  To utilize 
this system, load GenieLamp into any ASCII word processor or text editor.  
In the index you will find the following example:

                   HUMOR ONLINE ............ [HUM]
                    Genie Fun & Games.

     To read this article, set your find or search command to [HUM].  If 
you want to scan all of the articles, search for [EOA].  [EOF] will take 
you to the last page, whereas [IDX] will bring you back to the index.

MESSAGE INFO   To make it easy for you to respond to messages reprinted
""""""""""""   here in GenieLamp, you will find all the information you 
need immediately following the message.  For example:

                    (SMITH, CAT6, TOP1, MSG:58/M475)
        _____________|   _____|__  _|___    |____ |_____________
       |Name of sender   CATegory  TOPic    Msg.#   Page number|

     In this example, to respond to Smith's message, log on to page 475 
enter the bulletin board and set CAT 6.  Enter your REPly in TOPic 1.

     A message number that is surrounded by brackets indicates that this 
message is a "target" message and is referring to a "chain" of two or more 
messages that are following the same topic.  For example:  {58}.

ABOUT Genie   Genie's pricing plans are as low as $7.95 per month for up
"""""""""""   to five hours of email use.  Genie services, such as software 
downloads, bulletin boards, chat lines, and an Internet gateway, are 
included at Genie's non-prime time connect rate of $2.75.  Other plans are 
available.  Prices are subject to change without notice.  To sign up for 
Genie, call (with modem) 1-800-638-8369 in the USA or 1-800-387-8330 in 
Canada.  Upon connection wait for the U#= prompt.  Type:  JOINGENIE and hit 
RETURN.  The system will then prompt you for your information.  Need more 
information?  Call Genie's customer service line (voice) at 1-800-638-9636.

GET GENIELAMP ON THE NET!   Now you can get your GenieLamp issues from
"""""""""""""""""""""""""   the Internet.  If you use a web browser, 
connect to "gopher://gopher.genie.com/11/magazines".  When using a gopher 
program, connect to "gopher.genie.com" and then choose item 7 (Magazines 
and Newsletters from Genie's RoundTables).

                        *** GET INTO THE LAMP! ***
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""



           //////////////////////////////////////// Genie_QWIK_QUOTE ////
          /   AppleVision is still better than some of the QuickTime   /
         /    movies out there :)                                     /
        ////////////////////////////////////////////  R.SUENAGA1  ////



[EOA]
[FRM]//////////////////////////////
                 FROM MY DESKTOP /
/////////////////////////////////
Notes From The Editor
"""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



                   >>> NEW WAYS TO READ GENIELAMP A2 <<<
                   """""""""""""""""""""""""""""""""""""

     Last month, GenieLamp A2 was published in two new formats:  HyperCard 
and HyperStudio stacks.  The response has been terrific!  We've been 
worried about losing readership through the change in Genie's rates.  We 
needn't have worried.  The March 1996 issue of Genielamp A2 was the second 
most popular since we introduced an Apple II-specific version two years 
ago!

     The AppleWorks word processor file and standard text file versions 
continue to be popular, and they're both here to stay.  So those of you 
with classic Apple II's needn't worry.  But now IIgs users have a choice of 
a stack version as well.

     Joshua Calvin's GenieLamp HyperCard stack is a marvel.  It takes a 
standard text file version of GenieLamp and _automatically_ converts it to 
a HyperCard stack--creating a contents page on the fly!--in less than five 
minutes.  Just click on a file, wait a bit, and you're done.  Josh's stack 
doesn't just work on the current month's GenieLamp A2.  It will work on 
back issues, and it will work on GenieLamp A2Pro, GenieLamp Mac, GenieLamp 
PPC... on any edition of GenieLamp that's available as a text file.  If 
you'd like to pat Josh on the back, here's his Genie mail address:  
J.CALVIN.

     For those of you who don't want to convert a textfile into a 
HyperCard stack, we'll be providing a pre-converted stack.  The conversion 
is done, so you don't have to wait--just download and enjoy!

     What does the HyperCard version offer that you can't find in the 
standard text versions?  It offers an index page, so that you can click on 
the articles you want to read in the order you want to read them.  It 
offers a find function, so you can search for a specific word or phrase.

     If you don't have HyperCard IIgs, you can get it!  Visit the A2Pro 
(NOTE:  A2_Pro_) RoundTable libraries on Genie page 530;3 and download file 
#3900.  A hard drive is seriously recommended if you want to run HyperCard 
IIgs; the HyperCard program takes up almost 600K of a 800K disk.

     If you don't have a hard drive, may we interest you in our new 
HyperStudio version?  You don't have to own HyperStudio in order to use 
this new version--just download file #24732:  HS3.1RJ.BXY

     The HyperStudio version of GenieLamp A2 also offers you an index page 
(like the HyperCard version) so that you can read the articles you want in 
the order you want to read them.  There are a few little goodies like 
animated buttons and pretty pictures and sub-menus, but basically we've 
kept the HyperStudio stack very simple.  This is for two reasons.  The 
first is that if we add too many bells and whistles, this version will 
become a _big_ download.  The second is that we have very little time to 
produce a HyperStudio version (and it's not automatic, like the HyperCard 
version), so we need something that will require almost no changes from 
month to month.

     There's an extra goodies in this month's HyperStudio version (I'll 
see if I can figure out how to add it to the HyperCard version, too)--a 
comic strip!  Gwendel [Genie mail:  SAMWISE] draws and writes a strip 
called Hog Heaven--check it out and let us know if you'd like it to become 
a regular feature.

     Here's what some of you have said about the new hyper-versions of 
GenieLamp A2:

     "A really nice job..."

     "Four Stars!"

     "I =love= the new HyperStudio version of GenieLamp A2."

     "I hope to continue to see both versions."

     "...it's very well designed right down to the neat graphics and
animated buttons."

     "Nice graphics and interface."

     "...outstanding! I can think of no negatives."

     I'm pretty sure you'll be impressed with our new hyper-editions, too! 
 Why not download one?  Heck, download both.  We don't mind at all.

                                 [*][*][*]


     In last month's editorial, I echoed the often-heard hope that Genie's 
new management would come up with a billing plan for those of us who cannot 
afford a minimum charge of $18.95 a month (or $23.95 if you're a new 
member).  This month, thank goodness, Genie has done just that.

     The new GenieLite option costs just $7.95/month.  This method of 
subscribing offers 5 free hours of E-mail use.  There are no free 
RoundTable hours whatsoever, but it should be a good deal if you only visit 
RoundTables occasionally--say two hours' worth a month.

     Also, you now have the ability to switch your current Genie account 
to a new subscription plan.  Just type the keyword BILL or SET and choose 
option 5 from the menu.  Then just follow the prompts!

-- Doug Cuff

Genie Mail:  EDITOR.A2                       Internet:  editor.a2@genie.com



        __________________________________________________________
       |                                                          |
       |                   REPRINTING GENIELAMP                   |
       |                                                          |
       |   If you want to reprint any part of GenieLamp, or       |
       |   post it to a bulletin board, please see the very end   |
       |   of this file for instructions and limitations.         |
       |__________________________________________________________|



                                                           ASCII ART BEGINS

      _____            _      _                              ___  ___  
     / ____|          (_)    | |                            / _ \|__ \ 
    | |  __  ___ _ __  _  ___| |     __ _ _ __ ___  _ __   | |_| |  ) |
    | | |_ |/ _ \ '_ \| |/ _ \ |    / _` | '_ ` _ \| '_ \  |  _  | / / 
    | |__| |  __/ | | | |  __/ |___| (_| | | | | | | |_) | | | | |/ /_ 
     \_____|\___|_| |_|_|\___|______\__,_|_| |_| |_| .__/  |_| |_|____|
                                                   | |                 
                                                   |_|                 

                                                             ASCII ART ENDS


[EOA]
[MAI]//////////////////////////////
                 FROM MY MAILBOX /
/////////////////////////////////
Letters To The Editor
"""""""""""""""""""""



TIMEOUT SHRINKIT-PLUS   In the Jan '96 GenieLamp [A2], you mentioned 
"""""""""""""""""""""   TimeOut ShrinkIt v5.  I have looked for this update 
on all the ftp sites and can't find it anywhere.  Can you please suggest a 
place to locate it or perhaps a place to find more information on what new 
features it has?  Thank you very much.....

Brian Wiser
brwiser@xmission.com

          The reason you can't find TimeOut ShrinkIt-Plus v5 on any ftp 
          sites is that it is only available commercially.

          As I mentioned in the November 1995 issue of GEnieLamp A2, you 
          can upgrade your v4.x copy of TimeOut ShrinkIt-Plus to v5.x for 
          $5.00 plus $2.50 shipping and handling.  You can also purchase 
          the package new for $29.95 plus $2.50 shipping and handling:

               Office Productivity Software
               P.O. Box 2132
               LaGrange, GA  30241-2132

               E-mail:  d.gum@genie.com

          As of 20 March, EGO Systems also carries the full line of Office 
          Productivity Software's AppleWorks utilities, including TimeOut 
          ShrinkIt-Plus.  (See this month's HEY MISTER POSTMAN.)  TimeOut 
          ShrinkIt-Plus requires AppleWorks v4.0.1 or later and is 
          available on 3.5-inch disks only.

               EGO Systems
               7918 Cove Ridge Road
               Hixson, TN 37343

               phone orders:  800-662-3634 (9 am to 5 pm Eastern Time)
               phone inquiries:  423-843-1775 (9 am to 5 pm Eastern Time)
               fax:  423-843-0661 (24 hours a day)

               e-mail:  GSPlusDiz@aol.com
                        diz@genie.com

          EGO Systems also charges $29.95, plus $3.00 shipping and 
          handling.  EGO Systems accepts Visa and MasterCard for phone or 
          fax orders, plus checks or money orders through the mail.--DGC


MACLEAN'S DOS 3.3 LAUNCHER   G'day Doug,
""""""""""""""""""""""""""
     Just a followup on the March A2 GenieLamp:

-----
DOS33.LNCHR.BXY   This program lets you install DOS 3.3 programs--or entire
"""""""""""""""   disks--on your ProDOS hard disk or on 3.5-inch disks.
You can then launch these DOS 3.3 programs via your favourite program
launcher.  This unofficial release (v2.1) supposedly supports networks.
Daniel Pfarrer assembled this version from the remains of the source code
on programmer John MacLean's hard disk.
-----

     The last sentence of this description is factually incorrect.  How 
this sentence actually came to be created is really intriguing, I'd really 
like to know....

     I am the person responsible for assembling v2.1.  I have John's 
actual hard disk and his actual source code (not remains).  I have no idea 
who Daniel Pfarrer is.

     I believe Richard Bennett has written to the A2 Librarians to try and 
correct the file description and the text file addition to the archive.

     I would appreciate it if you could correct the description in an 
upcoming GenieLamp after it has been corrected by the A2 Librarians.

     I spoke to John MacLean recently.  He has given his permission for a 
lot of the contents of his hard drive to be distributed for the benefit of 
others.  There's some very interesting source code on the drive, some 
finished, some unfinished.  The unfinished code includes a PROLOG 
interpreter and an HardPressed(Tm)-type utility.  The finished code 
includes a LISP interpreter and a personal expense tracker.

     I'll be making the contents available as I find the time.

     The first release, inspired by the mixup with Dos 3.3 Launcher v2.1, 
is...

     An official Dos 3.3 Launcher v2.2, which now can be launched on an 
Apple IIgs from ProDOS, and which can now launch 'disk images' (used by 
Apple II emulators) of Dos 3.3 disks.

Regards,
Andrew Roughan
posty@triode.apana.org.au

          Thanks for the correction, Andrew!  My faces is pretty red, but 
          I'm afraid I cannot answer your question about how the 
          description was written.  I was responsible for uploading the 
          ersatz DOS 3.3 Launcher v2.1 to Genie, but I obtained it from 
          CompuServe over a year ago, and the description I used was based 
          on the file description in the CompuServe libraries.  Sorry to 
          have to pass the buck on this one, but it's really all I know.

          I spotted a posting from Daniel Pfarrer on the Usenet newsgroup 
          comp.sys.apple2 recently, and asked him if he could shed any 
          light on the puzzle.  Here's his reply:

          "Yea, I remember uploading this to CIS a long time ago.  No, I 
          did NOT assemble this version from the remains of the source code 
          on programmer John MacLean's hard disk.  I got it from the 
          Washington Apple Pi (when I was a member a few years ago) and 
          just uploaded it to CIS.  I have no A2 programming skills 
          what-so-ever, with the exception of basic and metal (from the bbs 
          prog)....

          "I just logged on to CIS to get a copy of this program, and it 
          appears that someone has tampered the orig. file description I 
          entered many years ago, so I'm going to bug the file admins of 
          the APPUSER forum on CIS and try to get it fixed."

          Likewise, I am sending copies of your letter and Pfarrer's 
          letter to the A2 RoundTable librarian in hopes of getting Genie's 
          file description fixed.  I'm sorry to have contributed to the 
          confusion over this file.--DGC


TIMEMASTER II CLOCK CARD WANTED   I have been reading the GEnieLamp [A2] 
"""""""""""""""""""""""""""""""   and was wondering if it is possible to 
find out if an AE TimeMaster II H.O. is being offered for sale.  Since I am 
in Japan, I can't get into your trading area.  I would pay with a U.S. 
check and would have it sent to my parents home in Calif.  Thanks for any 
help you might be able to give.

Jack Sonnabaum
Tokyo, Japan
jsonnaba@asij.ac.jp

          Being a member has its benefits.  I guess one of the benefits 
          being a member of Genie is the chance to post messages in the 
          RoundTables.  I'm afraid I'm not comfortable posting a message in 
          the A2 RoundTable on your behalf, but I've printed your E-mail 
          address above, so if any of our readers has this clock card for 
          sale, they can contact you.

          I assume that either you've already tried posting in the Usenet 
          newsgroup comp.sys.apple.marketplace, or that your Internet 
          account doesn't carry that newsgroup, or that you've tried 
          c.s.a.marketplace with no luck.--DGC



[EOA]
[HEY]//////////////////////////////
              HEY MISTER POSTMAN /
/////////////////////////////////
Is That A Letter For Me?
""""""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]

        o A2 POT-POURRI

                o HOT TOPICS

                        o WHAT'S NEW

                             o THROUGH THE GRAPEVINE

                                  o MESSAGE SPOTLIGHT



                           >>> A2 POT-POURRI <<<
                           """""""""""""""""""""

ANIMASIA AWAY FOR TWO WEEKS   Animasia will be away from the online world 
"""""""""""""""""""""""""""   for about two weeks.  If you need to contact 
Animasia, I can be reached until 29-Mar-96 by phone at 407-380-9932.  After 
that time, you can send e-mail to animasia@genie.com and I'll reply post 
haste.

Michael Lutynski
Animasia
                  (ANIMASIA, CAT13, TOP41, MSG:58/M645;1)

>>>>>   BTW, I'm happy to announce that Michael Lutynski will be the latest
"""""   addition to the Logicware team, joining the likes of Eric "Sheppy" 
Shepherd, James (T2) C. Smith, "Burger" Bill Heineman, Steve "Focus Drive" 
Parsons, and of course myself (Greg Templeman).

     Of course, there are a half dozen other people at Logicware, but as 
they aren't Apple II'ers, I won't mention them. ;-)

-G.T. Barnabas
                  (BARNABAS, CAT13, TOP41, MSG:59/M645;1)


WHAT APPLEWORKS HATH BROUGHT TOGETHER...   Is there a formula for splitting 
""""""""""""""""""""""""""""""""""""""""   apart first and last names?  I 
have a macro that does that, but it would be nice to be able to do it 
through the options menu.

:: Dan ::
                (D.CRUTCHER, CAT17, TOP31, MSG:106/M645;1)

>>>>>   Absotively!  I use it in GEM's Email.Names file for that very
"""""   purpose.  It requires two extra categories for precalculation:

     LAST NAME: @Right([Full Name],(@Len([Full Name])-[Calc2]))

     FIRST NAME: @Left([Full Name],[Calc2])

     CALC1: @Find(" ",[Full Name],1+(@Find(" ",[Full Name],1,0)),0)

     CALC2: @If(@Val([Calc1])=0,@Find(" ",[Full Name],1,0),@Find(" ",[Full 
Name],[Calc1],0))

     This will include any middle initials with the first names.

   |
 -(+)-
   |
   |
     ...Will (Cat 13, Top 11)
                 (W.NELKEN1, CAT17, TOP31, MSG:108/M645;1)


CONTEST WINNER:  TOUCHED BY THE HAND OF WOZ   Last week, I sent a listing 
"""""""""""""""""""""""""""""""""""""""""""   of all current Shareware 
Solutions II subscribers to Steve Wozniak, via Steve's employee Auri 
Rahidzedeh.    Audi printed out the list of potential winners and gave 
that to Steve Wozniak.

     I've just received an e-mail from Auri, and this is what he had to 
say:

     "The winner of the HD is Keith Sagalow.  We threw all of the papers 
into the air, then we threw a dart at one.  There were three names hit by 
the dart, but only one was legible, so Keith was the winner."

     By only regret is that they didn't capture the selection process on 
video <g>.

     In any case, Keith will soon be the proud owner of a 120 Megabyte 
Focus Gold Hard Drive. I hope that it provides Keith with many years of 
use.

     Thank you, Keith, for your ongoing support of Shareware Solutions II. 
And of course, thank you to everyone who was eligible for partaking in the 
Shareware Solutions II Hard Drive Give Away!

     Keith...I plan to send it out via the US Mail, and wonder if I should 
ask for a signature to assure that it arrives OK.  Are you, or someone in 
your family, around during the day to sign for the package?  If not, I will 
simply insure it without requring a signature. Let me know via e-mail what 
would work best for you.

Joe Kohn
Publisher, Shareware Solutions II
                  (JOE.KOHN, CAT28, TOP4, MSG:106/M645;1)


APPLEWORKS 5:  CAREFUL WITH APPEND TO CLIPBOARD!   OK, Mike.  You're not 
""""""""""""""""""""""""""""""""""""""""""""""""   going crazy.  :)  I can 
confirm that you can crash AW 5.1 with append to clip.

     To make it crash, I appended very large chunks (a 10K file) over and 
over. The crash is spectacular.  Drive in slot 6 drive 1 spins, the whole 
screen goes bezerk, mainly with "]" characters.  A ctrl-reset partially 
restored things, but made the drive spin again (makes me shudder).  AW 
would not restart at all, even though I finally got the Main Menu screen 
back - the keyboard was dead.  A three finger (oa-ctrl-reset) didn't work 
the first time, the second time GS/OS crashed into the monitor, as well as 
the third time, the fourth time finally rebooted.  Wow!

     My _guess_ is that the append command can overflow the available 
memory, knocking AW out of memory and control.

     I'll let Randy know.  :)

 __!__
   |     Terrell Smith
   |       tsmith@ivcf.org
                 (T.SMITH59, CAT17, TOP30, MSG:46/M645;1)


APPLEWORKS 5 DATE QUIRK   Using an AW 5.1 data base, I discovered that I 
"""""""""""""""""""""""   cannot search for a date using (say) Mar 6 1978 
or 3/5/78.  I have to use Mar  6 1978 (two spaces in front of the 6.  I 
understand why this is so, and the work around is to enter the two spaces, 
but I find this inconvenient (and I get forgetting to do it!!! :(  ).  
Other than this, I haven't had a single problem with AW 5.3 since I moved 
up from AW 4.3 a couple weeks ago.  [Thanks, Randy!]

     Eric  ( o= =o === =ooo oo oo= == )

     PS  I was thanking Randy for AW 5.1, not for anticipated assistance.  
I know he's feeding his kids elsewhere these days.
                (J.SCHONBLOM, CAT17, TOP31, MSG:118/M645;1)


MAKING APPLEWORKS 5 GET ALONG WITH GS/OS   Has anyone here been bothered by 
""""""""""""""""""""""""""""""""""""""""   AW5.1 having a decreased amount 
of desktop memory available after multiple launches during a single 
session?  (This also occurs after using GS.SYSTEM from the Beagle Compiler, 
BTW.)

     On a machine with 4 megs or so and few, if any, relaunches, this is 
not a big problem.  On a 1 meg machine, though, you can suffer a 
substantial reduction in desktop size on a relaunch.

     I have discovered why this is happening and have _hacked_ together a 
small machine language program to run prior to launching AW5.1 to fix this.

     AW _appears_ to do an incomplete job of using the GS memory manager's 
toolbox routines on exit, with the result that some of the memory blocks 
are not de-allocated and the ID number is not deleted.

     The hack calls the DiposALL and DeleteID tools to 'clear' the memory 
AW used.  With the memory cleared, AW can start with a fresh slate, so to 
speak.

     AppleWorks seems to request ID $1101 to start.  On the next launch, 
$1101 is taken, so it gets $1102.  Unfortunately, $1101 is still present 
and some of those blocks of memory are still allocated.  Will the hack mess 
up some GS/OS application program using blocks with the same ID's?  You 
tell me.  I don't know.

     I'll try to post it here this week to get your comments.  As always, 
I'd appreciate it if the rest of you would round off the rough edges.  The 
hack will be in BASIC (requiring BASIC.SYSTEM).  It would be nice to make 
it a SYSTEM file that when executed, would subsequently start 
APLWORKS.SYSTEM.

     Does anyone want to bother Randy with this? <g>

Hugh...
                  (H.HOOD, CAT17, TOP27, MSG:290/M645;1)

<<<<<   I've completed (with the fine help of Harold and ghosts of
"""""   programmers' past) a SYSTEM program that, on a GS, 'nukes' 
(Harold's word, not mine) allocated application memory and memory ID's 
($1001-$1003) prior to running APLWORKS.SYSTEM.

     It's FRESHER.SYSTEM.  Let's call it version 0.9. <g>

     For use with Appleworks 5.1 on a GS in ProDOS 8 only mode, (although 
I don't _think_ it would hurt under GS/OS) it eliminates the problem of 
diminishing desktop memory that occurs when AppleWorks is re-launched 
during a single session.

     I'll try to upload it this week.  BTW, are there instructions for 
uploading a file correctly on your first try?  This will be my first 
program that I feel is semi-worthy of A2.

     BTW, Harold, getting it to quit to APLWORKS.SYSTEM really made me get 
out the books and disassemble some code as well.  Merely passing the 
Startup name and quitting via the MLI doesn't do the trick with the P8 
mini-selector.  The program itself must take the startup name and do the 
work.

     Thanks for challenging me.  Obsessive behavior is either a blessing 
or a curse.  Perhaps both.

Hugh...
                  (H.HOOD, CAT17, TOP27, MSG:345/M645;1)


THE REAL WORLD   I was in a self-storage place near Princeton, NJ, 
""""""""""""""   yesterday, and saw they had their alarm system software 
running on an Apple //e with one Disk II drive.  I couldn't get close 
enough to see exactly what software was running, but the display was 40 
column.  The counter help didn't know anything about the setup.

     It's a real pleasure to see a II still hard at work.  Anybody else 
have a sighting to report of an Apple II at work?

Bill Dooley - Apple II Forever!
                   (A2.BILL, CAT2, TOP7, MSG:225/M645;1)

>>>>>   I was in Park City Utah a couple of years ago and found several
"""""   retail stores  using Apple II GS's as cash registers.  I was quite 
impressed.  It was the  first and only time I have ever seen an Apple doing 
duty in a retail  situation.  Actually I take that back.  I was in a 
gallery in Stowe, Vermont  last month and the owner had a PowerBook which 
she used to do accounting for  the store and keep mailing lists.

Bob
                 (BOB.CHERRY, CAT2, TOP7, MSG:252/M645;1)

>>>>>   I'm being a little slow on the uptake on this..
"""""
     I am using a ||gs with DB Master Professional at the front desk of my 
veterinary hospital.  Using it as a cash register and reminder system. 
AW5.1 for book-keepeing and inventory and lots of other things.  GWIII for 
desk-top publishing of client information things, admission releases, etc.

     Only shortcoming is that I can't access the veterinary databases, 
either on-line or CD-ROM.

Ray
   the Sauer Kraut
                (R.SCHUERGER1, CAT2, TOP7, MSG:255/M645;1)

>>>>>   Our Engineering Test Laboratories have some really exotic fast data
"""""   acquisition systems.  But there are also two Apple II plus 
computers out in the Environmental Labs that have AE 12 bit 16 channel data 
acquisition cards and TimeMaster II clocks.  They work great on some of the 
small tests and any programmer/engineer can add his own data reduction 
program in AppleSoft for quick look data.

     I bet some of the raw data from those Apples end up in the Cray.

Bob, AF6C
                 (R.ECKWEILER, CAT2, TOP7, MSG:263/M645;1)

>>>>>   The Apple IIGS sometimes shows up in the least likely places..
"""""
     On April 26, 1996, Basic Books will be publishing a new textbook 
entitled "Textbook of Transpersonal Psychiatry and Psychology."  I'm sure 
the publisher has absolutely no idea, but I know, and now you know.  It was 
edited entirely on an Apple IIGS.

<grin>

Joe
                  (JOE.KOHN, CAT28, TOP4, MSG:174/M645;1)


DISK DRIVE REPAIR   I just recently sent an Apple 3.5 disk drive that had 
"""""""""""""""""   been given to me to ISR Floppy Drive Repairs because it 
was not working properly.  I was getting i/o errors and there was a rasping 
sound coming from the drive.

     It seems that there was a foreign object in the drive (looked like a 
small piece of plastic).  The drive was also cleaned, lubed, aligned, and 
tested before it was returned to me.  The turn around time (from when I 
mailed it until when I received it) was 2 weeks.

     It appears to be working properly now. :)

     The cost was $17.00 plus 5.95 for shipping.

     Here is some additional information about them.

          Infinite Service & Repair
          2217 Downing Ln.
          Leander, Texas 78641

          1-800-458-6778

     They work on 3.5, 5.25, and 8 inch drives.  Their labor charges are 
guaranteed to be between $1 and $25, with the average cost of $17.  Parts 
are extra, and will be installed with prior customer consent.  They 
indicate that most repairs do not require parts at all.

     The end price is based on the type of drive, the amount of work per 
drive, and the number of drives per month with quantity discounts.

     There is a $1 minimum Diagnostic fee if the drive is too costly to 
repair or if there is no problem found.  ISR will contact you with the 
diagnosis.

     They require that you call first for a repair authorization number.  
They will use this number to identify your equipment and all work done to 
it.  Write this number on your shipping label and have it ready if you call 
them for information about your drives.

     They offer a 6 month warranty, and accept VISA and MasterCard.  They 
also ship UPS COD.

     Joe Kohn gave me their name.  Thanks, Joe. :)

Charlie Hartley
                 (A2.CHARLIE, CAT4, TOP46, MSG:43/M645;1)


                            >>> HOT TOPICS <<<
                            """"""""""""""""""

 ______      _____
(      )   (      )     ____________________________________________
 |     |  /     /      (        _____________________________'96____)
 |     | /     /        |      |
 |     |/     /         |      |                         II Infinitum!!
 |           /          |      |______
 |     |\     \         |      _______) __________    ________   ________
 |     | \     \        |      |       (     _____)  (     ___) (__    __)
 |     |  \     \       |      |        |   |__       \    \       |   |
 |     |   \     \      |      |        |    __)       \    \      |   |
 |     |    \      \    |      |        |   |_____   ___\    \     |   |
(______)    (________) (_______)       (__________) (_________)    |___|


     KFest is back again!  With two Extra Nights!!!!!

     This summer, in Kansas City, Missouri, you can once again:

          - spend 3 days and 4 nights guided by Dante Alighieri on a trip 
            through the dormitories of Avila College
          - meet friends old and new
          - learn about how to get the most out of your computer and 
            peripherals
          - see demonstrations of new products
          - meet the celebrities of our little world <grin>
          - experience the InterNet and all that it has to offer to us and 
            our computers
          - take advantage of special KFest '96 offers from Apple II and 
            Macintosh vendors
          - and, of course, stay up all night shoot pool and foment 
            insurrection, if you want to :)

     KFest '96 will again be held on the campus of Avila College in Kansas 
City, Missouri.  It will take place from July 17-21, 1996.  The cost will 
be only $325, which _includes_ use of a double room in the Avila College 
dorms _and_ meals!  (If you wish to stay at one of the many hotels or 
motels near the college then the cost is $225, but you would, of course, 
have to find and pay for your own room).

                                    ***

     If you have been thinking about attending KFest '96. now is the time 
to sign up.  The registration fee will be going up on May 19th to $375 for 
the dormitory and conference, and $275 for the conference only.  The last 
date pre-registrations will be accepted is July 13th.

                                    ***

     KFest this year is being sponsored by Kellers' Auto Electric, Inc.  
You can send your completed registration forms to:

          KFest '96
          c/o Kellers
          PO Box 391
          Brielle, NJ  08730

     * Please make checks payable to: Kellers' / Kfest '96

                                    ***

     Credit Card registration is also available.  Just call Kellers' Auto 
Electric, Inc with your VISA, MasterCard or American Express number at 
(908) 775-0371, Mon-Fri 8:00AM-5:00PM Eastern Time.  If you prefer, you can 
FAX your registration with VISA or MasterCard account number and expiration 
date to: (908) 223-0678 Mon-Fri 8:00AM-5:00PM Eastern Time.

     If you prefer registering by Email, and are using a VISA, MasterCard, 
or American Express for payment, you can send the completed registration 
form via InterNet Email to:

     timothyk@injersey.com

                                    ***

     If you have questions about KFest, or wish to make suggestions, you 
can find answers via the InterNet.  Just send an email message to 
kfest.info$@genie.com, or visit the World Wide Web page at 
http://www.primenet.com/~adams/kfest.html   

                          ****** (cut here)******
-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -


                    KFest '96 Registration Form

Name:

Address:

City, State, Zip:

Phone  (w):                                 (h):

Email address:

Roommate preference (if any):

Do you prefer a smoker or non-smoker?

Hey!  Howbout a frighteningly collectible, soft and thrilling KFest '96 
T-Shirt?  Yes I want One (Many)!!!  [ ]

T-shirts are $15.00 each and are available in Small, Medium, Large, X- 
Large.


Please circle size(s)    Small     Medium    Large     X-Large

Check here [] for additional X's on your X-Large T-shirt and indicate size 
needed:   _________

Send this completed registration form and fee to:

     KFest '96
     c/o Kellers
     PO Box 391
     Brielle, NJ   08730

* Please make checks payable to:  Kellers'/KFest '96

For Credit Card registration please check:     [] Visa    [] MasterCard


                                               [] American Express

Please charge my  account number:  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
Expiration: _ _ / _ _
                   (CINDY.A, CAT44, TOP8, MSG:49/M645;1)


NOW _THAT_'S A SPECIAL GUEST!   Actually, I've heard from a somewhat 
"""""""""""""""""""""""""""""   reliable source that the Woz has been 
informed and is considering attending KFest.

     Seriously.

     And if that's the case, we can have one heckuva birthday party :)

Ryan--new preferred email: rsuenaga@kestrok.com
                 (R.SUENAGA1, CAT2, TOP24, MSG:125/M645;1)


TOP 10 AT KANSASFEST   THE TOP TEN CATEGORIES FOR WHY YOU SHOULD CHOOSE 
""""""""""""""""""""   KFEST '96 OVER LIFE, AS USUAL:

NUMBER 10!   TELEVISION: YOU'VE EXHAUSTED YOUR 30 DAY CALL IN ALLOWANCE TO 
''''''''''   C-SPAN FOR THE NEXT 6 MONTHS

NUMBER 9!   FAMILY:  YOU'VE ASSIGNED YOUR FAMILY PRODOS PATHNAMES, AND YOUR 
'''''''''   MATE RETURNS AN  $0046 ERROR

NUMBER 8!   EXTRA TERRESTRIALS: STEVE FORBES HAS ALREADY FLAT TAXED MARS
'''''''''

NUMBER 7!   STAR TREK: YOU WONDER IF DATA CRASHES WHEN TWILIGHT II KICKS IN
'''''''''

NUMBER 6!   APPLETALK:  YOU THINK APPLE SHARE IS A USER GROUP SCAM
'''''''''

NUMBER 5!   POLITICS: YOU KNOW THAT BOB DOLE WILL BE BUSY ELSEWHERE AND 
'''''''''   KANSAS IS A SAFE PLACE TO BE

NUMBER 4!   MICROSOFT: YOU WANT TO BREAK ANY MORE THAN 95 WINDOWS YOU COUNT 
'''''''''   AT AVILA COLLEGE

NUMBER 3!   POWERMACS:  YOU DON'T CARE HOW FAST MACDONALD'S MAKES A 
'''''''''   CAFEINATED HAMBURGER

NUMBER 2!   TRIVIA:  THE ANSWER TO THE 1995 TOURNAMENT OF CHAMPIONS FINAL 
'''''''''   JEOPARDY QUESTION:  WHAT THE HECK IS AN APPLE II?

     AND THE NUMBER ONE REASON FOR ATTENDING KFEST '96....

     REALITY: HECK, WE NEED THE MONEY!!!!

                   (KELLERS, CAT44, TOP8, MSG:51/M645;1)


A RIDE TO/FROM THE AIRPORT   KFest 1996 is just around the corner!!  Now is 
""""""""""""""""""""""""""   the time to be making your travel plans to 
take advantage of cheaper airfares.

     We can begin checking in at Avila on Wednesday, July 17th.  We have 
to be out of the dorms by noon on Sunday, July 21st.

     This topic is a good place to let others know when you will be 
arriving in Kansas City so you can coordinate rides from the airport.  For 
the record, the Kansas City airport is called KCI, but that's not the code 
that is on luggage tags.

     Quicksliver is a shuttle service that can be reached at 1-800-888-8294

     As of today, their rates to Avila are:

          $21.50 for 1 person
          $28.50 for 2 people
          $35.50 for 3 people
          $37.50 for 4 people
          $50.00 for 5 or more

     They only need a few days advance reservation.  People can arrive on 
different flights and different airlines but need to be within 5-10 minutes 
to take the same shuttle.

Cindy
(KFest Big Cheese)
                   (CINDY.A, CAT44, TOP4, MSG:1/M645;1)


IIGS IN A TOWER CASE WISH LIST   If you could design your own "Tower GS", 
""""""""""""""""""""""""""""""   without the limitations of an 
off-the-shelf case, what features would you like to have?  That is, 
assuming you start with a stock GS logic board...

     A few have been mentioned before, like a headphone and ADB jack 
mounted in the front, or a hard drive activity light.  Any other brainwaves 
out there?

     Just curious...

:froggie
                 (FROG.MAN, CAT12, TOP31, MSG:142/M645;1)

>>>>>   A. 3.5 floppy drive bays with switches for eject mechanisms
"""""   (wouldn't have to line up with the actual mechanical switches, you 
could use a paperclip, ala Macintosh for manual eject).

     B. A better speaker (amplified, with volume control, would be nice, 
but not mandatory).  (A small amplifier with external outputs would be 
really good.:)

     C. INTERNAL bays for a minimum of two 3.5 form factor drives, and 
external bays for a minimum of 3 5.25 form factor drives.  (With wiring for 
hard drive activity lights from the internal bays to the case front.)

     D. Front mounted LEDs connected to leads with snap on clips for 
connecting to the activity lights on things like ZIPs and TWGSs.

     E. Here's a biggy.....

     A "slot" for a PC style internal modem.  This would simply be a card 
edge holder with the appropriate fingers wired to a serial cable 
appropriate for plugging into the modem port.  Done properly, this rig 
would include a front mounted switch to power cycle the modem, and an LED 
to show power status.  (I'm not sure if the signals are present at the card 
edge, and standard enough, to allow the inclusion of REAL activity lights, 
but it would be nice. :)

     F. Serious fannage.  (You KNOW how I feel about fannage. :)

     G. A "dimple" suitable for mounting the little metal Apple logo from 
the front of the GS case.  (It would probably have to be round or square to 
avoid copyright problems, but that metal logo pops right out if you do it 
right.)

     H. Some switched outlets on the back to handle the monitor, printer, 
etc.  While you're at it, it would probably be good to have at least three 
switches, one for "system power" which would fire up the hard drives and 
monitor, one for "CPU power" which would fire up the motherboard, and one 
for "peripheral power" for anything else hooked up.  You can figure that 
out better than I can, probably, but a LOT of people like to turn on thier 
hard drives and monitors first....

     And of course, a SERIOUS power supply. :)

     Give me a case like that, and I'd buy it REAL quick (and probably a 
Second Sight card and a bigger monitor as well :).

Gary R. Utter
                (GARY.UTTER, CAT12, TOP31, MSG:144/M645;1)

>>>>>   I'd like a case that had the following
"""""
     1) At least 5 bays, 3 of which could be accessed externally for 
things like CD-ROM, Floptical, Zip or SyQuest, etc.

     2) Headphone and Sound input (maybe we should get R. Wagner's 
digitizer as an add on to this)

     3) Game Port and ADB in the front

     4) Several switches (as Gary noted) for turning on power...to 
peripherals, to devices in the case, to the CPU

     5)Something that was not platinum (grey) :)

     6) Comes with the connector converter so that the motherboard can be 
plugged right in.

Steve
               (S.CAVANAUGH1, CAT12, TOP31, MSG:145/M645;1)

>>>>>   One nice feature to have in a IIXS would be a separate "chamber"
"""""   for floppy drives, tape backup drives, and the like (drives which 
don't generate much heat or are used seldomly).  This chamber would be 
sealed and separated from the rest of the case and not subject to 
ventilation by the fan for the rest of the Tower.  That way one wouldn't 
have to worry as much about dust being pulled into those drives.

     Also, a bottom "chamber" tall enough to let you easily mount the GS 
in there "vertically" so that ventilation of a motherboard and lots of 
cards would be more efficient.  (The case I got doesn't have enough height 
in the bottom part to do that).

I\/I ark  I<

     Hmmm...   more ideas:

     Extra AC power outlets would be dandy.... oh, I see Gary mentioned 
that one.  All of the ideas people mentioned are great!

     It's too cool to see that you're really thinking of building a case, 
Froggie!  I'd like to let you know that I'd be interested in purchasing one 
from you, also :)
                  (M.KLINE, CAT12, TOP31, MSG:150/M645;1)

>>>>>   Lots of great suggestions so far!  I'd like to add one more: a
"""""   swinging or sliding door on the exposed drive area.  My tower (a 
Jaba case) has a swinging door and it has done wonders for my dust 
problems.  In its old external enclosure, my tape drive used to have a 
significant build-up of dust around the drive shutter in only a few days.  
It's been in the tower for over 2 months now, the tower fan on all the 
time, and not a speck of dust in it!  I'm not at all worried about mounting 
my floppy drives in there now.

Give me that, a nice suitable front display (e.g. drop the speed readout 
and turbo button, unless it can be somehow tied to a Zip/TW), and a full 
set of accessible ports (on a full tower, I'd prefer them at the top of the 
back panel, since the tower typically sits on the floor -- I hear one of 
Apple's new models may have the ports on the top!), and I'd be pretty 
happy.

I'd love to be in your position, as I did design my own "ideal" case when I 
was trying to find one last fall.  I settled for a case that was nowhere 
near as functional or unique as my own, but I do not possess the skill to 
actually take my design to a finished product as you do.  I wish you the 
best of luck, that we might all benefit from your work!
                 (M.HACKETT, CAT12, TOP31, MSG:155/M645;1)


                            >>> WHAT'S NEW <<<
                            """"""""""""""""""

APPLE BLOSSOM NEWSLETTER   The Apple Blossom's first issue of 1996 will be 
""""""""""""""""""""""""   mailed tomorrow, to Apple II users in 30 states 
and Canada.  Thanks for the great support and encouragement that you've 
given me so far.  I hope that those of you who have subscribed will enjoy 
the issue that reaches you next week.

     For those of you who have not yet subscribed, you still can :)  As an 
incentive, I have offered a printed copy of the Apple II Vendor Directory, 
which has been updated constantly by me over the past year (and was being 
updated up until 10 seconds before I printed it last night).  It weighs in 
at 11 pages, and gives mail, email, Web and product info, along with phone 
numbers for over 90 vendors of Apple II hardware, software and services.

     All subscriptions post marked by March 7th will be entitled to a free 
copy of the Vendor Directory, which will be mailed via first class postage 
with your first issue.

     Over the weekend I will be uploading a HyperCard Stack that is 
described in the newsletter.  Later, I will post a description of this 
issue's contents (but I'll let some of the subscribers get their copy 
first...seems fair, no?).

     Volume 2 Number 1 is 14 pages (2 extra pages, couldn't keep it at 12) 
of densely packed 10 pt type (well, my wife said it was pretty dense, at 
least...hey, you don't suppose she meant something other than the print 
density, do you?).

Steve Cavanaugh
The Apple Blossom
http://members.aol.com/newblossom/
               (S.CAVANAUGH1, CAT13, TOP17, MSG:253/M645;1)


NAUG PUBLIC DOMAIN AND NAUG-ON-DISK   As I'm sure most of you know, 
"""""""""""""""""""""""""""""""""""   Shareware Solutions II has been 
chosen by the National AppleWorks User Group (NAUG) to be the repository of 
all of the disk based AppleWorks resource materials that had been compiled 
and distributed by NAUG.

     There are now 2 types of disks available from SSII...the NAUG Public 
Domain Library disks, and the monthly NAUG-On-Disk disks.

     Many, but probably not all, of the NAUG PD Library disks are also 
available here on GEnie.  The NAUG-On-Disk series of 60 disks, however, can 
only be purchased through Shareware Solutions II.

     To let folks know what is currently available from SSII, I've 
uploaded a few informational files to the A2 library over the past 2 days. 
Unfortunately, I didn't note their file numbers when I uploaded them ;-(

     In any case, the first file I uploaded was a file that described all 
of the AppleWorks public domain disks currently available from SSII.

     The 2nd file I uploaded was a series of AW Database files that listed 
all the articles that appeared in the AW Forum newsletter from 1/90 to 
12/95.  For that second upload, I created one database for each year, so 
that the files are manageable by anyone, as the biggest one is only about 
40K.  They are AW v3.0 files.  For each month listed in those databases, 
there is a corresponding NAUG-On-Disk disk available from SSII for $5.

     Feel free to download and pass around the files to anyone.

Joe
                  (JOE.KOHN, CAT28, TOP4, MSG:131/M645;1)


EGO SYSTEMS NOW CARRIES OPS APPLEWORKS UTILITIES   March 20, 1996 -- EGO 
""""""""""""""""""""""""""""""""""""""""""""""""   Systems is pleased to 
announce that it now carries the full line of Office Productivity 
Software's AppleWorks utilities.  These titles include:

About Time v1.1   This is an AppleWorks TimeOut module that allows you to 
'''''''''''''''   quickly and easily perform date and time calculations and 
then move the results into your AppleWorks documents.

     About Time performs many different types of calculations involving 
dates and times including: day-of-week, number of days/weeks/months/years 
between two dates, number of  minutes/hours between two times, and unit 
conversions.  It also converts between Gregorian and Julian Calendar  
systems, and can change the current system time for your computer to any 
date or time you choose.  About Time can input data from Data Bases or 
SpreadSheets and output its results to any file on the AppleWorks desktop.

     About Time requires AppleWorks v2.0 or later and it even works with 
Deja ][ on the Macintosh!  About Time is just $14.95 from EGO Systems, and 
that price includes First Class or Air Mail shipping to anywhere in the 
world!

(Note:  This product is available on 3.5-inch disks only.)

TimeOut ShrinkIt-Plus v5.0   This is an AppleWorks TimeOut module that lets 
''''''''''''''''''''''''''   you manipulate ShrinkIt archives without ever 
having to leave AppleWorks!

     TimeOut ShrinkIt-Plus can extract files from archives, add files to 
archives, remove files from archives, and even create new archives!  And, 
better still, TimeOut ShrinkIt- Plus allows you to extract files from an 
archive directly to the AppleWorks desktop!  You can even take a file 
that's on the AppleWorks desktop and put it directly into a ShrinkIt 
archive, and you never have to leave AppleWorks!

     TimeOut ShrinkIt-Plus requires AppleWorks v4.0.1 or later and it even 
works with Deja ][ on the Macintosh!  TimeOut ShrinkIt-Plus is just $29.95 
from EGO Systems, and that price includes First Class shipping in the USA 
or surface mail shipping to anywhere else in the world.  Air Mail shipping 
is $3 extra.

     (Note:  This product is available on 3.5-inch disks only.)

TimeOut Statistics v1.0   This is an AppleWorks TimeOut module that lets 
'''''''''''''''''''''''   you perform complex statistical analysis on the 
data in your AppleWorks databases.

     TimeOut Statistics comes with three different utilities that allow 
you to analyze single-sample databases, multiple-sample databases, and 
nonparametric databases.  Among the many different types of statistical 
operations you can perform are:  Variance, Mean, Standard Deviation, 
Hypothesis Testing, Linear Regression, Two Sample t-Test, Analysis of 
Variance, Histogram, Wilcoxon Rank Sum, and more!

     TimeOut Statistics requires AppleWorks v3.0 or later and it even 
works with Deja ][ on the Macintosh!  TimeOut Statistics is just $79.95 
from EGO Systems, and that price includes First Class shipping in the USA 
or surface mail shipping to anywhere else in the world.  Air Mail shipping 
is $3 extra.

(Note:  This product is available on 3.5-inch disks only.)

TimeOut Disk Tools v5.0   This is an AppleWorks TimeOut utility that makes 
'''''''''''''''''''''''   several powerful disk utilities available from 
within AppleWorks. These utilities are:

     TimeOut Compare WP - This utility allows you to compare the contents 
of any two AppleWorks WP files.  The two you select are presented in a 
'split screen' view with the differences between the two files hilighted.

     TimeOut File Backup - Like the name implies, this TimeOut utility 
lets you perform file-by-file backup and restore operations without ever 
having to leave AppleWorks!

     TimeOut Volume Backup - This utility allows you to create or restore 
disk- image backups from within AppleWorks!  

     TimeOut Wherzit? - So, you know you wrote that angry letter to your 
Congressperson, but you can't find it?  This utility will let you search 
your hard drive for any file, using a variety of search criteria (file 
name, type, date, and more)!

     TimeOut Disk Tools requires AppleWorks v3.0 or later  and it even 
works with Deja ][ on the Macintosh!  (Note that some of the utilities 
provided with TimeOut Disk Tools only work with ProDOS disks, so not all 
utilities will function on Mac disks while running under Deja ][.)  TimeOut 
Disk Tools is just $49.95 from EGO Systems, and that price includes First 
Class shipping in the USA or surface mail shipping to anywhere else in the 
world.  Air Mail shipping is $3 extra.

(Note:  This product is available on 3.5-inch disks only.)

Special Bundle Offer   If you are an AppleWorks lover, you'll probably want 
''''''''''''''''''''   to have all four of these great utilities.  If so, 
you can buy them all at once from EGO Systems for just $150 and save 
$24.80!  To get this special price, just ask for the OPS bundle when you 
place your order.  (If you live outside the USA and want Air Mail shipping 
for  this bundle offer, please add $9 for shipping.)

How to Order   To order any of these products, or to request a catalog of 
''''''''''''   our Apple II, IIGS and Macintosh products, you can contact 
EGO Systems by one of the following means:

     Orders ONLY:  800-662-3634 (9 am to 5 pm Eastern Time)
     FAX:  423-843-0661 (24 hours a day)
     Inquiries:  423-843-1775 (9 am to 5 pm Eastern Time)

     Ground Mail:
     EGO Systems
     7918 Cove Ridge Rd.
     Hixson, TN 37343

     e-mail:  GSPlusDiz@aol.com   or   diz@genie.com

     We accept Visa and MasterCard for phone or FAX orders.  If you prefer 
to send a check or money order, make it payable to "EGO Systems".  Orders 
for in-stock items ship no later than the next business day.  If you order  
an in-stock item before 10 am (Eastern Time), it will ship the same day.  
All orders ship via First Class mail.

     Macintosh and Apple II are trademarks of Apple Computer, Inc.  
AppleWorks is a trademark of Claris.  About Time, TimeOut Disk Tools, 
TimeOut ShrinkIt-Plus and TimeOut Statistics are trademarks of Office 
Productivity Software.  ShrinkIt is a trademark of Andrew E. Nicholas.  
Deja ][ is a trademark of JEM Software.
                     (DIZ, CAT33, TOP2, MSG:14/M645;1)


IIGS GAMES FIND NEW HOME   Shareware Solutions II is pleased to announce 
""""""""""""""""""""""""   that it has been named by Seven Hills Software 
as the exclusive distributor of Bright Software's games for the Apple IIGS: 
The Gate and Space Fox.

     The Gate blends action, adventure, aesthetic pleasure and 
intellectual challenges to create a stimulating adventure game for the 
IIGS.  You must battle enemies and solve puzzles to escape a castle's 
prison.  Ultimately, your goal is to find and defeat your captor to bring 
peace to the land.  The Gate features brain-teasing challenges, lots of 
action in smooth, synchronized animations plus exciting stereo music and 
sound effects.

     The Gate requires a IIGS with 1 megabyte of RAM.  Although The Gate 
is not copy protected, it cannot be installed on a hard drive.

     Space Fox is an exciting arcade game where you are the ace pilot who 
must guide your space ship through nine levels of assorted bad guys.  Your 
mission is to destroy The Brain at level ten but you'll need to collect 
fuel, shield power and new weapons as you destroy your enemies.  Skill and 
a bit of luck are needed to complete your mission because the higher the 
level, the harder it is to survive.  Space Fox features fast action, 
smooth, synchronized animations and sixteen voice stereo music.

     Space Fox requires a IIGS with 1 megabyte of RAM and at least one 
3.5" disk drive.

     The Gate and Space Fox are available for $20 each, plus $3 s/h per 
order.

     Shareware Solutions II can accept payment by check or money order 
only, made payable in US Funds, to "Joe Kohn."  Send all orders to:

Joe Kohn
c/o Shareware Solutions II
166 Alpine Street
San Rafael, CA 94901-1008
USA
                  (JOE.KOHN, CAT28, TOP4, MSG:184/M645;1)


                       >>> THROUGH THE GRAPEVINE <<<
                       """""""""""""""""""""""""""""

MS-DOS UTILITIES DESK ACCESSORY   Here's what Peter Watson had to say:
"""""""""""""""""""""""""""""""
     "Sometime in the next month or two I'll be sending you a disk for 
your library containing version 1 of a new NDA called MUG!, which will be 
the long awaited (by some anyway :-) GUI version of my MSDOS Utilities 
(MUG! = MSDOS Utilities Graphic !nterface). Apple II Forver."

     The hits just keep on coming...

Joe
                  (JOE.KOHN, CAT28, TOP4, MSG:73/M645;1)


SPECTRUM V2.1 AND KERMIT   If I remember correctly, the Kermit download 
""""""""""""""""""""""""   protocol is in beta testing.  If you have it, 
you have to run a script file to use it.  When you run the script, you get 
the icon.

David W.
                 (D.WALLIS2, CAT43, TOP15, MSG:257/M645;1)

>>>>>   Yes it is... but no bugs have been found for some time.  Spectrum
"""""   v2.1 adds the ability for an XCMD to easily add menu items to 
Spectrum itself.  So Kermit shows up on the 'Send' and 'Receive' pop-ups, 
and the Settings menu, with the rest of the protocol commands.

     We are in final Beta testing of v2.1, so hopefully it will not be too 
long before it is released.

 Ewen (Speccie)
 Delivered by: CoPilot v2.5.5 and Spectrum 2.0
                 (E.WANNOP, CAT43, TOP15, MSG:260/M645;1)


RICH TEXT FORMAT FOR GRAPHICWRITER   OK everybody.... I've been looking it 
""""""""""""""""""""""""""""""""""   over, and I think the first thing I'm 
going to try to get done is the RTF translator for GraphicWriter III.  So, 
does anybody out there know of any problems with the RTF translation that's 
in EGOed v2.0?  If so, let me know about it ASAP so I can correct the 
problems when I move that code from EGOed into a GWIII translator.

Diz EGO Systems
                     (DIZ, CAT33, TOP4, MSG:67/M645;1)


SPELLING CHECKER NDA   > I think Softdisk GS has been working--for years--
""""""""""""""""""""   > on a spellchecker NDA.

     Correction:  Softdisk G-S had a partially-completed spellchecker NDA 
waiting for time to work on it for years.  When I left Softdisk, the 
spellchecker project was effectively killed (as no GS people remain at 
Softdisk).

     I have plans for another, VERY clever and innovative way to make a 
spelling checker NDA.  When time is found...

-G.T. Barnabas
                  (BARNABAS, CAT33, TOP4, MSG:103/M645;1)


LIVE BRUTUAL DELUXE ANNOUNCEMENTS   I wonder if we set an all-time record 
"""""""""""""""""""""""""""""""""   for attendees at an online RoundTable 
discussion?  At one point, I counted 32 people.

     I'd like to thank everyone for giving Olivier Zardini such a warm 
welcome to GEnie.  He certainly enjoyed his 3+ hours online.

     Among the highlights of his visit were (semi) announcements...

     A free Convert 3200 update is being worked on that will offer some 
support for the Second Sight.

     Brutal Deluxe's "DeluxeWare" CD-ROM, packed with 643 megabytes of 
well organized Apple IIGS programs and files (33,000 of them) will be 
available shortly.  At present, it is available only in France, as there 
are many many many text files included on that disk that are written in 
French.  So, the CD-ROM needs to be re-worked a little bit before it can be 
released for the International market.  When completed, that CD-ROM will be 
available from Shareware Solutions II.

     At the conclusion of the rtc, James Gray was chosen to be the 
recipient of a present from Brutal Deluxe.  Since he already owns Convert 
3200, James will receive a free copy of The DeluxeWare CD-ROM.

     Brutal Deluxe has authorized me to upload to A2Pro their Merlin 16 
library of routines for the Second Sight card.  Look for that within the 
next week or so.

     Olivier hinted that Brutal Deluxe is working on several new IIGS 
games.

     There was some discussion of Brutal Deluxe's System 6.0.2 update, and 
Olivier admitted that it is currently installed and running on my ROM4 
computer.  What he failed to mention is that everything is in French, and I 
have to be quite honest and say that my high school French isn't making it 
easy for me to understand what has, or hasn't, been worked on.  Olivier 
also said that it was the other half of the Brutal Deluxe team that is 
working on that update, and unfortunately, he had a hard drive crash.  So, 
I am quite in the dark about the status of that update.  He did say that 
he'd be happy to share the source code with folks on A2Pro.

     As I sat back and watched the rtc, I was aware that Olivier had some 
difficulties understanding some of the English being used, and I hope that 
everyone understood Olivier's English.  As he stated, and as I can confirm, 
English is not his native language.

     All in all, I'd say that a good time was had by all.  Thank you for 
making it possible.

Joe Kohn
                  (JOE.KOHN, CAT28, TOP6, MSG:31/M645;1)

<<<<<   I'm afraid that some of the French gems on the DeluxeWare CD-ROM
"""""   may also be lost to the rest of the world.  I'm not sure that I 
want to distribute the CD because of the inclusion of some X rated 
materials.  I've asked Brutal Deluxe to substitute those graphics and 
animations, to no avail.

     It seems counter-productive to me to include 630 megs of phenomenal 
Apple II materials, and a few megs of porno stuff.  Personally, I don't 
want to deal with anything that even one person might find offensive.

Joe
                  (JOE.KOHN, CAT28, TOP6, MSG:38/M645;1)


II ALIVE (NOW QUARTERLY)   Any word on when the next issue is being planned 
""""""""""""""""""""""""   for?
                 (J.LOFTIS, CAT42, TOP10, MSG:222/M645;1)

>>>>>   I have no idea.  I pitched a proposal to SQC for me to edit and
"""""   publish the publication, but they couldn't afford me.  The magazine 
is now being edited by SQC's art director, Carl Sperber. Articles are being 
gathered by reviews editor Jeff Hurlburt, who's not online as far as I 
know.
                  (JERRY.K, CAT42, TOP10, MSG:223/M645;1)


                         >>> MESSAGE SPOTLIGHT <<<
                         """""""""""""""""""""""""

Category 11,  Topic 2
Message 176       Thu Mar 28, 1996
HAROLD.H [Hdwr Hacker]       at 23:38 EST
 
     Sticktion problems on Quantum drives:

     Series: ProDrive
     Included models: Q-40-S, Q-80-S, Q-105-S, etc.

     If your drive is starting to show signs of what is commonly known as 
'sticktion' (doesn't want to start spinning, particularly when cold, may 
need a gentle 'thwack' or 'snap spin' to get it going) there appears to be 
some hope for extending it's usable lifespan.

     Looking at the board that is mounted on the bottom of the drive 
itself, locate the spindle motor controller chip.  This is the chip that 
has a heat sink tab sticking out of it, this tab is mounted to the cast 
aluminum chamber with two small screws.  (Chip designation is U212, chip 
itself is marked HA13441 and is made by Hitatchi)

     Immediately next to this chip (twords the 'rear' of the drive) you 
should find a ceramic bodied resistor, typically blue in color.  This 
resistor is noted as being R202 on the board's silk screening.  This 
resistor is a 0.47 ohm power resistor. (the value is stamped on the 
resistor in text, NOT color code bands)

     If the value of R202 is reduced to 0.25 ohms (approx) many of these 
drives will start reliably, even when they suffer from extreem sticktion 
symptoms.  This change in value is best effected by adding a second 0.47 
ohm 1 watt resistor in parrellel with the original one.

CAVEATS:

     R202 limits the current thru U212 (and the spindle motor windings) 
during the initial starting of the spindle motor.  Reducing it's value too 
far, or shorting it out, will very likely cause irreparable damage to the 
spindle motor itself, the spindle motor controller chip, or (in extreem 
cases) may cause the stuck heads to be ripped away from their suspension 
springs inside the sealed chamber.  This can result in a totall useless 
drive.

     Reducing the value of R202 increases the current drawn thru the 
spindle motor windings, the spindle motor controller chip, and increases 
the starting torque of the spindle motor.  It =MAY= also have other effects 
that I am not aware of, such effects MAY cause other problems as time goes 
by.  (I have not encountered such, but am covering myself just incase :)

     I have come up with this modification totally on my own, and with no 
support of any kind from Quantum, LaCie, Hitatchi, or any other company or 
individuals.  After having personally modified several drives in this 
manner, with no ill effects being noted, and full 'recovery' from the 
-symptoms- of sticktion being the only apparent change, I =THINK= that it 
is a safe modification.  In any event the =ONLY= party responsible for 
=ANY= results achived by anyone attempting this modification shall be the 
person =making= the modification.  Under no circumstances whatsoever shall 
Harold Hislop, Syndicomm, Quantum, and / or LaCie be liable in any manner 
for any end results, be those results desirable or catostophic in nature.

     Note:  This is =NOT= a cure for sticktion on these drives.  The cause 
of the sticktion (deteriorated platter lubricant) is still present and 
totally unaltered by this modification.  The intent of this modification is 
to increase the starting torque of the spindle motor, typically -HIDING- 
the pre-existing sticktion problem.  One should still seriously consider 
timely replacement of any such drive as it is nearing the end of it's 
reliably usable lifespan.

     Note:  There are often two different versions of drives from the 
ProDrive series... 'Generic' ones, and 'Apple' ones.  The Apple ones have a 
small Apple sticker on the chamber somewheres, and are recognised by the HD 
SC Setup program on a Mac as being an Apple drive.  The Generic drives are 
not 'seen' by HD SC Setup.  The only difference between these drives is the 
rom (or eprom) that is installed in a zero profile socket on the drive's PC 
board.  The Generic rom provides a HIGHER starting torque than the Apple 
rom. (sometimes higher enough to hide symptoms of sticktion, sometimes not) 
These roms (eproms) appear to be -directly- interchangable, with no loss of 
data ever having been noted by myself.

     This message MAY be freely reproduced in any group newsletters, etc. 
without securing prior permission from myself, PROVIDED that the entire 
message as originally posted on Genie (including message header, tag line, 
and all text between) is reproduced without -ANY- alterations of ANY kind 
being made (the only exception to this would be for formatting purposes.  
NO wording changes (incl spelling corrections) may be made!)  The original 
source of this message (A2 RoundTable on Genie) must be suitably 
attributed.

-Harold
Resident Solder Slinger
Live Free or Die - New Hampshire state motto

                               [*][*][*]


    While on GEnie,  do  you spend most of your time  downloading files?
If so, you may be missing out some excellent information in the Bulletin
Board  area.   The messages  listed above  only scratch  the surface  of
what's available and waiting for you in the bulletin board area.

    If you are serious about your Apple II, the GEnieLamp staff strongly
urge  you to give the  bulletin board area a try.   There are  literally
thousands  of messages  posted  from people  like you from  all over the
world.



[EOA]
[HUM]//////////////////////////////
                    HUMOR ONLINE /
/////////////////////////////////
Fun & Games On Genie's A2 RoundTable
""""""""""""""""""""""""""""""""""""
by Gary Utter and Mike Westerfield
 [GARY.UTTER]          [BYTEWORKS]



          >>> YOU KNOW YOU'VE BEEN ON GENIE TOO LONG WHEN... <<<
          """"""""""""""""""""""""""""""""""""""""""""""""""""""

o  you're on a road trip, and you see a sign for the "RT 15 Cafe", and you 
   automatically translate that into the "RoundTable 15 Cafe" and wonder 
   why you've never heard of RoundTable 15.

o  you notice that you've just filled out a hotel registration card with a 
   period between your first and last names.

                                                            (Gary R. Utter)

o  you think it's odd that someone seems to know you on the phone or in 
   person, but can't place who it is until they tell you their screen name.

                                                         (Mike Westerfield)



[EOA]
[FOC]//////////////////////////////
                     FOCUS ON... /
/////////////////////////////////
The Apple Blossom AND Juiced.GS
"""""""""""""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



                     >>> NEW APPLE II NEWSLETTERS <<<
                     """"""""""""""""""""""""""""""""

     In my February 1996 editorial, "Read All About It", I worried a good 
deal about duplication of effort.  As with most worrying, I might just have 
well have not bothered.  Having taken a look at the two new Apple II 
newsletters on the scene, there is remarkably little _editorial_ 
duplication of effort.  (Mind you, each editor has to print address labels 
and lay out the pages and worry about just how late the printer will be.  
So I guess there's still that duplication of effort.)

     This article is going to look at both newsletters.  What's it not 
going to do is compare them.  No charts or tables laying them out side by 
side.  They're both fine publications and can stand on their own merits.


THE APPLE BLOSSOM   The first issue of _The Apple Blossom_ that paying 
"""""""""""""""""   subscribers received was Volume 2, Number 1.  The first 
volume was distributed freely.  The _Blossom_ started in January 1995 as a 
4-page product review, and publisher/editor Steven Cavanaugh produced four 
free issues in 1995.

     Now that the _Blossom_ is a commercial venture, it's 14 pages long.  
It has a newsletter feel still--it's single letter-sized sheets bound by a 
single staple in the top left corner.  The layout is clean but has a 
slightly rushed feeling.  Still, good use is made of tables, and the tables 
themselves are very clean and clear, which helps.

     Here are the contents of this first "commercial" issue:

     From the Publisher
     HyperTalking:  Drawing a Calendar
     Talking II:  Cindy Adams [by Ryan Suenaga]
     The Other Side of the Rainbow:  Cartoon Clip Art CD-ROM
     The Right Type:  Dingbat "Clip Art"
     The Dialog PDS No CLK Program [by David S. Stodolsky]
     Announcements
     Review:  Convert 3200
     Webster's Unabridged:  Cruising the Web

     The presence of writers other than Cavanaugh helps is encouraging.  
That's not a knock; one voice, no matter how mellifluous, can seem to drone 
after a long time--or even a short time!

     The HyperTalking column looks to be a draw.  It shows by example how 
to use the simple but powerful programming language that's built into 
HyperCard IIgs.  I predict this column will encourage others to explore 
HyperTalk, and perhaps at long last people will warm up to HyperCard.

     If the first issue has a focus, it's how to use material "for the 
other guys" on your Apple II.  Cavanaugh looks at the Cartoon Clip Art 
CD-ROM, which is intended for Windows users, and manages to slip in a 
mini-review of Convert 3200 in the process.  (There's a full-fledged review 
of Convert 3200 later in the issue.)  The article on using TrueType and 
PostScript dingbat fonts as clip art has a similar feel.  Scrounging from 
other platforms is a great way to extend the life of your Apple II!

     The Announcements column contains news from the Apple II world, and 
Cavanaugh has managed a scoop or two.  This section doesn't have any space 
between the news items, though, and runs together, making it harder to read 
than it needs to be.

     You won't be surprised to hear that I recommend you subscribe to _The 
Apple Blossom_.  As of mid-March, Cavanaugh had just under 100 subscribers 
in 35 states, 2 provinces, and in France.  If you were one of them, 
congratulations.  If you weren't you can still get the first issue by 
subscribing now.

     Cavanaugh hints that the next issue will review 2 CDs of sounds that 
can be used on an Apple IIgs, and explain how to print "two up" on a 
PostScript printer, plus more.

     The Apple Blossom
     1117 Maple Street
     Wilmington, DE 19805

     published 6 times a year (bi-monthly)

     $12 for U.S./Canadian Subscriptions
     $18 for overseas
     check or money order made out to Steve Cavanaugh


JUICED.GS   Max Jones has been in the newspaper business for over 20 years, 
"""""""""   so it shouldn't surprise you that his newsletter is both 
professionally and attractively laid out.  Very good use is made of 
half-tone photographs and of illustrations (often screen captures, never 
clip art for clip art's sake) and the newsletter has the feel of a magazine 
as result.  Jones also knows how to use pull-out quotes--the Mike 
Westerfield quote on the bottom of page 11 is particularly 
arresting--though they do get overused just a little.

     The "premier" issue is 24 pages long, and is saddle-stitched (that 
is, bound with staples through the fold).  The contents are as follows:

     Cover story:  The Roller-Coaster Ride of '95
     GreetinGS:  We have liftoff!
     Feature:  The Lost Generation of the IIgs
     Review:  discQuest Encyclopedia 2.0
     Review:  PMPFax 1.0 [by Tim Kellers]
     Shareware spotlight:  Sounding off (rSounder, MegaBox, SoundIt!)
     DumplinGS:  News from the Apple II world
     II Be Named Later:  Power for the People [by Ryan M. Suenaga]

     The best part of the premiere issue is clearly the feature article on 
the prototype "Mark Twain" Apple IIgs.  Jones took some trouble to dig deep 
for details, and yet diplomatically avoids stealing Joe Kohn's thunder.  
(Kohn is expected to have an expose on the "Mark Twain" in the next issue 
of _Shareware Solutions II_.)

     The reviews of discQuest Encyclopedia and PMPFax are also extensive 
and good--plenty to sink your teeth into.  The shareware reviews are 
(appropriately) shorter.  This section drew my attention to a couple of 
pieces of shareware I might otherwise have missed.

     Ryan Suenaga makes an appearance here, too.  This, of course, comes 
as a surprise to no one.  Ryan has contributed to _GS+_, _GenieLamp A2_, 
_The Apple Blossom_, _Juiced.GS_, and is said to have an article in the 
upcoming second issue of _The AppleWorks Gazette_.  Plus he's a regular in 
the A2 RoundTable.  Where he gets his energy I do not know--must be all 
those macadamia nuts.

     Max Jones's DumplinGS column, a news round-up, shows that Jones knows 
how to research even the briefest story--there are facts in his coverage of 
the SecondSight card and Wolfenstein 3-D, for example, that I haven't read 
anywhere else.  This effort to talk to the people making the news, rather 
than just reporting it, is a must for every newsletter, newspaper, 
magazine, and broadcast... so why is it often overlooked?

     Jones reports that he has subscribers in 37 states and 4 foreign 
countries, and that he is accepting submissions and story proposals.  The 
next edition of _Juiced.GS_ will be published in late May 1996.

     In a recent real-time conference, Jones said that his is "now 
beginning to hear from folks who have no on-line access, who are hearing 
about the newsletter from user group newsletters".  This is good and 
significant news.  Apple II users who arrive on Genie are likely to behave 
like a desert traveller arriving at an oasis.  It's terrific to hear about 
Apple II users who aren't online, and to know that help is available for 
those who don't own a modem.

     Mind you, if you already own a modem (which is pretty likely if 
you're reading this issue of GenieLamp A2), a subscription to _Juiced.GS_ 
still couldn't hurt.

     Juiced.GS
     2217 Lakeview Drive
     Sullivan, IN  47882

     publisher 4 times a year (quarterly)

     $14 in U.S. and Canada
     $20 overseas
     check or money order made out to Max Jones



[EOA]
[PDQ]//////////////////////////////
                    PD_QUICKVIEW / 
///////////////////////////////// 
Yours For The Asking
""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



         Program Name    :  In The Breach of Centuries
         Filename        :  HB.FREE.1.BXY
         Library Area    :  29
         Program Number  :  16498
         File Size       :  275584
         Program Type    :  HyperStudio stack
         Author          :  Paul Wayne Hiaumet
         File Type       :  Freeware
         Requirements    :  HyperStudio run-time module
                            (file #24732, HS3.1RJ.BXY)

                                 [*][*][*]


     Have you been wondering how on earth you're going to use nine free 
hours each month?  Have you realized yet that you can now download some of 
the larger files in the library, which you previously thought of as being 
beyond your means?  The file we're going to look at this month should take 
approximately 21 minutes to download at 2400 baud, or 7 minutes to download 
at 9600 baud.  If you happen to have a 14,400 baud connection, then I guess 
you're going to have to find another way to use up your nine hours!

ABOUT THIS PROGRAM   "In The Breach of Centuries" is a HyperStudio stack 
""""""""""""""""""   reprinted from HyperBole #4, a magazine on disk 
published in 1991.  Although it was distributed on disk, the focus of 
HyperBole was literary--it was not a magazine about computers.

     This stack is a mixture of fact and fiction.  It tells the story of 
Galveston Island in September 1900, using historically accurate 
information--actual telegrams, correspondence, and news stories of the 
time--and fiction.  And what a story!  Although you will sense the ending 
from the very beginning--even if your history is a little sketchy--I want 
to avoid giving anything away here.

                                 [*][*][*]


                     .                       __ 
                    (^)        <^>         /~  ~\ 
                     \-=======_/"\_======-/     \)
PD_Q RATING          "\.        1       ./"
"""""""""""             "\._    _   _./"
 .                       __  (_____) .                       __
(^)        <^>         /~  ~\       (^)        <^>         /~  ~\
 \-=======_/"\_======-/     \)       \-=======_/"\_======-/     \)
 "\.        2       ./"              "\.        3       ./"
    "\._    _   _./"                    "\._    _   _./"
         (_____)                             (_____)
 .                       __          .                       __
(^)        <^>         /~  ~\       (^)        <^>         /~  ~\
 \-=======_/"\_======-/     \)       \-=======_/"\_======-/     \)
 "\.        4       ./"              "\.        5       ./"
    "\._    _   _./"                    "\._    _   _./"
         (_____)                             (_____)

                            FIVE LAMPS                       (1-5)


PD_Q COMMENTS   This is the most effective hypermedia presentation I have 
"""""""""""""   ever seen.  It really got to me.

     The stack begins with a short prologue, gives you the chance to 
explore five supplementary areas and one broad area, then presents a brief 
epilogue.  The prologue warns you to adjust your perceptions:  "During the 
later years of the nineteenth century, Galveston, Texas was one of the 
largest cities in the south.... she was the envy of her neighbours, 
including the struggling infant, Houston, to her north."

     After the prologue, you're presented with a map.  As the story 
unfolds, numbered markers appear on the map, giving you the chance to be 
drawn deeper into the story.  (WARNING:  For your own enjoyment, do not 
click on the city of Galveston until after all five markers have appeared.)

     An often-heard complaint about HyperStudio and HyperCard stacks is 
that they don't give the user enough to do.  They make you sit there as 
they drag the information they wish to present past your weary eyes, like 
an old-time panorama.  That's not the case here.  Each of the five markers 
leads you to a page with a standard layout.  You're shown the time and the 
date, and there's some text to read that's central to this chapter.

     Up at the top of each chapter page, there are four graphics you can 
click on.  The rightmost graphic of telegraph workers lets you read a 
telegram.  The graphic of a photographer shows you an photograph from the 
archives.  The graphic of a newsboy shows you a headline or story from the 
newspaper.  The leftmost graphic lets you read an account--sometimes 
factual, sometimes fictional--of one (or more) of six people who lived 
through the events:  a juvenile vagabond named Thaddeus; chief weather 
observer Isaac Cline; a nurse named Rebecca; Father Breeland, a priest; and 
two nameless narrators--one a sailor on board the _Kendall Castle_ and the 
other a shanty dweller.

     In terms of keeping you interested, the design of the page is 
adequate, not more.  What really draws you in is the text.  The words.  The 
writing.  The stories.  You get to know these people and care about what 
happens to them in an astonishingly short time.

     While you're reading, the stack plays a song that you may know the 
chorus of, though uou may not recognize it at first.  My advice is that you 
leave it playing for as long as you possibly can.  It can increase the 
tension and add to the atmosphere of the stack.  If it merely irritates 
you, by all means switch it off.

     By the time you get to the fifth marker--the closest on the map to 
Galveston--you should have a sense of foreboding.  This isn't alleviated 
any when you visit the fifth chapter and click on the telegraph graphic.  
The message that you read is simple but stark.

     Once you've passed all five markers, you can click on the city of 
Galveston.  The map of the coast will be replaced with a map of the city 
itself, and you'll have the chance to click on six unlabelled and 
unnumbered locations in the city and find out what became of Rebecca, 
Father Breeland, Isaac, Thaddeus, the sailor and the shanty-dweller.  Feel 
free to visit these locations in any order you like.

     You would have to have a heart of rock not to be affected by the 
final entry in Isaac Cline's journal.

     When you have visited all six locations on the map of the city, click 
the exit button in the bottom right corner of the screen for the 
disquieting epilogue.

     To be sure, there are a few flaws in this stack.  In one case, a 
button is missing, leaving you stranded on the screen!  Once you click on 
the third marker, you'll see the familiar chapter page.  When you click on 
the leftmost graphic, you'll get to scroll through the story of Rebecca, 
the nurse.  Once you've finished, you're stranded!  Instead of clicking, 
press OA-~.  (The ~ character is immediately to the left of the space bar.)  
This is HyperStudio shorthand for "go back", and should return you to the 
chapter page.

     Another curious feature of this stack is that it discourages 
returning to a chapter you've already seen.  If you've reached the fourth 
chapter and want to re-read part of the first chapter, you can accomplish 
that without hindrance.  The problem comes when you want to return to the 
fourth chapter--you can't, not without visiting each chapter page all over 
again.  To make this as painless as possible, click on each numbered marker 
as it appears on the map.  This will take you to a chapter page.  Then 
click on the small map of Texas in lower right corner to return to the map.  
Keep doing this until you are back at the chapter you last read.

     One final caution:  because this stack uses pre-System 6 tools, it do 
things to your IIgs that will cause it to crash soon after you have 
finished it.  Therefore you should save anything volatile before running 
this stack.  By the time the music has sped up or slowed down, you'll know 
something bad has happened, but it may be too late to do anything about it.

     This stack has earned every one of its five lamps.  Its may not be a 
perfect stack, but it's so good that a few imperfections cannot affect its 
perfect score.  My only fear is that your expectations have been raised too 
high by my praise, and that you will come away disappointed.  I hope not.  
"In The Breach of Centuries" is a wonderful example of the power of 
hypermedia and the power of good writing woven together almost seamlessly.

RATING SYSTEM
"""""""""""""

          5 LAMPS.........What? You haven't downloaded this program YET?
          4 LAMPS.........Innovative or feature rich, take a look!
          3 LAMPS.........Good execution, stable program.
          2 LAMPS.........Gets the job done.
          1 LAMP..........A marginal download.
          0 ..............GEnieLamp Turkey Award!



[EOA]
[BAN]//////////////////////////////
                  FILE BANDWAGON /
/////////////////////////////////
Top 10 Files for February
"""""""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



     This feature lists the ten most popular files for the month.  To give 
files a chance to seek their own levels, no files will be added to the list 
until they've been in place at least a month.  This month, we look at the 
files uploaded 1-29 February 1996.

     This isn't the Academy Awards ceremony, folks; it's more like the 
People's Choice Awards (both of which are trademarked, by the way).  The 
Top 10 doesn't necessarily tell you what's new and interesting--what files 
_you_ might find interesting--it simply tells you what files have been 
downloaded a lot--what other people found interesting!

     Because two versions of the same program appear in the Top 10 this 
month, we've added an eleventh file to the list, so that you get ten 
different files, and not just ten different file names.  Does that make 
sense?  I hope so, because here comes the list:

File #  Filename          Bytes  DLs  Short description
------  ---------------  ------  ---  -------------------------------------
26573   PUYOPUYO.BXY     479744  262  An awesome addictive new IIgs game!
26554   JPEGSV10.BXY     139496  212  JPEG image viewer.
26501   CHRONOS2.1.BXY     7552  136  Menu Bar clock and MUCH more!
26589   MARK.TWAIN.TXT     9316  129  Review of the Mark Twain IIgs
26646   INTERNET.BXY     121624  125  HyperStudio Internet Directory
26531   RPACKER1.0.BXY    12120  115  Finder Extra to compact resources
26583   DALEKS.BXY        23168  105  GS Desktop version of Daleks!
26522   GSE.V4.30.BXY    177408  104  GS Entertainment version 4.30
26601   GSE4.31.BXY      177408  101  GS Entertainment version 4.31
26516   MONITOR.FIX.BXY   12252   96  Harold Hislop's monitor fixes
26479   BEG.HTML.BXY      17292   95  Beginner's Guide to HTML


PUYOPUYO.BXY   PuyoPuyo IIgs by "Slixter" (Bret Victor) is a game where 
""""""""""""   colored balls fall down to the bottom of the screen in 
pairs, and you try to keep the resulting pile from reaching the top.  The 
only way to make balls go away is by having four balls of the same color 
touch.  It's kind of a hyped-up cross between Tetris and Columns, but even 
more fun, especially for children, who will enjoy the faces on the colored 
balls.  Can be played by one or two players.  Colorful graphics, music and 
sound effects.  A Right Triangle Production.  Freeware.

JPEGSV10.BXY   jpeGS v1.0 by Mark Marr-Lyon displays JPEG graphics on the 
""""""""""""   IIgs.  Supports the SecondSight card, but does _not_ require 
one.  Has a preview mode, and supports scaling of graphics (1:1, 1:2, 1:4, 
1:8), which is helpful, since JPEG graphics are often large and will not 
fit on the IIgs screen.  Freeware.

CHRONOS2.1.BXY   Chronos II v2.1 by Kris Olsson is a menu-bar clock from 
""""""""""""""   1991 that still works nicely under System 6.0.1.  What 
makes Chronos II special is that it doesn't just offer you the time and 
date--just by clicking on the menu bar, you also have a stopwatch, a free 
memory indicator, a report on the largest free block of memory, and mouse 
cursor coordinates.  Chronos II can be positioned anywhere in the menu bar 
so that it doesn't interfere with the legitimate menu-bar real estate in 
your favorite desktop program.  Shareware ($7-10).

MARK.TWAIN.TXT   This 1600-word article by Jim Pittman about the legendary 
""""""""""""""   "Mark Twain" Apple IIgs originally appeared in the 
AppleQuerque Computer Club's publication AppleTalk.  Learn how AppleQuerque 
got their hands on one of these legendary prototypes!

INTERNET.BXY   This HyperStudio stack collects an amazing amount of 
""""""""""""   information about Internet resources.  The information is 
organized alphabetically.  No author credit is given in the stack.  It was 
uploaded to Genie by B.MASON4.

     If you don't own HyperStudio, you'll need to download file #24732:  
HS3.1RJ.BXY.  This runtime version of HyperStudio will allow you to use the 
stack.

RPACKER1.0.BXY   The rPacker Finder Extra v1.0 by Brian D. Wells helps cut 
""""""""""""""   down on wasted space in a resource fork by compacting that 
resource fork.  This can mean that your favorite programs and data take up 
less space on your hard drive!  (rPacker ignores and does not compact 
TrueType fonts.)  Requires System 6.0.1.  Freeware.

DALEKS.BXY   Daleks! v1.0 by Roy LeCates is a classic game where you are 
""""""""""   being pursued by determined but stupid killer robots 
("Exterminate!  Exterminate!") and you try to lead them such a merry chase 
that they crash into one another.  Shareware ($5).

GSE.V4.30.BXY and GSE4.31.BXY   GS Entertainment by Clayburn W. Juniel, III 
"""""""""""""""""""""""""""""   is a kind of jukebox that plays music and 
shows pictures too.  It can display PNT graphics, PIC graphics, 256 and 
3200 color graphics, INI desktop graphics, and ANI animations!  It can 
simultaneously play music from Music Composer, Music Studio, SoundSmith, or 
SynthLab, as well as rSounds, HyperStudio and HyperCard IIgs formatted 
sound files, and "just plain sound files".  Shareware ($10).

     (NOTE:  If you want to download the most recent version of GS 
Entertainment, download file #26866, GSE4.32.BXY.  This version fixes bugs 
in the two versions mentioned above.)

MONITOR.FIX.BXY   This archive of a bulletin board message posted by Harold 
"""""""""""""""   Hislop will be extremely helpful to those trying to fix 
an AppleColor RGB monitor.  The file lists common symptoms and simple 
fixes.  GenieLamp A2 readers take note!:  This message appear in the HEY 
MISTER POSTMAN column in the March 1996 issue.

BEG.HTML.BXY   This is A Beginner's Guide to HTML.  HTML stands for 
""""""""""""   HyperText Markup Language, and is a way of formatting text 
for use on World Wide Web pages.  If you want to create your own web page, 
start by reading this 4800-word file!  Also available on the web at:

     http://www.ncsa.uiuc.edu/general/internet/WWW/HTMLPrimer.html



[EOA]
[LIB]//////////////////////////////
              THE ONLINE LIBRARY /
/////////////////////////////////
February Arrivals
"""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



     This month, I'd like to list all the files that have been uploaded to 
the A2 RoundTable libraries during February.  Since there are 141 of them, 
I can't examine them in detail, but the short description provided should 
give you an idea of what the file is all about.  If you'd like more 
in-depth coverage, please let me know!

     This month, I haven't included 36 message archives in the file 
listing.  So technically, there were 177 uploads in February, not just 141.


                             >>> PROGRAMS <<<
                             """"""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26627   ANIME.BXY         16064  Animation utility for paint programs
26501   CHRONOS2.1.BXY     7552  Menu Bar clock and MUCH more!
26498   DOS33LNCHR.BXY    41692  Load and launch DOS33 frm HD or 3.5"
26522   GSE.V4.30.BXY    177408  GS Entertainment version 4.30
26601   GSE4.31.BXY      177408  GS Entertainment version 4.31
26554   JPEGSV10.BXY     139496  JPEG image viewer.
26602   MPLAYER.CDA.BXY    8832  Music Player CDA version 1.3
26485   NULIB324.ZIP      63488  UnShrink files under MS-DOS
26531   RPACKER1.0.BXY    12120  Finder Extra to compact resources
26581   WINFLATE122.BXY   10112  NDA minimizes window to menubar


                               >>> GAMES <<<
                               """""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26583   DALEKS.BXY        23168  GS Desktop version of Daleks!
26491   EAMON.DOS09.BXY  794904  Eamon Collection: DOS 3.3 Disk 9
26569   EAMON.DOS10.BXY  770088  Eamon Collection: DOS 3.3 Disk 10
26591   EAMON.DOS11.BXY  784152  Eamon Collection: DOS 3.3 Disk 11
26573   PUYOPUYO.BXY     479744  An awesome addictive new IIgs game!


                            >>> HYPERMEDIA <<<
                            """"""""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26646   INTERNET.BXY     121624  HyperStudio Internet Directory
26530   MM.BXY            24832  Hyperstudio Daily Journal Program


                        >>> DATA AND TEMPLATES <<<
                        """"""""""""""""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26647   BB.DRFT.ADB.BXY   13544  AWGS draft templ. for Little League
26479   BEG.HTML.BXY      17292  Beginner's Guide to HTML
26618   BWPRICES.960221   12160  Byte Works Prices, 21 Feb 96
26520   CINDYCRD.BXY      10240  Publish It template for cards
26691   HP.PCL.BXY         3200  HP PCL Printer Codes
26589   MARK.TWAIN.TXT     9316  Review of the Mark Twain IIgs
26516   MONITOR.FIX.BXY   12252  Harold Hislop's monitor fixes
26585   TWGSCACHE32.BXY    6400  New TransWarp GS Cache available
26467   WIHICS.DOC.TXT    15340  Where In Hell Is Carmen Santiago DOX


                            >>> PERIODICALS <<<
                            """""""""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26477   IIS.960204.BXY    24532  II Something - Issue 15 - Feb 04 96
26535   IIS.960211.BXY    38656  II Something - Issue 16 - Feb 11 96
26590   IIS.960218.BXY    19840  II Something - Issue 17 - Feb 18 96
26652   IIS.960225.BXY    25984  II Something - Issue 18 - Feb 25 96



                >>> GENIE A2 ROUNDTABLE TOOLS AND FILES <<<
                """""""""""""""""""""""""""""""""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26568   A2.JAN.ADB.BXY     7436  ADB Update of A2 Library Index - JAN
26567   A2.JAN.TXT.BXY     7268  TXT Update of A2 Library Index - JAN
26465   A2NDX9602DB.BXY   18424  A2 BB Index 2/96 TXT for ADB  REV.
26466   A2NDX9602TX.BXY   18760  A2 BB Index 2/96 ASCII ver.  REV.
26692   A2NDX9603DB.BXY   18444  A2 BB Index 3/96 TXT for ADB REV.
26696   A2NDX9603TX.BXY   18756  A2 BB Index 3/96 ASCII ver. REV.
26529   ATCOP.ANN.BXY     11520  CoPilot/AT - read GEnie announcement
26562   CP.S.YOVFIX.BXY    5816  Required Update for CoPilot Spectrum
26645   GEM.COST511.BXY   51584  GEM COST 5.1.1 - GEM 5.1 macro


                         >>> SOUNDS AND MUSIC <<<
                         """"""""""""""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26622   BANANA.BXY        27520  SynthLAB song: Banana
26640   CARIBEN.BXY       91008  SynthLab sequence
26641   FANFARE.BXY       93824  SynthLab song: Fanfare for Man
26624   ISTANBUL.BXY      19968  SynthLAB song: Istanbul
26623   RHAPBLUE.BXY      29824  SynthLAB song: Rhapsody In Blue
26644   TILTHERE.BXY      26752  SynthLab song : Til There was You


                               >>> FONTS <<<
                               """""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26536   ADINEKIR.T1.BXY   41828  T1 PostScript Art Nouveau scrpt font
26537   ANIMALS.T1.BXY    71688  T1 PostScript animals font
26680   BLUEPRNT.T1.BXY   51304  T1 PostScript architectural font
26538   BORZOI.T1.BXY    115844  Good T1 PostScript body text font
26539   BRASSFLD.T1.BXY   47592  Good T1 PostScript body text font
26681   BUBBALOV.T1.BXY  133856  T1 PostScript thin display font
26523   FRANKS.T1.BXY     26044  T1 PostScript bold causal disp. font
26524   FRANKS.TT.BXY     16880  TrueType bold casual display font
26682   FRANKTIM.T1.BXY   38828  T1 PostScript body-text font
26683   GOODFELO.T1.BXY   37248  T1 PostScript Victorian display font
26684   INFORMAL.T1.BXY   31928  T1 PostScript casual font
26685   MACHBLOK.T1.BXY   13428  T1 PostScript bold headline font
26686   MONOTONY.T1.BXY   50352  T1 PostScript mono-spaced font
26687   MURIEL.T1.BXY     90020  T1 PostScript beautiful script font
26525   NIXON.T1.BXY      25908  T1 PostScript Chinese menu font
26534   NIXON.TT.BXY      19072  TrueType Chinese menu font
26527   OCTAVIAN.T1.BXY   31584  T1 PostScript font for body text
26688   PARKHAVN.T1.BXY   22720  T1 PostScript popular script font
26689   PLAYBILL.T1.BXY   28400  T1 PostScript western display font
26690   SCHZWALD.T1.BXY   25140  T1 PostScript bold display font


                             >>> GRAPHICS <<<
                             """"""""""""""""

File #  Filename          Bytes  Short description
------  ---------------  ------  ---------------------------------------
26518   ADAMS.GIF         45172  Color GIF of the Adams family
26608   ALAMO.GIF         26768  Color GIF of the Alamo
26619   AMER.GOTH.GIF    255460  Color GIF of An American Gothic
26514   ANDRMA.GIF       563832  Color GIF of a Delcroix painting
26576   APPLEII.GIF      140288  Apple II Guide Cover
26473   BEARS.GIF        221504  Color GIF of grizzly bears
26513   BOATS.GIF        806724  Color GIF of a Braque painting
26504   BOQUT.GIF        620912  Color GIF of a Renoir painting
26621   BORDERS1EPS.BXY   94436  10 nice EPS borders for PostScript
26503   BOWLS.GIF        589672  Color GIF of a Picasso painting
26519   CHUNK.GIF         27112  Color GIF of Chuck Stites
26507   CHURCH.GIF       601068  Color GIF of a Sisley painting
26609   CLOWN.GIF          6468  Color GIF of a clown
26532   CLUE.BXY          75680  Pictures by Ninjaforce's artist
26476   CNY.ASCII.TXT    122160  ASCII art for Chinese New Year
26508   CRAGS.GIF        704212  Color GIF of a Renoir painting
26632   DESERT.GIF        13092  Color GIF of a desert border
26607   EASTER.GIF         2128  B&W GIF of Jesus for Easter
26506   GEORGE.GIF        24024  Color GIF of George Washington
26512   GRTWV.GIF        399848  Color GIF of Hokusai painting
26493   GULL.GIF          93448  Color GIF of a seagull
26505   HARLQN.GIF       464932  Color GIF of a Picasso Painting
26517   HEARTS.BW.BXY     57276  Hearts Alphabet graphics.  B/W SHR.
26560   HEARTS.DESK.BXY   77808  Hearts desktop background INIs.
26544   HEARTS.GIF         8416  Hearts border in a color GIF
26606   HELLCAT.GIF      244428  Color GIF of a Hellcat airplane
26597   IRELAND.GIF        1168  Color GIF of the flag of Ireland
26610   KNIGHT.GIF        34760  Color GIF of a knight in armor
26469   LEPRA.BW.GIF      10004  B&W GIF of a leprechaun
26592   LEPRE.GIF         21496  Color GIF for St. Patrick's Day
26593   LEPRECH.GIF       26520  Color GIF for St. Patrick's Day
26611   MAID.GIF          14268  Color GIF of a drawing of a mermaid
26630   MAP.GIF            3716  Color GIF of map of Ireland
26478   MARDICRAB.BXY     46712  MardiGras Crab Poster - great EPS
26575   MARKTWAIN.GIF    232192  Cover Photo for Apple II Guide
26486   MONALS.GIF        24868  Color GIF of the Mona Lisa
26634   ONEWAY.GIF         8548  Color GIF of a oneway sign
26582   ONHOLD.BXY         3584  Rotating beachball cursor init
26490   PALTTE.GIF         2620  Color GIF of an artist's pallette
26620   PARIS.GIF        563280  Color GIF of a Cailleliotte painting
26633   PARROT.GIF        22904  Color GIF of a parrot
26487   PENGN.GIF          3460  Black and white GIF of a penguin
26488   POPCRN.GIF         3572  Color GIF of popcorn and ice cream
26636   PTGLD.GIF         30740  Color GIF of Rainbow and pot of gold
26492   RNBOW.GIF          7276  Color GIF of a rainbow
26540   ROSE1.GIF          5328  Color GIF of a yellow rose
26541   ROSE2.GIF          4704  Color GIF of pink rose
26542   ROSE3.GIF          8184  Color GIF of a pink rose
26667   RYB.GIF          302136  Color GIF of a Mondrian painting
26474   SCIENCE.GIF       29660  B&W clipart of science stuff
26489   SEAL.GIF          13512  A sea lion in 16 shades of gray
26468   SHAMRK.GIF         9268  Color GIF of a shamrock
26631   SIESTA.GIF        18832  Color GIF of naptime under a cactus
26494   SKI.GIF           69488  Color GIF of a skier
26475   ST.PAT.GIF         3492  B&W GIF for St. Patrick's Day
26496   STHWST.GIF        12208  B&W Southwestern clipart objects
26599   STPADDY1.GIF      23616  Color GIF for St. Patrick's Day
26598   STPADDY2.GIF      21988  Color GIF for St. Patrick's Day
26596   STPAT01.GIF        3860  B&W GIF for St. Patrick's Day
26595   STPAT02.GIF        2436  B&W GIF for St. Patrick's Day
26594   STPAT13.GIF        7136  B&W GIF for St. Patrick's Day
26603   STPATS3.GIF        8648  B&W GIF for St. Patrick's Day
26605   STPATS8.GIF        2620  Color GIF for St. Patrick's Day
26604   STPATS9.GIF       21248  B&W GIF for St. Patrick's Day
26472   SUNBW.GIF         28896  B&W GIF of 4 different suns
26495   SUNRS.GIF         24216  Color GIF of a sunrise painting
26497   SUNSET.GIF        35012  Color GIF of saguro cactus at dusk
26515   TELSCP.GIF       491380  Color GIF of a Magritte painting
26668   TEXTR1.DESK.BXY  595704  Desktop texture backgrounds.  Mar.96
26471   TONTO1.GIF        33520  Color GIF of Tonto Monument
26470   TONTO2.GIF        58752  Color GIF of Tonto Monument
26584   TWGSCACHE32.GIF   42176  New TransWarp GS 32K Cache (GIF)
26555   VALEN2.DESK.BXY  619392  Valentine Desktop background INIs.
26543   VALEN9.GIF        20032  Color GIF for Valentine's Day
26635   VIPER.GIF        264076  Color GIF of '96 Dodge Viper
26666   WALKWORK.GIF     499768  A color GIF of a Millet painting

     I know I've mentioned it already, but I'll just repeat that the 
message archives are excluded from this month's column.  I won't reinstate 
them unless I hear from YOU!



[EOA]
[SOF]//////////////////////////////
                     SOFTVIEW A2 /
/////////////////////////////////
GraphicWriter III v2.0
""""""""""""""""""""""
by Douglas Cuff
    [EDITOR.A2]



                        >>> WHAT'S IN A NUMBER? <<<
                        """""""""""""""""""""""""""

     GraphicWriter III is a page layout program for the Apple IIgs.  Until 
recently, users had to be satisfied with v1.1, written in 1991.  In 
February, Seven Hills Software began shipping v2.0.

     Australian programmer Richard Bennett is the man behind this update, 
despite the fact that he was not the original author of the programmer.  
(It can't have been a lot of fun trying to update someone else's code.)  
Bennett rewrote and optimized some code, and updated the program so that it 
was more System 6 compliant.

     For example, GraphicWriter III now:

     o  supports the standard system clipboard;

     o  supports the standard font menu;

     o  supports the Finder in that you can click on a GWIII document, and 
        this will cause GWIII to launch and open that document;

     o  supports AppleShare networks for its preferences;

     o  supports system event sounds;

     o  recognizes keyboard shortcuts such as Return for accepting a 
        default button, OA-. for Cancel, and--most importantly--OA-w to 
        close a document.  (In GWIII v1.1, OA-w displayed your document so 
        that it fit in the available window and you could see the whole 
        page.)

     Many of these changes are important, since the lack of them made the 
previous version of GraphicWriter III a pain to use.  GraphicWriter III 
didn't get along with all sorts of other utilities.  Now it does.  It gets 
along with the Twilight II screen saver now, too.

     Although these changes are important, it's hard to work up any real 
excitement over them.  The program did things in a non-standard way, and 
now it does them a standard way.  Everybody else has been doing things this 
way since System 6.0.1 was released.  Not the sort of thing that's going to 
make for a gripping demonstration at your next user group meeting.

     Fortunately, Seven Hills included a few goodies with v2.0 as well.  
The most significant is the new Object Specs feature.  This allows you to 
enter the position and size of an object in precise terms, by typing 
numbers in the specification box.  This allows you to be more accurate in 
the placing of objects--no more struggling with the mouse!

     Also useful are the reduction and enlargement buttons in the Object 
Specs window.  You can quickly reduce an object to half- or quarter-size, 
or enlarge it to twice or four times its original size.  These reductions 
and enlargements are based on the object's original size--clicking the 
"1/2" button does not continue to reduce the size of an object by 50%.

     These reduction buttons make it possible to get very sharp-looking 
graphics when you print!  If you copy a picture while in a standard paint 
program such as Platinum Paint, you can now paste it into a GraphicWriter 
document... but wait!  If you click the Object Specs "1/2" or "1/4" 
reduction button immediately after pasting your picture, GraphicWriter will 
reduce the size of the graphic but attempt to keep the original resolution 
when it prints.  You owe it to yourself to try this out--it's very slick!

     If image quality is important to you, don't get too used to being 
able to paste graphics into GraphicWriter.  If you're keeping the graphic 
its original size--no reduction--then the old method of import a graphic 
sometimes yields better quality than pasting at the original size.  I tried 
both importing and pasting a MacPaint graphic, and got better image quality 
with importing.

     The concept of master pages is important to most page layout 
programs, and GraphicWriter III is no exception.  GraphicWriter has always 
had master pages, but now there's an option to erase the guides on whatever 
page you're working on, and replace them with the guides from the master 
pages.

     That about wraps up the major functional changes to GraphicWriter III 
v2.0, which is kind of disappointing.  There are still some minor but still 
convenient changes.

     My favorite minor change is in the OA-Jump to page window.  Until 
now, you had to use the mouse to select the page from a scrolling list.  
With this update, you can type the number of the page you want, and get 
there _fast_!  You can enter L or R instead of a number if you want to jump 
to the left or right master page--now that's slick!

     Another handy feature is the way the "next page" function operates if 
you are on the last page of a document and there _is_ no last page:  a 
window pops up so that you can insert more pages after the last page.

     If you chose to have GraphicWriter measure in inches (instead of 
centimetres, picas, or points), then you'll be glad to know that the 
horizontal and vertical rulers are now marked in sixteenths of an inch, not 
tenths of an inch.  This is a considerable relief.

     There are other cosmetic changes, some of which the user will 
applaud, and others of which the user will scarcely notice.  For example, 
the paragraph and page layout windows now use different colors, which is 
noticeable but unexplained.  The old colors didn't seem to do any harm, but 
the same can be said of the new colors.  It's still possible to use either 
version with a monochrome monitor.  Other windows have been changed is 
similarly minor ways.  For example, the descriptive labels in the Text Wrap 
window have been made shorter.

     One design change that's well worth noting is in the Align Objects 
window.  Instead of short descriptions ("Tops", "Bottoms"), there are radio 
buttons in appropriate positions around the example window.  There's no 
longer any need for the user's brain to translate from words to visual 
effect, which is a nice touch.

     GraphicWriter III v2.0 now supports extras, which make it easier for 
programmers to write add-on modules.  This is potentially exciting, but 
practically worthless for now.  No one has yet written an extra, which 
means that the Extras menu doesn't even appear in v2.0.

     There's now a help menu, which new customers will appreciate.  The 
help is fairly extensive--it will sure save you the trouble of scrabbling 
for the manual if you just need to be reminded how to mix and match pica 
measurements and inch measurements in the same document, so it has value to 
old-timers, too.

     This new version fixes a few bugs in GraphicWriter.  It does not, 
however, address all the changes that were crying out to be made.  (My pet 
peeve:  If you want to print one page of a ten-page document, GraphicWriter 
III "builds" every last page.  This takes time, especially in a 
font-intensive document.)  No serious flaws remain, but irritants do.

     Slightly more serious is Seven Hills' claim that the spelling checker 
now handles "curly" apostrophes correctly.  In v1.1, "you'll" with a 
straight apostrophe was not flagged as incorrect, but "you'll" with a curly 
apostrophe was queried.  (How I wish I could demonstrate the difference 
between straight and curly, but ASCII text doesn't allow for it!)  This new 
version may handle curly apostrophes correctly more often, but I was still 
able to make it choke by the simple expedient of using the sample files 
included on the update disk!

     The list of new features also claims that Snap to Guides "code was 
greatly improved so that objects will snap to the same position even if 
they are slightly left/right or above/below the guide being snapped to."  
This may be a subjective thing, but I found v1.1 lacking in this respect, 
and I don't notice any difference in v2.0.  Possibly I need to spend more 
time with the updated package.

     One feature has been removed from GraphicWriter III v2.0--the ability 
to auto-save files.  Gone!  Gone without a cry!  I never used it and don't 
miss it, and admit that it's not the sort of thing you mention in an 
advertising campaign, but surely it should have been mentioned in the 
manual, shouldn't it?

     As is usual, this new version fixes some bugs and introduces one or 
two new ones.  I haven't found any serious bugs; no guaranteed crashes or 
hangs.  There's a cosmetic flaw:  the vertical ruler sometimes overdraws 
the "- Page +" box slightly, but no harm is done.  I've had problems with 
Find/Change that I didn't have before, but I suspect these are DUE TO INIT 
CONFLICTS, AND NOT THE FAULT OF GRAPHICWRITER, because they disappear when 
I shift-boot.  After all, you the user are responsible for what you add to 
your system.

     GraphicWriter III v2.0 is a welcome update, but not an impressive 
one.  With the update package, Seven Hills Software included a notice that 
they are suspending phone support for the products, so perhaps we should be 
grateful that any update was possible.  I have no bone to pick with Seven 
Hills, but users who have been led to expect great things by the fact that 
the version number jumped from 1.1 to 2.0 should temper their enthusiasm 
with reality.

     GraphicWriter III is a good program, and now it's better.  Long-time 
users will want this upgrade, and new users no longer have any reason to 
delay their purchase.  Just keep in mind that it's been refurbished, not 
remodeled, and you'll be happy when v2.0 arrives in your mailbox.









           //////////////////////////////////////// Genie_QWIK_QUOTE ////
          /  The IIgs outsold the entire Macintosh product line one    /
         /  Christmas season; I don't remember which year it was --   /
        /  I think it was either 1986 or 1987.                       /
       ////////////////////////////////////////////////  SHEPPY  ////



[EOA]
[LOG]//////////////////////////////
                         LOG OFF /
/////////////////////////////////
GenieLamp Information
"""""""""""""""""""""

    o   COMMENTS: Contacting GenieLamp

         o   GenieLamp STAFF: Who Are We?


GenieLamp Information   GenieLamp is published on the 1st of every month
"""""""""""""""""""""   on Genie page 515.  You can also find GenieLamp on 
the main menus in the following computing RoundTables.


RoundTable      Keyword  Genie Page     RoundTable      Keyword  Genie Page
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
DigiPub         DIGIPUB       1395      Atari ST        ST            475
Macintosh       MAC            605      IBM PC          IBMPC         615
Apple II        A2             645      Apple II Dev.   A2PRO         530
Macintosh Dev.  MACPRO         480      Geoworks        GEOWORKS     1050
BBS             BBS            610      CE Software     CESOFTWARE   1005
Programming     PROGRAMMING   1445      Data Comm.      DATACOMM     1450
IBM PC Prog     IBMPCPRO       617      PowerPC         PPC          1435
PowerPCProg     PPCPRO        1440


    GenieLamp is also distributed on CrossNet and many public and 
commercial BBS systems worldwide.

    o To reach GenieLamp on Internet send mail to genielamp@genie.com

    o Back issues of GenieLamp are available in the DigiPub RoundTable 
      Library #2 on page 1395 (M1395;3).

    o We welcome and respond to all E-Mail.  To leave comments, suggestions 
      or just to say hi, you can contact us in the DigiPub RoundTable 
      (M1395) or send GenieMail to John Peters at [GENIELAMP] on page 200.

    o If you would like to meet the GenieLamp staff "live" we meet every 
      Wednesday night in the Digi*Pub Real-Time Conference at 9:00 EDT 
      (M1395;2).

    o The Digital Publishing RoundTable is for people who are interested in 
      pursuing publication of their work electronically on Genie or via 
      disk-based media.  For those looking for online publications, the 
      DigiPub Software Libraries offer online magazines, newsletters, 
      short-stories, poetry and other various text oriented articles for 
      downloading to your computer.  Also available are writers' tools and 
      'Hyper-utilities' for text presentation on most computer systems. In 
      the DigiPub Bulletin Board you can converse with people in the 
      digital publishing industry, meet editors from some of the top 
      electronic publications and get hints and tips on how to go about 
      publishing your own digital book.  The DigiPub RoundTable is the 
      official online service for the Digital Publishing Association.  To 
      get there type DIGIPUB or M1395 at any Genie prompt.


                          >>> GENIELAMP STAFF <<<
                          """""""""""""""""""""""

  GenieLamp  o John Peters         [GENIELAMP]    Publisher
  """""""""  o Mike White          [MWHITE]       Managing Editor

   APPLE II  o Doug Cuff           [EDITOR.A2]    EDITOR
   """"""""  o Charlie Hartley     [A2.CHARLIE]   A2 Staff Writer

      A2Pro  o Tim Buchheim        [A2PRO.GELAMP] EDITOR
      """""

      ATARI  o Sheldon H. Winick   [GELAMP.ST]    ATARI EDITOR
      """""  o Bruce Smith         [B.SMITH123]   EDITOR/TX2
             o Jeffrey Krzysztow   [J.KRZYSZTOW]  EDITOR/HyperText
             o Mel Motogawa        [M.MOTOGAWA]   Atari Staff Writer
             o Lloyd E. Pulley     [LEPULLEY]     Atari Staff Writer
             o Michael J. Hebert   [ST.PAPA]      Atari Staff Writer

        IBM  o Sharon La Gue       [SHARON.LAMP]  IBM EDITOR
        ~~~

  MACINTOSH  o Richard Vega        [GELAMP.MAC]   MACINTOSH EDITOR
  """""""""  o Tom Trinko          [T.TRINKO]     Mac Staff Writer
             o Robert Goff         [R.GOFF]       Mac Staff Writer
             o Ricky J. Vega       [GELAMP.MAC]   Mac Staff Writer

   POWER PC  o Ben Soulon          [BEN.GELAMP]   POWER PC EDITOR
   """"""""  o Eric Shepherd       [SHEPPY]       Power PC Staff Writer


       ETC.  o Jim Lubin           [J.LUBIN]      Add Aladdin Scripts
       """"  o Scott Garrigus      [S.GARRIGUS]   Search-ME!
             o Mike White          [MWHITE]       (oo) / DigiPub SysOp
             o John Peters         [GENIELAMP]    DigiPub SysOp
             o Sanford E. Wolf     [S.WOLF4]      Contributing Columnist



\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////
 Opinions expressed herein are those  of the individual authors, and do
 not  necessarily  represent  the  opinions  of  Genie Online Services,
 Yovelle  Renaissance Corp.,  GenieLamp Online Magazines, or  T/TalkNet
 Online Publishing.  Bulletin board messages are reprinted verbatim and 
 are included  in this  publication with  permission from  Genie Online 
 Services and the source RoundTable.  Genie Online Services,  GenieLamp
 Online  Magazines,  and  T/TalkNet  Publishing  do  not  guarantee the
 accuracy or suitability of any information included herein. We reserve
 the right to edit all letters and copy.

 Material  published in  this edition may be  reprinted under the  fol-
 lowing terms only. Reprint permission granted, unless otherwise noted,
 to  registered computer  user groups and  not for profit publications.
 All articles  must remain unedited  and include  the issue  number and
 author  at the top of each article reprinted.  Please include the fol-
 lowing at the end of all reprints:

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////
 The  preceding  article  is reprinted  courtesy of  GenieLamp  Online
 Magazine.  (c) Copyright 1996 T/TalkNET  Publishing and Genie  Online
 Services.  To join Genie, set your modem to half duplex (local echo).
 Have  your  modem  dial  1-800-638-8369  in  the  United  States   or 
 1-800-387-8330 in Canada.   When you get a CONNECT message,  wait for
 the  U#=  prompt, type: JOINGENIE and hit the RETURN key.  Genie will 
 then  prompt  you for  your signup information.  If the signup server
 is  unavailable,  call (voice)  1-800-638-9636  for more information.
////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
[EOF]
       