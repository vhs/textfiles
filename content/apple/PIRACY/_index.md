+++
title = "Apple II: The Apple II Piracy Lore"
description = "Writings, Rags, Messages and Lore of the Apple II Pirates/Phreaks"
tabledata = "apple_piracy"
tablefooter = "There are 52 files for a total of 597,121 bytes."
+++

The Apple II is likely the first of the home computers to go from merely experiencing software piracy to being a place of pirate "Culture". Groups, alliances and lore begin rising up in the early 1980's, and continue throughout the decade. In the introduction screens of cracked Apple II games, groups list their memberships and declare pride for their work. Pirates jump through different groups, backstab, and generally exhibit the natural competitiveness of young computer users. As the pirates communicate through BBSes, they share messages, jibes, and "rag files" that attempt to decimate other, more inferior (in their opinion) groups. 

Collected here are pieces of these legends and battles. It would be in the best interest of all involved to not believe anything said in them, although no doubt grains of truth run through them all. This is as good as it gets.
