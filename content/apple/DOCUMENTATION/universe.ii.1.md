+++
linktitle = "universe.ii.1"
title = "universe.ii.1"
url = "apple/DOCUMENTATION/universe.ii.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UNIVERSE.II.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
