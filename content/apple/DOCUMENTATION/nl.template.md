+++
linktitle = "nl.template"
title = "nl.template"
url = "apple/DOCUMENTATION/nl.template.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NL.TEMPLATE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
