+++
linktitle = "summer.games.ii.txt"
title = "summer.games.ii.txt"
url = "apple/DOCUMENTATION/summer.games.ii.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUMMER.GAMES.II.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
