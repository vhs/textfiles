+++
linktitle = "magic.window.ii.txt"
title = "magic.window.ii.txt"
url = "apple/DOCUMENTATION/magic.window.ii.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MAGIC.WINDOW.II.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
