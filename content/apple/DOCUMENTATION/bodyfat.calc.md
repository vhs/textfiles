+++
linktitle = "bodyfat.calc"
title = "bodyfat.calc"
url = "apple/DOCUMENTATION/bodyfat.calc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BODYFAT.CALC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
