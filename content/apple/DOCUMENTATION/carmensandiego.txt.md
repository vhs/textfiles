+++
linktitle = "carmensandiego.txt"
title = "carmensandiego.txt"
url = "apple/DOCUMENTATION/carmensandiego.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CARMENSANDIEGO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
