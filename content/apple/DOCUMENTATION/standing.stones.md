+++
linktitle = "standing.stones"
title = "standing.stones"
url = "apple/DOCUMENTATION/standing.stones.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download STANDING.STONES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
