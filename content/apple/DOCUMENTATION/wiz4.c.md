+++
linktitle = "wiz4.c"
title = "wiz4.c"
url = "apple/DOCUMENTATION/wiz4.c.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WIZ4.C textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
