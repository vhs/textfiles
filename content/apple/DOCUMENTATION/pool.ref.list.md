+++
linktitle = "pool.ref.list"
title = "pool.ref.list"
url = "apple/DOCUMENTATION/pool.ref.list.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POOL.REF.LIST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
