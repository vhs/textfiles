+++
linktitle = "quit.to.docs"
title = "quit.to.docs"
url = "apple/DOCUMENTATION/quit.to.docs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download QUIT.TO.DOCS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
