+++
linktitle = "short.circuit.txt"
title = "short.circuit.txt"
url = "apple/DOCUMENTATION/short.circuit.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHORT.CIRCUIT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
