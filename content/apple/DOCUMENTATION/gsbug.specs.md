+++
linktitle = "gsbug.specs"
title = "gsbug.specs"
url = "apple/DOCUMENTATION/gsbug.specs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GSBUG.SPECS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
