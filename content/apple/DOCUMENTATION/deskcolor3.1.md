+++
linktitle = "deskcolor3.1"
title = "deskcolor3.1"
url = "apple/DOCUMENTATION/deskcolor3.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DESKCOLOR3.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
