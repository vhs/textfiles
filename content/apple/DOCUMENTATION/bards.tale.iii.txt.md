+++
linktitle = "bards.tale.iii.txt"
title = "bards.tale.iii.txt"
url = "apple/DOCUMENTATION/bards.tale.iii.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BARDS.TALE.III.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
