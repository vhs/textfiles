+++
linktitle = "bt2csc.txt"
title = "bt2csc.txt"
url = "apple/DOCUMENTATION/bt2csc.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BT2CSC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
