+++
linktitle = "printrix.tutor"
title = "printrix.tutor"
url = "apple/DOCUMENTATION/printrix.tutor.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRINTRIX.TUTOR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
