+++
linktitle = "fellow.creature"
title = "fellow.creature"
url = "apple/DOCUMENTATION/fellow.creature.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FELLOW.CREATURE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
