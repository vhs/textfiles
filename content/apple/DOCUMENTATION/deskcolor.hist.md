+++
linktitle = "deskcolor.hist"
title = "deskcolor.hist"
url = "apple/DOCUMENTATION/deskcolor.hist.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DESKCOLOR.HIST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
