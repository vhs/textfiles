+++
linktitle = "mind.pursuit.txt"
title = "mind.pursuit.txt"
url = "apple/DOCUMENTATION/mind.pursuit.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIND.PURSUIT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
