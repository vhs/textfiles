+++
linktitle = "leap.notes"
title = "leap.notes"
url = "apple/DOCUMENTATION/leap.notes.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LEAP.NOTES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
