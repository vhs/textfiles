+++
title = "Apple II: Soft Documentation"
description = "\"Soft Dox\" for Apple Programs"
tabledata = "apple_documentation"
tablefooter = "There are 810 files for a total of 9,419,303 bytes."
+++

As a large amount of programs were illegally copied on the Apple (a situation that was common on pretty much every Microcomputer platform), it became necessary for users to share the documentation from the original package so that the disks were useable. 

A small number of these "Soft Dox" are also for pirating programs themselves, which supplied their own documentation, often with a very entertaining bent in the writing.
