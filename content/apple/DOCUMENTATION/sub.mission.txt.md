+++
linktitle = "sub.mission.txt"
title = "sub.mission.txt"
url = "apple/DOCUMENTATION/sub.mission.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUB.MISSION.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
