+++
linktitle = "golfs.best.txt"
title = "golfs.best.txt"
url = "apple/DOCUMENTATION/golfs.best.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOLFS.BEST.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
