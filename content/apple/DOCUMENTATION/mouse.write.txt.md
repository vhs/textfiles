+++
linktitle = "mouse.write.txt"
title = "mouse.write.txt"
url = "apple/DOCUMENTATION/mouse.write.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MOUSE.WRITE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
