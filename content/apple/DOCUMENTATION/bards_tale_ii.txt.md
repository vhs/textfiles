+++
linktitle = "bards_tale_ii.txt"
title = "bards_tale_ii.txt"
url = "apple/DOCUMENTATION/bards_tale_ii.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BARDS_TALE_II.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
