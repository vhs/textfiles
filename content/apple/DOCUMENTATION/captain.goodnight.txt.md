+++
linktitle = "captain.goodnight.txt"
title = "captain.goodnight.txt"
url = "apple/DOCUMENTATION/captain.goodnight.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CAPTAIN.GOODNIGHT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
