+++
linktitle = "pitfali.ii.txt"
title = "pitfali.ii.txt"
url = "apple/DOCUMENTATION/pitfali.ii.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PITFALI.II.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
