+++
linktitle = "certif.maker1"
title = "certif.maker1"
url = "apple/DOCUMENTATION/certif.maker1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CERTIF.MAKER1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
