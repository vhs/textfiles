+++
linktitle = "roadwar_2000.txt"
title = "roadwar_2000.txt"
url = "apple/DOCUMENTATION/roadwar_2000.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ROADWAR_2000.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
