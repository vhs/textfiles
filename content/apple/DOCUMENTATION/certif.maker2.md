+++
linktitle = "certif.maker2"
title = "certif.maker2"
url = "apple/DOCUMENTATION/certif.maker2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CERTIF.MAKER2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
