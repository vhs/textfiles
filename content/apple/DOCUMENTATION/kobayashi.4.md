+++
linktitle = "kobayashi.4"
title = "kobayashi.4"
url = "apple/DOCUMENTATION/kobayashi.4.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KOBAYASHI.4 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
