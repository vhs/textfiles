+++
linktitle = "carmen.usa2"
title = "carmen.usa2"
url = "apple/DOCUMENTATION/carmen.usa2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CARMEN.USA2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
