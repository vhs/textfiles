+++
linktitle = "crisis.mountain"
title = "crisis.mountain"
url = "apple/DOCUMENTATION/crisis.mountain.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRISIS.MOUNTAIN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
