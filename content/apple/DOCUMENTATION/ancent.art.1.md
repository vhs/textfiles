+++
linktitle = "ancent.art.1"
title = "ancent.art.1"
url = "apple/DOCUMENTATION/ancent.art.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANCENT.ART.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
  split = true
+++
