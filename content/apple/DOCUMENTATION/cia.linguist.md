+++
linktitle = "cia.linguist"
title = "cia.linguist"
url = "apple/DOCUMENTATION/cia.linguist.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIA.LINGUIST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
