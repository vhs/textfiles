+++
linktitle = "temple.apshai"
title = "temple.apshai"
url = "apple/DOCUMENTATION/temple.apshai.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TEMPLE.APSHAI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
