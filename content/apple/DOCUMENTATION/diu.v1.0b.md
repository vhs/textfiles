+++
linktitle = "diu.v1.0b"
title = "diu.v1.0b"
url = "apple/DOCUMENTATION/diu.v1.0b.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIU.V1.0B textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
