+++
linktitle = "prontodos.txt"
title = "prontodos.txt"
url = "apple/DOCUMENTATION/prontodos.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRONTODOS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
  split = true
+++
