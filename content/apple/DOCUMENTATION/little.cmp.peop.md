+++
linktitle = "little.cmp.peop"
title = "little.cmp.peop"
url = "apple/DOCUMENTATION/little.cmp.peop.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LITTLE.CMP.PEOP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
