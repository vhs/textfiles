+++
linktitle = "gold.rush.sov"
title = "gold.rush.sov"
url = "apple/DOCUMENTATION/gold.rush.sov.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOLD.RUSH.SOV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
