+++
linktitle = "kermit3.87.txt"
title = "kermit3.87.txt"
url = "apple/DOCUMENTATION/kermit3.87.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KERMIT3.87.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
