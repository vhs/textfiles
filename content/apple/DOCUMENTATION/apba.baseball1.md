+++
linktitle = "apba.baseball1"
title = "apba.baseball1"
url = "apple/DOCUMENTATION/apba.baseball1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download APBA.BASEBALL1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
