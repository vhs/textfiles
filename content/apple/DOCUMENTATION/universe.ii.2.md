+++
linktitle = "universe.ii.2"
title = "universe.ii.2"
url = "apple/DOCUMENTATION/universe.ii.2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UNIVERSE.II.2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
