+++
linktitle = "fooblitzky.a.txt"
title = "fooblitzky.a.txt"
url = "apple/DOCUMENTATION/fooblitzky.a.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FOOBLITZKY.A.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
