+++
linktitle = "zippy.zombi"
title = "zippy.zombi"
url = "apple/DOCUMENTATION/zippy.zombi.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZIPPY.ZOMBI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
