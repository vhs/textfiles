+++
linktitle = "dearc.v1.2"
title = "dearc.v1.2"
url = "apple/DOCUMENTATION/dearc.v1.2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEARC.V1.2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
