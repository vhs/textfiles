+++
linktitle = "cest.la.vie.txt"
title = "cest.la.vie.txt"
url = "apple/DOCUMENTATION/cest.la.vie.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CEST.LA.VIE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
