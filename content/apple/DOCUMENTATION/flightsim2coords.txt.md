+++
linktitle = "flightsim2coords.txt"
title = "flightsim2coords.txt"
url = "apple/DOCUMENTATION/flightsim2coords.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FLIGHTSIM2COORDS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
