+++
linktitle = "sammy.lightfoot"
title = "sammy.lightfoot"
url = "apple/DOCUMENTATION/sammy.lightfoot.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SAMMY.LIGHTFOOT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
