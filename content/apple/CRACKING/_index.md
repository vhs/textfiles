+++
title = "Apple II: Cracking"
description = "Files about Deprotecting Software or Adding Cheats"
tabledata = "apple_cracking"
tablefooter = "There are 77 files for a total of 710,386 bytes."
+++

When pirating Apple II games, the challenge was to make it as easy to transfer the files as possible. Since 300 baud could mean a full disk transfer taking over an hour, any reduction in size would be welcomed.

One way to do this would be to take a program disk and turn the disk into a single, smaller file. Of course, most programs (especially games) would have protection schemes inside to check to see if this sort of manipulaiton had gone on. Enter the "Crackers", who would "Crack" the protection schemes so they didn't work anymore.

This battle waged and has continued to wage between Software Companies and Software Pirates.
