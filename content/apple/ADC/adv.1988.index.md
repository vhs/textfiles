+++
linktitle = "adv.1988.index"
title = "adv.1988.index"
url = "apple/ADC/adv.1988.index.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ADV.1988.INDEX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
