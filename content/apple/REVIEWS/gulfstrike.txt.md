+++
linktitle = "gulfstrike.txt"
title = "gulfstrike.txt"
url = "apple/REVIEWS/gulfstrike.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GULFSTRIKE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
