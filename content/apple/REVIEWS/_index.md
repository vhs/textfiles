+++
title = "Apple II: Reviews"
description = "Reviews of Apple II Software"
tabledata = "apple_reviews"
tablefooter = "There are 94 files for a total of 235,226 bytes."
[cascade.ansilove]
  render = true
+++

A collection of reviews of Apple II Software, mostly dated from around the time of the game's release (for some reason, people haven't felt the need to review a piece of software after its first decade or so). Especially interesting are the names of companies that have persisted over time, and the companies that have sunk into oblivion. 
