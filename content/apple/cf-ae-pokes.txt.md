+++
linktitle = "cf-ae-pokes.txt"
title = "cf-ae-pokes.txt"
url = "apple/cf-ae-pokes.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CF-AE-POKES.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
