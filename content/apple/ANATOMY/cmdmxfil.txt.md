+++
linktitle = "cmdmxfil.txt"
title = "cmdmxfil.txt"
url = "apple/ANATOMY/cmdmxfil.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CMDMXFIL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
