+++
linktitle = "rwts.d1.format.txt"
title = "rwts.d1.format.txt"
url = "apple/ANATOMY/rwts.d1.format.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RWTS.D1.FORMAT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
