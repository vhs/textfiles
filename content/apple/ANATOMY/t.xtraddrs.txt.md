+++
linktitle = "t.xtraddrs.txt"
title = "t.xtraddrs.txt"
url = "apple/ANATOMY/t.xtraddrs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download T.XTRADDRS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
