+++
linktitle = "file.manager.s.txt"
title = "file.manager.s.txt"
url = "apple/ANATOMY/file.manager.s.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILE.MANAGER.S.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
