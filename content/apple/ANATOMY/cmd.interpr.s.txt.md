+++
linktitle = "cmd.interpr.s.txt"
title = "cmd.interpr.s.txt"
url = "apple/ANATOMY/cmd.interpr.s.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CMD.INTERPR.S.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
