+++
linktitle = "rwtsd1.format.2.txt"
title = "rwtsd1.format.2.txt"
url = "apple/ANATOMY/rwtsd1.format.2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RWTSD1.FORMAT.2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
