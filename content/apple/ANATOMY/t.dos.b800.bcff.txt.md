+++
linktitle = "t.dos.b800.bcff.txt"
title = "t.dos.b800.bcff.txt"
url = "apple/ANATOMY/t.dos.b800.bcff.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download T.DOS.B800.BCFF.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
