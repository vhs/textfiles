+++
linktitle = "t.dos.a510.a850.txt"
title = "t.dos.a510.a850.txt"
url = "apple/ANATOMY/t.dos.a510.a850.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download T.DOS.A510.A850.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
