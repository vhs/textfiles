+++
linktitle = "doscoldstart.txt"
title = "doscoldstart.txt"
url = "apple/ANATOMY/doscoldstart.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOSCOLDSTART.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
