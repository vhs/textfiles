+++
linktitle = "t.dos.bd00.bfff.txt"
title = "t.dos.bd00.bfff.txt"
url = "apple/ANATOMY/t.dos.bd00.bfff.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download T.DOS.BD00.BFFF.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
