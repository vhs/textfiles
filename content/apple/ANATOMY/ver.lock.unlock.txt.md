+++
linktitle = "ver.lock.unlock.txt"
title = "ver.lock.unlock.txt"
url = "apple/ANATOMY/ver.lock.unlock.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VER.LOCK.UNLOCK.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
