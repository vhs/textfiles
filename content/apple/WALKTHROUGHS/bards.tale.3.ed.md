+++
linktitle = "bards.tale.3.ed"
title = "bards.tale.3.ed"
url = "apple/WALKTHROUGHS/bards.tale.3.ed.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BARDS.TALE.3.ED textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
