+++
linktitle = "birth.phoenix"
title = "birth.phoenix"
url = "apple/WALKTHROUGHS/birth.phoenix.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIRTH.PHOENIX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
