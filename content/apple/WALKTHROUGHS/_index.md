+++
title = "Apple II: Walkthroughs"
description = "Walkthroughs of Apple II Specific Adventures"
tabledata = "apple_walkthroughs"
tablefooter = "There are 101 files for a total of 601,358 bytes."
+++

As adventure games usually required a lot of brainpower to complete, it became a point of pride to release the "Walkthrough" for an adventure game as soon as possible after the actual game's release. These walkthroughs would often lack any ornamentation at all, and just push you through the adventure game as quickly as possible to a solution.

A few of the walkthroughs in this directory are a bit more coy and just give you hints or maps for the games.
