+++
linktitle = "shrinking.man"
title = "shrinking.man"
url = "apple/WALKTHROUGHS/shrinking.man.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHRINKING.MAN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
