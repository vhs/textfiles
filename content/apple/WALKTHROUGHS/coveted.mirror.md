+++
linktitle = "coveted.mirror"
title = "coveted.mirror"
url = "apple/WALKTHROUGHS/coveted.mirror.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COVETED.MIRROR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
