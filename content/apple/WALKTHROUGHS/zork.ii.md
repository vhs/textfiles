+++
linktitle = "zork.ii"
title = "zork.ii"
url = "apple/WALKTHROUGHS/zork.ii.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ZORK.II textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
