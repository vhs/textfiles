+++
linktitle = "claymorgue.cstl"
title = "claymorgue.cstl"
url = "apple/WALKTHROUGHS/claymorgue.cstl.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CLAYMORGUE.CSTL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
