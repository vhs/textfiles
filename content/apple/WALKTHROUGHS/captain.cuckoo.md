+++
linktitle = "captain.cuckoo"
title = "captain.cuckoo"
url = "apple/WALKTHROUGHS/captain.cuckoo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CAPTAIN.CUCKOO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
