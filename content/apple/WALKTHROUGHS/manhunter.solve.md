+++
linktitle = "manhunter.solve"
title = "manhunter.solve"
url = "apple/WALKTHROUGHS/manhunter.solve.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MANHUNTER.SOLVE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
