+++
linktitle = "critical.mass"
title = "critical.mass"
url = "apple/WALKTHROUGHS/critical.mass.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRITICAL.MASS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
