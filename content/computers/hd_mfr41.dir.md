+++
linktitle = "hd_mfr41.dir"
title = "hd_mfr41.dir"
url = "computers/hd_mfr41.dir.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HD_MFR41.DIR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
