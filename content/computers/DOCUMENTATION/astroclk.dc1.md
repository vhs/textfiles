+++
linktitle = "astroclk.dc1"
title = "astroclk.dc1"
url = "computers/DOCUMENTATION/astroclk.dc1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ASTROCLK.DC1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
