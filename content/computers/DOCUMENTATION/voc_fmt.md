+++
linktitle = "voc_fmt"
title = "voc_fmt"
url = "computers/DOCUMENTATION/voc_fmt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VOC_FMT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
