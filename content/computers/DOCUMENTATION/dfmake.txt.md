+++
linktitle = "dfmake.txt"
title = "dfmake.txt"
url = "computers/DOCUMENTATION/dfmake.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DFMAKE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
