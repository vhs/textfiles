+++
linktitle = "ipxctrl1.txt"
title = "ipxctrl1.txt"
url = "computers/DOCUMENTATION/ipxctrl1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IPXCTRL1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
