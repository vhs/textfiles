+++
linktitle = "ser#reg.txt"
title = "ser#reg.txt"
url = "computers/DOCUMENTATION/ser_reg.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SER#REG.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
