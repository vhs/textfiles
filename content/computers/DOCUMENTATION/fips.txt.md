+++
linktitle = "fips.txt"
title = "fips.txt"
url = "computers/DOCUMENTATION/fips.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIPS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
