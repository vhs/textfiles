+++
linktitle = "fuckyou!.txt"
title = "fuckyou!.txt"
url = "computers/DOCUMENTATION/fuckyou_.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUCKYOU!.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
