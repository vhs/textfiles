+++
linktitle = "en-cid12.doc"
title = "en-cid12.doc"
url = "computers/DOCUMENTATION/en-cid12.doc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EN-CID12.DOC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
[ansilove]
  render = true
+++
