+++
linktitle = "r3dspec.txt"
title = "r3dspec.txt"
url = "computers/DOCUMENTATION/r3dspec.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download R3DSPEC.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
