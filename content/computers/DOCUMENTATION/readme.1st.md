+++
linktitle = "readme.1st"
title = "readme.1st"
url = "computers/DOCUMENTATION/readme.1st.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download README.1ST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
