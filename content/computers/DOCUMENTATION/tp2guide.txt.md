+++
linktitle = "tp2guide.txt"
title = "tp2guide.txt"
url = "computers/DOCUMENTATION/tp2guide.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TP2GUIDE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
