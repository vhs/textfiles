+++
linktitle = "sym96inv.txt"
title = "sym96inv.txt"
url = "computers/DOCUMENTATION/sym96inv.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYM96INV.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
