+++
title = "General Computer Textfiles: Press Releases"
description = "Press Releases by Companies Long Forgotten"
tabledata = "computers_pressrelease"
tablefooter = "There are 84 files for a total of 566,900 bytes."
+++

Press releases have been around a long time, but the advantage of this collection is that you can now look back across the years and see which promises these companies made, and which came true. In many cases, the company making claim has disappeared, and their products are long gone. Other companies have risen up past the way they used to be and now offer even more. These sorts of press releases have really taken hold, although thankfully they don't tend to clog BBSes anymore.
