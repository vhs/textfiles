+++
linktitle = "ancoethr.txt"
title = "ancoethr.txt"
url = "computers/PRESSRELEASE/ancoethr.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANCOETHR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
