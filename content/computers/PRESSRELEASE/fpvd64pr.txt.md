+++
linktitle = "fpvd64pr.txt"
title = "fpvd64pr.txt"
url = "computers/PRESSRELEASE/fpvd64pr.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FPVD64PR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
