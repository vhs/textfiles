+++
linktitle = "highways-of-mind"
title = "highways-of-mind"
url = "computers/CYBERSPACE/highways-of-mind.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HIGHWAYS-OF-MIND textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
