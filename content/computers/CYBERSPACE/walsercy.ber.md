+++
linktitle = "walsercy.ber"
title = "walsercy.ber"
url = "computers/CYBERSPACE/walsercy.ber.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WALSERCY.BER textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
