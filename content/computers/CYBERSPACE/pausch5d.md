+++
linktitle = "pausch5d"
title = "pausch5d"
url = "computers/CYBERSPACE/pausch5d.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PAUSCH5D textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
