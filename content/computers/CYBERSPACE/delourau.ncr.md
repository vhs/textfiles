+++
linktitle = "delourau.ncr"
title = "delourau.ncr"
url = "computers/CYBERSPACE/delourau.ncr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DELOURAU.NCR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
