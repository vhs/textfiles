+++
linktitle = "muck_edi.hel"
title = "muck_edi.hel"
url = "computers/muck_edi.hel.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MUCK_EDI.HEL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
