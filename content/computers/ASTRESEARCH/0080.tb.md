+++
linktitle = "0080.tb"
title = "0080.tb"
url = "computers/ASTRESEARCH/0080.tb.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 0080.TB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
