+++
linktitle = "0213a.tb"
title = "0213a.tb"
url = "computers/ASTRESEARCH/0213a.tb.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 0213A.TB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
