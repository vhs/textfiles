+++
linktitle = "the_dark.er_"
title = "the_dark.er_"
url = "computers/the_dark.er_.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THE_DARK.ER_ textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
