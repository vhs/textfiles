+++
linktitle = "xt-640k.upd"
title = "xt-640k.upd"
url = "computers/xt-640k.upd.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download XT-640K.UPD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
