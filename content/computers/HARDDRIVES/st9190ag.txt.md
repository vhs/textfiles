+++
linktitle = "st9190ag.txt"
title = "st9190ag.txt"
url = "computers/HARDDRIVES/st9190ag.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ST9190AG.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
