+++
linktitle = "ftp2uk23.inf"
title = "ftp2uk23.inf"
url = "computers/ftp2uk23.inf.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FTP2UK23.INF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
