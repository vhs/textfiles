+++
title = "General Computer Textfiles"
description = "General Computer-Related Files"
tabledata = "computers"
tablefooter = "There are 428 files for a total of 26,338,463 bytes.<br>There are 5 directories."
+++

Your one-stop shop for all files of a "computery" nature, that cover how to rebuild a piece of computer hardware to do something it was never meant to do, or to see someone go really deeply into a computing subject that would cause most people to yawn and look for the snack table. Since BBSes were on computers, it was natural for people who used BBSes to focus on all aspects of these wonderful machines. The evidence of their interest and their experimentation resides below.

Of course, nearly ALL textfiles are computer-related in some fashion, and some subjects might fit here but fit even better in another section. For example, you should most definitely check out the <a href="/programming">programming</a> and <a href="/apple">apple</a> sections as well, as you might not agree with what I decided should go where.
