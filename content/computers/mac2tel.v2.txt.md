+++
linktitle = "mac2tel.v2.txt"
title = "mac2tel.v2.txt"
url = "computers/mac2tel.v2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MAC2TEL.V2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
