+++
linktitle = "pc1hrdsk.add"
title = "pc1hrdsk.add"
url = "computers/pc1hrdsk.add.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PC1HRDSK.ADD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
