+++
title = "Science Fiction Textfiles"
description = "Science Fiction reviews and lists"
tabledata = "sf"
tablefooter = "There are 158 files for a total of 5,120,252 bytes.<br>There are 2 directories."
+++

It's almost a natural progression that someone who was interested in expressing themselves through computers would show some amount of interest in science fiction. Science fiction has always tried to push the boundaries of what people know and think about, and that's kind of what you saw in BBSes. Usually, anyway.

What lives in this directory is commentary about science fiction, some amateur writing in the genre, overviews of important events in science finction, and lots and lots and lots of Star Trek.
