+++
linktitle = "xf-bugs.txt"
title = "xf-bugs.txt"
url = "sf/XFILES/xf-bugs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download XF-BUGS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
