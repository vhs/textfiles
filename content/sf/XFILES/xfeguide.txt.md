+++
linktitle = "xfeguide.txt"
title = "xfeguide.txt"
url = "sf/XFILES/xfeguide.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download XFEGUIDE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
