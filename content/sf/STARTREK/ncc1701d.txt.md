+++
linktitle = "ncc1701d.txt"
title = "ncc1701d.txt"
url = "sf/STARTREK/ncc1701d.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NCC1701D.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
