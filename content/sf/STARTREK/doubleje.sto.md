+++
linktitle = "doubleje.sto"
title = "doubleje.sto"
url = "sf/STARTREK/doubleje.sto.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOUBLEJE.STO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
