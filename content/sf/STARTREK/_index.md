+++
title = "Science Fiction Textfiles: Star Trek"
description = "Files by, about, and tentatively connected to Star Trek"
tabledata = "sf_startrek"
tablefooter = "There are 345 files for a total of 16,374,700 bytes."
+++

Star Trek. I should mention that Star Trek is copyright for now and forever by Paramount Communications, who own not just Star Trek but moral rights on any Science Fiction with weepy aliens who just want to find their inner humanity they share with the Federation. Because of the pervasiveness of both the original series that ran from 1966-1969, the many movies, and the three spin-off series that have taken over the airwaves for the last couple of decades, you can be sure that many, many people might draw inspiration. Both the BBS world and the later Internet Culture would use Star Trek as a jumping-off point for fiction, speculation, and bad computer jokes.
