+++
linktitle = "w95_trek.txt"
title = "w95_trek.txt"
url = "sf/STARTREK/w95_trek.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download W95_TREK.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
