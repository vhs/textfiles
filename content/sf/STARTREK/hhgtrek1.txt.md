+++
linktitle = "hhgtrek1.txt"
title = "hhgtrek1.txt"
url = "sf/STARTREK/hhgtrek1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HHGTREK1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
