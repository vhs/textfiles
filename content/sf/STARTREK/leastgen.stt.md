+++
linktitle = "leastgen.stt"
title = "leastgen.stt"
url = "sf/STARTREK/leastgen.stt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LEASTGEN.STT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
