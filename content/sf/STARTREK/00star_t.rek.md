+++
linktitle = "00star_t.rek"
title = "00star_t.rek"
url = "sf/STARTREK/00star_t.rek.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 00STAR_T.REK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
