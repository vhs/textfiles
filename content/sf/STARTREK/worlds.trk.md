+++
linktitle = "worlds.trk"
title = "worlds.trk"
url = "sf/STARTREK/worlds.trk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WORLDS.TRK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
