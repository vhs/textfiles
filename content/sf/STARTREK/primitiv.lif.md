+++
linktitle = "primitiv.lif"
title = "primitiv.lif"
url = "sf/STARTREK/primitiv.lif.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRIMITIV.LIF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
