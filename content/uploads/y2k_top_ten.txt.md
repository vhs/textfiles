+++
linktitle = "y2k_top_ten.txt"
title = "y2k_top_ten.txt"
url = "uploads/y2k_top_ten.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download Y2K_TOP_TEN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
