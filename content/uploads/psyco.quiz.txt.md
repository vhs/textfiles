+++
linktitle = "psyco.quiz.txt"
title = "psyco.quiz.txt"
url = "uploads/psyco.quiz.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PSYCO.QUIZ.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
