+++
linktitle = "buyvrsdld.txt"
title = "buyvrsdld.txt"
url = "uploads/buyvrsdld.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BUYVRSDLD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
