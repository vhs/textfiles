+++
linktitle = "inshoplifting.txt"
title = "inshoplifting.txt"
url = "uploads/inshoplifting.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download INSHOPLIFTING.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
