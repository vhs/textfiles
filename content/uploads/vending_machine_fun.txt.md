+++
linktitle = "vending_machine_fun.txt"
title = "vending_machine_fun.txt"
url = "uploads/vending_machine_fun.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VENDING_MACHINE_FUN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
