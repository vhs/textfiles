+++
linktitle = "poem_a_(child).txt"
title = "poem_a_(child).txt"
url = "uploads/poem_a_(child).txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POEM_A_(CHILD).TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
