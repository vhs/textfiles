+++
linktitle = "poem_b_(stalker).txt"
title = "poem_b_(stalker).txt"
url = "uploads/poem_b_(stalker).txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POEM_B_(STALKER).TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
