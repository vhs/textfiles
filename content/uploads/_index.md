+++
title = "Uploads: Generation TXT"
description = "The Textfiles of Today"
tabledata = "uploads"
tablefooter = "There are 560 files for a total of 4,871,961 bytes."
+++

While people who read the textfiles.com site might think that the world of g-files and text-only BBSes is dead and gone, there are actually many people who keep the faith, having information they want to see spread far and wide, and writing textfiles to get the word out. While the subjects are newer and the writing more internet-minded, the themes and forces touched upon in other files appear here as well, showing that the spirit is alive and thriving.

When BBSes were the way to get textfiles, you always shot over to the "Uploads" section to see what people had just added to the system, so you could get it first. That's where the name of this section comes from. In actuality, if you have a file you've just written you want to see up here, send mail to [jason@textfiles.com](jason@textfiles.com).

Note, by the way, that because ALL the new files are being put here, it's a bit of a mish-mash. Just like the old days!

<i>As time is going on, people have donated files with the same bad ideas about drugs, vandalism and the ol' boom-boom; hopefully in a mere homage to files of old. Don't follow instructions you find in random textfiles on the internet; it just leads to tears.</i>
