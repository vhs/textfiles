+++
linktitle = "fuct-001.can"
title = "fuct-001.can"
url = "magazines/FUCT/fuct-001.can.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUCT-001.CAN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
