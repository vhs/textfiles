+++
title = "Electronic Magazines: TANJ (There Ain't No Justice)"
description = "There Ain't No Justice (1988-Present)"
tabledata = "magazines_tanj"
tablefooter = "There are 139 files for a total of 2,546,875 bytes."
+++

Created in 1988, this collection of files starts out angry and informative, and moves quickly to fictional and philosophical. Over 10 years later, it keeps coming out, and even has a [home page](http://members.bellatlantic.net/~talmeta/tanjv1.html).

As the author himself explains:

<blockquote><pre>
Born from my desire not to endlessly retell the story of my arrest and subsequent prosecution at the hands of US Sprint, it rapidly
went from my attempts to make light of the incident to more serious fiction/humor. I have no schedule, I release files when I feel like
it; anywhere from days to years can go between files if nobody sends me anything worth publishing. 
</pre></blockquote>
