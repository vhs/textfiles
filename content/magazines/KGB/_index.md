+++
title = "Electronic Magazines: K-Rad GNUz and Bullshit"
description = "K-Rad GNUz and Bullshit (1994)"
tabledata = "magazines_kgb"
tablefooter = "There are 5 files for a total of 182,791 bytes."
+++

This "We're Elite" e-zine comes off as intending to be a parody of the world of IRC and the K-RAD Denziens within, but then starts to shift in the later issues to actual essays about the state of IRC. Of course, this move doesn't exactly come off as smooth as it could, and to be honest, the author completely buys into the IRC world enough that it seems they're splitting hairs, but the whole run stays cohesive throughout. Enjoyable.
