+++
linktitle = "harold.herald.3.5"
title = "harold.herald.3.5"
url = "magazines/HAROLDHERALD/harold.herald.3.5.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAROLD.HERALD.3.5 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
