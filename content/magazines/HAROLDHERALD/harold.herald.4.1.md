+++
linktitle = "harold.herald.4.1"
title = "harold.herald.4.1"
url = "magazines/HAROLDHERALD/harold.herald.4.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAROLD.HERALD.4.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
