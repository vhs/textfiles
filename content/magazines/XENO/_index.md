+++
title = "Electronic Magazines: Xenocide/Electronic Deicide"
description = "Xenocide / Electronic Deicide (1992-1993)"
tabledata = "magazines_xeno"
tablefooter = "There are 5 files for a total of 258,539 bytes."
+++

<blockquote>
<pre>
Electronic Deicide was a Death/Thrash/Extreme Metal Zine dedicated to reviewing
this genre of music, and alerting people to new releases by these bands. Some
of the names of the bands are beyond belief. 
</pre>
</blockquote>

As an aside, I really really wanted to find out the exact date that this Zine came out, as I'm kind of insistent that way. After a short time, I found that the editor, [Jon Konrath](http://www.rumored.com/jkonrath), has his own web page and has gone on to do other similar work, as well as smashing a car and moving from Seattle to New York to be with his girlfriend. You can click on his name to read his page, or read his old work below.
