+++
title = "Politics Online Magazine (1994-1995)"
description = "Politics Online Magazine (1994-1995)"
tabledata = "magazines_pom"
tablefooter = "There are 4 files for a total of 91,528 bytes."
+++

Josh Renaud writes <q>"&gt;Just found a series of four issues of a short-lived mag I did when I was a kid (1994-95). It was called "Politics Online Magazine." I hope you'll add them to textfiles.com. It's funny to read these now. My articles come off pretty shallow and ill-supported compared to the other articles we ran. It's too bad the mag didn't last longer. But since we started it when BBSes were nearing their death throes, it was pretty much doomed to fail from the beginning.</q>

<q>"The first two or three issues were originally distributed as ZIPs that included an ascii version of the magazine, as well as a custom ansi version that could be read only by using the enclosed viewer program (for pc/dos). I don't have these original ZIPs, but I do have the ascii versions of all the issues."</q>
