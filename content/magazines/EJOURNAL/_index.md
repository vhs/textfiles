+++
title = "Electronic Magazines: The EJOURNAL"
description = "EJournal (1991-1996)"
tabledata = "magazines_ejournal"
tablefooter = "There are 24 files for a total of 922,921 bytes."
+++

The University of Albany's magazine of the effect and implications of Electronic Journals is a refreshing academic read about the issues of ownership, personal expression, and similar concerns about expression. There seems to be a rather small press run here (or at least a small number of issues per year), but they're all worth going through.
