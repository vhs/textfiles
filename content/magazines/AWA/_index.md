+++
title = "Electronic Magazines: The Anti-Warez Association"
description = "Anti-Warez Association (1993)"
tabledata = "magazines_awa"
tablefooter = "There are 6 files for a total of 194,938 bytes."
+++

A very amusing collection of issues with parodies and stories of some of the more common aspects of the pirating/leeching cultures, with some really interesting mid-80's references coupled with internet-era events. Good stuff if you lived through these times, not all that good if you've never heard the terms 'Zero Day' or 'Leech'.
