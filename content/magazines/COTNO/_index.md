+++
title = "Electronic Magazines: Communications of the New Order"
description = "Communications of the New Order (1993-1995)"
tabledata = "magazines_cotno"
tablefooter = "There are 7 files for a total of 748,839 bytes."
+++

Solid Hack/Phreak magazine from the mid 1990's. Contains convention reviews, phone-related files, and information on various busts that were occuring at the time, including Operation Sun Devil.
