+++
linktitle = "watcher.v08"
title = "watcher.v08"
url = "magazines/MISC/watcher.v08.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WATCHER.V08 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
