+++
linktitle = "watcher.v09"
title = "watcher.v09"
url = "magazines/MISC/watcher.v09.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WATCHER.V09 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
