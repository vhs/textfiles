+++
linktitle = "newmag.010"
title = "newmag.010"
url = "magazines/MISC/newmag.010.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEWMAG.010 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
