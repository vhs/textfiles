+++
linktitle = "takeoff!.002"
title = "takeoff!.002"
url = "magazines/MISC/takeoff_.002.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAKEOFF!.002 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
