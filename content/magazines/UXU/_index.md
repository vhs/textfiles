+++
title = "Electronic Magazines: UXU (Underground eXperts United) (1991-2002)"
description = "Underground eXperts United (1991-Present)"
tabledata = "magazines_uxu"
tablefooter = "There are 620 files for a total of 7,076,302 bytes."
+++

Founded in 1991, based in Sweden, uXu was a good example of a textfile endeavor where you can actually track the progression of a small social group through nearly a decade. The files begin with descriptions of the computer "scene" of Sweden, bombs, technology and eventually expands over the years to journal entries, musings about philosphy, song lyrics, interviews, and a bunch of nice fellows just letting you know what they're up to. All in all, good solid quality and a fascinating study.

UXU put out its last issue in 2002.
