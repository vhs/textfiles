+++
title = "Electronic Magazines: N.A.S.T.Y."
description = "National Alliance of Superior Technological Youth (1991)"
tabledata = "magazines_nasty"
tablefooter = "There are 3 files for a total of 350,734 bytes."
+++

The National Alliance of Superior Technological Youth was a Net-Savvy hackers magazine that claims it was trying to spread knowledge for future hackers to have. They focused on credit reports, internet IP addresses, and giving other groups the smackdown.
