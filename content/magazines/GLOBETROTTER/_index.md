+++
title = "Electronic Magazines: Globe Trotter"
description = "Globe Trotter (1988)"
tabledata = "magazines_globetrotter"
tablefooter = "There are 6 files for a total of 400,034 bytes."
+++

Hailing from Australia, this computer hacking-centric magazine produced a few very good issues with a concentration of Australian and New Zealand information. The writing is clear and the attempt at regular columns was well-thought out. It must have been a very tiring year for "THE FORCE", and the effort shows.
