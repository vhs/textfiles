+++
title = "Electronic Magazines: Information, Communication, Supply"
description = "Information, Communication, Supply (1993-1995)"
tabledata = "magazines_ics"
tablefooter = "There are 20 files for a total of 1,371,550 bytes."
+++

ICS is a "An Electronic Magazine from Western State College" that contains scads of well-thought out fiction, poetry, and most importantly, essays on human communication through an inhuman medium. All of the issues are quite readable, and the staff (which is huge) was able to split the duties between them and come out with a solid body of work.
