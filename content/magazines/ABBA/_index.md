+++
title = "Electronic Magazines: ABBA (The Anti Big Brother Association)"
description = "The Anti-Big Brother Association (1993-1994)"
tabledata = "magazines_abba"
tablefooter = "There are 8 files for a total of 214,102 bytes."
+++

This charming group of fellows claim that they saw magazines on phreaking/hacking, explosives, and drugs, but didn't see a magazine that combined all three. How charming! They seem to have produced 4 issues before running out of steam, or maybe they got stoned and blew up a phone. You never know.

It would appear that they changed their name a while later to "People's Freedom Association" and proceeded to ruin any good spelling or writing they had in their first few issues. A shame, really. I'd almost consider them two separate entities, but not quite.
