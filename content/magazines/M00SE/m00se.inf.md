+++
linktitle = "m00se.inf"
title = "m00se.inf"
url = "magazines/M00SE/m00se.inf.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download M00SE.INF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
