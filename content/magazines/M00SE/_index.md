+++
title = "Electronic Magazines: M00SE Droppings"
description = "M00se Dr0pings Magazine (1988-1990)"
tabledata = "magazines_m00se"
tablefooter = "There are 57 files for a total of 1,274,430 bytes."
+++

Maybe it's just me, but as I browse through these files, they seem to be about Moose, more Moose and then about things that people who know each other are interested in becasue they all happen to like moose. After a while, this leads (naturally?) to the Steve Jackson BBS Raid. There's an awful lot of moose here.
