+++
title = "Electronic Magazines: Freedom"
description = "Freedom Magazine (1992-1993)"
tabledata = "magazines_freedom"
tablefooter = "There are 2 files for a total of 125,312 bytes."
+++

Iceman's PhrackAlike takes on phreaking, anarchy, and even a few politics across a couple issues.
