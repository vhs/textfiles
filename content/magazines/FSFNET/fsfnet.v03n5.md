+++
linktitle = "fsfnet.v03n5"
title = "fsfnet.v03n5"
url = "magazines/FSFNET/fsfnet.v03n5.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FSFNET.V03N5 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
