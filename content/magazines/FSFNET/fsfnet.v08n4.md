+++
linktitle = "fsfnet.v08n4"
title = "fsfnet.v08n4"
url = "magazines/FSFNET/fsfnet.v08n4.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FSFNET.V08N4 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
