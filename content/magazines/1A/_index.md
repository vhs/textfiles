+++
title = "Electronic Magazines: Phirst Amendment"
description = "Phirst Amendment (1993)"
tabledata = "magazines_1a"
tablefooter = "There are 6 files for a total of 522,094 bytes."
+++

Phirst Amendment is a collection of editorials and files about whatever was really annoying the editors that day. Subjects include viruses, poetry, goverment weirdness, and dumpster diving. They care enough about (or prefer to hide to behind) the First Amendment enough to put it at the beginning and end of their issues.

I am unable to find any evidence of this magazine beyond these issues; they come as a result of the Electronic Frontier Foundation's Computer Underground Archives, and I am not convinced that any other issues exist on the internet.
