+++
linktitle = "tap.root-1.2"
title = "tap.root-1.2"
url = "magazines/TAPROOT/tap.root-1.2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAP.ROOT-1.2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
