+++
linktitle = "tap.root-2.0a"
title = "tap.root-2.0a"
url = "magazines/TAPROOT/tap.root-2.0a.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAP.ROOT-2.0A textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
