+++
linktitle = "taproot-5a"
title = "taproot-5a"
url = "magazines/TAPROOT/taproot-5a.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAPROOT-5A textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
