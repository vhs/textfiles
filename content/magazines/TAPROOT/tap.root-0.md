+++
linktitle = "tap.root-0"
title = "tap.root-0"
url = "magazines/TAPROOT/tap.root-0.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAP.ROOT-0 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
