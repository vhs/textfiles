+++
title = "Electronic Magazines: Xenon Foundation X-Files"
description = "Xenon Foundation / The X Files (1993-1994)"
tabledata = "magazines_xenon"
tablefooter = "There are 16 files for a total of 734,914 bytes."
+++

<blockquote>
<pre>This isn't exactly the most heart-stirring collection of files, in terms
of making much sense chronologically. While each file calls itself an "issue",
and mentions being one of a series of files, they seem to have been put together
with some weird numbering scheme. Nearly all the files are written by Erik
Turbo, with a few friends.
</pre>
</blockquote>
