+++
title = "Electronic Magazines: Intertext"
description = "InterText (1991-1993)"
tabledata = "magazines_intertext"
tablefooter = "There are 67 files for a total of 7,870,632 bytes."
+++

Intertext is a competent compilation of different works of fiction and poetry running up into the hundreds of K an issue, due to the perseverance of Jason Snell, who acts as Master of Ceremonies as a stream of Novellas make themselves known, with varying results.

Professionally handled, smooth to read.
