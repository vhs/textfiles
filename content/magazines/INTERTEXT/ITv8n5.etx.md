+++
linktitle = "ITv8n5.etx"
title = "ITv8n5.etx"
url = "magazines/INTERTEXT/ITv8n5.etx.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ITV8N5.ETX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
