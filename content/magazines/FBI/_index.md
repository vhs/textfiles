+++
title = "Electronic Magazines: Freaker's Bureau Incorporated"
description = "Freakers Bureau Incorporated (1991)"
tabledata = "magazines_fbi"
tablefooter = "There are 6 files for a total of 606,202 bytes."
+++

Part of that strange subset of hacker magazines that insisted on having the same initials as other, more common acronyms, Freaker's Bureau Incorporated offered lots of lists of the burgeoning Internet underground, and peppered lots of quaint text graphics.
