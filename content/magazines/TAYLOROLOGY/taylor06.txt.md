+++
linktitle = "taylor06.txt"
title = "taylor06.txt"
url = "magazines/TAYLOROLOGY/taylor06.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAYLOR06.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
