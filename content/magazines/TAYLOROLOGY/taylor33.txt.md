+++
linktitle = "taylor33.txt"
title = "taylor33.txt"
url = "magazines/TAYLOROLOGY/taylor33.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAYLOR33.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
