+++
linktitle = "taylor88.txt"
title = "taylor88.txt"
url = "magazines/TAYLOROLOGY/taylor88.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAYLOR88.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
