+++
linktitle = "taylor48.txt"
title = "taylor48.txt"
url = "magazines/TAYLOROLOGY/taylor48.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAYLOR48.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
