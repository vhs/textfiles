+++
linktitle = "taylor15.txt"
title = "taylor15.txt"
url = "magazines/TAYLOROLOGY/taylor15.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAYLOR15.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
