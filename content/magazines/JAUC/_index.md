+++
title = "Electronic Magazines: The Journal of American Underground Computing"
description = "The Journal of American Underground Computing (1994-1995)"
tabledata = "magazines_jauc"
tablefooter = "There are 8 files for a total of 1,368,696 bytes."
+++

Well, it comes out a bit more than quarterly, and it's not so much Underground as a compilation of generally available texts, but otherwise, it's a pretty nice job. Written in the Wired Magazine style, that is, very intelligent, very thorough, but somehow lacking in real meat, this Journal covers all sorts of aspects of the world of Computers, which might theoretically be of interest to those in the "Underground", whatever that is. All in all, solid but not awe-inspiring. Every single issue tries to sell you Legion of Doom T-Shirts.
