+++
title = "Electronic Magazines: The Birmingham Telecommunications News"
description = "Birmingham Telecommunications News (1992-1996?)"
tabledata = "magazines_btn"
tablefooter = "There are 73 files for a total of 4,681,578 bytes."
+++

A large thank you to Mark Maisel, who found the majority of these issues in a long-lost hard drive and has made them available once again.
