+++
linktitle = "jeff_smart.txt"
title = "jeff_smart.txt"
url = "magazines/ILLEGAL/jeff_smart.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEFF_SMART.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
