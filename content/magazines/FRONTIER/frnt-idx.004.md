+++
linktitle = "frnt-idx.004"
title = "frnt-idx.004"
url = "magazines/FRONTIER/frnt-idx.004.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FRNT-IDX.004 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
