+++
linktitle = "frntr-05.txt"
title = "frntr-05.txt"
url = "magazines/FRONTIER/frntr-05.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FRNTR-05.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
