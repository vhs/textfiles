+++
linktitle = "gwdnew20.txt"
title = "gwdnew20.txt"
url = "magazines/GWD/gwdnew20.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GWDNEW20.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
