+++
title = "Greeny World Domination"
description = "Greeny World Domination (1993-Present)"
tabledata = "magazines_gwd"
tablefooter = "There are 181 files for a total of 2,760,625 bytes."
+++

Greeny World Domination answers the question of what they are in their own FAQ:

<blockquote>
<pre>
Q:  What is this whole GwD thing, anyway?
A:  GwD is a paramilitary fanatical political activist group dedicated to 
    making GREENY, son of the goddess Grene, Emperor of the world. 
   
Q:  So are you political or religious?
A:  We're pseudo-political AND quasi-religious.  After all, politics and
    religion are the same thing.  Pray to the President and vote for God.

Q:  What do you do?
A:  We publish an e-zine and we raise hell.  We used to write stupid programs,
    but we quit.  Basically, we plan for world Domination and we have fun doing
    it.

Q:  Are you better than me?
A:  Yes, unless you're in GwD.
</pre>
</blockquote>

Having said that, the files range on all sorts of subjects, and have that common trait among long-lived groups: They start to take most anything after the first few years. Worth a dive.
