+++
linktitle = "gwdnew21.txt"
title = "gwdnew21.txt"
url = "magazines/GWD/gwdnew21.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GWDNEW21.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
