+++
linktitle = "gwdnew13.txt"
title = "gwdnew13.txt"
url = "magazines/GWD/gwdnew13.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GWDNEW13.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
