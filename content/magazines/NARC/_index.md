+++
title = "Electronic Magazines: Nuclear Phreakers Hackers Carders"
description = "Nuclear Phreakers Hackers Carders (1989)"
tabledata = "magazines_narc"
tablefooter = "There are 10 files for a total of 55,876 bytes."
+++

How do you get NARC out of "Nuclear Phreakers Hackers Carders"? You bend the rules. And this gang, while they do spend a little time on phreaking and hacking, is definitely out to Card. They produce a large spate of issues in 1989, then pull out a few more in 1990 before disappearing entirely. Small, but specialized.
