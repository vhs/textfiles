+++
title = "Electronic Magazines: Anarchy N' Explosives"
description = "Anarchy N' Explosives (1989)"
tabledata = "magazines_ane"
tablefooter = "There are 12 files for a total of 696,446 bytes."
+++

<blockquote>
<pre>
Anarchy N' Explosives seems to mainly be a collection of explosive information
files created by one Doctor Dissector throughout the year of 1989. The first
file (The "Prelude") inconsistently contains lots of hacking information.
</pre>
</blockquote>
