+++
title = "Electronic Magazines: The Church of Virtuality Reality"
description = "The Church of Virtuality Reality (1992)"
tabledata = "magazines_churchofvr"
tablefooter = "There are 8 files for a total of 634,889 bytes."
+++

What a complete mess. This set of "scriptures" claims to be an important aspect of an online game, but in fact appears to be a completely disorganized mess of text only classifyable by the filenames. Very meaningful to the authors, but not all that interesting outside of them.
