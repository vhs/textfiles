+++
title = "Electronic Magazines: KULTCHA"
description = "The Issue of Kultcha and the Culture of Issue (1986)"
tabledata = "magazines_kultcha"
tablefooter = "There are 30 files for a total of 87,435 bytes."
+++

Kerry Wendell Thornley's KULTCHA magazine is a series of not-so-well-thought-out essays covering a series of subjects, supposedly centered around the idea of culture. In fact, it appears to be either rants about the RSVP Party (a group of Thornley's own choosing) or weird conspiracy theories about things that are not that high enough on the radar to refute.
