+++
title = "Electronic Magazines: The Holy Temple of Mass Consumption"
description = "The Holy Temple of Mass Consumption (1993)"
tabledata = "magazines_htomc"
tablefooter = "There are 33 files for a total of 1,233,060 bytes."
+++

The Holy Temple of Mass Consumption, a Church of the Sub-Genius newsletter, actually calls itself the "Church News" in several issues. It spews out a good twenty issues of Slack, Vibe, and "Bob" before collapsing in some sort of heap by the end of their first year. Effort-free.
