+++
title = "Electronic Magazines: Phreaking, Hacking, and Terrorism Enterprises"
description = "Phreaking Hacking and Terrorism Enterprises (1992-1993)"
tabledata = "magazines_phate"
tablefooter = "There are 3 files for a total of 271,651 bytes."
+++

Certainly designed with a name to make alarm bells go off, PHATE concentrates entirely on Phreaking and Hacking and doesn't really terrorize at all, unless you consider taking Acid to be some form of terror. The whole magazine has a strong UK slant and the issues are large, due in part to the large screen captures of some of the hacking information.
