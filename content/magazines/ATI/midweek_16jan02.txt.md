+++
linktitle = "midweek_16jan02.txt"
title = "midweek_16jan02.txt"
url = "magazines/ATI/midweek_16jan02.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIDWEEK_16JAN02.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
