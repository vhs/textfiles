+++
linktitle = "midweek_3feb01.txt"
title = "midweek_3feb01.txt"
url = "magazines/ATI/midweek_3feb01.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIDWEEK_3FEB01.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
