+++
linktitle = "midweek_4sep_02.txt"
title = "midweek_4sep_02.txt"
url = "magazines/ATI/midweek_4sep_02.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIDWEEK_4SEP_02.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
