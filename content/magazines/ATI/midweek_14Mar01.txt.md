+++
linktitle = "midweek_14Mar01.txt"
title = "midweek_14Mar01.txt"
url = "magazines/ATI/midweek_14Mar01.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MIDWEEK_14MAR01.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
