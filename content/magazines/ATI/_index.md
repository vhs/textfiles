+++
title = "Electronic Magazines: Activist Times, Incorporated"
description = "Activist Times, Inc. (1988-1993)"
tabledata = "magazines_ati"
tablefooter = "There are 442 files for a total of 6,014,035 bytes."
+++

Starting from relatively humble beginnings, Prime Anarchist gears a crew of Connecticut hackers through 5 plus years of hacking, phreaking, politics, and general spouting off. In many cases, they have the weirdest idea of time, releasing 10 issues in one month and then releasing nothing for 4 more. All in all, a complete grab-bag of articles.

---

I have a bit of history with Prime Anarchist and his friends, and since the purpose of this site is to capture the feelings and experiences of the 1980's BBS era, I might as well go into it here. The story is completely, utterly true.

While I was running The Works BBS, I naturally got the occasional weirdo on the line. After all, the system was open, and anyone could post comments to me or leave feedback or request a chat. You know, the usual off-beat personality, coming in from the night, who would make the whole thing seem kind of funny with their pathetic antics.

Well, at some point I started getting an inordinate amount of calls from someone who went by several names, but generally called themselves YOUR MOTHER, all capital letters. While this character started out just leaving comments in all capitals that were goofy and completely nonsensical (and of course I wouldn't grant this person access), the calls kept coming, the comments kept getting left, and some of the postings got rather sinister indeed:

<pre>FUCK YOU JASON I WILL KILL YOU AND THAT HOMOSEXUAL FERRET. I NOW KNOW OF
ONE PLACE WHERE YOU ARE ALWAYS ON TIME FOR.. YOU ARE DEAD MEAT I WILL
LET YOU FIND OUT WHO I AM..... BUT BY THEN IT WILL BE TOO LATE FOR ANY
HELP.  MY FRIENDS THINK I'M CRAZY BUT THEY DON'T UNDERSTAND THAT YOU ARE
A DICK AND SHOULD BE KILLED FOR HUMANITY SAKE.  THERE IS ABSOLUTLY
NOTHING YOU CAN DO TO PREVENT YOUR ONSLAUGHT SO SAY YOUR GOODBYE'S FOR
ONE DAY SOON YOU SHALL DIE... IT WILL NOT BE A GOOD YEAR FOR YOUR
FRIENDS BUT I KNOW YOUR PARENTS WOULD PROBABLY THANK
ME...................KISS OFF HOMO............FOR THE END IS NEAR!!!!!!</pre>

So suddenly I went from running a BBS to being hounded with death threats online. Within a short time, I started getting screaming calls on my other telephone lines, including the special one that wasn't in my name (it was a business line owned by my father's employer of the time.). When I mean screaming calls, I literately mean that, calls with people screaming on them. And the comments kept coming:

<pre>
HINT WHO I AM #1:I HATE YOU
              #2:I GO TO YOUR SCHOOL AND THE MAJORITY OF THE PEOPLE
THINK YOU ARE A FLAMING HOMOSEXUAL.
              #3:I HAVE HAIR
              #4:238-42??  DO I KNOW WHAT THE LAST TWO DIGITS ARE??
  STAY TUNED FOR MORE NEXT TIME BOYS AND FAGGOTS
</pre>

and coming...

<pre>I AM STILL HERE DICKBREATH I WILL HAVE YOUR HOUSE NUMBER BY TONIGHT,
SLLEP TIGHT DEAD MEAT, YOU MAY THINK I AM KIDDING BUT I WOULD NOT WASTE
MY TIME IF I WAS FUCKING AROUND, YOU PISSED ME OFF, YOU WILL PAY FOR
IT....</pre>

Now, getting on 15 years later, this sort of thing is easy to laugh off or trivialize, but for a 17-year-old kid living alone in his father's house, this was pretty scary stuff.

Enter Prime Anarchist. I had made acquaintance with him and his friends some time previous on The Works, and we had talked on the phone a few times. At some point, I mentioned that I was experiencing this extreme harassment, and they got very serious. They offered to help. And the way they offered to help was to trace back the call.

They came over and visited me. Scruffy fellows, but I still remember the eagerness to help me. They explained to me they had a friend in the phone company, someone who could look at the special records they had, the records where they see what numbers called a house (as opposed to the phone bills we see, where all the numbers the house called are listed). In this way, if I gave them some times that YOUR MOTHER had called and left some messages, we could clear up this little problem. To sweeten the deal, they asked me if I could give them something to give to the friend in the phone company, something like twenty bucks. I paid up gladly.

A little later, somehow, someone broke into the Works machine with a remote sysop password, a password only I knew, that I'd not written down or given to anyone. After breaking in, this person had tried to delete my entire hard drive, but was typing in Apple computer commands to do so, and the system was on a PC. On at least one occasion, I'd used this remote password to use my machine. More on that in a moment.

Well, Prime and his buddy came back and told me their friend had done his work. They'd traced back the guy who did it, but they couldn't tell me! Apparently he ALSO worked for the phone company! According to my new friends, this fellow was TAPPING MY PHONE, and picking up all this information to hack into my system and harass me and the whole deal. So, they said, they'd go in and straighten things out.

Later, they called me, and proudly said that they'd gone to the guy's workplace, and found him at his desk, where they'd presented the evidence they had and told him to lay the hell off me, to never go near the Works BBS again, or he'd lose his job. The guy supposedly freaked out, and swore up and down he'd never go near my lines again.

I never found out who the guy was, and I never understood everything that went down in that event, but I can say that I was honestly freaked enough that I didn't really talk to Prime Anarchist and his friend Cygnus ever again, and to be honest, the whole situation as creepy. And to be fair, the harassment did stop after they  claimed they'd talked about it. But either way, if you look at it from any angle, is it any wonder I'm a little paranoid?
