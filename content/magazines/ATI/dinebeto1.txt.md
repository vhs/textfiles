+++
linktitle = "dinebeto1.txt"
title = "dinebeto1.txt"
url = "magazines/ATI/dinebeto1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DINEBETO1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
