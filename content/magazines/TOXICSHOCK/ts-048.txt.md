+++
linktitle = "ts-048.txt"
title = "ts-048.txt"
url = "magazines/TOXICSHOCK/ts-048.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TS-048.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
