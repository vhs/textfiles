+++
linktitle = "gozerhistory.txt"
title = "gozerhistory.txt"
url = "magazines/ONESHOTS/gozerhistory.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOZERHISTORY.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
