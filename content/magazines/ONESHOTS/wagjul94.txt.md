+++
linktitle = "wagjul94.txt"
title = "wagjul94.txt"
url = "magazines/ONESHOTS/wagjul94.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WAGJUL94.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
