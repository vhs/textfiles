+++
title = "Electronic Magazines: Single Issues"
description = "Zines That Only Produced One Issue"
tabledata = "magazines_oneshots"
tablefooter = "There are 128 files for a total of 4,932,172 bytes."
+++

It seems somewhat ludicrous to create separate directories and histories for each and every single-issue zine that was produced; in many cases, the single zine issue almost recedes back to textfile status, except for the issue number and introduction promising many issues to come. Faced with the prospect of month after month of scrounging up articles, many would-be editors simply produced one large file and then gave up or never got back to working on the next issue. This directory contains all the single-shot issues I could find.

There is a possibility that some of these zines <i>have</i> had other issues created that I have not acquired or known about. In the event that further issues are found, I will move these zines into the main directory.

Where I can, I've tried to put a small review in anyway.
