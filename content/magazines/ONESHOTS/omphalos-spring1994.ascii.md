+++
linktitle = "omphalos-spring1994.ascii"
title = "omphalos-spring1994.ascii"
url = "magazines/ONESHOTS/omphalos-spring1994.ascii.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OMPHALOS-SPRING1994.ASCII textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
