+++
linktitle = "cog-0.0"
title = "cog-0.0"
url = "magazines/ONESHOTS/cog-0.0.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COG-0.0 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
