+++
linktitle = "issue1.dta"
title = "issue1.dta"
url = "magazines/ONESHOTS/issue1.dta.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ISSUE1.DTA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
