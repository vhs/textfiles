+++
linktitle = "i-diseases-03.txt"
title = "i-diseases-03.txt"
url = "magazines/ONESHOTS/i-diseases-03.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download I-DISEASES-03.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
