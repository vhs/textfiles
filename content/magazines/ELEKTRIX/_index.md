+++
title = "Electronic Magazines: Elektrix"
description = "Elektrix Magazine (1990)"
tabledata = "magazines_elektrix"
tablefooter = "There is 1 file for a total of 86,416 bytes."
+++

The only issue of Elektrix that seems to have come out announces that the next issue was coming "in July". It's a shame it didn't; this UK-based magazine provided clear writing, good diagrams, a plethora of subjects, and seems to have been well researched.
