+++
title = "Electronic Magazines: Hackers Unlimited"
description = "Hackers Unlimited (1989)"
tabledata = "magazines_hu"
tablefooter = "There are 3 files for a total of 236,698 bytes."
+++

Dark Lord and Cardiac Arrest of the Mickey Mouse Club do their best to oversee a multi-faceted hacking/phreaking magazine loaded with information and schematics. They produce two very good issues.
