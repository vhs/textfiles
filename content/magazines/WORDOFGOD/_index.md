+++
title = "Electronic Magazines: The Word of God"
description = "The Word of God (1989)"
tabledata = "magazines_wordofgod"
tablefooter = "There are 6 files for a total of 62,718 bytes."
+++

"We actually made it to a second newsletter!" proclaims issue #2 of The Word of God. They go on for 4 more issues before giving up. Notably, Sam Hain tries his best to keep things going, although it's obvious that by the end of the series most of his energy is gone. Each of these issues is a small collection of passwords, hacking information, and a strange religious theme.
