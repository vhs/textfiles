+++
linktitle = "tshrew6.txt"
title = "tshrew6.txt"
url = "magazines/TAMERSHREW/tshrew6.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TSHREW6.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
