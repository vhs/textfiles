+++
title = "Electronic Magazines: HAKD"
description = "H0'z ASCII/Anarchy Komputers and Drugz (1994)"
tabledata = "magazines_hakd"
tablefooter = "There are 22 files for a total of 97,796 bytes."
+++

The title stands for "Ho'z ASCII/Anarchy Komputerz and Drugz". This is actually a series of small textfiles, each aimed around one specific subject, with the large header of a magazine at top.

A excellent textbook example of extremely poor writing and goals, this set of files goes from subject to subject almost randomly, dispensing useless information in an intentionally badly-spelled, dismissive, meandering fashion. While I hesitate to play the vintage game, these files date from 1994, about the time that it started really becoming the fashion to use major amounts of oddly spelled words (h4(k for HACK, etc.) with no sense of irony or meaning. Truly a pointless mess.
