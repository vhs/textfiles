+++
title = "Electronic Magazines: ZIG ZAG Magazine"
description = "ZigZag Magazine (1993-1994)"
tabledata = "magazines_zigzag"
tablefooter = "There are 5 files for a total of 116,002 bytes."
+++

[Steven Grossman](http://home.att.net/~sdgross/)'s magazine of "Marxist Dialectical Revolution" and other Political Issues of the day goes way above my head, but has a lot of fun up there anyway.
