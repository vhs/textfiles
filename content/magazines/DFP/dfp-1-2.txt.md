+++
linktitle = "dfp-1-2.txt"
title = "dfp-1-2.txt"
url = "magazines/DFP/dfp-1-2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DFP-1-2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
