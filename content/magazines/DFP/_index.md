+++
title = "Electronic Magazines: Digital Free Press"
description = "Digital Free Press (1992)"
tabledata = "magazines_dfp"
tablefooter = "There are 10 files for a total of 435,633 bytes."
+++

From the Statement of Purpose of Digital Free Press:
<pre>
    The Digital Free Press is an uncensored forum to document current
activities in and of the world of modern technology. It is published under the
premise that it is better to know, rather than not know, so no attempt is made
to hide any information no matter how dangerous it may be. Information is a
double edged sword. It is neither good nor bad, and can be used for either
good or bad. Warning: Some information in this document could be used for
illegal activities. Use at your own risk. Articles are the opinion of the
authors listed, and not of the editor (unless of course the editor wrote
it).
</pre>
