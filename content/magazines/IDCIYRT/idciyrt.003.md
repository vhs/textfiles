+++
linktitle = "idciyrt.003"
title = "idciyrt.003"
url = "magazines/IDCIYRT/idciyrt.003.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IDCIYRT.003 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
