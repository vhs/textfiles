+++
linktitle = "idciyrt.004"
title = "idciyrt.004"
url = "magazines/IDCIYRT/idciyrt.004.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IDCIYRT.004 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
