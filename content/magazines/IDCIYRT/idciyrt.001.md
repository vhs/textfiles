+++
linktitle = "idciyrt.001"
title = "idciyrt.001"
url = "magazines/IDCIYRT/idciyrt.001.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IDCIYRT.001 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
