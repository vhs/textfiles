+++
linktitle = "idciyrt.005"
title = "idciyrt.005"
url = "magazines/IDCIYRT/idciyrt.005.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IDCIYRT.005 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
