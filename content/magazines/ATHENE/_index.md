+++

title = "Electronic Magazines: ATHENE"
description = "The ATHENE Amateur Creative Writing Magazine (1989-1990)"
tabledata = "magazines_athene"
tablefooter = "There are 7 files for a total of 710,307 bytes."
+++

<blockquote>
<pre>
ATHENE is the Online Magazine of Amateur Creative Writing. 
It's loaded with many stories, not really falling along any particular lines.
</pre>
</blockquote>
