+++
title = "Electronic Magazines"
description = "Phuckin' Phield Phreakers (aka Outdoor Life) (1989)"
tabledata = "magazines_ppp"
tablefooter = "There are 2 files for a total of 29,526 bytes."
pagefooter = "<b>Note on this directory:</b> I am <i>very</i> aware there are a lot of doubled files, and files desperately needing some editing. When I have personally verified which of two files is the more complete, I will make it the \"canonical\" version. Currently, I am just trying to compile a rough, \"version 1.0\" version of my textfile collection, with as little lost data as possible; this will be refined in the near future. Volunteers are always welcome."
+++
