+++
linktitle = "oracle.09"
title = "oracle.09"
url = "magazines/OCEANIAORACLE/oracle.09.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ORACLE.09 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
