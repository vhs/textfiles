+++
linktitle = "oracle.07"
title = "oracle.07"
url = "magazines/OCEANIAORACLE/oracle.07.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ORACLE.07 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
