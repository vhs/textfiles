+++
title = "Electronic Magazines: CORE"
description = "CORE Magazine (1993)"
tabledata = "magazines_core"
tablefooter = "There are 15 files for a total of 338,321 bytes."
+++

As it says in its introductions, CORE is an electronic journal of poetry, fiction, essays, and criticism. Mostly an artifact of the brainy Electronic Freedom Foundation crowd, the issues contained here have a nice mix of text.
