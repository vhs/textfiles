+++
linktitle = "demonews.049"
title = "demonews.049"
url = "magazines/DEMONEWS/demonews.049.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEMONEWS.049 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
