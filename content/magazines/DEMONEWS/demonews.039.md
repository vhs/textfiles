+++
linktitle = "demonews.039"
title = "demonews.039"
url = "magazines/DEMONEWS/demonews.039.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEMONEWS.039 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
