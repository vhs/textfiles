+++
linktitle = "roadtrip.txt"
title = "roadtrip.txt"
url = "magazines/WHATEVER/roadtrip.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ROADTRIP.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
