+++
title = "Electronic Magazines: Citronic Journal"
description = "Citronic Journal (1994)"
tabledata = "magazines_citro"
tablefooter = "There are 6 files for a total of 257,131 bytes."
+++

Besides an amazing inability to spell correctly, this magazine had a lot of interesting contributors and a wide variety of subjects, although most of the writing was bent towards destroying something ot another. It seems to have ended rather abruptly.
