+++
linktitle = "bangsonic-1.5"
title = "bangsonic-1.5"
url = "magazines/BANGSONIC/bangsonic-1.5.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BANGSONIC-1.5 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
