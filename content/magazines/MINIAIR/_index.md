+++
title = "Electronic Magazines: The Annals of Improbable Research"
description = "The mini-Annals of Improbable Research (\"mini-AIR\") (1994-1995)"
tabledata = "magazines_miniair"
tablefooter = "There are 8 files for a total of 180,415 bytes."
+++

After the Journal of Irreproducible Results got into a bit of a legal/ownership battle, a group of people broke off and started a new scientific parody group, the Annals of Improbable Research, or AIR. Within the pages of this magazine, you can find some of the cleverest, intellectual humor out there, humor based on the best minds of the country letting off some steam.

This particular set of "Mini-AIR" missives seem to clarify or announce items with the main, printed magazine. The humor shines through.
