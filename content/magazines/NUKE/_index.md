+++
title = "The NuKE Info Journal (1991-1994)"
description = "The NuKE Info Journal (1991-1994)"
tabledata = "magazines_nuke"
tablefooter = "There are 16 files for a total of 2,159,923 bytes."
+++

In the name of completeness, the Journal is also presented in the zip/executable form the original documents came in, wherein a script was run that uncompressed the individual files and then removed the recompression.
