+++
title = "Electronic Magazines: Master Anarchists Giving Illicit Knowledge"
description = "Master Anarchists Giving Illicit Knowledge (1993)"
tabledata = "magazines_magik"
tablefooter = "There are 2 files for a total of 135,005 bytes."
+++

This two-issue wonder is a classic case of a group going through enormous hoops to create a neat acronym for their name. Both issues have a solid amount of information, but the whole effort isn't exactly brimming with any sort of style. 
