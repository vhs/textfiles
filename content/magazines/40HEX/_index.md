+++
title = "Electronic Magazines: 40HEX"
description = "40HEX Virus Magazine (1992-Present)"
tabledata = "magazines_40hex"
tablefooter = "There are 15 files for a total of 2,382,086 bytes."
+++

To quote from the readme file:

> 40Hex has existed as an online magazine since June 1991. The magazine has changed management several times and has consequently gone through many stages. However, the purpose of the magazine has always been to distribute information on the computer virus phenomenon in an uncensored forum. 40Hex online has become the most popular virus magazine in the underground.
