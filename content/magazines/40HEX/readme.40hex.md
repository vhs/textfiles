+++
linktitle = "readme.40hex"
title = "readme.40hex"
url = "magazines/40HEX/readme.40hex.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download README.40HEX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
