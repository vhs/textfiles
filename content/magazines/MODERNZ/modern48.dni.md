+++
linktitle = "modern48.dni"
title = "modern48.dni"
url = "magazines/MODERNZ/modern48.dni.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN48.DNI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
