+++
linktitle = "modern30.nj3"
title = "modern30.nj3"
url = "magazines/MODERNZ/modern30.nj3.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN30.NJ3 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
