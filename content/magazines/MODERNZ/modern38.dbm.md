+++
linktitle = "modern38.dbm"
title = "modern38.dbm"
url = "magazines/MODERNZ/modern38.dbm.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN38.DBM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
