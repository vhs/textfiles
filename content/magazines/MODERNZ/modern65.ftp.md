+++
linktitle = "modern65.ftp"
title = "modern65.ftp"
url = "magazines/MODERNZ/modern65.ftp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN65.FTP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
