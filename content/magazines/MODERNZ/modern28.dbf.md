+++
linktitle = "modern28.dbf"
title = "modern28.dbf"
url = "magazines/MODERNZ/modern28.dbf.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN28.DBF textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
