+++
linktitle = "modern18.nj1"
title = "modern18.nj1"
url = "magazines/MODERNZ/modern18.nj1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN18.NJ1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
