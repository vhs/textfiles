+++
linktitle = "modern29.nj2"
title = "modern29.nj2"
url = "magazines/MODERNZ/modern29.nj2.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN29.NJ2 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
