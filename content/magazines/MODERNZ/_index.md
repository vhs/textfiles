+++
title = "Electronic Magazines: THE PHOENIX/PANTHER MODERNS"
description = "The Phoenix/Panther Modernz Magazine (1990-1992)"
tabledata = "magazines_modernz"
tablefooter = "There are 87 files for a total of 2,676,641 bytes."
+++

It's a little hard to tell what this group is about; they experienced some traumatic splitting early in their history due to the bust of their leader, who went off to run the magazine TANJ (which then shows up as some of the "issues" of this magazine anyway) and leaving the bulk of work to the Digital Demon. Digital Demon takes up nicely, trying to give as much information as possible about Network and Internet exploration, only to have a few crashing interferences by Bad Hash, who just wants to get stoned and play games. A mess.
