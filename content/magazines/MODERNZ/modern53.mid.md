+++
linktitle = "modern53.mid"
title = "modern53.mid"
url = "magazines/MODERNZ/modern53.mid.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN53.MID textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
