+++
linktitle = "modern16.scr"
title = "modern16.scr"
url = "magazines/MODERNZ/modern16.scr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MODERN16.SCR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
