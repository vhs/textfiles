+++
title = "Electronic Magazines: BLaH"
description = "Big Long and Hairy (1992-1994)"
tabledata = "magazines_blah"
tablefooter = "There are 53 files for a total of 357,145 bytes."
+++

A pleasant surprise... the creators of BLaH experienced several years of sporadic bursts of interesting file releases (usually over a dozen in a single week, and then months of quiet), covering the usual explosives information, but moving into politics, poetry, and Social Criticism. Created with lots of energy, and it shows.
