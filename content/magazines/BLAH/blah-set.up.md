+++
linktitle = "blah-set.up"
title = "blah-set.up"
url = "magazines/BLAH/blah-set.up.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BLAH-SET.UP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
