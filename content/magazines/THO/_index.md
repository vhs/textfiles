+++
title = "Magazines: The Hidden Obvious"
description = "The Hidden Obvious (1994)"
tabledata = "magazines_tho"
tablefooter = "There are 36 files for a total of 309,882 bytes."
+++

According to the founder, Pip the Angry Youth:

<blockquote>
<pre>"i figured a
good way to kill time was to start writing a magazine (i had this dream
ever since i read subbacultcha on that fateful day). so i started up a
magazine called the hidden obvious, and it lasted for about 7 months,
released 35 issues, and became incredibly popular in my bbsing area.  so
i killed it in july of '94 because i was a complete moron."
</pre>
</blockquote>

Pip also makes an appearance in <b>MILK</b>, <b>Tea</b>, and <B>GASP</B>. Quite a prolific, angry youth.

There is absolutely nothing special about The Hidden Obvious.
