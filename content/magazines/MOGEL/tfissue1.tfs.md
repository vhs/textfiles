+++
linktitle = "tfissue1.tfs"
title = "tfissue1.tfs"
url = "magazines/MOGEL/tfissue1.tfs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TFISSUE1.TFS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
