+++
title = "Electronic Magazines: KCAH"
description = "K.C.A.H. (1990)"
tabledata = "magazines_kcah"
tablefooter = "There are 2 files for a total of 49,542 bytes."
+++

Well, they never explain what KCAH stands for, if anything, but The Rebel and his gang are able to get out two issues of their zine in a few months time in 1990. They use the grade-school trick of including the title page as an article, but other than that they try their best.
