+++
linktitle = "nwr.1.5.txt"
title = "nwr.1.5.txt"
url = "magazines/NWR/nwr.1.5.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NWR.1.5.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
