+++
title = "Electronic Magazines: Cheap Truth"
description = "Cheap Truth (1983-1986)"
tabledata = "magazines_cheaptruth"
tablefooter = "There are 18 files for a total of 179,268 bytes."
+++

From the Web Site that these were taken from:

<blockquote>
<pre>
"Here are the complete collected issues of Vincent Omniavertas' CHEAP
TRUTH fanzine. The addresses and phone numbers listed are out of date:
currently the SMOF-BBS is virtual, at http://www.io.com/~shiva/SMOF-BBS.html
-- shiva@io.com, for the Electronic Edition."
</pre>
</blockquote>
