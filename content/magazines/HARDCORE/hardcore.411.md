+++
linktitle = "hardcore.411"
title = "hardcore.411"
url = "magazines/HARDCORE/hardcore.411.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HARDCORE.411 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
