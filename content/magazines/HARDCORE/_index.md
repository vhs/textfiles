+++
title = "Electronic Magazines: HardC.O.R.E."
description = "HardC. O. R. E. (1993-1994)"
tabledata = "magazines_hardcore"
tablefooter = "There are 21 files for a total of 1,743,509 bytes."
+++

The C.O.R.E. in HardC.O.R.E. stands for "Committee for Rap Excellence", although this isn't really evident until the second volume. This is a zine dedicated to rap artists and lyrics, giving discographies, news, and reviews about rap albums being released.  Starting out somewhat lightweight, the newsletter really gels with Volume two, with dozens of contributors being culled from the Usenet Newsgroup "alt.rap".
