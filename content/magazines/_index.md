+++
title = "Electronic Magazines"
description = "Collections of \"E-Zines\", including Phrack"
tabledata = "magazines"
tablefooter = "There are 2 files for a total of 62,869 bytes.<br>There are 289 directories."
+++

Of all the sections, this one should be the most out-of-date and incomplete. Essentially this section will be all the "Electronic Magazines" or "E-Zines" that started out on BBSes and spread to the Internet.

Instead of losing individual textfiles in the sea of BBSes, many writers chose instead to move to the "Magazine" model, where they would band together textfiles and release them as a group. This strengthened the chances of the files surviving and also made for impressive file sizes, a sign of quality to people browsing sites.

In many cases, the years listed are just what years I have collected, not the canonically known years the publication has existed. Every once in a while I go through and try to find more recent (or older) copies of the work to add.
