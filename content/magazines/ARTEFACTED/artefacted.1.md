+++
linktitle = "artefacted.1"
title = "artefacted.1"
url = "magazines/ARTEFACTED/artefacted.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARTEFACTED.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
