+++
title = "Electronic Magazines: Digital Murder"
description = "Digital Murder (1991)"
tabledata = "magazines_digitalmurder"
tablefooter = "There are 8 files for a total of 488,015 bytes."
+++

Digital Murder, the "Magazine for Cyberpunks and Other Hi-Tech Low-Lifes", presented a collection of anarchy and hacking-related files, all nicely formatted for a dot-matrix printer (including page codes and the like). Surprisingly literate and well-formatted.
