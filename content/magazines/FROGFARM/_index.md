+++
title = "Electronic Magazines: The Frog Farm"
description = "The Frog Farm (1993)"
tabledata = "magazines_frogfarm"
tablefooter = "There are 12 files for a total of 946,676 bytes."
+++

The Frog Farm appears to be a large collection of essays, writings, and information about issues of Freedom, Self-Expression, and Governmental Control. It spans many different subjects, and mentions the US Constitution in a very large way. Always an interesting read, and very well written.
