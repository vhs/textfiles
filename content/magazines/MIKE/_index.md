+++
title = "Electronic Magazines: Mike's Madness"
description = "Mike's Madness (1989-1990)"
tabledata = "magazines_mike"
tablefooter = "There are 26 files for a total of 251,214 bytes."
+++

Mike Beebe fills issue after issue of his "Madness" with collections of jokes, quotes, puns, and insults on the Peoples of Australia.
