+++
linktitle = "inform-5.phk"
title = "inform-5.phk"
url = "magazines/INFORMATIK/inform-5.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download INFORM-5.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
