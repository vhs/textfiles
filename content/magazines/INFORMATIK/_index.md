+++
title = "Electronic Magazines: INFORMATIK"
description = "Informatik: The Journal of Priv. Information (1991-1992)"
tabledata = "magazines_informatik"
tablefooter = "There are 5 files for a total of 811,388 bytes."
+++

Informatik, the "Journal of Privileged Information", produced an excellent set of issues, each one packed with information and extremely well-written. The efforts to produce a a zine of this quality takes its toll and the editors mention as such towards the end. At least one of the editors, Rafe Colburn, has gone on to run an excellent site at [rc3.org](http://www.rc3.org) which is worth checking out. Interesting folks weigh in through the issues. Good work.
