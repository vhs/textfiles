+++
title = "Electronic Magazines: The Chaos Chronicles"
description = "The Chaos Chronicles (1991)"
tabledata = "magazines_chaos"
tablefooter = "There are 10 files for a total of 53,299 bytes."
+++

A Collection of Hacking and Phreaking texts, collapsing into car theft and ANSI bombs by the last couple of issues. Almost all were created by Inphiniti, and a gaggle of buddies. A questionable way to spend the summer of 1991.
