+++
linktitle = "ucph8.008"
title = "ucph8.008"
url = "magazines/UCPH/ucph8.008.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UCPH8.008 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
