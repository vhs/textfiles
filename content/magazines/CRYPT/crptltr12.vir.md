+++
linktitle = "crptltr12.vir"
title = "crptltr12.vir"
url = "magazines/CRYPT/crptltr12.vir.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRPTLTR12.VIR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
