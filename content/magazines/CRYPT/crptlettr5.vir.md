+++
linktitle = "crptlettr5.vir"
title = "crptlettr5.vir"
url = "magazines/CRYPT/crptlettr5.vir.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRPTLETTR5.VIR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
