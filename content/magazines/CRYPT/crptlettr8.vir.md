+++
linktitle = "crptlettr8.vir"
title = "crptlettr8.vir"
url = "magazines/CRYPT/crptlettr8.vir.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRPTLETTR8.VIR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
