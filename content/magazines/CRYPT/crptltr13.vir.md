+++
linktitle = "crptltr13.vir"
title = "crptltr13.vir"
url = "magazines/CRYPT/crptltr13.vir.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRPTLTR13.VIR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
