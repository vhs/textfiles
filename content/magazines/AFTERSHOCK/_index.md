+++
title = "Electronic Magazines: Aftershock"
description = "AfterShoCk! (1993)"
tabledata = "magazines_aftershock"
tablefooter = "There are 12 files for a total of 169,841 bytes."
+++

<blockquote>
<pre>
All the rage of the 907 area code comes through in this collection of 
textfiles. From transcribed rap lyrics to calls for the downfall of priests and
the government, the writers craft a profanity-laced set of screeds you can
read in less than 10 minutes. Very obviously, Nitro 187 was the geek and
Meaty Smurf his rap-listening, destructive buddy.
</pre>
</blockquote>
