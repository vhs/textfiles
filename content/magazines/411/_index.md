+++
title = "Electronic Magazines: 411"
description = "The 411 Newsletter (1994)"
tabledata = "magazines_411"
tablefooter = "There are 2 files for a total of 80,273 bytes."
+++

I've only found a couple issues so far, but 411 seems to have been a pleasant enough collection of boxing plans and scans of exchanges. Middle of the Road.
