+++
linktitle = "critical.1st"
title = "critical.1st"
url = "magazines/CRITICAL/critical.1st.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRITICAL.1ST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
