+++
linktitle = "critical.5th"
title = "critical.5th"
url = "magazines/CRITICAL/critical.5th.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRITICAL.5TH textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
