+++
linktitle = "00-index.sur"
title = "00-index.sur"
url = "magazines/SURFPUNK/00-index.sur.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 00-INDEX.SUR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
