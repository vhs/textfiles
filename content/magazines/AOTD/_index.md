+++
title = "Electronic Magazines: The Art of Technology Digest"
description = "The Art of Technology Digest (1992)"
tabledata = "magazines_aotd"
tablefooter = "There are 6 files for a total of 242,806 bytes."
+++

<blockquote>
<pre>
Chris Cappuccio edits a collection of files that include a lot of USENET
announcements around technology, as well as EFF bulletins and the first few 
technical releases sent out by some upstart named Linus Torvalds with his
Linux.
</pre>
</blockquote>
