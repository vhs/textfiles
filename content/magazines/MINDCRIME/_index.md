+++
title = "Electronic Magazines: MiNDCRiME"
description = "Mindcrime (1994)"
tabledata = "magazines_mindcrime"
tablefooter = "There are 15 files for a total of 159,598 bytes."
+++

Without the first issue to guide me in the average quality, I can say that the second issue is a mish-mash of ranting about the quality of IRC, source code for exploiting sendmail and a reprinted news story. Not shining.
