+++
title = "Electronic Magazines: PHRACK Magazine"
description = "PHRACK Magazine (1985-Present)"
tabledata = "magazines_phrack"
tablefooter = "There are 63 files for a total of 22,774,964 bytes."
+++

One thing that always comes out in history, especially recent history, are some heavy debates about who was the "first". The first time, the first time with this type of person, the first time using this new technology, and the rest. While there's probably lots of room to argue about who or what was the first "Hacker E-Zine", without any doubt the most famous is PHRACK.

Founded by Taran King and Knight Lightning in 1985, PHRACK started getting the attention of the BBS world a short time into its run, with its collections of information-filled articles and "Phrack World News" which would add some real character to the subculture. Later, Phrack "Pro-Philes" gave stories behind the handles and let people learn what made the more famous or infamous hackers what they were.

The format and genotype of these issues are brilliant, or at least a brilliant move. Instead of putting out a singular file by themselves, hackers and writers were encouraged to become part of the PHRACK brand, ensuring a wide audience for their writings and preventing themselves from being lost in the extremely choppy distribution channels of BBSes. (Although, as you've probably seen by the rest of the TEXTFILES.COM site, many files did very well on their own). Obviously this wasn't the initial intent of the editors of PHRACK, but no doubt their own success told them they were onto something big, very quickly into their run.

Speaking of Choppy, PHRACK's own history is very difficult to keep track of without a scorecard (which actually shows up in [Issue #55](PHRACK55)!). The original editors gave way to new authors, who then found themselves not interested in the zine past a few issues and were taken up by still others. Taran King and Knight Lightning make appearances as editors for a second run, although this led to the infamous "E911" Affair, which is covered excellently in Bruce Sterling's Book, "The Hacker Crackdown". Yes, the document is still in the issue below, although truth be told it's in a lot of other places as well (and it's extremely boring, as many people have noted).

The PHRACK pages function as a who's who or yearbook of the more media-savvy and textfile-driven members of the different online communities. Through its many editors, writers, history and fame, PHRACK has stood alone.
