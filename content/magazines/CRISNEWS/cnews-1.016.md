+++
linktitle = "cnews-1.016"
title = "cnews-1.016"
url = "magazines/CRISNEWS/cnews-1.016.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CNEWS-1.016 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
