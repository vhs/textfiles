+++
title = "Electronic Magazines: Cracking 101"
description = "Cracking 101 (1990)"
tabledata = "magazines_c101"
tablefooter = "There are 5 files for a total of 139,908 bytes."
+++

Buckaroo Banzai is one of the few copy protection breakers who took the time to write lessons for others to learn. Cracking 101 is geared towards the IBM PC, and covers some of the basics.

Note that the top award still goes to Kracowicz, whose files are both in the top 100 and in the Apple section.
