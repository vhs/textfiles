+++
title = "Electronic Magazines: Barata Eletrica"
description = "Barata Eletrica (1994-1995)"
tabledata = "magazines_be"
tablefooter = "There are 11 files for a total of 936,467 bytes."
+++

There aren't many non-english hacker magazines in this collection, but what the heck. Barata Eletrica has a good smattering hacking related articles, with the occasional english press release buried inside. The articles center around the usual hacking/phreaking/2600-related news.
