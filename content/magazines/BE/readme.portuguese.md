+++
linktitle = "readme.portuguese"
title = "readme.portuguese"
url = "magazines/BE/readme.portuguese.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download README.PORTUGUESE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
