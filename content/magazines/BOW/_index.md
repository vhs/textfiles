+++
title = "Electronic Magazines: The Brotherhood of Warez"
description = "The Brotherhood of Warez (1994-1999)"
tabledata = "magazines_bow"
tablefooter = "There are 10 files for a total of 603,051 bytes."
+++

If the Brotherhood of Warez isn't a direct-on parody of the hacker/warez scene of the late 90's, it's one of the saddest examples of literary deficiency to make itself generally available. No plural noun escapes having a "z" on the end, and the subject matter ranges from banal to street-corner rude. A lucite cube of "warez humor".
