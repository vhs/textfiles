+++
title = "Electronic Magazines: DargonZine"
description = "DargonZine (1988-Present)"
tabledata = "magazines_dargonzine"
tablefooter = "There are 109 files for a total of 9,334,588 bytes."
+++


As explained in the introduction to DargonZine:

<blockquote>
<pre>
      DargonZine is an electronic magazine printing stories written for
 the Dargon Project, a shared-world  anthology similar to (and inspired
 by)  Robert  Asprin's Thieves'  World  anthologies,  created by  David
 "Orny" Liscomb in his now retired magazine, FSFNet. The Dargon Project
 centers around a medieval-style duchy called Dargon in the far reaches
 of the  Kingdom of  Baranur on  the world named  Makdiar, and  as such
 contains stories with  a fantasy fiction/sword and  sorcery flavor. At
 this  time, DargonZine  has no  plans to  publish anything  other than
 Dargon  Project stories,  but there  are several  other magazines  out
 there to satisfy this need.
</pre>
</blockquote>
