+++
title = "Electronic Magazines: The Legion of Doom/Hackers Technical Journal"
description = "The Legion of Doom/Hacking Technical Journal (1987-1993)"
tabledata = "magazines_lod"
tablefooter = "There are 9 files for a total of 1,035,351 bytes."
+++

The LOD/H Technical Journal got a lot of attention when it first came out in the middle of the 80's, for two main reasons: It was a large publication put out by the Legion of Doom, who got a lot of attention, and it was friggin' HUGE. Each of these files were well over 100k, and most were double that. Each issue was full to the brim of intense technical information about all sorts of computer and phone subjects, and they were all written in that "we're doing this for your benefit" LOD style you don't see in many other places.

The last issue of the LOD/H Journal (#5) is suspiciously Canadian.
