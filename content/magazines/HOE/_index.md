+++
title = "Electronic Magazines: The Hogs of Entropy"
description = "The Hogs of Entropy (1994-Present)"
tabledata = "magazines_hoe"
tablefooter = "There are 1,076 files for a total of 8,997,759 bytes."
+++

Where to begin with the Hogs of Entropy? Well, I can tell you that the first words that came to my mind during my initial browsing were "Cult of the Dead Cow Ripoff Group". Everything, from the dead pig logo, to the method of dating files with a file number at the end, to a lot of the subject matter, told me that they just took a look at the cDc and said "Yeah, sure, we can do that." and then set off to do it. Mogel seems to have been the primary motivating force behind their initial files, and his influence is all over them.

What I did NOT expect was to go out and download all the current HoE files (I only had 60) and suddenly discover they had over SIX HUNDRED FILES created. And these weren't just transcriptions of news stories or lyrics; these were honestly created text files, adding up past the five meg mark in total. That's just a lot of files, by any standard.

And as for the files themselves... well, it's VERY hard to write or supervise the writing of that many files and not start to increase in quality over time. As it stands, though, the quality is pretty enjoyable from the get-go and continues to be entertaining over the years. The Hogs of Entropy also have a nice selection of interviews with other electronic magazine writers, making for an interesting study from the Zine World perspective. There's even files and interviews with cDc members peppered in there.

All in all, definitely worth your time, although you should be prepared to set aside a LOT of time to read them. A lot of fun.
