+++
linktitle = "hoe-0488.txt"
title = "hoe-0488.txt"
url = "magazines/HOE/hoe-0488.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HOE-0488.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
