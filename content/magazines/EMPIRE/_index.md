+++
title = "Electronic Magazines: The Empire Times"
description = "The Empire Times (1992-1995)"
tabledata = "magazines_empire"
tablefooter = "There are 7 files for a total of 672,144 bytes."
+++

Sort of a proto-PHRACK, the Empire Times tried to capture all the magic of hacking, and encourage others to learn and experiment and have fun. The only downside was the occasionally incredible distance between issues (one stretch lasted 2 years). It billed itself as the "True Hacker's Magazine" and they stayed true to their vision.
