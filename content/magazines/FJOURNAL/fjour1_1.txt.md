+++
linktitle = "fjour1_1.txt"
title = "fjour1_1.txt"
url = "magazines/FJOURNAL/fjour1_1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FJOUR1_1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
