+++
linktitle = "fjour2_5.txt"
title = "fjour2_5.txt"
url = "magazines/FJOURNAL/fjour2_5.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FJOUR2_5.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
