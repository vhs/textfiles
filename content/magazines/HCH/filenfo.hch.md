+++
linktitle = "filenfo.hch"
title = "filenfo.hch"
url = "magazines/HCH/filenfo.hch.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FILENFO.HCH textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
