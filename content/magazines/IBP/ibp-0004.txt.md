+++
linktitle = "ibp-0004.txt"
title = "ibp-0004.txt"
url = "magazines/IBP/ibp-0004.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IBP-0004.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
