+++
linktitle = "ibp-0008.txt"
title = "ibp-0008.txt"
url = "magazines/IBP/ibp-0008.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IBP-0008.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
