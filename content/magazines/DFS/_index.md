+++
title = "Electronic Magazines: Damned Fucking Shit"
description = "Damned Fucking Shit (1993-1996)"
tabledata = "magazines_dfs"
tablefooter = "There are 61 files for a total of 571,731 bytes."
+++

The Damned Fucking Shit magazine reigned somewhat supreme along a group of BBSes in 414 (including an Ascii Express Line!) and the beginning of the 90's. With a focus on BBS humor and occasional poetry and lyrics, they haphazardly gave out files from 1993 until they put out a final five issues in 1995.

The DFS "Mafia" continue to maintain a page with their work at [www.dfs.org](http://www.dfs.org).
