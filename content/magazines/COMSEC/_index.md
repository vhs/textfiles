+++
title = "Electronic Magazines: The COMSEC Bulletin"
description = "The Communications Security Bulletin (1984-Present)"
tabledata = "magazines_comsec"
tablefooter = "There are 11 files for a total of 752,445 bytes."
+++

This weekly newsletter, dedicated to communications security and the political and technical issues therein, compiled each year's worth of newsletters into compilations, shown below. They seem to have made a big effort to encorporate and seem to have been a forefront of individual privacy in the digital age. Dry, but honorable.
