+++
linktitle = "newvol05.irg"
title = "newvol05.irg"
url = "magazines/IRG/newvol05.irg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEWVOL05.IRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
