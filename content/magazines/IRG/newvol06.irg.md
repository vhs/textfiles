+++
linktitle = "newvol06.irg"
title = "newvol06.irg"
url = "magazines/IRG/newvol06.irg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEWVOL06.IRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
