+++
linktitle = "newvol03.irg"
title = "newvol03.irg"
url = "magazines/IRG/newvol03.irg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NEWVOL03.IRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
