+++
title = "Electronic Magazines: Hacking and Computer Krashing (HACK)"
description = "Hacking and Computer Crashing (1984)"
tabledata = "magazines_hack"
tablefooter = "There are 7 files for a total of 38,993 bytes."
+++

Starting out as a list of passwords and dialins, this little collection of information put out by Grey Wolf sort of grew into a magazine, although "bulletin" would perhaps describe it better. Done throughout 1984.
