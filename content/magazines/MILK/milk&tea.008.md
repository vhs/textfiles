+++
linktitle = "milk&tea.008"
title = "milk&tea.008"
url = "magazines/MILK/milk&tea.008.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILK&TEA.008 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
