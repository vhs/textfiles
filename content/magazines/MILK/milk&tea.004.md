+++
linktitle = "milk&tea.004"
title = "milk&tea.004"
url = "magazines/MILK/milk&tea.004.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILK&TEA.004 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
