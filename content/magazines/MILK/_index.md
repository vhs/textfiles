+++
title = "Electronic Magazines: MiLK (and MiLK and Tea)"
description = "Mighty Illicit Liquid Kollection (1994-1995)"
tabledata = "magazines_milk"
tablefooter = "There are 83 files for a total of 605,833 bytes."
+++

MiLK is a classic, strong example of the trend of mid-1990's textfile writing groups: They give a good laugh, a lot of entertainment, and they make you feel like you're a part of their group as they tell you how screwed up they think the world is, and how much there is to laugh at. They team up with a group called "Tea" for a number of issues, as well as a couple of tag files for their name and BBS list.

Apparently, they didn't talk to each other a lot, because a lot of issue numbers are doubled. It all works out in the end, anyway.

Maybe I'm just tired from reading thousands of textfiles, but Issue #40 (The Guide to Atari) very nearly made me fall out of chair, laughing. Good stuff.
