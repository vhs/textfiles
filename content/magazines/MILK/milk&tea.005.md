+++
linktitle = "milk&tea.005"
title = "milk&tea.005"
url = "magazines/MILK/milk&tea.005.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILK&TEA.005 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
