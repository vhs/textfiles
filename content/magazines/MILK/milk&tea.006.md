+++
linktitle = "milk&tea.006"
title = "milk&tea.006"
url = "magazines/MILK/milk&tea.006.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILK&TEA.006 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
