+++
linktitle = "milk-tea.010"
title = "milk-tea.010"
url = "magazines/MILK/milk-tea.010.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILK-TEA.010 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
