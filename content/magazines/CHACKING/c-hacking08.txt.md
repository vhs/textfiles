+++
linktitle = "c-hacking08.txt"
title = "c-hacking08.txt"
url = "magazines/CHACKING/c-hacking08.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download C-HACKING08.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
