+++
linktitle = "ch20addendum.txt"
title = "ch20addendum.txt"
url = "magazines/CHACKING/ch20addendum.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CH20ADDENDUM.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
