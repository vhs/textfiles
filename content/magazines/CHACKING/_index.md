+++
title = "Electronic Magazines: Commodore Hacking"
description = "Commodore Hacking Magazine (1992-Present)"
tabledata = "magazines_chacking"
tablefooter = "There are 21 files for a total of 4,539,416 bytes."
+++

Commodore Hacking is a very large, very intense technical journal centering around all the aspects of the Commodore family of computers. It concentrates on knowing all you can about these machines, and has informative articles with deep, deep exploration of Commodres and the time-honored practice of getting them to do things they were never intended to do. Highly reccommended.
