+++
title = "Electronic Magazines: The Legions of Lucifer"
description = "Legions of Lucifer (1990-1991)"
tabledata = "magazines_lol"
tablefooter = "There are 24 files for a total of 488,483 bytes."
+++

This unpleasant collection of textfiles is probably best highlighted by the fake "We're closing shop" issue (#9) that someone else distributed, and which they proceeded to add to their official collection. They also merge with PHUCK later in their press run, which doesn't fix matters much. A pretty odd little group, one of whom isn't too fond of his mom.
