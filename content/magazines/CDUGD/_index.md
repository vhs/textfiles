+++
title = "Electronic Magazines: Computer Down-Under-Ground Digest"
description = "Computer Down-Under-Ground Digest (1991-1992)"
tabledata = "magazines_cdugd"
tablefooter = "There are 3 files for a total of 76,378 bytes."
+++

A New-Zealand-based version of the Computer Underground Digest. By the second issue they already ask for money, and by the third, they're trying to charge for files. Inauspicious.
