+++
title = "Electronic Magazines: Full Disclosure"
description = "Full Disclosure (1991)"
tabledata = "magazines_fulldisc"
tablefooter = "There are 16 files for a total of 168,029 bytes."
+++

Glen Roberts is one angry man. Concerned about the nature of privacy and personal information online, he has created a ton of newsletters and even runs a radio show centering around the proliferation of your personal data. In doing this, he makes himself a lightning rod for all sorts of madness. On the whole, quite an impressive pedigree. This collection is only a small part of all the files he's written; if I can find an easy way to get them all, I'll put them up. All intelligently written and worth a read.
