+++
title = "Electronic Magazines: Corrupt Programming International"
description = "Corrupted Programing International (1989)"
tabledata = "magazines_cpi"
tablefooter = "There are 3 files for a total of 203,659 bytes."
+++

An E-zine dedicated to Trojans, Viruses, and other unwanted marauders on the computer front. The files seem to have been distributed as chapters but have been combined into two large newsletters.
