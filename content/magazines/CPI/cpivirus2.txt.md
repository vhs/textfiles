+++
linktitle = "cpivirus2.txt"
title = "cpivirus2.txt"
url = "magazines/CPI/cpivirus2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CPIVIRUS2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
