+++
title = "Electronic Magazines: LINE NOIZ"
description = "Line Noiz (1993-1995)"
tabledata = "magazines_lnoiz"
tablefooter = "There are 25 files for a total of 906,034 bytes."
+++

Disregarding the fearsome headings to each issue, Line Noiz contains a salad mix of intelligent reviews and essays related to CyberPunk: The Music, the Clothes, the Hardware. While definitely not completely stunning in subject matter, they stay on theme very well for the run of the magazine. Cyber.
