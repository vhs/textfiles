+++
title = "Electronic Magazines: H-NET"
description = "H-Net Magazine (1990)"
tabledata = "magazines_hnet"
tablefooter = "There are 19 files for a total of 147,014 bytes."
+++

Another PHRACKalike zine, this 1990 issue appears to have had only one release, consisting of 20 files, three of which advertise the board the issue came from. Lots of stories about JANET, (the Joint Academic NETwork) and a smattering of unix-related hacking files. A promising first issue, sadly the only one.
