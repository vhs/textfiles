+++
linktitle = "phuck_5.txt"
title = "phuck_5.txt"
url = "magazines/CYBERPHUCK/phuck_5.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PHUCK_5.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
