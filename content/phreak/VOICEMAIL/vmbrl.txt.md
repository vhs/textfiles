+++
linktitle = "vmbrl.txt"
title = "vmbrl.txt"
url = "phreak/VOICEMAIL/vmbrl.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VMBRL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
