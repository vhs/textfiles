+++
linktitle = "vmb.rl"
title = "vmb.rl"
url = "phreak/VOICEMAIL/vmb.rl.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VMB.RL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
