+++
title = "Phone Phreaking : Dumpster Diving"
description = "The Art of Dumpster Diving to Get Information"
tabledata = "phreak_trashing"
tablefooter = "There are 17 files for a total of 135,468 bytes."
+++

In retrospect, it seems pretty simple: to find out more about the telephone system, just go through the garbage of a telephone company office, to see what they throw out. Among the coffee rinds and the stale donuts you'll more than likely find old manuals, papers, instructions and whatever else they thought was useless but which you considered gold. These files explain this simple process.
