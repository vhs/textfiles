+++
linktitle = "avoidcap.txt"
title = "avoidcap.txt"
url = "phreak/PHREAKING/avoidcap.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AVOIDCAP.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
