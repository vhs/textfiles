+++
linktitle = "mail-frd.mia"
title = "mail-frd.mia"
url = "phreak/PHREAKING/mail-frd.mia.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MAIL-FRD.MIA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
