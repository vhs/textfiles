+++
title = "Phone Phreaking: Cellular Telephony"
description = "Information, Instructions, Articles, and Essays on Cellular Telephony"
tabledata = "phreak_cellular"
tablefooter = "There are 158 files for a total of 2,932,540 bytes."
+++

When portable (cellular) telephones became generally available, they had a deadly combination: they were absolutely useful, and they were amazingly expensive. The power of being able to reach into your pocket and call anyone in the world was perhaps the perfect holy grail to young phreaks looking for the next big thing. Since cellular telephones are simply technology with broadcasting ability, it was just a matter of time before the wave of curiousity that has always driven phreaking would turn towards this new horizon. In the early 90's, it truly exploded, but there are traces of interest in the late 80's, and the battle rages on.
