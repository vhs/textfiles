+++
linktitle = "cmtelco1.txt"
title = "cmtelco1.txt"
url = "phreak/CELLULAR/cmtelco1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CMTELCO1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
