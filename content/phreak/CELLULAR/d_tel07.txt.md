+++
linktitle = "d_tel07.txt"
title = "d_tel07.txt"
url = "phreak/CELLULAR/d_tel07.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download D_TEL07.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
