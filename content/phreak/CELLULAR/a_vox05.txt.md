+++
linktitle = "a_vox05.txt"
title = "a_vox05.txt"
url = "phreak/CELLULAR/a_vox05.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download A_VOX05.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
