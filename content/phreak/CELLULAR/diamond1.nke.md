+++
linktitle = "diamond1.nke"
title = "diamond1.nke"
url = "phreak/CELLULAR/diamond1.nke.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIAMOND1.NKE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
