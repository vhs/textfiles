+++
linktitle = "d_tel02.txt"
title = "d_tel02.txt"
url = "phreak/CELLULAR/d_tel02.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download D_TEL02.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
