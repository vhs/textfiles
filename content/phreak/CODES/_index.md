+++
title = "Phone Phreaking: Telephone Access Codes (Codez)"
description = "Lists of Non-Working Telephone Access Codes"
tabledata = "phreak_codes"
tablefooter = "There are 16 files for a total of 179,360 bytes."
+++

It used to cost an enormous amount of money to call long distance for any large amount of time. One phone company, one price, one <i>high</i> price. And if you were connecting to a BBS across the country for hours at 300 baud, that first phone bill realigned what your parents thought a telephone bill could cost.

Some of the BBS community, intent on calling anywhere they wanted to whenever they wanted to, started learning how to scan for telephone access codes, where you would dial an access number for another telephone company or voice mail system, and then hit random digits until you stumbled onto a "code" that let you dial anywhere you wanted to. An entire subculture rose up to trade in these "codez", and to many casual observers this seemed to be the whole of Phone Phreaking.
