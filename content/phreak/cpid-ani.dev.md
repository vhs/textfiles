+++
linktitle = "cpid-ani.dev"
title = "cpid-ani.dev"
url = "phreak/cpid-ani.dev.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CPID-ANI.DEV textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
