+++
title = "Phone Phreaking: Telephone Cans"
description = "Files related to interacting with Telephone Company Boxes"
tabledata = "phreak_canning"
tablefooter = "There are 23 files for a total of 180,678 bytes."
+++

Outside of your house, there are secondary connections to your phone line. Somewhere near where you live, there are really big boxes that have connections to all the phone lines around you. Sometimes, people can get into these boxes, and with the right equipment, connect to your phone line. Usually these people are Telephone Repairmen. Usually.
