+++
linktitle = "bioc2.wri"
title = "bioc2.wri"
url = "phreak/BIOCAGENT/bioc2.wri.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIOC2.WRI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
