+++
linktitle = "bioc1.doc"
title = "bioc1.doc"
url = "phreak/BIOCAGENT/bioc1.doc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIOC1.DOC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
