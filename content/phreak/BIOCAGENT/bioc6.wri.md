+++
linktitle = "bioc6.wri"
title = "bioc6.wri"
url = "phreak/BIOCAGENT/bioc6.wri.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIOC6.WRI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
