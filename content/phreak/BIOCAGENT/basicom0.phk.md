+++
linktitle = "basicom0.phk"
title = "basicom0.phk"
url = "phreak/BIOCAGENT/basicom0.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BASICOM0.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
