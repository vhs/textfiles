+++
linktitle = "bioc3.wri"
title = "bioc3.wri"
url = "phreak/BIOCAGENT/bioc3.wri.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIOC3.WRI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
