+++
linktitle = "bioc4.wri"
title = "bioc4.wri"
url = "phreak/BIOCAGENT/bioc4.wri.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIOC4.WRI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
