+++
title = "Phone Phreaking"
description = "Files about, from, and against the Phone Company."
tabledata = "phreak"
tablefooter = "There are 770 files for a total of 11,018,184 bytes.<br>There are 14 directories."
+++

Just as Hacking quickly became a word meaning "Using Computers for Evil Purposes" and eventually 'Any Evil Use of Technology", the word "Phreak", a play off the spelling of "Phone", has spread majestically out into almost complete meaningless. No doubt the pioneering souls of the 60's and 70's, fascinated with the amazing technical achievements of the telephone system, used the term "Fone Phreak" with a warm pride and conspiratorial air, unable to explain to the general populace what drove them to understand the special tones and codes that could propel their voices together and across the world.

By the 80's, the term Phreak was also fading into a general meaning of "Any evil done with telephones", and ultimately "Any evil done at all using any sort of communications equipment."
