+++
linktitle = "fonemods.txt"
title = "fonemods.txt"
url = "phreak/HOMEPHONES/fonemods.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FONEMODS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
