+++
linktitle = "tele-ent.txt"
title = "tele-ent.txt"
url = "phreak/HOMEPHONES/tele-ent.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TELE-ENT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
