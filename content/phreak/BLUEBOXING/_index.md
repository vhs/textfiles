+++
title = "Phone Phreaking: Blue Boxing"
description = "Files about Blue Boxes (See BOXING)"
tabledata = "phreak_blueboxing"
tablefooter = "There are 64 files for a total of 717,526 bytes."
+++

To a young person dabbling in the underground in the early 1980s, few concepts were as powerful, alluring, or exciting as the "Blue Box". A number of simple circuits enclosed with push buttons, it changed everything in the relationship between the Phone Company (Ma Bell) and her customers (The Peons).

Created in the late 1960's and working well into the 1980s (and maybe beyond), the Blue Box took advantage of a design flaw in the entire telephone system: it was possible to gain access to parts of phone switching equipment and send them false signals to make the machinery do whatever you wanted.

With the blast of a 2600hz tone down a telephone line (signifying you Meant Serious Business), you could seize the telephone trunk and gain a sort of "Administrator" access to the phone switching equipment. And since operators were unlikely to do the sort of wild experimentation that a phreak with a blue box would try, all sorts of amazing things would happen with a few button presses.

The Blue Box was a tangible Swiss Army Knife of the telephone, a powerful tool that gave the whole world to you in a small box. Little like it has been seen since. It inspired dozens and dozens of other [phone-related home-grown circuits](../BOXES), who all were given the label of "(Color) Box" in homage.
