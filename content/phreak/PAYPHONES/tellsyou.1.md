+++
linktitle = "tellsyou.1"
title = "tellsyou.1"
url = "phreak/PAYPHONES/tellsyou.1.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TELLSYOU.1 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
