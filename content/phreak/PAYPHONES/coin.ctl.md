+++
linktitle = "coin.ctl"
title = "coin.ctl"
url = "phreak/PAYPHONES/coin.ctl.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COIN.CTL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
