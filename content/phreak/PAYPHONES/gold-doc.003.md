+++
linktitle = "gold-doc.003"
title = "gold-doc.003"
url = "phreak/PAYPHONES/gold-doc.003.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GOLD-DOC.003 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
