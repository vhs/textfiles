+++
linktitle = "detectbug.phk"
title = "detectbug.phk"
url = "phreak/WIRETAPPING/detectbug.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DETECTBUG.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
