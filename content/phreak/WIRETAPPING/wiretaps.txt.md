+++
linktitle = "wiretaps.txt"
title = "wiretaps.txt"
url = "phreak/WIRETAPPING/wiretaps.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WIRETAPS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
