+++
title = "Phone Phreaking: Wiretapping and Bugs"
description = "Files about Tapping Calls or Intercepting Communications"
tabledata = "phreak_wiretapping"
tablefooter = "There are 56 files for a total of 472,516 bytes."
+++

The Great Fear of people who use telephones to conduct their business (private or professional) is that someone is listening in, taking notes, recording. It's a concern that has allowed some very rough laws to be put into place, and it's a central theme of these files. They tell you how to bug telephones or rooms, get in the way of electronic transmissions, and they even touch on some of the politics behind this method of information gathering.

Of course, it should be noted that not everyone out there wiretapping is a private citizen.
