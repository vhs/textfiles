+++
linktitle = "wait.tap"
title = "wait.tap"
url = "phreak/WIRETAPPING/wait.tap.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WAIT.TAP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
