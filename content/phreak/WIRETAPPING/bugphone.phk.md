+++
linktitle = "bugphone.phk"
title = "bugphone.phk"
url = "phreak/WIRETAPPING/bugphone.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BUGPHONE.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
