+++
linktitle = "numbs29.txt"
title = "numbs29.txt"
url = "phreak/NUMBERS/numbs29.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NUMBS29.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
