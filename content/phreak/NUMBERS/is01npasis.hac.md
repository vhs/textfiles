+++
linktitle = "is01npasis.hac"
title = "is01npasis.hac"
url = "phreak/NUMBERS/is01npasis.hac.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IS01NPASIS.HAC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
