+++
linktitle = "lata_nam.tab"
title = "lata_nam.tab"
url = "phreak/NUMBERS/lata_nam.tab.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LATA_NAM.TAB textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
