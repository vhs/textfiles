+++
linktitle = "ctrycodedoc.phk"
title = "ctrycodedoc.phk"
url = "phreak/NUMBERS/ctrycodedoc.phk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CTRYCODEDOC.PHK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
