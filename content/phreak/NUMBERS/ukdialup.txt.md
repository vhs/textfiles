+++
linktitle = "ukdialup.txt"
title = "ukdialup.txt"
url = "phreak/NUMBERS/ukdialup.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UKDIALUP.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
