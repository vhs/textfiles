+++
linktitle = "mongel.msg"
title = "mongel.msg"
url = "phreak/NUMBERS/mongel.msg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MONGEL.MSG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
