+++
linktitle = "800carie.ass"
title = "800carie.ass"
url = "phreak/NUMBERS/800carie.ass.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 800CARIE.ASS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
