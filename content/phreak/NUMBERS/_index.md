+++
title = "Phone Phreaking: Telephone Scans And General Numbers"
description = "Lists or Scans of Exchanges, Area Codes, or Wardialing"
tabledata = "phreak_numbers"
tablefooter = "There are 294 files for a total of 6,192,921 bytes."
+++

By simply calling every telephone number in an exchange, you could find some of the most amazing things (as well as hundreds of angry citizens you'd woken up in the middle of the night). Test numbers, numbers that told you where you were calling from, odd recordings, you name it. An easy way to make a name for yourself was to simply choose a given telephone exchange, dial everything in it, and casually report all the great stuff you'd found.

If time-consuming scanning wasn't your bag, you could just do a little side research in government and public documents, and find out interesting phone numbers as well.

Finally, you'll find some files that dealt specifically with the telephone numbers themseves; how the Phone Company assigned them, what they consisted of, what interesting issues were being raised by how the telephone system itself was arranged.
