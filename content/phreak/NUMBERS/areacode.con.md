+++
linktitle = "areacode.con"
title = "areacode.con"
url = "phreak/NUMBERS/areacode.con.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AREACODE.CON textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
