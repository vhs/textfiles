+++
linktitle = "intro.to.boxing"
title = "intro.to.boxing"
url = "phreak/BOXES/intro.to.boxing.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download INTRO.TO.BOXING textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
