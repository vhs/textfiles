+++
linktitle = "acrylic.box"
title = "acrylic.box"
url = "phreak/BOXES/acrylic.box.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ACRYLIC.BOX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
