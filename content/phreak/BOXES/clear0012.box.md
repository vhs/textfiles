+++
linktitle = "clear0012.box"
title = "clear0012.box"
url = "phreak/BOXES/clear0012.box.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CLEAR0012.BOX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
