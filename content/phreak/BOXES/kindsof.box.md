+++
linktitle = "kindsof.box"
title = "kindsof.box"
url = "phreak/BOXES/kindsof.box.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KINDSOF.BOX textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
