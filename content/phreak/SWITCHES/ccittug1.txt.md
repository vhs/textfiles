+++
linktitle = "ccittug1.txt"
title = "ccittug1.txt"
url = "phreak/SWITCHES/ccittug1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CCITTUG1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
