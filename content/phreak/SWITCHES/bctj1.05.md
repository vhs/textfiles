+++
linktitle = "bctj1.05"
title = "bctj1.05"
url = "phreak/SWITCHES/bctj1.05.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BCTJ1.05 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
