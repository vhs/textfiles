+++
title = "School Reports and Term Papers"
description = "Sheets for the Cheats"
tabledata = "reports"
tablefooter = "There are 64 files for a total of 1,248,760 bytes.<br>There is 1 directory."
+++

With evenings and late nights blown away by the wonders of the BBS world, many school-going users found themselves with a tough reality: poor grades. This was all the more frustrating to parents and teachers because the student would otherwise seem to be bright and understanding. Worst of all, this lack of attention/attendance to school subjects would possibly lead to banishment from the computer.

To circumvent this, it because a sort of tradition to upload term papers or essays created in school to BBSes. This served two equal needs: Lagging students would seize this cheap and easy influx of information for their otherwise intolerable studies, and students who posted their essays got the thrill and pride of knowing their work was reaching a much wider audience than just the teacher, and might even be used to save the occasional butt.
