+++
title = "School Reports and Term Papers: The ACE Collection"
description = "The Arrogant Couriers with Essays (ACE) Collection"
tabledata = "reports_ace"
tablefooter = "There are 646 files for a total of 6,868,368 bytes."
+++

In 1994, Arrogant Couriers with Essays (ACE) went back to basics and improved the distribution methods, classification, and approach to online school essays. The result is this large collection of reports, stories, and summaries of an amazing range of subjects. The efforts of this group to accomplish both usefulness and speed of research are reflected in the (mostly) accurate summaries and classifications that appear at the top of the files. If known, the grade level, state, and the occasional short review are given for easy browsing.

Regardless of the opinion of these sort of "cheat sheets", the implementation is admirable.
