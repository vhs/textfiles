+++
linktitle = "c_americ.rpt"
title = "c_americ.rpt"
url = "reports/c_americ.rpt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download C_AMERIC.RPT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
