+++
linktitle = "r_school.rpt"
title = "r_school.rpt"
url = "reports/r_school.rpt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download R_SCHOOL.RPT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
