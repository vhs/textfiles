+++
linktitle = "virgin_i.phr"
title = "virgin_i.phr"
url = "news/virgin_i.phr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VIRGIN_I.PHR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
