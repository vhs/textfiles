+++
title = "News Stories"
description = "Often poorly transcribed News Stories"
tabledata = "news"
tablefooter = "There are 184 files for a total of 2,403,386 bytes."
+++

Before the news media made the transition to online servies, everything was on paper, and if a BBS user wanted to share, they had to type in the entire article themselves and pass it along. This directory will fill as time goes on with professionally transcribed stories, but peppered here and there are those labors of love, trying to spread the word.
