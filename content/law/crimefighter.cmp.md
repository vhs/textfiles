+++
linktitle = "crimefighter.cmp"
title = "crimefighter.cmp"
url = "law/crimefighter.cmp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CRIMEFIGHTER.CMP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
