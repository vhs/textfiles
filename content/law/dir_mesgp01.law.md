+++
linktitle = "dir_mesgp01.law"
title = "dir_mesgp01.law"
url = "law/dir_mesgp01.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIR_MESGP01.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
