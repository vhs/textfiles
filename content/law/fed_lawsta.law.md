+++
linktitle = "fed_lawsta.law"
title = "fed_lawsta.law"
url = "law/fed_lawsta.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FED_LAWSTA.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
