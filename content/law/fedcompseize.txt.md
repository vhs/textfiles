+++
linktitle = "fedcompseize.txt"
title = "fedcompseize.txt"
url = "law/fedcompseize.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FEDCOMPSEIZE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
