+++
title = "Laws and Law Discussion"
description = "Textfiles recounting laws or their application"
tabledata = "law"
tablefooter = "There are 533 files for a total of 16,062,590 bytes."
+++

From the first time a rumor went by that the FCC was going to institute a "modem tax" (this one shows up about every two years without fault), BBS users took an interest in the funny little laws that govern the country and how the jackasses in the funny white building were going to mess up the fun. To this end, transcripts and discussions about laws have been around for a long time. 

Because of the somewhat odd subject matter of some of the files, some of the files you would expect to be located in this section are also located in the <a href="/politics">politics</a> section. Some of these files would probably be better put in the Politics section as well. 

Oh, and this would normally go without saying, but it's probably not a good idea to depend on these files for actual, your-property-and-well-being law advice. Sometimes the law has radically changed since the file was written, or the file falls under that special "The US never submitted a full approval to all 50 states so actually you can punch a cop" type of conspiratorial section. See also <a href="/conspiracy">conspiracy</a>.
