+++
linktitle = "plateaup16.law"
title = "plateaup16.law"
url = "law/plateaup16.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATEAUP16.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
