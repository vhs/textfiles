+++
linktitle = "ia_lawsta.law"
title = "ia_lawsta.law"
url = "law/ia_lawsta.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IA_LAWSTA.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
