+++
linktitle = "tx_lawsta.law"
title = "tx_lawsta.law"
url = "law/tx_lawsta.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TX_LAWSTA.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
