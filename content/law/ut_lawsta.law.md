+++
linktitle = "ut_lawsta.law"
title = "ut_lawsta.law"
url = "law/ut_lawsta.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UT_LAWSTA.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
