+++
linktitle = "pol_pracp24.law"
title = "pol_pracp24.law"
url = "law/pol_pracp24.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POL_PRACP24.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
