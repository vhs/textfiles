+++
linktitle = "jcs-report-1947.txt"
title = "jcs-report-1947.txt"
url = "fun/jcs-report-1947.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JCS-REPORT-1947.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
