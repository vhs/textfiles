+++
linktitle = "vietnam.cpu"
title = "vietnam.cpu"
url = "fun/vietnam.cpu.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VIETNAM.CPU textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
