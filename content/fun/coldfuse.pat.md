+++
linktitle = "coldfuse.pat"
title = "coldfuse.pat"
url = "fun/coldfuse.pat.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COLDFUSE.PAT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
