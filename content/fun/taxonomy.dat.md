+++
linktitle = "taxonomy.dat"
title = "taxonomy.dat"
url = "fun/taxonomy.dat.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TAXONOMY.DAT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
