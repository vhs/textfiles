+++
title = "Textfiles With a Sense of Fun"
description = "A Weird Grab Bag of Oddness"
tabledata = "fun"
tablefooter = "There are 368 files for a total of 10,406,112 bytes.<br>There are 2 directories."
+++

When the textfiles.com collection was just a lot of files at the Works BBS (which I was the SysOp of from 1986-1988), I had several major collections to sort all the text into: Humor, Phreak, Hack, Anarchy, etc. Of course, when I sat back to create this new website from many more files, I found it was better to add more classifications so we wouldn't end up with too many thousand-file directories. 

However, there always seemed to be those files that were easily classifiable, but which would have only one or two files in it at most, if I were to make a whole directory for them.  Or, I'd have to make a section that was just gardening. I don't want to make a directory dedicated to gardening.

So, I'd shove these weird files into the 'Fun' section. They were always entertaining in some fashion, but they weren't humor. They often preached whacky things, but they weren't conspiracies or political rants. They were just... ...fun.

I've gone through this section a few times, and cleaned it out to go into other sections, but every time I'm adding files, some of the whacky ones always end up here. I guess that's how it'll always be: The Grab Bag.
