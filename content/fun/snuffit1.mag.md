+++
linktitle = "snuffit1.mag"
title = "snuffit1.mag"
url = "fun/snuffit1.mag.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SNUFFIT1.MAG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
