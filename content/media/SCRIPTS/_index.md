+++
title = "Mass Media: Television and Movie Scripts"
description = "Transcription of Performances or Original Movie/TV Scripts"
tabledata = "media_scripts"
tablefooter = "There are 31 files for a total of 1,658,590 bytes."
+++

A popular and disk-consuming type of file have is the complete shooting script or transcription of a popular movie or television show. In the cases where the movie is considered to be start-to-finish brilliant or well-written (such as Monty Python and the Holy Grail), the script is a studying tool to see what the actors were saying that you might have missed or to get some pointers on writing comedy. Some scripts seem to be almost labors of love, with additional comments put in for the reader (the Rocky Horror Scripts do this). All in all, a very involved type of file, and almost completely a USENET/Internet experience.
