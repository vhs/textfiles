+++
linktitle = "gd_sgrnd.txt"
title = "gd_sgrnd.txt"
url = "media/EPISODES/gd_sgrnd.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GD_SGRND.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
