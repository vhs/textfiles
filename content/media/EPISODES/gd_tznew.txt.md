+++
linktitle = "gd_tznew.txt"
title = "gd_tznew.txt"
url = "media/EPISODES/gd_tznew.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GD_TZNEW.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
