+++
title = "Mass Media: Episode Guides"
description = "Episode Guides or Lists of Shows/Movies"
tabledata = "media_episodes"
tablefooter = "There are 44 files for a total of 1,564,534 bytes."
+++

Episode Guides are less a BBS than a USENET/Internet event, due mostly to the cross-country communication that could result in quick edits and verified information about all the aspects of television episodes. You'll see some of the most anal and overworked analysis about television shows here; if it was shown with a weird edit in some television markets, it will be covered.

The amount of work that has gone into these documents are some of the most breathtaking of all the mid 80's/Early 90's Internet Documents. In its own way, it shows some of the incredible results of people working together towards the same goal. Maybe the subject matter didn't deserve it, but then again a lot of people would seem to disagree.
