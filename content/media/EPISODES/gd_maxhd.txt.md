+++
linktitle = "gd_maxhd.txt"
title = "gd_maxhd.txt"
url = "media/EPISODES/gd_maxhd.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GD_MAXHD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
