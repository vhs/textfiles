+++
linktitle = "gd_frasr.txt"
title = "gd_frasr.txt"
url = "media/EPISODES/gd_frasr.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GD_FRASR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
