+++
title = "Television and other Mass Media Guides"
description = "Television and Movie Minutae"
tabledata = "media"
tablefooter = "There are 533 files for a total of 16,062,590 bytes."
+++

Everyone loves television, except of course for those that think it's an inherent evil out to remove all vestiges of culture and sanity from today's world. But buried among the hours of useless screen time, one could and can find some real jewels. BBS users would share with each other the cool shows out there, and try to find out the stories behind the episodes. They started collecting lists of shows, and it all out a little out of hand.

In many ways, this was one of the first unusual uses of the information networks; to create perfect, complete episode guides of television shows. People could work together, collaborate, and discuss, and ultimately, you'd know every weird reference in the Twilight Zone and who starred in what. It was, at the time, something to behold.

If you're wondering where the Star Trek stuff is, it's in the Science Fiction Directory, in a special [Star Trek Section](/sf/STARTREK).
