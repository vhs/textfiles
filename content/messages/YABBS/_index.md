+++
title = "BBS Messages: Alex Wetmore's YABBS"
description = "Message Bases from Alex Wetmore's YABBS (1994)"
tabledata = "messages_yabbs"
tablefooter = "There are 16 files for a total of 5,387,429 bytes."
+++

In 1991, looking for a networkable BBS program, Freshman Alex Wetmore decided to write his own. He named it YABBS, ported it to Linux as it became available, and opened it to the public. It ran for two years, while he updated the software, and then died a quiet death in April, 1994. Alex has been kind enough to put the message bases from his BBS up on the internet, where a copy of them resides here. Good solid stuff.
