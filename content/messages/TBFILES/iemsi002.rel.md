+++
linktitle = "iemsi002.rel"
title = "iemsi002.rel"
url = "messages/TBFILES/iemsi002.rel.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IEMSI002.REL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
