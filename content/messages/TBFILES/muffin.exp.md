+++
linktitle = "muffin.exp"
title = "muffin.exp"
url = "messages/TBFILES/muffin.exp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MUFFIN.EXP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
