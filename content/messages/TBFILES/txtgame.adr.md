+++
linktitle = "txtgame.adr"
title = "txtgame.adr"
url = "messages/TBFILES/txtgame.adr.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TXTGAME.ADR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
