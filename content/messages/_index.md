+++
title = "BBS Messages"
description = "Samples of message bases from different BBSs"
tabledata = "messages"
tablefooter = "There are 125 files for a total of 5,311,731 bytes.<br>There are 5 directories."
pagefooter = "It is very important to give credit where it's due; in 1993-1994, the remaining members of the Legion of Doom, now called LOD Communications, decided to take on a vast project of bringing the past to the BBS users of the present. They would do this by transcribing BBS messages from all the most popular and active phreaking and hacking boards of the 1980s and presenting them in their original 40 or 80 column format, all the spelling and grammatical errors intact, and giving a profile of the boards the messages came from. In many ways, textfiles.com had the same idea, 6 years later. The project got underway, but never really achieved the nationwide/mass acceptance it was supposed to, and these important files were in danger of being lost a second time.<p>I'm pleased to be a part of bringing many of these files back to you, and give a bow to these pioneers of BBS nostalgia/history."
+++
