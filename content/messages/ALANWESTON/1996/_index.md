+++
title = "BBS Messages: The Alan Weston Collection: 1996"
description = "Messages from 1996 (Compuserve and Ocean Beach BBS)"
tabledata = "messages_alanweston_1996"
tablefooter = "There are 23 files for a total of 328,569 bytes."
+++

In 2005, Alan Weston began selling copies of his collection of BBS textfiles on the Ebay auction site, offering 20 megabytes (uncompressed) of textfiles for $5 plus shipping. Negotiations for a donation were unsuccessful, so I purchased the CD-ROM for a total of $9.50; now you get it for free. Much thanks to Alan for offering this collection to the world, even if there was a toll involved.
