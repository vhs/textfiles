+++
linktitle = "OCEAN02_25.txt"
title = "OCEAN02_25.txt"
url = "messages/ALANWESTON/1996/OCEAN02_25.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN02_25.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
