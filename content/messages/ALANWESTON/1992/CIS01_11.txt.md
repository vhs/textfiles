+++
linktitle = "CIS01_11.txt"
title = "CIS01_11.txt"
url = "messages/ALANWESTON/1992/CIS01_11.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS01_11.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
