+++
linktitle = "OCEAN05_01.txt"
title = "OCEAN05_01.txt"
url = "messages/ALANWESTON/1994/OCEAN05_01.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN05_01.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
