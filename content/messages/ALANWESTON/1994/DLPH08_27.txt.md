+++
linktitle = "DLPH08_27.txt"
title = "DLPH08_27.txt"
url = "messages/ALANWESTON/1994/DLPH08_27.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH08_27.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
