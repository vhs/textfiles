+++
linktitle = "CIS12_21.txt"
title = "CIS12_21.txt"
url = "messages/ALANWESTON/1994/CIS12_21.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS12_21.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
