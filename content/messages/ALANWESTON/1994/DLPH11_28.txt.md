+++
linktitle = "DLPH11_28.txt"
title = "DLPH11_28.txt"
url = "messages/ALANWESTON/1994/DLPH11_28.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH11_28.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
