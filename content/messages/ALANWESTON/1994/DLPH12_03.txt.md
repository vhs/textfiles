+++
linktitle = "DLPH12_03.txt"
title = "DLPH12_03.txt"
url = "messages/ALANWESTON/1994/DLPH12_03.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH12_03.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
