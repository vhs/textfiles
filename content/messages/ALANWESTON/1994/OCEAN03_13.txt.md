+++
linktitle = "OCEAN03_13.txt"
title = "OCEAN03_13.txt"
url = "messages/ALANWESTON/1994/OCEAN03_13.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN03_13.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
