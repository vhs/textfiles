+++
linktitle = "CIS07_20.txt"
title = "CIS07_20.txt"
url = "messages/ALANWESTON/1994/CIS07_20.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS07_20.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
