+++
linktitle = "OCEAN08_23.txt"
title = "OCEAN08_23.txt"
url = "messages/ALANWESTON/1994/OCEAN08_23.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN08_23.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
