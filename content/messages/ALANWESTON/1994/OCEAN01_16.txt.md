+++
linktitle = "OCEAN01_16.txt"
title = "OCEAN01_16.txt"
url = "messages/ALANWESTON/1994/OCEAN01_16.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN01_16.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
