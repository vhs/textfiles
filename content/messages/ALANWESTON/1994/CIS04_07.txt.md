+++
linktitle = "CIS04_07.txt"
title = "CIS04_07.txt"
url = "messages/ALANWESTON/1994/CIS04_07.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS04_07.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
