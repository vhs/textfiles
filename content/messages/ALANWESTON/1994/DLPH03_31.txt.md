+++
linktitle = "DLPH03_31.txt"
title = "DLPH03_31.txt"
url = "messages/ALANWESTON/1994/DLPH03_31.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH03_31.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
