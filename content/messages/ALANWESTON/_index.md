+++
title = "BBS Messages: The Alan Weston Collection"
description = "The Alan Weston BBS Message Collection (1990-1996)"
tabledata = "messages_alanweston"
tablefooter = "There are 7 directories."
+++

In 2005, Alan Weston began selling copies of his collection of BBS textfiles on the Ebay auction site, offering 20 megabytes (uncompressed) of textfiles for $5 plus shipping. Negotiations for a donation were unsuccessful, so I purchased the CD-ROM for a total of $9.50; now you get it for free. Much thanks to Alan for offering this collection to the world, even if there was a toll involved.
