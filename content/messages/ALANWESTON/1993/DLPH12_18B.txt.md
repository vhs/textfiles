+++
linktitle = "DLPH12_18B.txt"
title = "DLPH12_18B.txt"
url = "messages/ALANWESTON/1993/DLPH12_18B.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH12_18B.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
