+++
linktitle = "CIS10_16.txt"
title = "CIS10_16.txt"
url = "messages/ALANWESTON/1993/CIS10_16.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS10_16.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
