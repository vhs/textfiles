+++
linktitle = "OCEAN11_14.txt"
title = "OCEAN11_14.txt"
url = "messages/ALANWESTON/1993/OCEAN11_14.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN11_14.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
