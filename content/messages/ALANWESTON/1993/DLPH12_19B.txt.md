+++
linktitle = "DLPH12_19B.txt"
title = "DLPH12_19B.txt"
url = "messages/ALANWESTON/1993/DLPH12_19B.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH12_19B.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
