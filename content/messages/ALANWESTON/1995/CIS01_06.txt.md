+++
linktitle = "CIS01_06.txt"
title = "CIS01_06.txt"
url = "messages/ALANWESTON/1995/CIS01_06.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS01_06.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
