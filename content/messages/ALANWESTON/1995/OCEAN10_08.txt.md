+++
linktitle = "OCEAN10_08.txt"
title = "OCEAN10_08.txt"
url = "messages/ALANWESTON/1995/OCEAN10_08.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN10_08.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
