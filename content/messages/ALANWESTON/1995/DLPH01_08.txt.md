+++
linktitle = "DLPH01_08.txt"
title = "DLPH01_08.txt"
url = "messages/ALANWESTON/1995/DLPH01_08.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH01_08.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
