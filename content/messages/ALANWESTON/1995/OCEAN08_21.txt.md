+++
linktitle = "OCEAN08_21.txt"
title = "OCEAN08_21.txt"
url = "messages/ALANWESTON/1995/OCEAN08_21.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN08_21.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
