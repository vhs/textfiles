+++
linktitle = "DLPH02_26.txt"
title = "DLPH02_26.txt"
url = "messages/ALANWESTON/1995/DLPH02_26.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DLPH02_26.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
