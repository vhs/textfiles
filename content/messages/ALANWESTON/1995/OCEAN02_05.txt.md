+++
linktitle = "OCEAN02_05.txt"
title = "OCEAN02_05.txt"
url = "messages/ALANWESTON/1995/OCEAN02_05.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OCEAN02_05.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
