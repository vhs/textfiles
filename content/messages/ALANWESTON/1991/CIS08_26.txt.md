+++
linktitle = "CIS08_26.txt"
title = "CIS08_26.txt"
url = "messages/ALANWESTON/1991/CIS08_26.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CIS08_26.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
