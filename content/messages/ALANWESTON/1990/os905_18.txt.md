+++
linktitle = "os905_18.txt"
title = "os905_18.txt"
url = "messages/ALANWESTON/1990/os905_18.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OS905_18.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
