+++
title = "Jason Scott's Top 100 Textfiles"
description = "No time to browse? Read my favorite 100"
tabledata = "top100"
tablefooter = "There are 100 files for a total of 2,853,285 bytes."
tablehighlight = true
+++

It's perfectly understandable that someone finding textfiles.com for the first time wouldn't be interested in sifting through thousands of textfiles to get an idea of what the site is about. For this reason, I've selected a "best of" collection of one hundred textfiles that I think capture the spirit of this site and the unique culture that it attempts to preserve.

While in many cases, there are slicker or longer examples of these files, I felt that these specific examples best captured the genre they belonged to. I invite you to browse through this section, read up, and if you find something that intrigues you, to read more about it in the bulk of the site.

These files are arranged alphabetically, not in any order of importance.
