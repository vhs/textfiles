+++
linktitle = "stevenson-moral-638.txt"
title = "stevenson-moral-638.txt"
url = "etext/AUTHORS/STEVENSON/stevenson-moral-638.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download STEVENSON-MORAL-638.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
