+++
linktitle = "milton-on-525.txt"
title = "milton-on-525.txt"
url = "etext/AUTHORS/MILTON/milton-on-525.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-ON-525.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
