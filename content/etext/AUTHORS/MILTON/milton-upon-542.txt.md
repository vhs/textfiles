+++
linktitle = "milton-upon-542.txt"
title = "milton-upon-542.txt"
url = "etext/AUTHORS/MILTON/milton-upon-542.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-UPON-542.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
