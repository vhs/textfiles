+++
linktitle = "milton-samson-534.txt"
title = "milton-samson-534.txt"
url = "etext/AUTHORS/MILTON/milton-samson-534.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-SAMSON-534.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
