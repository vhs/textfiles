+++
linktitle = "milton-lycidas-524.txt"
title = "milton-lycidas-524.txt"
url = "etext/AUTHORS/MILTON/milton-lycidas-524.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-LYCIDAS-524.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
