+++
linktitle = "milton-at-519.txt"
title = "milton-at-519.txt"
url = "etext/AUTHORS/MILTON/milton-at-519.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-AT-519.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
