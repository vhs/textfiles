+++
linktitle = "milton-l-523.txt"
title = "milton-l-523.txt"
url = "etext/AUTHORS/MILTON/milton-l-523.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-L-523.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
