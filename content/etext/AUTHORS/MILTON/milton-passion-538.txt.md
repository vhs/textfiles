+++
linktitle = "milton-passion-538.txt"
title = "milton-passion-538.txt"
url = "etext/AUTHORS/MILTON/milton-passion-538.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-PASSION-538.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
