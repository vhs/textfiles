+++
linktitle = "milton-fifth-537.txt"
title = "milton-fifth-537.txt"
url = "etext/AUTHORS/MILTON/milton-fifth-537.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-FIFTH-537.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
