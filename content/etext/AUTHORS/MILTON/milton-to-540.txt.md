+++
linktitle = "milton-to-540.txt"
title = "milton-to-540.txt"
url = "etext/AUTHORS/MILTON/milton-to-540.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-TO-540.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
