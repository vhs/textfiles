+++
linktitle = "milton-sonnets-536.txt"
title = "milton-sonnets-536.txt"
url = "etext/AUTHORS/MILTON/milton-sonnets-536.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-SONNETS-536.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
