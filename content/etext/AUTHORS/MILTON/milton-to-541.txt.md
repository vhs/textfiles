+++
linktitle = "milton-to-541.txt"
title = "milton-to-541.txt"
url = "etext/AUTHORS/MILTON/milton-to-541.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-TO-541.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
