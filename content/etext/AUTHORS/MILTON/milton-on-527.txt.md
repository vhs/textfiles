+++
linktitle = "milton-on-527.txt"
title = "milton-on-527.txt"
url = "etext/AUTHORS/MILTON/milton-on-527.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-ON-527.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
