+++
linktitle = "milton-comus-521.txt"
title = "milton-comus-521.txt"
url = "etext/AUTHORS/MILTON/milton-comus-521.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-COMUS-521.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
