+++
linktitle = "milton-another-517.txt"
title = "milton-another-517.txt"
url = "etext/AUTHORS/MILTON/milton-another-517.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-ANOTHER-517.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
