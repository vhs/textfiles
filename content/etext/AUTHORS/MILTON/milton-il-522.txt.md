+++
linktitle = "milton-il-522.txt"
title = "milton-il-522.txt"
url = "etext/AUTHORS/MILTON/milton-il-522.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MILTON-IL-522.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
