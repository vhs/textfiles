+++
linktitle = "burroughs-gods-320.txt"
title = "burroughs-gods-320.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-gods-320.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-GODS-320.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
