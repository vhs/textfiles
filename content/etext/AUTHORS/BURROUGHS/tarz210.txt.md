+++
linktitle = "tarz210.txt"
title = "tarz210.txt"
url = "etext/AUTHORS/BURROUGHS/tarz210.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TARZ210.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
