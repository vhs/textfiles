+++
linktitle = "tarz610.txt"
title = "tarz610.txt"
url = "etext/AUTHORS/BURROUGHS/tarz610.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TARZ610.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
