+++
linktitle = "burroughs-warlord-364.txt"
title = "burroughs-warlord-364.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-warlord-364.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-WARLORD-364.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
