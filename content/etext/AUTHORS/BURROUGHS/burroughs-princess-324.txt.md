+++
linktitle = "burroughs-princess-324.txt"
title = "burroughs-princess-324.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-princess-324.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-PRINCESS-324.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
