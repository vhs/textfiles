+++
linktitle = "tarz410.txt"
title = "tarz410.txt"
url = "etext/AUTHORS/BURROUGHS/tarz410.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TARZ410.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
