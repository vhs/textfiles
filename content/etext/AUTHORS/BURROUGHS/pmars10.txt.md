+++
linktitle = "pmars10.txt"
title = "pmars10.txt"
url = "etext/AUTHORS/BURROUGHS/pmars10.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PMARS10.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
