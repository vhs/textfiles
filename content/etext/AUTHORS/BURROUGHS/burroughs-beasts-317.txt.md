+++
linktitle = "burroughs-beasts-317.txt"
title = "burroughs-beasts-317.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-beasts-317.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-BEASTS-317.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
