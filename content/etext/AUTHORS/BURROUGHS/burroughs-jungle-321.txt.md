+++
linktitle = "burroughs-jungle-321.txt"
title = "burroughs-jungle-321.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-jungle-321.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-JUNGLE-321.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
