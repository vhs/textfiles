+++
linktitle = "wmars10.txt"
title = "wmars10.txt"
url = "etext/AUTHORS/BURROUGHS/wmars10.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WMARS10.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
