+++
linktitle = "gmars11.txt"
title = "gmars11.txt"
url = "etext/AUTHORS/BURROUGHS/gmars11.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GMARS11.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
