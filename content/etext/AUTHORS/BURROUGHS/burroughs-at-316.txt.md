+++
linktitle = "burroughs-at-316.txt"
title = "burroughs-at-316.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-at-316.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-AT-316.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
