+++
linktitle = "burroughs-monster-323.txt"
title = "burroughs-monster-323.txt"
url = "etext/AUTHORS/BURROUGHS/burroughs-monster-323.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BURROUGHS-MONSTER-323.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
