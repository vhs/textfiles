+++
linktitle = "hawthorne-dr-468.txt"
title = "hawthorne-dr-468.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-dr-468.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-DR-468.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
