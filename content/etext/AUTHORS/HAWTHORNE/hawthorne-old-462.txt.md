+++
linktitle = "hawthorne-old-462.txt"
title = "hawthorne-old-462.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-old-462.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-OLD-462.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
