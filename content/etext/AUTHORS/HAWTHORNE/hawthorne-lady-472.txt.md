+++
linktitle = "hawthorne-lady-472.txt"
title = "hawthorne-lady-472.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-lady-472.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-LADY-472.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
