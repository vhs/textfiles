+++
linktitle = "hawthorne-young-465.txt"
title = "hawthorne-young-465.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-young-465.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-YOUNG-465.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
