+++
linktitle = "hawthorne-birthmark-459.txt"
title = "hawthorne-birthmark-459.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-birthmark-459.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-BIRTHMARK-459.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
