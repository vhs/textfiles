+++
linktitle = "hawthorne-my-475.txt"
title = "hawthorne-my-475.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-my-475.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-MY-475.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
