+++
linktitle = "hawthorne-ambitious-467.txt"
title = "hawthorne-ambitious-467.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-ambitious-467.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-AMBITIOUS-467.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
