+++
linktitle = "hawthorne-wedding-480.txt"
title = "hawthorne-wedding-480.txt"
url = "etext/AUTHORS/HAWTHORNE/hawthorne-wedding-480.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HAWTHORNE-WEDDING-480.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
