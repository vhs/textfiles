+++
linktitle = "shakespeare-life-56.txt"
title = "shakespeare-life-56.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-life-56.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-LIFE-56.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
