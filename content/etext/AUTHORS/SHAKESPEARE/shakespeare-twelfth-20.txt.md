+++
linktitle = "shakespeare-twelfth-20.txt"
title = "shakespeare-twelfth-20.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-twelfth-20.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-TWELFTH-20.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
