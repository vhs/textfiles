+++
linktitle = "synopsis.001"
title = "synopsis.001"
url = "etext/AUTHORS/SHAKESPEARE/synopsis.001.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYNOPSIS.001 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
