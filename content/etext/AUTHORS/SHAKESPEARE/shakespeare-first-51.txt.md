+++
linktitle = "shakespeare-first-51.txt"
title = "shakespeare-first-51.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-first-51.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-FIRST-51.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
