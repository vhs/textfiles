+++
linktitle = "synopsis.004"
title = "synopsis.004"
url = "etext/AUTHORS/SHAKESPEARE/synopsis.004.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYNOPSIS.004 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
