+++
linktitle = "shakespeare-lovers-62.txt"
title = "shakespeare-lovers-62.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-lovers-62.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-LOVERS-62.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
