+++
linktitle = "spencer-epithalamion-190.txt"
title = "spencer-epithalamion-190.txt"
url = "etext/AUTHORS/SHAKESPEARE/spencer-epithalamion-190.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SPENCER-EPITHALAMION-190.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
