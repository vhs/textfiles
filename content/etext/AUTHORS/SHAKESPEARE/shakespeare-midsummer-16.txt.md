+++
linktitle = "shakespeare-midsummer-16.txt"
title = "shakespeare-midsummer-16.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-midsummer-16.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-MIDSUMMER-16.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
