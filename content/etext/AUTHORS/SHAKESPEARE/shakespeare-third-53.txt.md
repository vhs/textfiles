+++
linktitle = "shakespeare-third-53.txt"
title = "shakespeare-third-53.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-third-53.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-THIRD-53.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
