+++
linktitle = "synopsis.002"
title = "synopsis.002"
url = "etext/AUTHORS/SHAKESPEARE/synopsis.002.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYNOPSIS.002 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
