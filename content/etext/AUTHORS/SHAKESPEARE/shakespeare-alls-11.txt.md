+++
linktitle = "shakespeare-alls-11.txt"
title = "shakespeare-alls-11.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-alls-11.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-ALLS-11.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
