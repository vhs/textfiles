+++
linktitle = "shakespeare-merchant-5.txt"
title = "shakespeare-merchant-5.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-merchant-5.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-MERCHANT-5.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
