+++
linktitle = "shakespeare-pericles-21.txt"
title = "shakespeare-pericles-21.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-pericles-21.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-PERICLES-21.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
