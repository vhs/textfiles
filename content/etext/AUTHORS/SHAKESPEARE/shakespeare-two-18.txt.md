+++
linktitle = "shakespeare-two-18.txt"
title = "shakespeare-two-18.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-two-18.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-TWO-18.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
