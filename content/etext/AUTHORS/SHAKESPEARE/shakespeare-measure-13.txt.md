+++
linktitle = "shakespeare-measure-13.txt"
title = "shakespeare-measure-13.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-measure-13.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-MEASURE-13.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
