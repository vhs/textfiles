+++
linktitle = "shakespeare-merry-15.txt"
title = "shakespeare-merry-15.txt"
url = "etext/AUTHORS/SHAKESPEARE/shakespeare-merry-15.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHAKESPEARE-MERRY-15.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
