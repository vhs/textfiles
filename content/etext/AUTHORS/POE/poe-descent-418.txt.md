+++
linktitle = "poe-descent-418.txt"
title = "poe-descent-418.txt"
url = "etext/AUTHORS/POE/poe-descent-418.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POE-DESCENT-418.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
