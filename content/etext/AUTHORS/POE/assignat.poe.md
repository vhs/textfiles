+++
linktitle = "assignat.poe"
title = "assignat.poe"
url = "etext/AUTHORS/POE/assignat.poe.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ASSIGNAT.POE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
