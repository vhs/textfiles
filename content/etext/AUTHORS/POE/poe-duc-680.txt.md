+++
linktitle = "poe-duc-680.txt"
title = "poe-duc-680.txt"
url = "etext/AUTHORS/POE/poe-duc-680.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POE-DUC-680.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
