+++
linktitle = "poe-lake-688.txt"
title = "poe-lake-688.txt"
url = "etext/AUTHORS/POE/poe-lake-688.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download POE-LAKE-688.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
