+++
linktitle = "wilde-sphinx-616.txt"
title = "wilde-sphinx-616.txt"
url = "etext/AUTHORS/WILDE/wilde-sphinx-616.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WILDE-SPHINX-616.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
