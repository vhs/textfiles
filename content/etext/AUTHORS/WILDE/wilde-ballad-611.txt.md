+++
linktitle = "wilde-ballad-611.txt"
title = "wilde-ballad-611.txt"
url = "etext/AUTHORS/WILDE/wilde-ballad-611.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WILDE-BALLAD-611.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
