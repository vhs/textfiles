+++
linktitle = "wilde-fourth-613.txt"
title = "wilde-fourth-613.txt"
url = "etext/AUTHORS/WILDE/wilde-fourth-613.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WILDE-FOURTH-613.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
