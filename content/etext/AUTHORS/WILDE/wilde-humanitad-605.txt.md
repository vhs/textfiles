+++
linktitle = "wilde-humanitad-605.txt"
title = "wilde-humanitad-605.txt"
url = "etext/AUTHORS/WILDE/wilde-humanitad-605.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WILDE-HUMANITAD-605.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
