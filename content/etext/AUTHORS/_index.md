+++
title = "ETEXT.TEXTFILES.COM: AUTHORS"
headingtext = "Authors"
description = "Collections of transcribed books and novels written by celebrated historical authors."
tabledata = "etext_authors"
tablefooter = "There are 17 directories."
+++
