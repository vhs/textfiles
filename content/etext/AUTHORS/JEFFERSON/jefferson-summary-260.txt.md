+++
linktitle = "jefferson-summary-260.txt"
title = "jefferson-summary-260.txt"
url = "etext/AUTHORS/JEFFERSON/jefferson-summary-260.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEFFERSON-SUMMARY-260.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
