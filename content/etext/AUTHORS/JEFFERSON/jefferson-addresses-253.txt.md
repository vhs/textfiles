+++
linktitle = "jefferson-addresses-253.txt"
title = "jefferson-addresses-253.txt"
url = "etext/AUTHORS/JEFFERSON/jefferson-addresses-253.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEFFERSON-ADDRESSES-253.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
