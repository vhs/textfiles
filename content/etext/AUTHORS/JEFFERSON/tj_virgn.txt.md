+++
linktitle = "tj_virgn.txt"
title = "tj_virgn.txt"
url = "etext/AUTHORS/JEFFERSON/tj_virgn.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TJ_VIRGN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
