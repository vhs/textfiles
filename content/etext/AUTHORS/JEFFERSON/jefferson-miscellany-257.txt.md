+++
linktitle = "jefferson-miscellany-257.txt"
title = "jefferson-miscellany-257.txt"
url = "etext/AUTHORS/JEFFERSON/jefferson-miscellany-257.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEFFERSON-MISCELLANY-257.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
