+++
linktitle = "jefferson-notes-258.txt"
title = "jefferson-notes-258.txt"
url = "etext/AUTHORS/JEFFERSON/jefferson-notes-258.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEFFERSON-NOTES-258.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
