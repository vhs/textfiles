+++
linktitle = "jefferson-public-259.txt"
title = "jefferson-public-259.txt"
url = "etext/AUTHORS/JEFFERSON/jefferson-public-259.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEFFERSON-PUBLIC-259.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
