+++
linktitle = "aristotle-categories-79.txt"
title = "aristotle-categories-79.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-categories-79.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-CATEGORIES-79.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
