+++
linktitle = "aristotle-on-269.txt"
title = "aristotle-on-269.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-on-269.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-ON-269.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
