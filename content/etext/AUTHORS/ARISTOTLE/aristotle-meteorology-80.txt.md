+++
linktitle = "aristotle-meteorology-80.txt"
title = "aristotle-meteorology-80.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-meteorology-80.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-METEOROLOGY-80.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
