+++
linktitle = "aristotle-nicomachean-81.txt"
title = "aristotle-nicomachean-81.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-nicomachean-81.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-NICOMACHEAN-81.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
