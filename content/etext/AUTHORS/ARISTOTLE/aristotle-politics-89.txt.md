+++
linktitle = "aristotle-politics-89.txt"
title = "aristotle-politics-89.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-politics-89.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-POLITICS-89.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
