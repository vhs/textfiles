+++
linktitle = "aristotle-on-265.txt"
title = "aristotle-on-265.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-on-265.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-ON-265.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
