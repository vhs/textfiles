+++
linktitle = "aristotle-history-78.txt"
title = "aristotle-history-78.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-history-78.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-HISTORY-78.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
