+++
linktitle = "aristotle-metaphysics-77.txt"
title = "aristotle-metaphysics-77.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-metaphysics-77.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-METAPHYSICS-77.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
