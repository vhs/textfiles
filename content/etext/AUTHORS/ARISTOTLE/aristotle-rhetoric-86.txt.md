+++
linktitle = "aristotle-rhetoric-86.txt"
title = "aristotle-rhetoric-86.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-rhetoric-86.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-RHETORIC-86.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
