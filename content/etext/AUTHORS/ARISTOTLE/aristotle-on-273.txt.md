+++
linktitle = "aristotle-on-273.txt"
title = "aristotle-on-273.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-on-273.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-ON-273.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
