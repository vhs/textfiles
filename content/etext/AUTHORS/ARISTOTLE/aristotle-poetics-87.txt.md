+++
linktitle = "aristotle-poetics-87.txt"
title = "aristotle-poetics-87.txt"
url = "etext/AUTHORS/ARISTOTLE/aristotle-poetics-87.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARISTOTLE-POETICS-87.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
