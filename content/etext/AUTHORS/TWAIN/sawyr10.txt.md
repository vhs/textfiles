+++
linktitle = "sawyr10.txt"
title = "sawyr10.txt"
url = "etext/AUTHORS/TWAIN/sawyr10.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SAWYR10.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
