+++
linktitle = "twain-mark-414.txt"
title = "twain-mark-414.txt"
url = "etext/AUTHORS/TWAIN/twain-mark-414.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TWAIN-MARK-414.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
