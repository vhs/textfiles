+++
linktitle = "sawy311.txt"
title = "sawy311.txt"
url = "etext/AUTHORS/TWAIN/sawy311.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SAWY311.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
