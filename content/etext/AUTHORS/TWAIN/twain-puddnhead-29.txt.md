+++
linktitle = "twain-puddnhead-29.txt"
title = "twain-puddnhead-29.txt"
url = "etext/AUTHORS/TWAIN/twain-puddnhead-29.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TWAIN-PUDDNHEAD-29.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
