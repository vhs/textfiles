+++
linktitle = "sawy210.txt"
title = "sawy210.txt"
url = "etext/AUTHORS/TWAIN/sawy210.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SAWY210.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
