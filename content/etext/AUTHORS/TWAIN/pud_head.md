+++
linktitle = "pud_head"
title = "pud_head"
url = "etext/AUTHORS/TWAIN/pud_head.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PUD_HEAD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
