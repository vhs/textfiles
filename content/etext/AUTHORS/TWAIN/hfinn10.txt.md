+++
linktitle = "hfinn10.txt"
title = "hfinn10.txt"
url = "etext/AUTHORS/TWAIN/hfinn10.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HFINN10.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
