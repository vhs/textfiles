+++
linktitle = "twain-what-42.txt"
title = "twain-what-42.txt"
url = "etext/AUTHORS/TWAIN/twain-what-42.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TWAIN-WHAT-42.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
