+++
linktitle = "prince_pauper"
title = "prince_pauper"
url = "etext/AUTHORS/TWAIN/prince_pauper.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRINCE_PAUPER textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
