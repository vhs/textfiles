+++
linktitle = "twain-adventures-28.txt"
title = "twain-adventures-28.txt"
url = "etext/AUTHORS/TWAIN/twain-adventures-28.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TWAIN-ADVENTURES-28.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
