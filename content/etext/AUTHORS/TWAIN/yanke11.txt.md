+++
linktitle = "yanke11.txt"
title = "yanke11.txt"
url = "etext/AUTHORS/TWAIN/yanke11.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download YANKE11.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
