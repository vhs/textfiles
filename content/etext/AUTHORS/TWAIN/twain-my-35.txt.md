+++
linktitle = "twain-my-35.txt"
title = "twain-my-35.txt"
url = "etext/AUTHORS/TWAIN/twain-my-35.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TWAIN-MY-35.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
