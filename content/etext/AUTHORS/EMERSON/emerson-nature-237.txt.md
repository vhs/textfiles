+++
linktitle = "emerson-nature-237.txt"
title = "emerson-nature-237.txt"
url = "etext/AUTHORS/EMERSON/emerson-nature-237.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EMERSON-NATURE-237.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
