+++
linktitle = "emerson-representative-238.txt"
title = "emerson-representative-238.txt"
url = "etext/AUTHORS/EMERSON/emerson-representative-238.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EMERSON-REPRESENTATIVE-238.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
