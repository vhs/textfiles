+++
linktitle = "emerson-transcendentalist-239.txt"
title = "emerson-transcendentalist-239.txt"
url = "etext/AUTHORS/EMERSON/emerson-transcendentalist-239.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EMERSON-TRANSCENDENTALIST-239.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
