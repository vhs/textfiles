+++
linktitle = "emerson-young-241.txt"
title = "emerson-young-241.txt"
url = "etext/AUTHORS/EMERSON/emerson-young-241.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EMERSON-YOUNG-241.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
