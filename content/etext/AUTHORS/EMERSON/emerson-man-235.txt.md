+++
linktitle = "emerson-man-235.txt"
title = "emerson-man-235.txt"
url = "etext/AUTHORS/EMERSON/emerson-man-235.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EMERSON-MAN-235.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
