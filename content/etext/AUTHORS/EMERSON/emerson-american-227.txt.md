+++
linktitle = "emerson-american-227.txt"
title = "emerson-american-227.txt"
url = "etext/AUTHORS/EMERSON/emerson-american-227.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EMERSON-AMERICAN-227.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
