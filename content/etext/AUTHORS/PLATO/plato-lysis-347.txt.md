+++
linktitle = "plato-lysis-347.txt"
title = "plato-lysis-347.txt"
url = "etext/AUTHORS/PLATO/plato-lysis-347.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-LYSIS-347.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
