+++
linktitle = "plato-laches-345.txt"
title = "plato-laches-345.txt"
url = "etext/AUTHORS/PLATO/plato-laches-345.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-LACHES-345.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
