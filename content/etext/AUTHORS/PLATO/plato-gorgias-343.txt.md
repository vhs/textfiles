+++
linktitle = "plato-gorgias-343.txt"
title = "plato-gorgias-343.txt"
url = "etext/AUTHORS/PLATO/plato-gorgias-343.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-GORGIAS-343.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
