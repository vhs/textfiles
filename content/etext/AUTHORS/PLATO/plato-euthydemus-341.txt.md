+++
linktitle = "plato-euthydemus-341.txt"
title = "plato-euthydemus-341.txt"
url = "etext/AUTHORS/PLATO/plato-euthydemus-341.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-EUTHYDEMUS-341.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
