+++
linktitle = "plato-euthyphro-342.txt"
title = "plato-euthyphro-342.txt"
url = "etext/AUTHORS/PLATO/plato-euthyphro-342.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-EUTHYPHRO-342.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
