+++
linktitle = "plato-meno-348.txt"
title = "plato-meno-348.txt"
url = "etext/AUTHORS/PLATO/plato-meno-348.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-MENO-348.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
