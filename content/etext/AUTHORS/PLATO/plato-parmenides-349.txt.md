+++
linktitle = "plato-parmenides-349.txt"
title = "plato-parmenides-349.txt"
url = "etext/AUTHORS/PLATO/plato-parmenides-349.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-PARMENIDES-349.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
