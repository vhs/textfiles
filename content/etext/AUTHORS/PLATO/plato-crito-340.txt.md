+++
linktitle = "plato-crito-340.txt"
title = "plato-crito-340.txt"
url = "etext/AUTHORS/PLATO/plato-crito-340.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-CRITO-340.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
