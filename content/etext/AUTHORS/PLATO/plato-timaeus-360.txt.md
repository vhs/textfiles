+++
linktitle = "plato-timaeus-360.txt"
title = "plato-timaeus-360.txt"
url = "etext/AUTHORS/PLATO/plato-timaeus-360.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-TIMAEUS-360.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
