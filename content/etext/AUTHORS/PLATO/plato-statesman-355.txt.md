+++
linktitle = "plato-statesman-355.txt"
title = "plato-statesman-355.txt"
url = "etext/AUTHORS/PLATO/plato-statesman-355.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-STATESMAN-355.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
