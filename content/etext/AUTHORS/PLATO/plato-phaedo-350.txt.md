+++
linktitle = "plato-phaedo-350.txt"
title = "plato-phaedo-350.txt"
url = "etext/AUTHORS/PLATO/plato-phaedo-350.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-PHAEDO-350.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
