+++
linktitle = "plato-sophist-354.txt"
title = "plato-sophist-354.txt"
url = "etext/AUTHORS/PLATO/plato-sophist-354.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-SOPHIST-354.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
