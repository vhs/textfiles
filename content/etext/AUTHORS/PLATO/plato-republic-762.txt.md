+++
linktitle = "plato-republic-762.txt"
title = "plato-republic-762.txt"
url = "etext/AUTHORS/PLATO/plato-republic-762.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-REPUBLIC-762.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
