+++
linktitle = "plato-charmides-337.txt"
title = "plato-charmides-337.txt"
url = "etext/AUTHORS/PLATO/plato-charmides-337.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-CHARMIDES-337.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
