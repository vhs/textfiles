+++
linktitle = "plato-protagoras-353.txt"
title = "plato-protagoras-353.txt"
url = "etext/AUTHORS/PLATO/plato-protagoras-353.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-PROTAGORAS-353.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
