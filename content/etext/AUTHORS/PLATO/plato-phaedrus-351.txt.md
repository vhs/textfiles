+++
linktitle = "plato-phaedrus-351.txt"
title = "plato-phaedrus-351.txt"
url = "etext/AUTHORS/PLATO/plato-phaedrus-351.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-PHAEDRUS-351.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
