+++
linktitle = "plato-critias-339.txt"
title = "plato-critias-339.txt"
url = "etext/AUTHORS/PLATO/plato-critias-339.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-CRITIAS-339.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
