+++
linktitle = "plato-seventh-358.txt"
title = "plato-seventh-358.txt"
url = "etext/AUTHORS/PLATO/plato-seventh-358.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-SEVENTH-358.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
