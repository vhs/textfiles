+++
linktitle = "plato-theaetetus-359.txt"
title = "plato-theaetetus-359.txt"
url = "etext/AUTHORS/PLATO/plato-theaetetus-359.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-THEAETETUS-359.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
