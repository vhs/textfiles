+++
linktitle = "plato-cratylus-338.txt"
title = "plato-cratylus-338.txt"
url = "etext/AUTHORS/PLATO/plato-cratylus-338.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PLATO-CRATYLUS-338.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
