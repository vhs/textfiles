+++
linktitle = "keats-for-485.txt"
title = "keats-for-485.txt"
url = "etext/AUTHORS/KEATS/keats-for-485.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-FOR-485.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
