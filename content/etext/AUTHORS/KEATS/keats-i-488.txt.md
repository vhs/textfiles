+++
linktitle = "keats-i-488.txt"
title = "keats-i-488.txt"
url = "etext/AUTHORS/KEATS/keats-i-488.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-I-488.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
