+++
linktitle = "keats-on-500.txt"
title = "keats-on-500.txt"
url = "etext/AUTHORS/KEATS/keats-on-500.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-ON-500.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
