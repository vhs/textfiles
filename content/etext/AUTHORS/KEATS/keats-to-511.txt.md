+++
linktitle = "keats-to-511.txt"
title = "keats-to-511.txt"
url = "etext/AUTHORS/KEATS/keats-to-511.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-TO-511.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
