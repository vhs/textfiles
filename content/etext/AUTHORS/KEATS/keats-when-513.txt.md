+++
linktitle = "keats-when-513.txt"
title = "keats-when-513.txt"
url = "etext/AUTHORS/KEATS/keats-when-513.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-WHEN-513.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
