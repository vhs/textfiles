+++
linktitle = "keats-o-492.txt"
title = "keats-o-492.txt"
url = "etext/AUTHORS/KEATS/keats-o-492.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-O-492.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
