+++
linktitle = "keats-endymion-484.txt"
title = "keats-endymion-484.txt"
url = "etext/AUTHORS/KEATS/keats-endymion-484.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-ENDYMION-484.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
