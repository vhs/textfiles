+++
linktitle = "keats-lines-491.txt"
title = "keats-lines-491.txt"
url = "etext/AUTHORS/KEATS/keats-lines-491.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-LINES-491.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
