+++
linktitle = "keats-over-501.txt"
title = "keats-over-501.txt"
url = "etext/AUTHORS/KEATS/keats-over-501.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-OVER-501.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
