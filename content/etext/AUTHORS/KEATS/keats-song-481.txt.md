+++
linktitle = "keats-song-481.txt"
title = "keats-song-481.txt"
url = "etext/AUTHORS/KEATS/keats-song-481.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-SONG-481.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
