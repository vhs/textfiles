+++
linktitle = "keats-day-504.txt"
title = "keats-day-504.txt"
url = "etext/AUTHORS/KEATS/keats-day-504.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-DAY-504.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
