+++
linktitle = "keats-to-508.txt"
title = "keats-to-508.txt"
url = "etext/AUTHORS/KEATS/keats-to-508.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-TO-508.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
