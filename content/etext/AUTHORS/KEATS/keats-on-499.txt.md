+++
linktitle = "keats-on-499.txt"
title = "keats-on-499.txt"
url = "etext/AUTHORS/KEATS/keats-on-499.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-ON-499.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
