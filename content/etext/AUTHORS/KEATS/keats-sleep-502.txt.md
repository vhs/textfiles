+++
linktitle = "keats-sleep-502.txt"
title = "keats-sleep-502.txt"
url = "etext/AUTHORS/KEATS/keats-sleep-502.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-SLEEP-502.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
