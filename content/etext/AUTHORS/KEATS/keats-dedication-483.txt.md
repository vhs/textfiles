+++
linktitle = "keats-dedication-483.txt"
title = "keats-dedication-483.txt"
url = "etext/AUTHORS/KEATS/keats-dedication-483.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-DEDICATION-483.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
