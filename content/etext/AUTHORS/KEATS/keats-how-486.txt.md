+++
linktitle = "keats-how-486.txt"
title = "keats-how-486.txt"
url = "etext/AUTHORS/KEATS/keats-how-486.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-HOW-486.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
