+++
linktitle = "keats-ode-495.txt"
title = "keats-ode-495.txt"
url = "etext/AUTHORS/KEATS/keats-ode-495.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-ODE-495.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
