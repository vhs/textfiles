+++
linktitle = "keats-why-514.txt"
title = "keats-why-514.txt"
url = "etext/AUTHORS/KEATS/keats-why-514.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-WHY-514.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
