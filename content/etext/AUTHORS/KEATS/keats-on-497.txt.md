+++
linktitle = "keats-on-497.txt"
title = "keats-on-497.txt"
url = "etext/AUTHORS/KEATS/keats-on-497.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-ON-497.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
