+++
linktitle = "keats-character-482.txt"
title = "keats-character-482.txt"
url = "etext/AUTHORS/KEATS/keats-character-482.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-CHARACTER-482.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
