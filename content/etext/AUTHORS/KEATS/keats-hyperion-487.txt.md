+++
linktitle = "keats-hyperion-487.txt"
title = "keats-hyperion-487.txt"
url = "etext/AUTHORS/KEATS/keats-hyperion-487.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-HYPERION-487.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
