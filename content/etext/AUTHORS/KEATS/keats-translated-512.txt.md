+++
linktitle = "keats-translated-512.txt"
title = "keats-translated-512.txt"
url = "etext/AUTHORS/KEATS/keats-translated-512.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KEATS-TRANSLATED-512.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
