+++
linktitle = "kant-science-146.txt"
title = "kant-science-146.txt"
url = "etext/AUTHORS/KANT/kant-science-146.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KANT-SCIENCE-146.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
