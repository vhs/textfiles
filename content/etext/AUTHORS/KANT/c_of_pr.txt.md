+++
linktitle = "c_of_pr.txt"
title = "c_of_pr.txt"
url = "etext/AUTHORS/KANT/c_of_pr.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download C_OF_PR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
