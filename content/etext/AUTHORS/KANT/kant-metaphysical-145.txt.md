+++
linktitle = "kant-metaphysical-145.txt"
title = "kant-metaphysical-145.txt"
url = "etext/AUTHORS/KANT/kant-metaphysical-145.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KANT-METAPHYSICAL-145.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
