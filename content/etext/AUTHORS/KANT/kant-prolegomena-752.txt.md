+++
linktitle = "kant-prolegomena-752.txt"
title = "kant-prolegomena-752.txt"
url = "etext/AUTHORS/KANT/kant-prolegomena-752.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download KANT-PROLEGOMENA-752.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
