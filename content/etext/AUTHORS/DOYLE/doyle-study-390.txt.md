+++
linktitle = "doyle-study-390.txt"
title = "doyle-study-390.txt"
url = "etext/AUTHORS/DOYLE/doyle-study-390.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-STUDY-390.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
