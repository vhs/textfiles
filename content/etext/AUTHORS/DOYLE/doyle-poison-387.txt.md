+++
linktitle = "doyle-poison-387.txt"
title = "doyle-poison-387.txt"
url = "etext/AUTHORS/DOYLE/doyle-poison-387.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-POISON-387.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
