+++
linktitle = "doyle-lost-385.txt"
title = "doyle-lost-385.txt"
url = "etext/AUTHORS/DOYLE/doyle-lost-385.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-LOST-385.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
