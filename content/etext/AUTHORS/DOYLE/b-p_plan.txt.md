+++
linktitle = "b-p_plan.txt"
title = "b-p_plan.txt"
url = "etext/AUTHORS/DOYLE/b-p_plan.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download B-P_PLAN.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
