+++
linktitle = "doyle-hound-383.txt"
title = "doyle-hound-383.txt"
url = "etext/AUTHORS/DOYLE/doyle-hound-383.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-HOUND-383.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
