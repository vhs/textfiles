+++
linktitle = "doyle-valley-392.txt"
title = "doyle-valley-392.txt"
url = "etext/AUTHORS/DOYLE/doyle-valley-392.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-VALLEY-392.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
