+++
linktitle = "doyle-adventures-380.txt"
title = "doyle-adventures-380.txt"
url = "etext/AUTHORS/DOYLE/doyle-adventures-380.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-ADVENTURES-380.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
