+++
linktitle = "sussex-v.txt"
title = "sussex-v.txt"
url = "etext/AUTHORS/DOYLE/sussex-v.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUSSEX-V.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
