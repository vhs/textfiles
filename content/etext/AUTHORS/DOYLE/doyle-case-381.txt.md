+++
linktitle = "doyle-case-381.txt"
title = "doyle-case-381.txt"
url = "etext/AUTHORS/DOYLE/doyle-case-381.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-CASE-381.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
