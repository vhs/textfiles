+++
linktitle = "doyle-return-388.txt"
title = "doyle-return-388.txt"
url = "etext/AUTHORS/DOYLE/doyle-return-388.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DOYLE-RETURN-388.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
