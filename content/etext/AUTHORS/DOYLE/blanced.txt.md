+++
linktitle = "blanced.txt"
title = "blanced.txt"
url = "etext/AUTHORS/DOYLE/blanced.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BLANCED.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
