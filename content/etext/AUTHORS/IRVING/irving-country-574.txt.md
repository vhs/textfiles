+++
linktitle = "irving-country-574.txt"
title = "irving-country-574.txt"
url = "etext/AUTHORS/IRVING/irving-country-574.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-COUNTRY-574.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
