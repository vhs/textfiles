+++
linktitle = "irving-royal-585.txt"
title = "irving-royal-585.txt"
url = "etext/AUTHORS/IRVING/irving-royal-585.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-ROYAL-585.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
