+++
linktitle = "irving-rural-584.txt"
title = "irving-rural-584.txt"
url = "etext/AUTHORS/IRVING/irving-rural-584.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-RURAL-584.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
