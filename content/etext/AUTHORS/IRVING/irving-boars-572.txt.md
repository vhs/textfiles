+++
linktitle = "irving-boars-572.txt"
title = "irving-boars-572.txt"
url = "etext/AUTHORS/IRVING/irving-boars-572.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-BOARS-572.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
