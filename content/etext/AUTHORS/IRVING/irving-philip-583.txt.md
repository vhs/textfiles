+++
linktitle = "irving-philip-583.txt"
title = "irving-philip-583.txt"
url = "etext/AUTHORS/IRVING/irving-philip-583.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-PHILIP-583.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
