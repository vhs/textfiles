+++
linktitle = "irving-sunday-591.txt"
title = "irving-sunday-591.txt"
url = "etext/AUTHORS/IRVING/irving-sunday-591.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-SUNDAY-591.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
