+++
linktitle = "irving-roscoe-587.txt"
title = "irving-roscoe-587.txt"
url = "etext/AUTHORS/IRVING/irving-roscoe-587.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-ROSCOE-587.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
