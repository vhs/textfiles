+++
linktitle = "irving-wife-596.txt"
title = "irving-wife-596.txt"
url = "etext/AUTHORS/IRVING/irving-wife-596.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-WIFE-596.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
