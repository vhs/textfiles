+++
linktitle = "irving-widow-595.txt"
title = "irving-widow-595.txt"
url = "etext/AUTHORS/IRVING/irving-widow-595.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-WIDOW-595.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
