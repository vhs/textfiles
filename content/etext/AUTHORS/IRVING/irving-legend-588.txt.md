+++
linktitle = "irving-legend-588.txt"
title = "irving-legend-588.txt"
url = "etext/AUTHORS/IRVING/irving-legend-588.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-LEGEND-588.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
