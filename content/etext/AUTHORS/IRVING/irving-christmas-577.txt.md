+++
linktitle = "irving-christmas-577.txt"
title = "irving-christmas-577.txt"
url = "etext/AUTHORS/IRVING/irving-christmas-577.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-CHRISTMAS-577.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
