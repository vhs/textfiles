+++
linktitle = "irving-stratford-590.txt"
title = "irving-stratford-590.txt"
url = "etext/AUTHORS/IRVING/irving-stratford-590.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-STRATFORD-590.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
