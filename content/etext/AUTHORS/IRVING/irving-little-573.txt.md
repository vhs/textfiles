+++
linktitle = "irving-little-573.txt"
title = "irving-little-573.txt"
url = "etext/AUTHORS/IRVING/irving-little-573.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-LITTLE-573.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
