+++
linktitle = "irving-inn-581.txt"
title = "irving-inn-581.txt"
url = "etext/AUTHORS/IRVING/irving-inn-581.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-INN-581.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
