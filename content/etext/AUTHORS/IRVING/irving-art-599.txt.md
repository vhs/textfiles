+++
linktitle = "irving-art-599.txt"
title = "irving-art-599.txt"
url = "etext/AUTHORS/IRVING/irving-art-599.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-ART-599.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
