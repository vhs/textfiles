+++
linktitle = "irving-mutability-582.txt"
title = "irving-mutability-582.txt"
url = "etext/AUTHORS/IRVING/irving-mutability-582.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-MUTABILITY-582.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
