+++
linktitle = "irving-rural-579.txt"
title = "irving-rural-579.txt"
url = "etext/AUTHORS/IRVING/irving-rural-579.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-RURAL-579.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
