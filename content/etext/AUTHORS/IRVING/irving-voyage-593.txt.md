+++
linktitle = "irving-voyage-593.txt"
title = "irving-voyage-593.txt"
url = "etext/AUTHORS/IRVING/irving-voyage-593.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-VOYAGE-593.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
