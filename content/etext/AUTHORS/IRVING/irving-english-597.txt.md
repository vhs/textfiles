+++
linktitle = "irving-english-597.txt"
title = "irving-english-597.txt"
url = "etext/AUTHORS/IRVING/irving-english-597.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-ENGLISH-597.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
