+++
linktitle = "irving-london-598.txt"
title = "irving-london-598.txt"
url = "etext/AUTHORS/IRVING/irving-london-598.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download IRVING-LONDON-598.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
