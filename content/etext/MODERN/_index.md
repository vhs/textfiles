+++
title = "ETEXT.TEXTFILES.COM: MODERN"
headingtext = "Modern"
description = "Books and other transcribed works from authors of the last few decades."
tabledata = "etext_modern"
tablefooter = "There are 33 files for a total of 9,897,094 bytes."
+++
