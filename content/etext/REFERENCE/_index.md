+++
title = "ETEXT.TEXTFILES.COM: REFERENCE"
headingtext = "Reference"
description = "Reference books, aimed to be accurate representations, now saved for later generations."
tabledata = "etext_reference"
tablefooter = "There are 52 files for a total of 44,657,141 bytes."
+++
