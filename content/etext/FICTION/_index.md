+++
title = "ETEXT.TEXTFILES.COM: FICTION"
headingtext = "Fiction"
description = "Stories and novels considered fiction. Bestsellers. Authors with only a few transcribed work end up in here."
tabledata = "etext_fiction"
tablefooter = "There are 328 files for a total of 157,494,070 bytes."
+++
