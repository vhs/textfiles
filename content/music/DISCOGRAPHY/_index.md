+++
title = "Music Files: Discographies"
description = "Collection of Artist Discographies"
tabledata = "music_discography"
tablefooter = "There are 47 files for a total of 1,450,593 bytes."
+++

One of the real benefits that filtered down from the Internet was the ability of music fans around the world to come together and really compare notes as to what album and single releases a band REALLY came out with for the past years. While some countries might put out an album with a certain song list, others might not opt to release the album at all. Through the hard work of these fans, they found that this communications network let them comprise highly accurate lists of releases.

One individual who can't be discounted or forgotten is John Relph, who assembled the majority of these discographies and still maintains them. His work shines through.
