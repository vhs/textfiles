+++
linktitle = "sonic-y.dis"
title = "sonic-y.dis"
url = "music/DISCOGRAPHY/sonic-y.dis.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SONIC-Y.DIS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
