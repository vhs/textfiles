+++
linktitle = "genesis.dis"
title = "genesis.dis"
url = "music/DISCOGRAPHY/genesis.dis.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GENESIS.DIS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
