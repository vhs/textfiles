+++
linktitle = "metallic.adi"
title = "metallic.adi"
url = "music/DISCOGRAPHY/metallic.adi.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download METALLIC.ADI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
