+++
linktitle = "anti.metal.punk"
title = "anti.metal.punk"
url = "music/anti.metal.punk.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANTI.METAL.PUNK textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
