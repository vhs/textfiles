+++
linktitle = "syd_roio.txt"
title = "syd_roio.txt"
url = "music/PINKFLOYD/syd_roio.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SYD_ROIO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
