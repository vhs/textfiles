+++
title = "Music Files: The Pink Floyd"
description = "Articles and Textfiles about Pink Floyd"
tabledata = "music_pinkfloyd"
tablefooter = "There are 67 files for a total of 1,540,786 bytes."
+++

Look, don't ask me why, but Pink Floyd got a lot of attention and adoration from the folks online, and that's best demonstrated in the pure mass of Floyd-related textfiles you found out on BBSes and the Internet. Here's a good solid sampling. I do remember that the band made a strong impression on me throughout my childhood, and maybe others were affected the same way that they affected me.
