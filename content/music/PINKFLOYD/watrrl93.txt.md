+++
linktitle = "watrrl93.txt"
title = "watrrl93.txt"
url = "music/PINKFLOYD/watrrl93.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WATRRL93.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
