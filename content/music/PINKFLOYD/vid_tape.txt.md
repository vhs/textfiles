+++
linktitle = "vid_tape.txt"
title = "vid_tape.txt"
url = "music/PINKFLOYD/vid_tape.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download VID_TAPE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
