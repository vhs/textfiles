+++
linktitle = "qwatr92.txt"
title = "qwatr92.txt"
url = "music/PINKFLOYD/qwatr92.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download QWATR92.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
