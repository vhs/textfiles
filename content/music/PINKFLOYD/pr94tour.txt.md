+++
linktitle = "pr94tour.txt"
title = "pr94tour.txt"
url = "music/PINKFLOYD/pr94tour.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PR94TOUR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
