+++
linktitle = "watrrc92.txt"
title = "watrrc92.txt"
url = "music/PINKFLOYD/watrrc92.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WATRRC92.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
