+++
linktitle = "uk_disc1.txt"
title = "uk_disc1.txt"
url = "music/PINKFLOYD/uk_disc1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download UK_DISC1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
