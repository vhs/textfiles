+++
linktitle = "cherubrock_crd.txt"
title = "cherubrock_crd.txt"
url = "music/GUITARTABS/cherubrock_crd.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHERUBROCK_CRD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
