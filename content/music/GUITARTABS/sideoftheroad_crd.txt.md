+++
linktitle = "sideoftheroad_crd.txt"
title = "sideoftheroad_crd.txt"
url = "music/GUITARTABS/sideoftheroad_crd.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SIDEOFTHEROAD_CRD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
