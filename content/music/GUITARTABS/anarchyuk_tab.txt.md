+++
linktitle = "anarchyuk_tab.txt"
title = "anarchyuk_tab.txt"
url = "music/GUITARTABS/anarchyuk_tab.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANARCHYUK_TAB.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
