+++
linktitle = "joshuatree_tab.txt"
title = "joshuatree_tab.txt"
url = "music/GUITARTABS/joshuatree_tab.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JOSHUATREE_TAB.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
