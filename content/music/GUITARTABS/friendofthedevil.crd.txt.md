+++
linktitle = "friendofthedevil.crd.txt"
title = "friendofthedevil.crd.txt"
url = "music/GUITARTABS/friendofthedevil.crd.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FRIENDOFTHEDEVIL.CRD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
