+++
linktitle = "whiteman_crd.txt"
title = "whiteman_crd.txt"
url = "music/GUITARTABS/whiteman_crd.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WHITEMAN_CRD.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
