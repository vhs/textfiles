+++
linktitle = "oyecomova_tab.txt"
title = "oyecomova_tab.txt"
url = "music/GUITARTABS/oyecomova_tab.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OYECOMOVA_TAB.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
