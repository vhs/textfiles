+++
linktitle = "stones_tab_1.txt"
title = "stones_tab_1.txt"
url = "music/GUITARTABS/stones_tab_1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download STONES_TAB_1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
