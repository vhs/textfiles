+++
linktitle = "cherubrock_tab.txt"
title = "cherubrock_tab.txt"
url = "music/GUITARTABS/cherubrock_tab.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHERUBROCK_TAB.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
