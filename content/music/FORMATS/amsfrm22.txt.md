+++
linktitle = "amsfrm22.txt"
title = "amsfrm22.txt"
url = "music/FORMATS/amsfrm22.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AMSFRM22.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
