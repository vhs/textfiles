+++
linktitle = "mid-frm1.txt"
title = "mid-frm1.txt"
url = "music/FORMATS/mid-frm1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MID-FRM1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
