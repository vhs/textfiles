+++
linktitle = "mid-frm2.txt"
title = "mid-frm2.txt"
url = "music/FORMATS/mid-frm2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MID-FRM2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
