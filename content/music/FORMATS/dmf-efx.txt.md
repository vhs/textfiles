+++
linktitle = "dmf-efx.txt"
title = "dmf-efx.txt"
url = "music/FORMATS/dmf-efx.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DMF-EFX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
