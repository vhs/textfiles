+++
linktitle = "snopes_u.fic"
title = "snopes_u.fic"
url = "conspiracy/snopes_u.fic.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SNOPES_U.FIC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
