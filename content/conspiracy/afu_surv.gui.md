+++
linktitle = "afu_surv.gui"
title = "afu_surv.gui"
url = "conspiracy/afu_surv.gui.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AFU_SURV.GUI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
