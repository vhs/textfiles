+++
linktitle = "eggroll1.mea"
title = "eggroll1.mea"
url = "food/eggroll1.mea.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EGGROLL1.MEA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
