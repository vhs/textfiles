+++
linktitle = "frogeye1.sal"
title = "frogeye1.sal"
url = "food/frogeye1.sal.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FROGEYE1.SAL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
