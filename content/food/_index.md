+++
title = "Food"
description = "Food and Eating"
tabledata = "food"
tablefooter = "There are 213 files for a total of 2,129,022 bytes."
+++

If a textfile's about something that's edible, it's probably here.

Here are files with instructions on how to make really great food, or which purport to give you in side scoop on secret food information (like the original Coke formula). There are also files about making drinks, which got to be pretty popular as a lot of users reached drinking age (or at least an age when they started drinking).

Believe it or not, I'd warn again mixing the drinks or recipes in this section unless you know what you're doing; after all, this is stuff you're putting into YOURSELF, or your friends. Caveat Gourmet.
