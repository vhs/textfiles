+++
linktitle = "choco-ch.ips"
title = "choco-ch.ips"
url = "food/choco-ch.ips.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CHOCO-CH.IPS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
