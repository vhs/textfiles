+++
linktitle = "extentio.oct"
title = "extentio.oct"
url = "groups/OCTOTHORPE/extentio.oct.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EXTENTIO.OCT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
