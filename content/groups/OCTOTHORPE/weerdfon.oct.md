+++
linktitle = "weerdfon.oct"
title = "weerdfon.oct"
url = "groups/OCTOTHORPE/weerdfon.oct.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WEERDFON.OCT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
