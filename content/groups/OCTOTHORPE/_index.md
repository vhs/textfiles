+++
title = "Groups: Octothorpe Productions"
description = "Octothorpe Productions, the official Works BBS Group"
tabledata = "groups_octothorpe"
tablefooter = "There are 52 files for a total of 414,792 bytes."
+++

After getting well over a thousand textfiles, The Works decided that it should give back by creating a textfile writing group. Jason was fond of the word "Octothorpe", which was the Bell System name for the pound button on a telephone keypad. Hence, Octothorpe Productions was born.

As the leader and maintainer of these files, I can give insight into the experience of a typical textfile group. Unfortunately, most of my insight is that I simply kept a special file upload area on the BBS open for my friends to add files to. Some did, and many did not. The Cruiser took it and ran with it, producing all sorts of files about the typical subjects a good group would cover, like phreaking and music. The Mathematician kept donating fiction, and I buzzed in here and there with any and all writings I came up with, branding them "Octothorpe Productions".

Octothorpe Productions lived with the first Works BBS, from 1986-1988.
