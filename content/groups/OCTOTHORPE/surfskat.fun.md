+++
linktitle = "surfskat.fun"
title = "surfskat.fun"
url = "groups/OCTOTHORPE/surfskat.fun.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SURFSKAT.FUN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
