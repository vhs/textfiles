+++
linktitle = "xmas.oct"
title = "xmas.oct"
url = "groups/OCTOTHORPE/xmas.oct.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download XMAS.OCT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
