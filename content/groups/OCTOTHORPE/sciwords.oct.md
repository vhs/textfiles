+++
linktitle = "sciwords.oct"
title = "sciwords.oct"
url = "groups/OCTOTHORPE/sciwords.oct.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SCIWORDS.OCT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
