+++
title = "Groups: Anarchy Incorporated"
description = "Anarchy Incorporated"
tabledata = "groups_anarchyinc"
tablefooter = "There are 68 files for a total of 556,234 bytes."
+++

Located around the San Jose, Califronia area and founded around 1984, Anarchy Incorporated provided a steady stream of humorous and inside-joke-laden text files for AE Lines and BBSes for well around 4 years. 

The combination of the technical expertise of the members and their shared deep interest in BBS Software (Waffle BBS was created by a member) ensured that the files recieved a wide distribution among nationwide communities. Many of the members have gone on to careers in companies like Netscape, Apple, and Adobe, showing their abilities extended far beyond textfiles.

While some of the files wander into treatises on vandalism and phreaking how-tos, the vast majority of the work of Anarchy Inc. simply described fictional and non-fictional accounts of happenings between the members, and writings about the world that almost approach stream-of-consciousness. A sizeable percentage of the files ridicule or lambast a fictional character named Matt Ackeret who most certainly does not work at Apple Computer and who doesn't really exist. Really.
