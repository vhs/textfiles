+++
linktitle = "welcome.012"
title = "welcome.012"
url = "groups/BANANAREPUBLIC/welcome.012.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WELCOME.012 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
