+++
linktitle = "welcome.018"
title = "welcome.018"
url = "groups/BANANAREPUBLIC/welcome.018.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WELCOME.018 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
