+++
linktitle = "welcome.008"
title = "welcome.008"
url = "groups/BANANAREPUBLIC/welcome.008.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WELCOME.008 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
