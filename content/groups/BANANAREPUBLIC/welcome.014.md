+++
linktitle = "welcome.014"
title = "welcome.014"
url = "groups/BANANAREPUBLIC/welcome.014.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WELCOME.014 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
