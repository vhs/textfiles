+++
linktitle = "welcome.019"
title = "welcome.019"
url = "groups/BANANAREPUBLIC/welcome.019.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WELCOME.019 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
