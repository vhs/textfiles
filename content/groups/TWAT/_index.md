+++
title = "Groups: The Winner ANSI Team"
description = "The Winner ANSI Team"
tabledata = "groups_twat"
tablefooter = "There are 5 files for a total of 73,265 bytes."
+++

Intentionally badly spelled parody of IRC spelling style slowly turning serious through each issue until eventually the only mispellings are because they can't spell, and the subjects are how to use USENET and remailers. Oh, and there's no ANSI.

Give it a pass.
