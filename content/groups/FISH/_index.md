+++
title = "Groups: F.I.S.H."
description = "Folks In Santa Hats"
tabledata = "groups_fish"
tablefooter = "There are 28 files for a total of 276,732 bytes."
+++

Standing for "Folks in Santa Hats", this California-based group produced a number of files in the 1990-1995 time period.
