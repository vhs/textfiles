+++
linktitle = "thiscou.txt"
title = "thiscou.txt"
url = "groups/METALCOMMUNICATIONS/thiscou.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THISCOU.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
