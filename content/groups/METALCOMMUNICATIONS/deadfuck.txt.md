+++
linktitle = "deadfuck.txt"
title = "deadfuck.txt"
url = "groups/METALCOMMUNICATIONS/deadfuck.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEADFUCK.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
