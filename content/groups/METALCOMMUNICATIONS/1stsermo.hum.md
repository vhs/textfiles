+++
linktitle = "1stsermo.hum"
title = "1stsermo.hum"
url = "groups/METALCOMMUNICATIONS/1stsermo.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1STSERMO.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
