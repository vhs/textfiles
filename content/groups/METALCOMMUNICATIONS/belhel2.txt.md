+++
linktitle = "belhel2.txt"
title = "belhel2.txt"
url = "groups/METALCOMMUNICATIONS/belhel2.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BELHEL2.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
