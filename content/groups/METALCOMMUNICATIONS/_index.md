+++
title = "Groups: Neon Knights / Metal Communications"
description = "Neon Knights and Metal Communications"
tabledata = "groups_metalcommunications"
tablefooter = "There are 82 files for a total of 584,912 bytes."
+++

Based around New Jersey but quickly spreading nationwide, Metal Communications was one of the odder blends of Textfile Writing Geeks and Metalheads. Heavy Metal music comprises a major theme in the textfiles, as well as a very strong bent of generally messing up the planet with a crowbar, one mailbox at a time. Metal Communications lasted about three years, from 1983 to 1986.

The influence of Metal Communications (both the strong influx of anarchy files and a general satanic bent) is evident in a lot of freelance anarchy files and the work of such groups as the Cult of the Dead Cow, who had members in Metal Communications before cDc really became a strong voice of its own.

A smaller group of members of Metal Communications started calling themselves The Neon Knights, which included the leaders of Metal Communications and a few chosen minions. Their files tended to be of a higher quality than the rest, or at least longer. Generally, the Neon Knights also ran the Metal Communications distribution BBSs and AE lines.
