+++
linktitle = "dr00gs.txt"
title = "dr00gs.txt"
url = "groups/METALCOMMUNICATIONS/dr00gs.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DR00GS.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
