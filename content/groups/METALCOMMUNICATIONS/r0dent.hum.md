+++
linktitle = "r0dent.hum"
title = "r0dent.hum"
url = "groups/METALCOMMUNICATIONS/r0dent.hum.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download R0DENT.HUM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
