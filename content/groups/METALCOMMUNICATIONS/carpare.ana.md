+++
linktitle = "carpare.ana"
title = "carpare.ana"
url = "groups/METALCOMMUNICATIONS/carpare.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CARPARE.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
