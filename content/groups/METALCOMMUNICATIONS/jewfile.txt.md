+++
linktitle = "jewfile.txt"
title = "jewfile.txt"
url = "groups/METALCOMMUNICATIONS/jewfile.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JEWFILE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
