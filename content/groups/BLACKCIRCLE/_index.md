+++
title = ">Groups: Black Circle Productions"
description = "Black Circle Productions"
tabledata = "groups_blackcircle"
tablefooter = "There are 9 files for a total of 23,738 bytes."
+++

Black Circle Productions, an IRC-centric anarchy and hacking group from the mid 90's, wrote this series of files to encourage people to cause mayhem, prank call the stars, and generally make a little trouble.
