+++
linktitle = "clipwarn.pht"
title = "clipwarn.pht"
url = "groups/PHAIT/clipwarn.pht.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CLIPWARN.PHT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
