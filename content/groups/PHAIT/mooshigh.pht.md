+++
linktitle = "mooshigh.pht"
title = "mooshigh.pht"
url = "groups/PHAIT/mooshigh.pht.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MOOSHIGH.PHT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
