+++
linktitle = "compcard.omn"
title = "compcard.omn"
url = "groups/OMNIPOTENT/compcard.omn.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COMPCARD.OMN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
