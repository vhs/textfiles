+++
linktitle = "rumpus5.omn"
title = "rumpus5.omn"
url = "groups/OMNIPOTENT/rumpus5.omn.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RUMPUS5.OMN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
