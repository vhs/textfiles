+++
linktitle = "rumpus6.omn"
title = "rumpus6.omn"
url = "groups/OMNIPOTENT/rumpus6.omn.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download RUMPUS6.OMN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
