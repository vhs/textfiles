+++
linktitle = "nighhack.omn"
title = "nighhack.omn"
url = "groups/OMNIPOTENT/nighhack.omn.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NIGHHACK.OMN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
