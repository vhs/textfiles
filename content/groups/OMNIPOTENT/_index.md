+++
title = "Groups: Omnipotent, Inc."
description = "Omnipotent, Inc."
tabledata = "groups_omnipotent"
tablefooter = "There are 36 files for a total of 531,665 bytes."
pagefooter = "I'm happy to report that after contact with the writer, The Reflex, it has been verified that this is the Complete Omnipotent, Inc. Collection."
+++

A one-man burst of textfile-writing, The Reflex created file after file on all ranges of subjects, ranging from Anarchy and Phreaking to transcriptions of books on revenge and original fiction. His hallmarks were good spelling, nice formatting, and intense hate of a guy named 'Rumpus'.

The Reflex could always be depended upon for his high-quality work.
