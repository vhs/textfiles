+++
title = "Groups: The Phone Losers of America (PLA)"
description = "The Phone Losers of America"
tabledata = "groups_phonelosers"
tablefooter = "There are 46 files for a total of 1,132,118 bytes."
+++

Since they've described themselves so eloquently on their site (which is <a href="http://www.phonelosers.org">www.phonelosers.org</a>), I will just describe the PLA to you in their own words:

<blockquote>
<pre>
"PLA stands for Phone Losers of America. If you hadn't figured that out yet then you are a very slow person. PLA was originally an
electronic magazine created back in late 1994 and dealing with the usual underground topics of phone phreaking, a little
computer hacking, getting stuff for free and learning things that you're not supposed to. We also specialized in making everything
humorous and fun to read since at the time that was very hard to come by in the H/P world.
<P>
The magazine is no longer published and never will be again. The editor, RBCP, decided he was tired of writing it and wanted to
pursue other things such as a life. (Sell out) We've also released two prank phone call tapes which are sold on our page along
with other miscellaneous PLA t-shirts & caps, hoping to get filthy rich someday but actually only barely breaking even."
</pre>
</blockquote>

For what it's worth, I should mention that the descriptions for the files below come from the Phone Losers of America themselves. 
