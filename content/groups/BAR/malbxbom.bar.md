+++
linktitle = "malbxbom.bar"
title = "malbxbom.bar"
url = "groups/BAR/malbxbom.bar.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MALBXBOM.BAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
