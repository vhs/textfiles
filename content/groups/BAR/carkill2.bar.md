+++
linktitle = "carkill2.bar"
title = "carkill2.bar"
url = "groups/BAR/carkill2.bar.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CARKILL2.BAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
