+++
linktitle = "drink&go.bar"
title = "drink&go.bar"
url = "groups/BAR/drink&go.bar.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DRINK&GO.BAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
