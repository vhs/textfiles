+++
linktitle = "foodmch.bar"
title = "foodmch.bar"
url = "groups/BAR/foodmch.bar.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FOODMCH.BAR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
