+++
linktitle = "abigwar.oil"
title = "abigwar.oil"
url = "groups/BAR/abigwar.oil.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ABIGWAR.OIL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
