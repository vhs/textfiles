+++
linktitle = "futureae.tsg"
title = "futureae.tsg"
url = "groups/PPH/futureae.tsg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUTUREAE.TSG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
