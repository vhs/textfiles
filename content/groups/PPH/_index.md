+++
title = "Groups: The Phido Phreaks"
description = "The Phido Phreaks"
tabledata = "groups_pph"
tablefooter = "There are 32 files for a total of 275,398 bytes."
+++

A relatively minor textfile-writing group, but still very good friends of the Works BBS, The Phido Phreaks came out of a Fido BBS in Michigan called "Thieves' World", run by one Thomas Covenant. 

Thomas Covenant discovered textfiles.com and provided me with his collection of Phido Phreak files, including lots I didn't have and not providing me some I did. He wrote most of the descriptions you see below, as well as this note:

> All works by The Silver Ghost unless otherwise indicated. Comments by the carbon based life form formerly known as Thomas Covenant, now going by [damaged justice (frogfarm@hempseed.com)](mailto:frogfarm@hempseed.com).

> I have refrained from attempting to describe TSG's works of fiction, as no words of mine could do them justice.
