+++
linktitle = "1stcamp.tsg"
title = "1stcamp.tsg"
url = "groups/PPH/1stcamp.tsg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1STCAMP.TSG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
