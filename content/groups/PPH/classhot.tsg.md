+++
linktitle = "classhot.tsg"
title = "classhot.tsg"
url = "groups/PPH/classhot.tsg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CLASSHOT.TSG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
