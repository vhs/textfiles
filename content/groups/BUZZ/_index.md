+++
title = "Groups: The Buzz Brothers"
description = "The Buzz Brothers"
tabledata = "groups_buzz"
tablefooter = "There are 24 files for a total of 500,758 bytes."
+++

The Buzz Brothers seem to be predominantly occupied with the legalization of drugs, and cite many articles, statistics, and general essays on why this is a good idea. They're literate, they're clear, and they're easy to read. For some reason, they go into some other subjects, such as area code lists and lists of dirty words, but their hearts are sort of in the right place.
