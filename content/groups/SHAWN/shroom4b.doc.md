+++
linktitle = "shroom4b.doc"
title = "shroom4b.doc"
url = "groups/SHAWN/shroom4b.doc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHROOM4B.DOC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
