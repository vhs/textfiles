+++
title = "Groups: Shawn Da Lay Boy Productions"
description = "Shawn Da Lay Boy Productions"
tabledata = "groups_shawn"
tablefooter = "There are 72 files for a total of 861,592 bytes."
+++

Having no knowledge of this group except for the occasional file that had previously trickled into my collection, I present the quick introduction of a Mr. <a href="http://www.arlington.com/~tjames/">Jed Sanders</a>:

<i>The tradition of various text-file distribution "conglomerates" is an old one, reaching back to the very dawn of BBS history. Before the warring ANSI groups existed (indeed, before ANSI itself existed), before the rise in popularity of the mostly European "demo" (graphics + music) scene, there was the simple storyteller. Dating back to a time when text-files were the hottest-traded commodity online (and sometimes the only thing available), there are few of these pioneering groups left intact in the graphically oriented online world we see today. Cult of the Dead Cow (cDc) comes to mind; Lunatic Labs in Los Angeles. The San Francisco Bay Area had its own contribution: Shawn-Da-Lay Boy Productions, more commonly known as simply SDLBP.

Contributions to this group came primarily through three East Bay BBSes: The Electric Pub (defunct, Sir Death sysop), The Pirate's Hollow (now simply "The Hollow," Doctor Murdock original sysop, later - and currently - Powerful Paul), and RatHead Systems (now <a href="pspock.html">PolySpock</a>, Ratsnatcher sysop). The group flourished primarily in 1988-90, with an average of two or three new files per week shooting through the transom.

Many of these files are gone forever. Some of them deserve this fate. The writing is intensely </i>young<i> here, which isn't to suggest that there aren't a few hidden treasures among the muck. Don't expect much in the way of rational, well-thought opines: what follows is a product of its own age, with its own rules, not to be judged on any standards other than its own. What is most rewarding about the SDLBP files, however, is the opportunity to watch young writers emerge from a spot where nothing lay before. Many of the participants have gone on to much bigger things; some are sysadmins at major UNIX sites, others engaged in graduate study. Some are unemployed and some are lost.

These files remind you that the place that you came from is gone.</i>
