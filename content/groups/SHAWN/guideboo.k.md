+++
linktitle = "guideboo.k"
title = "guideboo.k"
url = "groups/SHAWN/guideboo.k.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GUIDEBOO.K textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
