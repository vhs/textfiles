+++
linktitle = "finnshrm.pst"
title = "finnshrm.pst"
url = "groups/SHAWN/finnshrm.pst.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FINNSHRM.PST textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
