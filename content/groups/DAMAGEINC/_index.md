+++
title = "Groups: Damage, Incorporated"
description = "Damage, Incorporated"
tabledata = "groups_damageinc"
tablefooter = "There are 62 files for a total of 3,928,151 bytes."
+++

BLACKENED's group of telecommunications enthusiasts have been creating telephone scanning lists, newsletters, and poetry since 1993. This collection comes courtesy of his efforts, which are greatly appreciated. A large portion of these files are the Damage, Inc. Newsletter, which pulls away any individal textfiles they might be working on and puts them into an easy to find (and read) collection. Solid telephone information and great to see them at it after so many years.
