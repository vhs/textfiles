+++
linktitle = "dam99fal.txt"
title = "dam99fal.txt"
url = "groups/DAMAGEINC/dam99fal.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DAM99FAL.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
