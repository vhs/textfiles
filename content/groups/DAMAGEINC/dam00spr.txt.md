+++
linktitle = "dam00spr.txt"
title = "dam00spr.txt"
url = "groups/DAMAGEINC/dam00spr.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DAM00SPR.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
