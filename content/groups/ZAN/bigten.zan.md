+++
linktitle = "bigten.zan"
title = "bigten.zan"
url = "groups/ZAN/bigten.zan.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BIGTEN.ZAN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
