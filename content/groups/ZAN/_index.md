+++
title = "Groups: Zhit Axis Nation"
description = "Zhit Access Nation"
tabledata = "groups_zan"
tablefooter = "There are 60 files for a total of 410,874 bytes."
+++

It must have been a hell of a summer. The vast majority of these files come from the middle of 1991, and the members of ZAN (Zhit Axis Nation) banded together to create a pretty positive and somewhat complicated allegiance of textfile writing. The ringleader appears to be Creature of Prometheus, who designed the format of the files and to which the others seem to defer. Exorcist preaches anarchy, fighting, and destruction, while Guido Sanchez thinks too much. When the character of every member makes itself known to you, you know you've got something special.

The group disappears after 1991. Guess they knew to stop when it was good.
