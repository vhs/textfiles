+++
linktitle = "proverbs.zan"
title = "proverbs.zan"
url = "groups/ZAN/proverbs.zan.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PROVERBS.ZAN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
