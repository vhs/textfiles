+++
linktitle = "want_2_b.zan"
title = "want_2_b.zan"
url = "groups/ZAN/want_2_b.zan.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WANT_2_B.ZAN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
