+++
linktitle = "supers.ana"
title = "supers.ana"
url = "groups/VIDEOVINDICATOR/supers.ana.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SUPERS.ANA textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
