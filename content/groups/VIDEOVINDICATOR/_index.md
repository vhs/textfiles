+++
title = "Textfile Writing Groups: The Video Vindicator"
description = "The Works of the Video Vindicator"
tabledata = "groups_videovindicator"
tablefooter = "There are 36 files for a total of 1,242,806 bytes."
+++

The Video Vindicator showed up a little late in the game, around 1991, and then proceeded to blow all competition out of the water. His files are works of informational art, describing all manner of subjects, all with a sense of completeness and dedication that rarely appears at any point in textfile-writing history.
