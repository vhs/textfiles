+++
linktitle = "9x_bbox.txt"
title = "9x_bbox.txt"
url = "groups/9X/9x_bbox.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 9X_BBOX.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
