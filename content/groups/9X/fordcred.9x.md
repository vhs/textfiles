+++
linktitle = "fordcred.9x"
title = "fordcred.9x"
url = "groups/9X/fordcred.9x.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FORDCRED.9X textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
