+++
title = "Groups: 9X"
description = "9X"
tabledata = "groups_9x"
tablefooter = "There are 20 files for a total of 223,538 bytes."
+++

9X seems to have been a late-blooming group with a strong lean for code and carrier scanning and pirate radio. Most of the files in this directory are transcriptions from a pirate radio text, and others are documentation for code scanning programs. The warning at the beginning of most of the files is precious.
