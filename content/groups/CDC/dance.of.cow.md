+++
linktitle = "dance.of.cow"
title = "dance.of.cow"
url = "groups/CDC/dance.of.cow.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DANCE.OF.COW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
