+++
linktitle = "prophecy.of.cow"
title = "prophecy.of.cow"
url = "groups/CDC/prophecy.of.cow.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PROPHECY.OF.COW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
