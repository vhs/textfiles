+++
linktitle = "song.of.the.cow"
title = "song.of.the.cow"
url = "groups/CDC/song.of.the.cow.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SONG.OF.THE.COW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
