+++
linktitle = "yo.s.ratte"
title = "yo.s.ratte"
url = "groups/CDC/yo.s.ratte.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download YO.S.RATTE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
