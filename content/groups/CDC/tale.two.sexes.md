+++
linktitle = "tale.two.sexes"
title = "tale.two.sexes"
url = "groups/CDC/tale.two.sexes.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TALE.TWO.SEXES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
