+++
linktitle = "bear.trap.love"
title = "bear.trap.love"
url = "groups/CDC/bear.trap.love.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BEAR.TRAP.LOVE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
