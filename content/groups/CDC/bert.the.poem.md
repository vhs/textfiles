+++
linktitle = "bert.the.poem"
title = "bert.the.poem"
url = "groups/CDC/bert.the.poem.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BERT.THE.POEM textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
