+++
linktitle = "cDc-0188.txt"
title = "cDc-0188.txt"
url = "groups/CDC/cDc-0188.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CDC-0188.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
