+++
linktitle = "nameless.pastur"
title = "nameless.pastur"
url = "groups/CDC/nameless.pastur.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NAMELESS.PASTUR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
