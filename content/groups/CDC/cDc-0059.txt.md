+++
linktitle = "cDc-0059.txt"
title = "cDc-0059.txt"
url = "groups/CDC/cDc-0059.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CDC-0059.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
