+++
linktitle = "trail.of.blood"
title = "trail.of.blood"
url = "groups/CDC/trail.of.blood.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TRAIL.OF.BLOOD textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
