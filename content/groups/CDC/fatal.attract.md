+++
linktitle = "fatal.attract"
title = "fatal.attract"
url = "groups/CDC/fatal.attract.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FATAL.ATTRACT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
