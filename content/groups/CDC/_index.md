+++
title = "Groups: The Cult of the Dead Cow"
description = "The Cult of the Dead Cow Library"
tabledata = "groups_cdc"
tablefooter = "There are 410 files for a total of 5,121,742 bytes."
+++

The Cult of the Dead Cow (cDc) rose in 1985, joining the sea of textfile groups that dominated the BBS scene. The majority of files they came out with in the beginning centered around anarchy, heavy metal, and odd stories. By sheer tenacity of will, they continue as a group to present time, and therefore have lasted well over a decade. This makes them the oldest surviving textfile group in existence.

With well over three hundred files in their library, cDc has left almost no textfile subject untouched. Besides the ubiquitous phreaking, hacking, and anarchy files, members have written poetry, fiction, music lyrics, erotica, and Scene news stories. By constantly reviewing their membership rolls and taking on new members when older ones retire, the group has stayed fresh and active as an entity.

Through the general slowdown of the early 1990's, cDc repositioned itself as a brand name by dint of the addition of Skeeve/Deth Vegetable, whose own media awareness (interviews on Dateline NBC and other outlets) ensured that Cult of the Dead Cow would recieve press and recieve much of it. cDc got into the business of clothing and stickers, and did a remarkable job of preserving its own heritage even as the majority of users moved from BBSs to the internet. Members of cDc are now routinely contacted by media when the opinion of the "underground" is sought or at least pursued.

In recent years the cDc has gained nationwide notoriety for its work with security-checking programs (a further expansion of the brand name) such as <b>Back Orifice</b>, which provides remote administration of Windows 95 and NT machines. 

The Cult of the Dead Cow have a web site at 
<a href="http://www.cultdeadcow.com">www.cultdeadcow.com</a>.
