+++
linktitle = "eye.am.a.modeme"
title = "eye.am.a.modeme"
url = "groups/CDC/eye.am.a.modeme.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download EYE.AM.A.MODEME textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
