+++
linktitle = "boredom.innocen"
title = "boredom.innocen"
url = "groups/CDC/boredom.innocen.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BOREDOM.INNOCEN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
