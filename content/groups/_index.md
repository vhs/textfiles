+++
title = "Textfile Writing Groups"
description = "Textfile Writer Collectives"
tabledata = "groups"
tablefooter = "There are 21 directories."
+++

It was natural that groups of close friends, working together to write textfiles on whatever subjects they considered their own, would start considering themselves a sort of rock and roll band, or maybe a gang. To this end, Textfile "Groups" starting making appearances early in the 1980's, and of course it's gotten completely out of hand.

Some groups had members that never actually met each other; they did all their work online and might be many many miles apart, or even in different countries. Some groups were open to anyone who wanted to be a member, and some were finite and refused to expand or contract. And then there's the cDc.
