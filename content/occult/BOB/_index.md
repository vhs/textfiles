+++
title = "Occult: The Church of the Subgenius"
description = "The World of J.R. \"Bob\" Dobbs, Slack, and the Church of the Subgenius"
tabledata = "occult_bob"
tablefooter = "There are 52 files for a total of 916,481 bytes."
+++

The Church of the Subgenius pushes the concept of "Slack" as a way of life, while indicating that all will come to those who know the true way of "Bob", that is, J.R. "Bob" Dobbs. The Church is one of the more infamous of the "humor" religions, where the energy and savvy of the followers ensures lots and lots of doctrines and Church-related projects. There are still occasional "Devivals" and lots of media and items for sale abound. The entire Church is mostly (mostly) taken care of by the Rev. Ivan Stang, who is a really neat guy.
