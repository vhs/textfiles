+++
linktitle = "thelmsex.occ"
title = "thelmsex.occ"
url = "occult/OTO/thelmsex.occ.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THELMSEX.OCC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
