+++
linktitle = "oshuti.set"
title = "oshuti.set"
url = "occult/OTO/oshuti.set.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OSHUTI.SET textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
