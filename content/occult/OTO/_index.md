+++
title = "Occult: The Ordo Templi Orientis"
description = "The Ordo Templi Orientis (Crowley)"
tabledata = "occult_oto"
tablefooter = "There are 261 files for a total of 9,407,886 bytes."
+++

From a biography of Crowley, founder of the OTO:

<blockquote>
<pre>
Edward Alexander (Aleister) Crowley [rhymes with "holy"] was born
October 12, 1875 in Leamington Spa, England. His parents were
members of the Plymouth Brethren, a strict fundamentalist Christian
sect. As a result, Aleister grew up with a thorough biblical education
and an equally thorough disdain of Christianity.

In 1910 Crowley was contacted by Theodore Reuss, the head of
an organization based in Germany called the Ordo Templi Orientis
(O.T.O.). This group of high-ranking Freemasons claimed to have
discovered the supreme secret of practical magick, which was taught
in its highest degrees. Apparently Crowley agreed, becoming a
member of O.T.O. and eventually taking over as head of the order
when Reuss suffered a stroke in 1921. Crowley reformulated the rites
of the O.T.O. to conform them to the Law of Thelema, and vested the
organization with its main purpose of establishing Thelema in the world.
The order also became independent of Freemasonry (although still
based on the same patterns) and opened its membership to women
and men who were not masons.

Aleister Crowley died in Hastings, England on December 1, 1947.
However, his legacy lives on in the Law of Thelema which he brought
to mankind (along with dozens of books and writings on magick and
other mystical subjects), and in the orders A.'. A.'. and O.T.O. which
continue to advance the principles of Thelema to this day.
</pre>
</blockquote>
