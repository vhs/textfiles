+++
linktitle = "artemis.rit"
title = "artemis.rit"
url = "occult/OTO/artemis.rit.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ARTEMIS.RIT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
