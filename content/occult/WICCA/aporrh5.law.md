+++
linktitle = "aporrh5.law"
title = "aporrh5.law"
url = "occult/WICCA/aporrh5.law.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download APORRH5.LAW textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
