+++
linktitle = "turkey.cur"
title = "turkey.cur"
url = "occult/ERIS/turkey.cur.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TURKEY.CUR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
