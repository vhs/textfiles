+++
linktitle = "mor-prof.txt"
title = "mor-prof.txt"
url = "occult/CHRISTIAN/mor-prof.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MOR-PROF.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
