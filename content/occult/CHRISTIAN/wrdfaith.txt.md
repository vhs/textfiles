+++
linktitle = "wrdfaith.txt"
title = "wrdfaith.txt"
url = "occult/CHRISTIAN/wrdfaith.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WRDFAITH.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
