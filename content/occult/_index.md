+++
title = "The Occult"
description = "Textfiles dealing with alternate religions"
tabledata = "occult"
tablefooter = "There are 675 files for a total of 17,494,567 bytes.<br>There are 16 directories."
+++

One of the groups that really grabbed a hold of the BBS world and held on for dear life were folks who had a different view of organized religion. BBSes immediately provided them with a place to discuss, organize, and put up doctrines. You could find massive libraries of doctines online, in fact; a BBS could be set up for a specific religion, encourage discussions, get new converts, the whole deal. This was popular with all sorts of religions, although of course it set up a nice easy target if the religion was inherently judgemental.

Another side of this coin were parody or joke religions, who found it easy to creative large amounts of lore and meaningless parables and rules that would almost seem real, if it weren't for the bad spelling and wanton profanity. Some of these, like the Church of the Subgenius, were both publishing phenomenons and online scions. It was all very silly.

This section is a little hard to sort, because it's very hard to tell what documents belong to what line of thought, and, even worse, some lines of thought have split down even further to two or more branches that hate each other. But I'll do my best. Please be sure to write in if things are completely missorted.
