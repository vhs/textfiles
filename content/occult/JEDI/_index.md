+++
title = "Occult: Jedi Realism"
description = "Jedi Realism"
tabledata = "occult_jedi"
tablefooter = "There are 5 files for a total of 178,322 bytes."
+++

eYeCoN writes:

<blockquote>
<pre>
"These files all relate to Jedi Realism.  The name is exactly what it sounds like. It is a group of
(approximately 10,000 people, worldwide) who take the teachings of the Jedi from the films and books
of George Lucas' Star Wars saga, expand upon them, and apply them to real-life situations - and in
fact, every day life. I, myself, am a Realist. :)"
</pre>
</blockquote>
