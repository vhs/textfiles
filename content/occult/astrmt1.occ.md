+++
linktitle = "astrmt1.occ"
title = "astrmt1.occ"
url = "occult/astrmt1.occ.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ASTRMT1.OCC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
