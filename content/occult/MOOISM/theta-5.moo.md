+++
linktitle = "theta-5.moo"
title = "theta-5.moo"
url = "occult/MOOISM/theta-5.moo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THETA-5.MOO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
