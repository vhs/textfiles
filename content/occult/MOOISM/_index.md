+++
title = "Occult: The Church of MOO"
description = "The Doctrines of the Church of MOO"
tabledata = "occult_mooism"
tablefooter = "There are 15 files for a total of 1,315,767 bytes."
+++

With an unbelievable burst of energy, the denziens of the Church of MOO put forth many doctrines, writings, and, ultimately, a massive bible centered around parodying religion. You can spend a lot of time trying to figure out what the tenets of MOOism ultimately ARE, or you can let it wash over you in a wave of bovine-themed revival.
