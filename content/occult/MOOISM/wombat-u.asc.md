+++
linktitle = "wombat-u.asc"
title = "wombat-u.asc"
url = "occult/MOOISM/wombat-u.asc.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WOMBAT-U.ASC textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
