+++
linktitle = "wombat.moo"
title = "wombat.moo"
url = "occult/MOOISM/wombat.moo.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WOMBAT.MOO textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
