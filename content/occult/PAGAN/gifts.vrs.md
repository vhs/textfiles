+++
linktitle = "gifts.vrs"
title = "gifts.vrs"
url = "occult/PAGAN/gifts.vrs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download GIFTS.VRS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
