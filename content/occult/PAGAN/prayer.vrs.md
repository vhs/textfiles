+++
linktitle = "prayer.vrs"
title = "prayer.vrs"
url = "occult/PAGAN/prayer.vrs.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PRAYER.VRS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
