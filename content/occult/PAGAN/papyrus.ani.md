+++
linktitle = "papyrus.ani"
title = "papyrus.ani"
url = "occult/PAGAN/papyrus.ani.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PAPYRUS.ANI textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
