+++
linktitle = "theme038.out"
title = "theme038.out"
url = "occult/WORLDSCRIPTURE/theme038.out.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THEME038.OUT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
