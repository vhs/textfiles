+++
linktitle = "theme005.out"
title = "theme005.out"
url = "occult/WORLDSCRIPTURE/theme005.out.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THEME005.OUT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
