+++
linktitle = "theme042.out"
title = "theme042.out"
url = "occult/WORLDSCRIPTURE/theme042.out.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THEME042.OUT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
