+++
title = "Holiday Textfiles"
description = "Files evoking a Holiday Spirit"
tabledata = "holiday"
tablefooter = "There are 73 files for a total of 379,033 bytes."
+++

These are textfiles that are attached to certain holidays. It's kind of sannoying to see files about subjects that don't represent year-round types of subjects, so they're all lumped in here. The majority of the files here are currently Christmas files.

An exception is the <a href="/occult"><b>Occult</b></a> section, which is loaded with Halloween stuff but it doesn't seem as clear to split the specific holiday stuff from and put here.

Interestingly enough, the most popular files are parodies of the time-honored poem "A visit from Saint Nicholas". Something about that particular poem (the idea that Santa's breaking into the house, the glimpse you get of him before he cuts out of town) inspires generations of geeks and self-styled humorists to make new, alternate versions.
