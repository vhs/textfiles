+++
linktitle = "anti_val.day"
title = "anti_val.day"
url = "holiday/anti_val.day.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANTI_VAL.DAY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
