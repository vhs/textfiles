+++
linktitle = "santa_mo.ves"
title = "santa_mo.ves"
url = "holiday/santa_mo.ves.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SANTA_MO.VES textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
