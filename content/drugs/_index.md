+++
title = "Drugs and Drug Use"
description = "An unnecessary amount of Drug information"
tabledata = "drugs"
tablefooter = "There are 515 files for a total of 11,709,827 bytes.<br>There are 2 directories."
+++

While it wasn't my thing, a lot of teenagers make it their life's goal to turn off (or 'expand') as much of their brains as possible. To this end, learning all the new and amazing ways they can mix household chemicals to create intense highs became quite the goal. A lot of these files deal with marijuana, pills, alcohol, and whatever else people could get their hands on. Some were talking completely out of their butts, and some approach the level of pharmaceutical papers in their clinical and terse writing. <i>Vive la Difference!</i>

When Usenet started to really come into play, people started writing textfiles and posting them as articles in the newsgroup "alt.drugs" and its brethren. I've put all these files into a different directory, since they encompass the spirit of textfiles, but not really the same style.
