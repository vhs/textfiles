+++
linktitle = "mde_as_m.txt"
title = "mde_as_m.txt"
url = "drugs/mde_as_m.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MDE_AS_M.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
