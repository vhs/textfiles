+++
linktitle = "shroom02doc.drg"
title = "shroom02doc.drg"
url = "drugs/shroom02doc.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SHROOM02DOC.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
