+++
title = "Drugs: Marijuana Textfiles"
description = "Textfiles about Marijuana and the Poltics Therein"
tabledata = "drugs_maryjane"
tablefooter = "There are 177 files for a total of 3,179,328 bytes."
+++

Having never had it, I can't tell you much about Cannabis except that it has an awful lot of attention from a lot of people in government, society, and whoever still watches "Cheech and Chong" movies. By far, it's the most popular drug to write about from the BBS era, although LSD comes in second. 
