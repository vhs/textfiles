+++
linktitle = "weedgard.fun"
title = "weedgard.fun"
url = "drugs/MARYJANE/weedgard.fun.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WEEDGARD.FUN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
