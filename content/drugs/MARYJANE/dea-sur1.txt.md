+++
linktitle = "dea-sur1.txt"
title = "dea-sur1.txt"
url = "drugs/MARYJANE/dea-sur1.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEA-SUR1.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
