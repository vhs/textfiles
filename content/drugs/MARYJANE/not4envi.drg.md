+++
linktitle = "not4envi.drg"
title = "not4envi.drg"
url = "drugs/MARYJANE/not4envi.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NOT4ENVI.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
