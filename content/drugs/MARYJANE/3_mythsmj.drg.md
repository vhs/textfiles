+++
linktitle = "3_mythsmj.drg"
title = "3_mythsmj.drg"
url = "drugs/MARYJANE/3_mythsmj.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 3_MYTHSMJ.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
