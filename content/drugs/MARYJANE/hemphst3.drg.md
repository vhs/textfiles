+++
linktitle = "hemphst3.drg"
title = "hemphst3.drg"
url = "drugs/MARYJANE/hemphst3.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HEMPHST3.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
