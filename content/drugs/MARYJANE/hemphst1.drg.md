+++
linktitle = "hemphst1.drg"
title = "hemphst1.drg"
url = "drugs/MARYJANE/hemphst1.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download HEMPHST1.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
