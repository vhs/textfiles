+++
linktitle = "caffeineinf.drg"
title = "caffeineinf.drg"
url = "drugs/caffeineinf.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download CAFFEINEINF.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
