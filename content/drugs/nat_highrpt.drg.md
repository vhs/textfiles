+++
linktitle = "nat_highrpt.drg"
title = "nat_highrpt.drg"
url = "drugs/nat_highrpt.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download NAT_HIGHRPT.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
