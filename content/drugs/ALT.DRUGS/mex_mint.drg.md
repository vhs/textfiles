+++
linktitle = "mex_mint.drg"
title = "mex_mint.drg"
url = "drugs/ALT.DRUGS/mex_mint.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MEX_MINT.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
