+++
linktitle = "paperrpt.drg"
title = "paperrpt.drg"
url = "drugs/ALT.DRUGS/paperrpt.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PAPERRPT.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
