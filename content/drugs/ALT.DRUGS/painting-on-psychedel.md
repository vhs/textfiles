+++
linktitle = "painting-on-psychedel"
title = "painting-on-psychedel"
url = "drugs/ALT.DRUGS/painting-on-psychedel.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PAINTING-ON-PSYCHEDEL textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
