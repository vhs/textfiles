+++
linktitle = "badtrip.txt"
title = "badtrip.txt"
url = "drugs/ALT.DRUGS/badtrip.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download BADTRIP.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
