+++
title = "Drugs: ALT.DRUGS Postings"
description = "Postings to the alt.drugs Usenet Newsgroup."
tabledata = "drugs_altdrugs"
tablefooter = "There are 346 files for a total of 3,814,136 bytes."
+++

I don't really consider these textfiles in the same sense of textfile.com's main mission of bring BBS files to you, but in many ways, the people posting the following articles were carrying on the spirit of the original BBSes with their own textfiles. Some may have even thought they WERE in the 1980's.
