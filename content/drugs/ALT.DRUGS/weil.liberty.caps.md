+++
linktitle = "weil.liberty.caps"
title = "weil.liberty.caps"
url = "drugs/ALT.DRUGS/weil.liberty.caps.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WEIL.LIBERTY.CAPS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
