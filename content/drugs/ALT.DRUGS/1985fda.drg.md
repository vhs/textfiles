+++
linktitle = "1985fda.drg"
title = "1985fda.drg"
url = "drugs/ALT.DRUGS/1985fda.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 1985FDA.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
