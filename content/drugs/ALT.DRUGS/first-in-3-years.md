+++
linktitle = "first-in-3-years"
title = "first-in-3-years"
url = "drugs/ALT.DRUGS/first-in-3-years.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FIRST-IN-3-YEARS textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
