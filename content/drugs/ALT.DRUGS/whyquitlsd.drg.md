+++
linktitle = "whyquitlsd.drg"
title = "whyquitlsd.drg"
url = "drugs/ALT.DRUGS/whyquitlsd.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download WHYQUITLSD.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
