+++
linktitle = "amaz_plt.drg"
title = "amaz_plt.drg"
url = "drugs/ALT.DRUGS/amaz_plt.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download AMAZ_PLT.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
