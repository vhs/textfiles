+++
linktitle = "2cbinf.drg"
title = "2cbinf.drg"
url = "drugs/ALT.DRUGS/2cbinf.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download 2CBINF.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
