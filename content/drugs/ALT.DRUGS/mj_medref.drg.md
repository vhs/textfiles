+++
linktitle = "mj_medref.drg"
title = "mj_medref.drg"
url = "drugs/ALT.DRUGS/mj_medref.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MJ_MEDREF.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
