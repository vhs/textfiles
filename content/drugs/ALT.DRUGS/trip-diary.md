+++
linktitle = "trip-diary"
title = "trip-diary"
url = "drugs/ALT.DRUGS/trip-diary.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download TRIP-DIARY textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
