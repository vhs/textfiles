+++
linktitle = "anti_potdoc.drg"
title = "anti_potdoc.drg"
url = "drugs/ALT.DRUGS/anti_potdoc.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ANTI_POTDOC.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
