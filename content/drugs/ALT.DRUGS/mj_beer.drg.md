+++
linktitle = "mj_beer.drg"
title = "mj_beer.drg"
url = "drugs/ALT.DRUGS/mj_beer.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MJ_BEER.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
