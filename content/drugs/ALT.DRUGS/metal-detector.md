+++
linktitle = "metal-detector"
title = "metal-detector"
url = "drugs/ALT.DRUGS/metal-detector.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download METAL-DETECTOR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
