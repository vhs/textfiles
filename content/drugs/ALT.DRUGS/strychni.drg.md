+++
linktitle = "strychni.drg"
title = "strychni.drg"
url = "drugs/ALT.DRUGS/strychni.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download STRYCHNI.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
