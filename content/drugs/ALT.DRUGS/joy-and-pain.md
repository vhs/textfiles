+++
linktitle = "joy-and-pain"
title = "joy-and-pain"
url = "drugs/ALT.DRUGS/joy-and-pain.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download JOY-AND-PAIN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
