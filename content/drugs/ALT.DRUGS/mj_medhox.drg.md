+++
linktitle = "mj_medhox.drg"
title = "mj_medhox.drg"
url = "drugs/ALT.DRUGS/mj_medhox.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MJ_MEDHOX.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
