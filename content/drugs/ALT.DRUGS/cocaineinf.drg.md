+++
linktitle = "cocaineinf.drg"
title = "cocaineinf.drg"
url = "drugs/ALT.DRUGS/cocaineinf.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download COCAINEINF.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
