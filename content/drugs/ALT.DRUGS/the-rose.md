+++
linktitle = "the-rose"
title = "the-rose"
url = "drugs/ALT.DRUGS/the-rose.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download THE-ROSE textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
