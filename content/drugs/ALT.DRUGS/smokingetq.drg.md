+++
linktitle = "smokingetq.drg"
title = "smokingetq.drg"
url = "drugs/ALT.DRUGS/smokingetq.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SMOKINGETQ.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
