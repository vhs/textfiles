+++
linktitle = "alt_beer.01"
title = "alt_beer.01"
url = "drugs/ALT.DRUGS/alt_beer.01.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download ALT_BEER.01 textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
