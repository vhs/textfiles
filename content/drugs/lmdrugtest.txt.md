+++
linktitle = "lmdrugtest.txt"
title = "lmdrugtest.txt"
url = "drugs/lmdrugtest.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LMDRUGTEST.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
