+++
linktitle = "dea-sur1hst.drg"
title = "dea-sur1hst.drg"
url = "drugs/dea-sur1hst.drg.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DEA-SUR1HST.DRG textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
