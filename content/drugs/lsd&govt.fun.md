+++
linktitle = "lsd&govt.fun"
title = "lsd&govt.fun"
url = "drugs/lsd&govt.fun.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download LSD&GOVT.FUN textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
