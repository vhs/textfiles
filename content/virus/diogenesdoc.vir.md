+++
linktitle = "diogenesdoc.vir"
title = "diogenesdoc.vir"
url = "virus/diogenesdoc.vir.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DIOGENESDOC.VIR textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
