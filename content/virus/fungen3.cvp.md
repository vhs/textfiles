+++
linktitle = "fungen3.cvp"
title = "fungen3.cvp"
url = "virus/fungen3.cvp.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download FUNGEN3.CVP textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
