+++
linktitle = "mcbstelt.txt"
title = "mcbstelt.txt"
url = "virus/DOCUMENTATION/mcbstelt.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download MCBSTELT.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
