+++
title = "Virus Textfiles: Documentation"
description = "Documentation for Different Viruses and Programs"
tabledata = "virus_documentation"
tablefooter = "There are 101 files for a total of 534,213 bytes."
+++

I include the documentation that comes with different virus-writing programs and viruses because I think it provides a fascinating mindset into the authors' mindsets. Why do they do it? Take a look.
