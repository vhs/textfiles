+++
linktitle = "offspr89.txt"
title = "offspr89.txt"
url = "virus/DOCUMENTATION/offspr89.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OFFSPR89.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
