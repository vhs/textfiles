+++
linktitle = "selfchkg.txt"
title = "selfchkg.txt"
url = "virus/DOCUMENTATION/selfchkg.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download SELFCHKG.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
