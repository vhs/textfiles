+++
linktitle = "offspr81.txt"
title = "offspr81.txt"
url = "virus/DOCUMENTATION/offspr81.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download OFFSPR81.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
