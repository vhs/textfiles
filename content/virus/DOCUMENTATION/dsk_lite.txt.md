+++
linktitle = "dsk_lite.txt"
title = "dsk_lite.txt"
url = "virus/DOCUMENTATION/dsk_lite.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download DSK_LITE.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
