+++
linktitle = "k-scitzo.txt"
title = "k-scitzo.txt"
url = "virus/DOCUMENTATION/k-scitzo.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download K-SCITZO.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
