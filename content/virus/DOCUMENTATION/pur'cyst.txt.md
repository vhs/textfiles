+++
linktitle = "pur'cyst.txt"
title = "pur'cyst.txt"
url = "virus/DOCUMENTATION/pur_cyst.txt.html"
layout = "textfile"
keywords = ["ascii", "ansi", "art", "artwork", "asciiart", "poster", "textfile", "txtfile"]
description = "Preview and download PUR'CYST.TXT textfile including MD5 shasum. Browse catalog of over 57,000 plain text ASCII/ANSI vintage poster art and textfiles."
+++
