+++
title = "Virus Textfiles"
description = "Computer Viruses, Trojan Horses, and Worms"
tabledata = "virus"
tablefooter = "There are 295 files for a total of 7,012,356 bytes.<br>There are 2 directories."
+++

Virii programs came into play in the 1980's in full force and to this day turn every online binary transfer into a potential hazard. Well, maybe that's not entirely true, but you wouldn't know it from the hype and hysteria bred in the world. Meanwhile, fascinating artistic studies of the programming of Virii exist out there and will be brought here. As a rule, I don't include Virus Source Code, since that's just programming, and boring as all hell. if there's a discussion or writing about how to make virii, I include that. This is the arbitrariness of the host.

If you're looking for the excellent 40HEX Virus Magazine, it's located in the [Magazines](http://www.textfiles.com/magazines) section.
